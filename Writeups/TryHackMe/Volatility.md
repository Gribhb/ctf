# CTF TryHackMe - Volatility 

## Install Volatitlity
```bash
sudo snap install volatility-phocean
```

## Let's figure out what profile we need to use
```bash
volatility -f MEMORY_FILE.raw imageinfo
```

In the output you will see :
```bash
Suggested Profile(s)
```

We can test these profiles using the pslist command, validating our profile selection by the sheer number of returned results

```bash
volatility -f MEMORY_FILE.raw –profile=PROFILE pslist
```

## View active network
In addition to viewing active processes, we can also view active network connections at the time of image creation

```bash
volatility -f MEMORY_FILE.raw –profile=PROFILE netscan
```

It’s fairly common for malware to attempt to hide itself and the process associated with it

```bash
volatility -f MEMORY_FILE.raw --profile=PROFILE psxview
```

In addition to viewing hidden processes via psxview, we can also check this with a greater focus via the command ‘ldrmodules’

```bash
volatility -f MEMORY_FILE.raw --profile=PROFILE ldrmodules | grep -i false
```

Injected code can be a huge issue and is highly indicative of very very bad things. We can check for this with the command `malfind`

```bash
volatility -f MEMORY_FILE.raw –profile=PROFILE malfind -D <Destination Directory>
```

Last but certainly not least we can view all of the DLLs loaded into memory

```bash
volatility -f MEMORY_FILE.raw --profile=PROFILE dlllist
```

Now that we’ve seen all of the DLLs running in memory, let’s go a step further and pull them out
```bash
volatility -f MEMORY_FILE.raw –profile=PROFILE –pid=PID dlldump -D <Destination Directory>
```

## Post Action
Upload the extracted files to VirusTotal for examination



