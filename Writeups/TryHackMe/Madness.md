# CTF TryHackMe - Madness

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.22.11  
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-30 19:26 CEST
Stats: 0:00:02 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 15.40% done; ETC: 19:26 (0:00:05 remaining)
Nmap scan report for 10.10.22.11
Host is up (0.080s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 ac:f9:85:10:52:65:6e:17:f5:1c:34:e7:d8:64:67:b1 (RSA)
|   256 dd:8e:5a:ec:b1:95:cd:dc:4d:01:b3:fe:5f:4e:12:c1 (ECDSA)
|_  256 e9:ed:e3:eb:58:77:3b:00:5e:3a:f5:24:d8:58:34:8e (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 18.05 seconds
```
Ports 80 and 22 open

## Check source code 
In source page of http://10.10.22.11 find :
```
        <img src="thm.jpg" class="floating_element"/>
<!-- They will never find me-->
```

## Let's download this 
```bash
wget http://10.10.22.11/thm.jpg
```

see this is an PNG file so change the extension
```bash
mv thm.jpg thm.png
```

https://hackmd.io/@FlsYpINbRKixPQQVbh98kw/Bk9Wj63vH

```bash
pngcheck -v thm.png
invalid chunk name "" (01 00 00 01)

so using hexeditor to modilfying the chunk name to IHDR
hexeditor thm.png (replace after png header 89 50 4E 47  0D 0A 1A 0A)

00 00 00 0D 	49 48 44 52
```

Redo an pngcheck

```bash
pngcheck -v thm.png
File: thm.png (22210 bytes)                                                                                          
  chunk IHDR at offset 0x0000c, length 13:  invalid image dimensions (65536x4292542531)                              
:  invalid sample depth (0)                                                                                          
:  invalid compression method (2)                                                                                    
:  invalid filter method (2)                                                                                         
:  invalid interlace method (3)                                                                                      
ERRORS DETECTED in thm.png
```

This time we need to modifying the image dimensions (to 800x600)
```bash
hexeditor thm.png

00 00 00 0D 49 48 44 52 00 00 03 20 00 00 02 58 10 06 00 00 00 15 14 15 27
```

check again
```bash 
pngcheck -v thm.png
File: thm.png (22210 bytes)
  chunk IHDR at offset 0x0000c, length 13
    800 x 600 image, 64-bit RGB+alpha, non-interlaced
  CRC error in chunk IHDR (computed cae65e33, expected 15141527)
ERRORS DETECTED in thm.png
```

Need to change the CRC, replace 15141527 by CAE65E33

```bash
pngcheck -v thm.png
File: thm.png (22210 bytes)
  chunk IHDR at offset 0x0000c, length 13
    800 x 600 image, 64-bit RGB+alpha, non-interlaced                                                                
  invalid chunk name " (03 04 05 08)                                                                                 
ERRORS DETECTED in thm.png
```

Need to change 03 04 05 08 to 49 44 41 54 to get IDAT as chunk name (with 00 00 00 0C before 49 44 41 54)

```bash
pngcheck -v thmtest.png 
File: thmtest.png (22210 bytes)
  chunk IHDR at offset 0x0000c, length 13
    800 x 600 image, 64-bit RGB+alpha, non-interlaced
  chunk IDAT at offset 0x00025, length 12
    zlib: deflated, 256-byte window, maximum compression
  CRC error in chunk IDAT (computed 18dd8db0, expected 18ddd8b0)
ERRORS DETECTED in thmtest.png
```
Need to change CRC of chunk IDAT replace 18 DD D8 B0 by 18 DD 8D B0

```bash
pngcheck -v thmtest.png 
File: thmtest.png (22210 bytes)
  chunk IHDR at offset 0x0000c, length 13
    800 x 600 image, 64-bit RGB+alpha, non-interlaced
  chunk IDAT at offset 0x00025, length 12
    zlib: deflated, 256-byte window, maximum compression
" (12 10 0d 0e) name "
ERRORS DETECTED in thmtest.png
```

Need to set another IDAT chunk and change the CRC until we get the error
invalid chunk name "" (12 14 15 14)

00 00 00 0C 49 44 41 54






