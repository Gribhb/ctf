# CTF TryHackMe - Corridor

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.194.149                                
Starting Nmap 7.93 ( https://nmap.org ) at 2023-01-04 01:44 EST
Nmap scan report for 10.10.194.149
Host is up (0.058s latency).
Not shown: 999 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
80/tcp open  http    Werkzeug httpd 2.0.3 (Python 3.10.2)
|_http-title: Corridor

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.87 seconds
```

* HTTP - 80 - Python 3.10.2

## Web Enumeration

Just a website with multiple doors that can be "clikable". I saw that the name of the images are md5. I've decode all of these for every url :

| Hash | Type | Result |
| ---- | ---- | ------ |
| c4ca4238a0b923820dcc509a6f75849b | md5 | 1 |
| c81e728d9d4c2f636f067f89cc14862c | md5 | 2 |
| eccbc87e4b5ce2fe28308fd9f2a7baf3 | md5 | 3 | 
| a87ff679a2f3e71d9181a67b7542122c | md5 | 4 | 
| e4da3b7fbbce2345d7772b0674a318d5 | md5 | 5 | 
| 1679091c5a880faf6fb5e6087eb1b2dc | md5 | 6 | 
| 8f14e45fceea167a5a36dedd4bea2543 | md5 | 7 | 
| c9f0f895fb98ab9159f51fd0297e236d | md5 | 8 |
| 45c48cce2e2d7fbdea1afc51c7c6ad26 | md5 | 9 | 
| d3d9446802a44259755d38e6d163e820 | md5 | 10 | 
| 6512bd43d9caa6e02c990b0a82652dca | md5 | 11 | 
| c20ad4d76fe97759aa27a0c99bff6710 | md5 | 12 | 
| c51ce410c124a10e0db5e4b97fc2af39 | md5 | 13 | 

Now i try to encode the number 14 and go to **http://10.10.194.149/aab3238922bcc25a6f606eb525ffdc56** but i got an *404 not found*

Trying to md5 encode the number 0 and again, going to **http://10.10.194.149/cfcd208495d565ef66e7dff9f98764da** And yeah, got the flag :D